﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScanDrive.Extensions.Code;
using System.Diagnostics;

namespace ScanDrive.Extensions.UnitTests.Core {
    [TestClass]
    public class CrawlerTests {
        [TestMethod]
        public async Task CrawlsBranches() {

            var branches = await @"C:\".ToReferenceType().PickBranches<String>();
            Assert.AreNotEqual(0, branches.Magnitude<String>());
            foreach (var branch in branches) {
                branch.AssertIfDirectory();
            }

        }

        [TestMethod]
        public async Task CrawlsLeaves() {

            var leaves = await Environment.CurrentDirectory.PickLeaves<String>();
            Assert.AreNotEqual(0, leaves.Magnitude<String>());
            foreach (var leaf in leaves) {
                leaf.AssertIfFile();
            }

        }

        [TestMethod]
        public async Task CrawlsTopDown() {

            var items = await @"D:\".ToReferenceType().CrawlTopDown<String>(
                async (str, etype) => await Task.Run(() => Debug.Print(str)));
            Debug.Print("success");
            Assert.AreNotEqual(0, items.Magnitude<String>());

        }
    }
}
