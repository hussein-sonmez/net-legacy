﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScanDrive.Extensions.Code;

namespace ScanDrive.Extensions.UnitTests.Core {
    [TestClass]
    public class CommitmentTests {
        [TestMethod]
        public void TrueYieldsToCommit() {

            var b = true;
            bool? r = null;
            b.Commit(_b => r = !_b);

            Assert.IsNotNull(r);
            Assert.IsTrue(r.HasValue);
            Assert.AreEqual(false, r.Value);


        }
        [TestMethod]
        public void AssertsDirectoryNotFoundException() {

            var negative = false;
            var msg = "Directory <{0}> Not Found".PostFormat("HelloWorld.exe");
            try {
                negative.Assert<DirectoryNotFoundException>(msg);
                Assert.Fail();
            } catch (DirectoryNotFoundException ex) {
                Assert.AreEqual(msg, ex.Message);
            } catch (Exception e){
                Assert.Fail();
            }

        }

    }
}
