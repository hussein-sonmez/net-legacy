﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScanDrive.Entity
{
    public class Element
    {
        //Ctors:
        public Element()
        {
            //Empty
        }
        public Element(string name, string extension, int? parentID)
        {
            _ID = -1;
            _Name = name;
            _Extension = extension;
            _ParentID = parentID;
            Children = new List<Element>();
        }


        //Properties:

        public List<Element> Children { get; set; }

        private int _ID;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private string _Extension;

        public string Extension
        {
            get { return _Extension; }
            set { _Extension = value; }
        }
        private int? _ParentID;

        public int? ParentID
        {
            get { return _ParentID; }
            set { _ParentID = value; }
        }
    }
}
