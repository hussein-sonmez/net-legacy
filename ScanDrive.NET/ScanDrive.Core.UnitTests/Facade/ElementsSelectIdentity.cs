﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScanDrive.Entity;
using ScanDrive.Facade.Derived;

namespace ScanDrive.Core.UnitTests.Facade {
    [TestClass]
    public class ElementsSelectIdentity {
        [TestMethod]
        public void SelectIdentity() {
            var e = new Element()
            {
                ParentID = null,
                Name = "Archive",
                Extension = "org"
            };
            Elements.Insert.New(e);

            var id = Elements.Select.Identity();
            Assert.AreEqual(1, id);

        }
    }
}
