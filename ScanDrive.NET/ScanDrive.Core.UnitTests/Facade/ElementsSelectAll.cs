﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScanDrive.Entity;
using ScanDrive.Facade.Derived;

namespace ScanDrive.Core.UnitTests.Facade {
    [TestClass]
    public class ElementsSelectAll {
        [TestMethod]
        public void SelectAll() {

            var dt = Elements.Select.All();
            Assert.AreEqual(0, dt.Rows.Count);

        }
    }
}
