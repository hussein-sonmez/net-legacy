﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScanDrive.Entity;
using ScanDrive.Facade.Derived;

namespace ScanDrive.Core.UnitTests.Facade {
    [TestClass]
    public class ElementsSelectChildren {
        [TestMethod]
        public void SelectChildren() {

            var dt = Elements.Select.Children((Int32?)Elements.Select.All().Rows[0]["ID"]);
            Assert.AreEqual(10, dt.Rows.Count);

        }
    }
}
