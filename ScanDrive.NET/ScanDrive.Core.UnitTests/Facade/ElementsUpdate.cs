﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScanDrive.Entity;
using ScanDrive.Facade.Derived;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace ScanDrive.Core.UnitTests.Facade {
    [TestClass]
    public class ElementsUpdate {
        [TestMethod]
        public void Update() {

            var dr = Elements.Select.All().Rows[0];
            var e = new Element()
            {
                ID = (Int32)dr["ID"],
                ParentID = dr["ParentID"] as Int32?,
                Extension = dr["Extension"].ToString(),
                Name = dr["Name"].ToString()
            };
            e.Extension = "extended!";
            Elements.Update.New(e);
            Assert.AreEqual("extended!", e.Extension);

        }
    }
}
