﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScanDrive.Entity;
using ScanDrive.Facade.Derived;

namespace ScanDrive.Core.UnitTests.Facade {
    [TestClass]

    public class ElementsDelete {
        [TestMethod]
        public void Delete() {

            var e = new Element()
            {
                ParentID = null,
                Name = "Archive",
                Extension = "org"
            };
            Elements.Insert.New(e);
            Elements.Delete.Old(Elements.ModifiedElement);
            Assert.AreEqual(0, Elements.Select.Identity());

        }
    }
}
