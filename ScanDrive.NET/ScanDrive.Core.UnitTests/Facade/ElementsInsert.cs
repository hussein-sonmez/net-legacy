﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScanDrive.Entity;
using ScanDrive.Facade.Derived;

namespace ScanDrive.Core.UnitTests.Facade {
    [TestClass]
    public class ElementsInsert {
        [TestMethod]
        public void Insert() {

            var e = new Element()
            {
                ParentID = null,
                Name = "Archive",
                Extension = "org"
            };
            Elements.Insert.New(e);
            var pid = Elements.ModifiedElement.ID;

            for (int i = 0; i < 10; i++) {

                var ech = new Element()
                {
                    ParentID = pid,
                    Name = "Archive" + i,
                    Extension = "org" + i
                };
                Elements.Insert.New(ech);

            }
            Assert.AreNotEqual(0, Elements.ModifiedElement.ID);

        }
    }
}
