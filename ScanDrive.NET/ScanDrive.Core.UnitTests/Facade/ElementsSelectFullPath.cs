﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScanDrive.Entity;
using ScanDrive.Facade.Derived;

namespace ScanDrive.Core.UnitTests.Facade {
    [TestClass]
    public class ElementsSelectFullPath {
        [TestMethod]
        public void SelectFullPath() {

            var e = new Element()
            {
                ParentID = null,
                Name = "Archive",
                Extension = "org"
            };
            Elements.Insert.New(e);
            var fp = Elements.Select.FullPath(Elements.ModifiedElement.ID);
            Assert.AreEqual("Archive", fp);
        }
    }
}
