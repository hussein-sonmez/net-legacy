USE [ScanDriveDB]
GO
/****** Object:  StoredProcedure [dbo].[GetFullPath]    Script Date: 10.1.2015 17:01:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFullPath]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetFullPath]
GO
/****** Object:  User [sdadmin]    Script Date: 10.1.2015 17:01:14 ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'sdadmin')
DROP USER [sdadmin]
GO
/****** Object:  User [sdadmin]    Script Date: 10.1.2015 17:01:14 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'sdadmin')
CREATE USER [sdadmin] FOR LOGIN [sdadmin] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [sdadmin]
GO
/****** Object:  StoredProcedure [dbo].[GetFullPath]    Script Date: 10.1.2015 17:01:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFullPath]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetFullPath] AS' 
END
GO
ALTER PROCEDURE [dbo].[GetFullPath]
	(
	@elementID int,
	@fullPath nvarchar(MAX) OUT
	)
AS
	/* SET NOCOUNT ON */
BEGIN
	DECLARE @curElementID int, @rootElementID int;
	DECLARE @seperator nchar(1);
	SET @seperator='\';
	SET @fullPath=CONCAT(@seperator, (SELECT [Name] FROM [Elements] WHERE [ID]=@elementID))
	SET @curElementID=(SELECT [ParentID] FROM [Elements] WHERE [ID]=@elementID);
	WHILE @curElementID IS NOT NULL
	BEGIN
		SET @fullPath=CONCAT(@seperator, (SELECT [Name] FROM [Elements] WHERE [ID]=@curElementID), @fullPath);
		SET @rootElementID=@curElementID;
		SET @curElementID=(SELECT [ParentID] FROM [Elements] WHERE [ID]=@curElementID);
	END
	SET @fullPath=RIGHT(@fullPath, LEN(@fullPath)-1);
	SET @fullPath=LTRIM(RTRIM(@fullPath));
	RETURN
END

GO
