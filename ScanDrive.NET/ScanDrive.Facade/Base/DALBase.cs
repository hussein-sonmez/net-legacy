﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ScanDrive.Resources.Manifests;
using System.Data;

namespace ScanDrive.Facade.Base
{
    public class DALBase
    {
        static DALBase()
        {
            _Adapter = new SqlDataAdapter();
            _Connection =
                new SqlConnection(TextManifest.Texts["StandartConnectionString"].Value);
            if (_Connection.State == ConnectionState.Open) {
                _Connection.Close();
            }
            lockObj = new object();
        }

        private static SqlDataAdapter _Adapter;
        private static SqlConnection _Connection;
        private static object lockObj;

        //Methods:
        protected static DataTable ExecuteDataAdapter(SqlCommand command)
        {
            command.Connection = _Connection;
            _Adapter.SelectCommand = command;
            DataTable dt = new DataTable();
            lock (lockObj)
            {
                _Adapter.Fill(dt);
            }
            return dt;
        }

        protected static int ExecuteIdentity(SqlCommand command)
        {
            int result;
            command.Connection = _Connection;
            lock (lockObj) {
                if (_Connection.State != ConnectionState.Open) {
                    _Connection.Open();
                }
                result = command.ExecuteNonQuery();
                //ErrorManifest.Errors["DMLCommandExecution"].Assert(result > 0, result);
                result = SelectIdentity();
                if (_Connection.State == ConnectionState.Open) {
                    _Connection.Close();
                }
            }
            return result;
        }

        protected static int SelectIdentity() {
            int result;
            SqlCommand identity = new SqlCommand(SqlManifest.Queries["SelectIdentity"].Value);
            identity.Connection = _Connection;
            if (_Connection.State != ConnectionState.Open) {
                _Connection.Open();
            }
            var id = identity.ExecuteScalar();
            result = id.Equals(DBNull.Value) ? 0 : Convert.ToInt32(id);
            if (_Connection.State == ConnectionState.Open) {
                _Connection.Close();
            }
            return result;
        }

        protected static SqlDataReader ExecuteReader(SqlCommand command)
        {
            SqlDataReader rdr;
            rdr = null;

            command.Connection = _Connection;

            _Connection.Open();
            rdr = command.ExecuteReader(CommandBehavior.CloseConnection);

            return rdr;
        }

        protected static int ExecuteNonQuery(SqlCommand command)
        {
            int result;
            command.Connection = _Connection;
            lock (lockObj)
            {
                _Connection.Open();
                result = command.ExecuteNonQuery();
                _Connection.Close();
            }
            return result;
        }

        protected static void ExecuteSprocCommand(SqlCommand command)
        {
            command.Connection = _Connection;
            lock (lockObj)
            {
                _Connection.Open();
                command.ExecuteNonQuery();
                _Connection.Close();
            }
        }
    }
}
