﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScanDrive.Facade.Base;
using System.Data;
using System.Data.SqlClient;
using ScanDrive.Entity;
using ScanDrive.Resources.Manifests;

namespace ScanDrive.Facade.Derived {
    public class Elements : DALBase {
        public static class Select {
            public static DataTable All() {
                var select = new SqlCommand(SqlManifest.Queries["GetAllElements"].Value);
                return ExecuteDataAdapter(select);
            }
            public static DataTable Children(int? parentID) {
                var select = new SqlCommand(SqlManifest.Queries["GetChildren"]
                    .Explain(parentID.HasValue ? "=" + parentID.Value : " IS NULL"));
                return ExecuteDataAdapter(select);
            }
            public static string FullPath(int elementID) {
                SqlCommand get = new SqlCommand(SqlManifest.Queries["GetFullPath"].Value);

                SqlParameter pfp = new SqlParameter();
                pfp.ParameterName = "@fp";
                pfp.Direction = ParameterDirection.Output;
                pfp.SqlDbType = SqlDbType.NVarChar;
                pfp.Size = 9999;

                get.Parameters.AddWithValue("@eid", elementID);
                get.Parameters.Add(pfp);

                ExecuteNonQuery(get);

                return get.Parameters["@fp"].Value.ToString();

                //get.CommandType = CommandType.StoredProcedure;
                //get.Parameters.AddWithValue("@eid", elementID);
                //SqlParameter pOut = new SqlParameter();
                //pOut.ParameterName = "@fullPath";
                //pOut.Direction = ParameterDirection.Output;
                //get.Parameters.Add(pOut);
                //ExecuteSprocCommand(get);
                //return (string)get.Parameters["@fullPath"].Value;
            }

            public static int Identity() {
                return SelectIdentity();
            }
        }

        public static class Insert {
            public static void New(Element e) {
                SqlCommand insert =
                    new SqlCommand(SqlManifest.Queries["InsertElement"].Value);
                insert.Parameters.AddWithValue("@name", e.Name);
                if (e.Extension == null)
                    insert.Parameters.AddWithValue("@extension", System.DBNull.Value);
                else
                    insert.Parameters.AddWithValue("@extension", e.Extension);
                if (!e.ParentID.HasValue)
                    insert.Parameters.AddWithValue("@parentID", System.DBNull.Value);
                else
                    insert.Parameters.AddWithValue("@parentID", e.ParentID.Value);
                e.ID = ExecuteIdentity(insert);

                modifiedElement = e;
            }
        }

        public static class Delete {
            public static void Old(Element e) {
                SqlCommand delete =
                    new SqlCommand(SqlManifest.Queries["DeleteElement"].Value);
                delete.Parameters.AddWithValue("@elementID", e.ID);
                ExecuteNonQuery(delete);

                modifiedElement = e;
            }

            public static void Truncate() {
                SqlCommand delete =
                    new SqlCommand(SqlManifest.Queries["TruncateElement"].Value);
                ExecuteNonQuery(delete);
            }
        }

        public static class Update {
            public static void New(Element e) {
                //ErrorManifest.Errors["ElementsIDNotSet"].Assert(e.ID != -1, e.Name, e.Extension, e.ParentID);
                SqlCommand update =
                    new SqlCommand(SqlManifest.Queries["UpdateElement"]
                    .Explain(e.ParentID.HasValue ? e.ParentID.Value.ToString() : "NULL"));
                update.Parameters.AddWithValue("@name", e.Name);
                update.Parameters.AddWithValue("@extension", e.Extension);
                update.Parameters.AddWithValue("@elementID", e.ID);
                e.ID = ExecuteIdentity(update);

                modifiedElement = e;
            }
        }

        #region Properties
        private static Element modifiedElement = null;

        public static Element ModifiedElement {
            get {
                return modifiedElement;
            }
        }
        #endregion
    }
}
