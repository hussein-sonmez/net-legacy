﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScanDrive.Resources.Texts;

namespace ScanDrive.Resources.Manifests
{
    public class MessageManifest
    {
        static MessageManifest()
        {
            Messages = new Dictionary<string, StringsBase>()
            {
                {
                    "MainProcessComplete", 
                    new Message("Main Process Completed")
                },
                {
                    "DurationInfo", 
                    new Message("DB Access Ended.\nDuration is {0} minutes.")
                },
                {
                    "DriveNotReady", 
                    new Message("Drive {0} Scan Caused IOException.\nException Message is {1}")
                }
            };
        }
        public static Dictionary<string, StringsBase> Messages { get; set; }
    }
}
