﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScanDrive.Resources.Texts;

namespace ScanDrive.Resources.Manifests
{
    public class TextManifest
    {
        static TextManifest()
        {
            Texts = new Dictionary<string, StringsBase>()
            {
                {
                    "StandartConnectionString", 
                    new Text(@"Data Source=.;Initial Catalog=ScanDriveDB;Persist Security Info=True;User ID=sdadmin;Password=1q2w3e4r5t")
                },
                {
                    "TimeElapsedInfo", 
                    new Text("Totally {0} hours {1} minutes and {2} seconds elapsed")
                }, 
                {
                    "PathInfo", 
                    new Text("{0}")
                }
            };
        }
        public static Dictionary<string, StringsBase> Texts { get; set; }
    }
}
