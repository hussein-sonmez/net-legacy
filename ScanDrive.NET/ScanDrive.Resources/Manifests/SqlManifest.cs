﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScanDrive.Resources.Texts;

namespace ScanDrive.Resources.Manifests
{
    public static class SqlManifest
    {
        static SqlManifest()
        {
            Queries = new Dictionary<string, StringsBase>()
            {
                {"SelectIdentity", new Sql("SELECT @@IDENTITY")},
                {"GetAllElements", new Sql("SELECT * FROM [Elements]")},
                {"GetChildren", new Sql("SELECT * FROM [Elements] WHERE [ParentID]{0}")},
                {"InsertElement", new Sql("INSERT INTO [Elements] VALUES(@name, @extension, @parentID)")},
                {"UpdateElement", new Sql("UPDATE [Elements] SET [Name]=@name, [Extension]=@extension, [ParentID]={0} WHERE [ID]=@elementID")},
                {"DeleteElement", new Sql("DELETE FROM [Elements] WHERE [ID]=@elementID")}, 
                {"TruncateElement", new Sql("TRUNCATE TABLE Elements")}, 
                {"GetFullPath", new Sql("EXEC GetFullPath @elementID=@eid, @fullPath=@fp output; SELECT @fp;")},
            }; 
            Scripts = new Dictionary<string, StringsBase>()
            {
            };
        }
        public static Dictionary<string, StringsBase> Queries { get; set; }
        public static Dictionary<string, StringsBase> Scripts { get; set; }
    }
}
