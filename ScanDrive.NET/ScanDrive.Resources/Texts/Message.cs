﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScanDrive.Resources.Texts
{
    public class Message : StringsBase
    {
        public Message(string message)
            : base(message)
        {

        }

        public override string Explain(params object[] args)
        {
            string formatted = String.Format(Value, args);
            MessageBox.Show(formatted);
            return formatted;
        }
    }
}
