﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScanDrive.Resources.Texts
{
    public class StringsBase
    {
        public StringsBase(string text)
        {
            _Value = text;
        }
        public string Value
        {
            get
            {
                return _Value;
            }
        }

        private string _Value;

        //Methods:
        public virtual string Explain(params object[] args)
        {
            return String.Format(Value, args);
        }
    }
}
