﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScanDrive.Entity;
using ScanDrive.Facade.Derived;

namespace ScanDrive.Extensions.Entity {
    public static class EntityExtensions {

        public async static Task<String> ToFullPath(this Element element) {

            return await Task.Factory.StartNew(() => Elements.Select.FullPath(element.ID));

        }

    }
}
