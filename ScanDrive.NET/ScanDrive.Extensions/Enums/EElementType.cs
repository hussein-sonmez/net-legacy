﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanDrive.Extensions.Enums {
    public enum EElementType {

        File = 1,
        Directory,
        JunctionalPoint

    }
}
