﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanDrive.Extensions.Code {
    public static class StringExtensions {

        public static String PostFormat(this String preformatString, params Object[] parameters) {

            return String.Format(preformatString, parameters);

        }

        public static String ToReferenceType(this String constantString) {

            return new StringBuilder(constantString).ToString();

        }

    }
}
