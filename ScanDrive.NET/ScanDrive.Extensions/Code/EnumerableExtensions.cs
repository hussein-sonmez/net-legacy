﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanDrive.Extensions.Code {
    public static class EnumerableExtensions {

        public static Int32 Magnitude<TItem>(this IEnumerable<TItem> array) {

            var c = 0;
            foreach (var item in array) {
                c++;
            }
            return c;
        }

        public static void Enumerate<TItem>(this IEnumerable<TItem> array
            , Action<TItem> commit) {

            foreach (var item in array) {
                commit(item);
            }

        }

        public static List<TItem> AddToSelf<TItem>(this IEnumerable<TItem> array 
            , TItem item) {

            var list = new List<TItem>(array);
            list.Add(item);
            return list;

        }

    }
}
