﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScanDrive.Facade.Derived;
using ScanDrive.Resources.Manifests;

namespace ScanDrive.TestWinUI
{
    public partial class TestForm : Form
    {
        public TestForm()
        {
            InitializeComponent();
            InitializeCustomComponents();
        }

        private BackgroundWorker scanner;
        private DateTime beginTime;
        private DateTime endTime;
        private TimeSpan duration;

        private void InitializeCustomComponents()
        {
            scanner = new BackgroundWorker();
            scanner.RunWorkerCompleted += 
                new RunWorkerCompletedEventHandler(BgWorker_RunWorkerCompleted);
            duration = new TimeSpan();
            Elements.Delete.Truncate();
            tmrDuration.Tick += new EventHandler(tmrDuration_Tick);
            dgvAllElements.DataSource = Elements.Select.All();
        }

        void tmrDuration_Tick(object sender, EventArgs e)
        {
            TimeSpan elapsed =  DateTime.Now.Subtract(beginTime);
            lblDuration.Text = 
                TextManifest.Texts["TimeElapsedInfo"].Explain(
                    elapsed.Hours, elapsed.Minutes, elapsed.Seconds);
            lblCurrentPathInfo.Text = TextManifest.Texts["PathInfo"].Explain(
                    elapsed.Seconds);
        }

        void BgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            tmrDuration.Stop();
            endTime = DateTime.Now;
            duration = endTime.Subtract(beginTime);
            MessageManifest.Messages["DurationInfo"].Explain(duration.Minutes);
            dgvAllElements.DataSource = Elements.Select.All();
            btnStart.Enabled = true;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            beginTime = DateTime.Now;
            tmrDuration.Start();
            scanner.RunWorkerAsync();
        }
    }
}
