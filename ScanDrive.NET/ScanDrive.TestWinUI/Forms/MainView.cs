﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScanDrive.Extensions.Entity;

namespace ScanDrive.TestWinUI.Forms {
    public partial class MainView : Form {
        public MainView() {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e) {

            fbdRootDirectory = fbdRootDirectory ?? new FolderBrowserDialog()
            {
                ShowNewFolderButton = true,
                RootFolder = Environment.SpecialFolder.DesktopDirectory,
                Description = "descdesc"
            };

            var response = fbdRootDirectory.ShowDialog();
            SelectRoot(response);
            //Task.Factory.StartNew(() => SelectRoot(response));

        }

        private void SelectRoot(DialogResult response) {
            if (response == DialogResult.OK) {
                fbdRootDirectory.SelectedPath.Scan(e=> pbScanning.Increment(1));
            }
        }

        FolderBrowserDialog fbdRootDirectory;
    }
}
