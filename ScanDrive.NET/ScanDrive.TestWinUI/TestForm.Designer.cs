﻿namespace ScanDrive.TestWinUI
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvAllElements = new System.Windows.Forms.DataGridView();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblDurationCaption = new System.Windows.Forms.Label();
            this.lblDuration = new System.Windows.Forms.Label();
            this.tmrDuration = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.lblCurrentPathInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllElements)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAllElements
            // 
            this.dgvAllElements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllElements.Location = new System.Drawing.Point(12, 54);
            this.dgvAllElements.Name = "dgvAllElements";
            this.dgvAllElements.Size = new System.Drawing.Size(635, 239);
            this.dgvAllElements.TabIndex = 0;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 36);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblDurationCaption
            // 
            this.lblDurationCaption.AutoSize = true;
            this.lblDurationCaption.Location = new System.Drawing.Point(252, 38);
            this.lblDurationCaption.Name = "lblDurationCaption";
            this.lblDurationCaption.Size = new System.Drawing.Size(50, 13);
            this.lblDurationCaption.TabIndex = 2;
            this.lblDurationCaption.Text = "Duration:";
            // 
            // lblDuration
            // 
            this.lblDuration.AutoSize = true;
            this.lblDuration.ForeColor = System.Drawing.Color.Red;
            this.lblDuration.Location = new System.Drawing.Point(308, 38);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(0, 13);
            this.lblDuration.TabIndex = 3;
            // 
            // tmrDuration
            // 
            this.tmrDuration.Interval = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(252, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Current Path:";
            // 
            // lblCurrentPathInfo
            // 
            this.lblCurrentPathInfo.AutoSize = true;
            this.lblCurrentPathInfo.ForeColor = System.Drawing.Color.Red;
            this.lblCurrentPathInfo.Location = new System.Drawing.Point(327, 19);
            this.lblCurrentPathInfo.Name = "lblCurrentPathInfo";
            this.lblCurrentPathInfo.Size = new System.Drawing.Size(0, 13);
            this.lblCurrentPathInfo.TabIndex = 2;
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 301);
            this.Controls.Add(this.lblDuration);
            this.Controls.Add(this.lblCurrentPathInfo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblDurationCaption);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.dgvAllElements);
            this.Name = "TestForm";
            this.Text = "Let\'s Make A Test.";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllElements)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAllElements;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblDurationCaption;
        private System.Windows.Forms.Label lblDuration;
        private System.Windows.Forms.Timer tmrDuration;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCurrentPathInfo;
    }
}

