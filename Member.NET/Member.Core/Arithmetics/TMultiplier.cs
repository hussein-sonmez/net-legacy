﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Member.Core.Types;
using Member.Core.Constraints;

namespace Member.Core.Arithmetics
{
    internal class TMultiplier
    {
        #region handlers
        internal TMember Multiply(TMember term1, TMember term2)
        {
            dynamic terms = SCalculator.ConstructTerms(term1, term2);
            TMember multiplicand = terms.greater;
            TMember multiplier = terms.smaller;
            TMember product = terms.opresult;

            for (int i = 0; i < multiplier.Length; i++)
            {
                product += MultiplyDigit(multiplicand, multiplier[i]).PadBegin(new TDigit(0, multiplicand.Radix), (uint)i);
            }

            return product;
        }
        internal TMember MultiplyDigit(TMember multiplicand, TDigit digit)
        {
            SValidator<TDigit>.Validate(d => d.Radix.Equals(multiplicand.Radix), digit);

            TMember product = new TMember(multiplicand.Radix);
            uint c = 0;
            for (int i = 0; i < multiplicand.Length; i++)
            {
                product.AppendDigit(Carrydigit(multiplicand[i], digit, ref c));
            }
            if (c > 0)
                product.AppendDigit(new TDigit(c, multiplicand.Radix));
            return product;
        }
        private TDigit Carrydigit(TDigit d1, TDigit d2, ref uint carry)
        {
            uint radix = d1.Radix.Value;
            uint digit = 0;
            uint productWithCarry = d1.Value * d2.Value + carry;
            if (productWithCarry > radix)
            {
                carry = (uint)(productWithCarry / radix);
                digit = productWithCarry % radix;
            }
            else if (productWithCarry == radix)
            {
                carry = 1;
                digit = 0;
            }
            else
            {
                carry = 0;
                digit = productWithCarry;
            }
            return new TDigit(digit, d1.Radix);
        }
        #endregion
    }
}
