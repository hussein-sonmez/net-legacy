﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Member.Core.Types;

namespace Member.Core.Arithmetics
{
    internal class TComparer
    {
        #region handlers
        internal bool Eq(TMember term1, TMember term2)
        {
            if (term1.Length != term2.Length)
                return false;
            //term1.Length = term2.Length
            for (int i = term1.Length - 1; i >= 0; i--)
            {
                if (term1[i].Value != term2[i].Value)
                    return false;
            }
            return true;
        }
        internal bool Ne(TMember term1, TMember term2)
        {
            if (term1.Length != term2.Length)
                return true;
            //term1.Length = term2.Length
            for (int i = term1.Length - 1; i >= 0 ; i--)
            {
                if (term1[i].Value != term2[i].Value)
                    return true;
            }
            return false;
        }
        internal bool Gt(TMember term1, TMember term2)
        {
            if (term1.Length > term2.Length)
                return true;
            else if (term1.Length < term2.Length)
                return false;
            //term1.Length = term2.Length
            for (int i = term1.Length - 1; i >= 0; i--)
            {
                if (term1[i].Value == term2[i].Value)
                    continue;
                else if (term1[i].Value > term2[i].Value)
                    return true;
                else //t1 < t2
                    return false;
            }
            //all terms passed t1 == t2
            return false;
        }
        internal bool Lt(TMember term1, TMember term2)
        {
            if (term1.Length < term2.Length)
                return true;
            else if (term1.Length > term2.Length)
                return false;
            //term1.Length = term2.Length
            for (int i = term1.Length - 1; i >= 0; i--)
            {
                if (term1[i].Value == term2[i].Value)
                    continue;
                else if (term1[i].Value < term2[i].Value)
                    return true;
                else //t1 < t2
                    return false;
            }
            //all terms passed t1 == t2
            return false;
        }
        internal bool Ge(TMember term1, TMember term2)
        {
            if (term1.Length > term2.Length)
                return true;
            else if (term1.Length < term2.Length)
                return false;
            //term1.Length = term2.Length
            for (int i = term1.Length - 1; i >= 0; i--)
            {
                if (term1[i].Value == term2[i].Value)
                    continue;
                else if (term1[i].Value > term2[i].Value)
                    return true;
                else //t1 < t2
                    return false;
            }
            //all terms passed
            return true;
        }
        internal bool Le(TMember term1, TMember term2)
        {
            if (term1.Length < term2.Length)
                return true;
            else if (term1.Length > term2.Length)
                return false;
            //term1.Length = term2.Length
            for (int i = term1.Length - 1; i >= 0; i--)
            {
                if (term1[i].Value == term2[i].Value)
                    continue;
                else if (term1[i].Value < term2[i].Value)
                    return true;
                else //t1 > t2
                    return false;
            }
            //all terms passed
            return true;
        }

        #endregion

        internal bool Gt(TMember term1, TDigit term2)
        {
            return Gt(term1, TMember.FromInt(term2.Value, term2.Radix));
        }

        internal bool Lt(TMember term1, TDigit term2)
        {
            return Lt(term1, TMember.FromInt(term2.Value, term2.Radix));
        }

        internal bool Ge(TMember term1, TDigit term2)
        {
            return Ge(term1, TMember.FromInt(term2.Value, term2.Radix));
        }

        internal bool Le(TMember term1, TDigit term2)
        {
            return Le(term1, TMember.FromInt(term2.Value, term2.Radix));
        }

        internal bool Eq(TMember term1, TDigit term2)
        {
            return Eq(term1, TMember.FromInt(term2.Value, term2.Radix));
        }

        internal bool Ne(TMember term1, TDigit term2)
        {
            return Ne(term1, TMember.FromInt(term2.Value, term2.Radix));
        }
    }
}
