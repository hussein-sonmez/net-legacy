﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Member.Core.Types;
using Member.Core.Constraints;

namespace Member.Core.Arithmetics
{
    public class SCalculator
    {
        #region ctors
        static SCalculator()
        {
            Converter = new TConverter();
            Adder = new TAdder();
            Subtractor = new TSubtractor();
            Multiplier = new TMultiplier();
            Divisor = new TDivisor();
            Comparer = new TComparer();
        }
        #endregion
        
        #region methods
        public static dynamic SplitInteger(uint n, uint radix)
        {
            n = SValidator<uint>.Validate(_n => _n >= 0, n);
            radix = SValidator<uint>.Validate(r => r > 0, radix);
            return new { carry = n % radix, truediv = (uint)(n / radix) };
        }
        public static dynamic ConstructTerms(TMember term1, TMember term2)
        {
            TMember greater = null;
            TMember smaller = null;
            TMember opresult = null;
            if (term1 >= term2)
            {
                greater = term1;
                smaller = term2;
            }
            else
            {
                greater = term2;
                smaller = term1;
            }
            SValidator<TMember>.Validate(m => m.Radix == smaller.Radix, greater);
            opresult = new TMember(greater.Radix);
            return new
            {
                smaller = smaller,
                greater = greater,
                opresult = opresult
            };
        }
        #endregion

        #region properties
        internal static TConverter Converter { get; private set; }
        internal static TAdder Adder { get; private set; }
        internal static TSubtractor Subtractor { get; private set; }
        internal static TMultiplier Multiplier { get; private set; }
        internal static TDivisor Divisor { get; private set; }
        internal static TComparer Comparer { get; private set; }
        #endregion
    }
}
