﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Member.Core.Constraints;

namespace Member.Core.Types
{
    public class TRadix : TSelfValidator<uint>, IEquatable<TRadix>, IEquatable<uint>
    {
        #region ctors
        public TRadix(uint radix)
            : base(r => r > 0)
        {
            _Value = Validate(radix);
        }
        #endregion

        #region properties
        private uint _Value;
        public uint Value { get { return _Value; } set { _Value = Validate(value); } }
        #endregion

        #region operator
        public static bool operator ==(TRadix self, TRadix other)
        {
            return self.Value == other.Value;
        }
        public static bool operator !=(TRadix self, TRadix other)
        {
            return self.Value != other.Value;
        }
        #endregion

        public bool Equals(TRadix other)
        {
            return Value == other.Value;
        }
        public bool Equals(uint other)
        {
            return _Value == other;
        }
        public override bool Equals(object obj)
        {
            if (obj is TRadix)
                return Equals(obj as TRadix);
            else if (obj is uint)
                return Equals((uint)obj);
            return _Value.Equals(obj);
        }
        public override int GetHashCode()
        {
            return _Value.GetHashCode();
        }
    }
}
