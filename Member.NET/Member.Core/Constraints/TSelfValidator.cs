﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Member.Core.Constraints
{
    public class TSelfValidator<TTElement>
    {
        #region ctors
        public TSelfValidator(DValidator<TTElement> validateDelegate)
        {
            _ValidateDelegate = validateDelegate;
        }
        #endregion

        #region methods
        protected TTElement Validate(TTElement e)
        {
            Debug.Assert(_ValidateDelegate(e)
                , "Assertion Failed", "Assertion failed with {0}", e.ToString());
            return e;
        }
        #endregion

        #region
        private DValidator<TTElement> _ValidateDelegate;
        #endregion
    }
}
