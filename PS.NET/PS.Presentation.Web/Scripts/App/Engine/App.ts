interface ConfigSeen {
	TransitionLength: number;
	VirtualDirectory: string;
	ServiceHosts: { [key: string]: () => string };
	Patterns: { [key: string]: RegExp };
	Messages: { [key: string]: string };
	Constants: { [key: string]: any };
	Captions: { [key: string]: string };
	WebFontConfig: { google: { families: string[] } };
	AjaxTransform: (key: string, genome: Genome<any>, done: (data) => void) => void;
}

interface AppTask {
	Stream: () => MainStream;
	Glyph: () => Glyph;
	SiteMaster: () => SiteMaster;
	Playground: () => Playground;
	NewItemModal: () => NewItemModal;
	Core: {
		ExtractID: (from: string) => string;
		IDExists: (from: string) => boolean;
		Union: (first: any, second: any) => any;
	};
}

class App {

    static run() {

		App.Task.Stream();
		App.Task.Glyph();
		App.Task.SiteMaster();
		App.Task.Playground();
		App.Task.NewItemModal();
        //Test.make();

    }
    static get Task() : AppTask {
		var task = {
			Stream: () => MainStream.generate(App.Config).registerFlow(),
			Glyph: () => Glyph.morph(App.Config),
			SiteMaster: () => SiteMaster.ConstructUI(App.Config),
			Playground: () => Playground.ConstructUI(App.Config),
			NewItemModal: () => NewItemModal.ConstructUI(App.Config),
			Core: {
				ExtractID: (from: string) => {
					var m = from.match(/\d+/g);
					return m ? m[0] : '';
				},
				IDExists: (path: string) => {
					return task.Core.ExtractID(path) != '';
				},
				Union: (first: any, second: any) => {
					var un = {};
					for (var e1 in first) {
						if (first.hasOwnProperty(e1)) {
							un[e1] = first[e1];
						}
					}
					for (var e2 in second) {
						if (second.hasOwnProperty(e2)) {
							un[e2] = second[e2];
						}
					}
					return un;
				}
			}
		};
		return task;
    }
    static get Config(): ConfigSeen {


		var cfg: ConfigSeen = {
			TransitionLength: 1.2,
			VirtualDirectory: '/playground/',
			ServiceHosts: {
				'Generic': () => cfg.VirtualDirectory + 'Service/GenericService.asmx',
				'Transform': () => cfg.VirtualDirectory + 'Service/TransformService.asmx'
			},
			Patterns: {
				"password": /(.+){8,64}/i,
				"email": /[a-zA-Z][a-zA-Z0-9\.]*@\w+\.\w{2,4}/i
			},
			Messages: {
				"locatorMistyped": "Provided email does not meet the required criteria",
				"passwordMistyped": "Provided password does not meet the required criteria",
				"HandshakeRejected": "Handshake Rejected",
				"NotLocated": "Why Not Join Us",
				"PleaseTryAgain": "Please Try Again",
			},
			Constants: {
				"LocatorRangeCount": 6,
				"LocatorSlideCount": 31,
				"RequestedModel": "Achievement"
			},
			WebFontConfig: {
				google: { families: ['Marmelad::latin,latin-ext,cyrillic'] }
			},
			Captions: {
				"RemoveItem": "Delete Item",
				"DetailsItem": "Details Of",
				"UpdateItem": "Update Item",
				"CommandsColumn": "Commands",
			},
			AjaxTransform: (key: string, genome: Genome<any>, done: (data) => void) => {
				new Asyncll(AsyncMode.jQueryAjax, cfg.ServiceHosts['Transform'](), AsyncMethod.Post)
					.transmit(genome.Add(Code.Construct('key', key)), 'GetTemplate', done, (err, genome) => { });
			}
		};
		return cfg;

    }

}

