﻿requirejs.config({
	paths: {
		'html5slider': 'html5slider',
		'one': 'one'
	},

	shim: {
		'one': {
			deps: ['html5slider'],
		}

	},
	urlArgs: "bust=" + (new Date()).getTime()

});

requirejs(['one'], function () {
	App.run();
});