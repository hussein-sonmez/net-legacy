﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using PS.Business.Controller.Base;
using PS.Business.Controller.Infrastructure;
using PS.Core.Definitions.Web;
using PS.Core.Model.Entity.POCO;
using PS.Emitter.Ligand.Emit;

namespace PS.Presentation.Api.Controllers {
    [RoutePrefix(SControllerDefinitions.ApiRoute + "/" + SControllerDefinitions.UserInRoleRoute)]
    public class UserInRoleApiController : GenericApiController<UserInRole> {

        protected override BaseController<UserInRole> ApiController {
            get {
                return SLigand.Attach<BaseController<UserInRole>>();
            }
        }


        #region Get Actions
        // GET: api/Message
        [Route("all"), HttpGet]
        public async Task<ICollection<UserInRole>> All() {

            return await ApiController.All();

        }

        #endregion

        #region Post Actions

        [Route("insert"), HttpPost]
        public async Task<IHttpActionResult> Insert([FromBody] UserInRole userInRole) {

            var inserted = await ApiController.Insert(userInRole);
            return Ok(inserted);

        }

        #endregion

    }
}
