﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Model.Base;
using PS.Core.Extensions.ParserExtensions;
using PS.Core.Model.Entity.Enum;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.Model.Repositories {
    internal class UserInRoleRepository : GenericRepository<UserInRole> {

        public UserInRoleRepository() {
            var r = new Random();
            var users = new UserRepository().Seed();
            var roles = new RoleRepository().Seed();

            base.SetSeeder(index => new UserInRole()
            {
                ID = index,
                IsDeleted = false,
                User = users.ElementAt(r.Next(users.Count())),
                Role = roles.ElementAt(r.Next(roles.Count()))
            });
        }

    }
}
