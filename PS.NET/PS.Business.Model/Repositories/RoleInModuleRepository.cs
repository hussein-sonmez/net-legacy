﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Model.Base;
using PS.Core.Extensions.ParserExtensions;
using PS.Core.Model.Entity.Enum;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.Model.Repositories {
    internal class RoleInModuleRepository : GenericRepository<RoleInModule> {

        public RoleInModuleRepository() {
            var r = new Random();
            var modules = new ModuleRepository().Seed();
            var roles = new RoleRepository().Seed();

            base.SetSeeder(index => new RoleInModule()
            {
                ID = index,
                IsDeleted = false,
                Module = modules.ElementAt(r.Next(modules.Count())),
                Role = roles.ElementAt(r.Next(roles.Count()))
            });
        }

    }
}
