﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Model.Base;
using PS.Core.Extensions.ParserExtensions;
using PS.Core.Model.Entity.Enum;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.Model.Repositories {
    internal class ModuleRepository : GenericRepository<Module> {

        public ModuleRepository() {
            var r = new Random();
            var functions = new FunctionRepository().Seed();

            base.SetSeeder(index => new Module()
            {
                ID = index,
                IsDeleted = false,
                Name = "Module-" + index,
                Functions = new List<Function>(functions.Take(index.ParseToInt32()))
            });
        }

    }
}
