﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Model.Base;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.Model.Repositories {
    internal class TextRepository :  GenericRepository<Text>{

        public TextRepository() {
            base.SetSeeder(index => new Text()
            {
                ID = index,
                IsDeleted = false,
                Content = "content - " + index,
                Dismissed = false,
                PublishedOn = DateTime.Now
            });
        }

    }
}
