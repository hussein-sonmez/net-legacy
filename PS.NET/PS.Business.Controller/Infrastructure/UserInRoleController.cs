﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Controller.Base;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.Controller.Infrastructure {
    public class UserInRoleController : BaseController<UserInRole> {

        public UserInRoleController() {

            Repository
                .AddReference(m => m.User)
                .AddReference(m => m.Role);
        }

    }
}
