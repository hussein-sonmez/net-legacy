﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Controller.Base;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.Controller.Infrastructure {
    public class RoleInModuleController : BaseController<RoleInModule> {

        public RoleInModuleController() {

            Repository
                .AddReference(m => m.Module)
                .AddReference(m => m.Role);
        }

    }
}
