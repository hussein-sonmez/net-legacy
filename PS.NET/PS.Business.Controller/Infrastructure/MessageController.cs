﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Controller.Base;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.Controller.Infrastructure {
    public class MessageController : BaseController<Message> {

        #region Ctor

        public MessageController() {

            Repository
                .AddReference(m => m.Sender)
                .AddReference(m => m.Receiver)
                .AddCollection(m => m.Texts);

        }

        #endregion


    }
}
