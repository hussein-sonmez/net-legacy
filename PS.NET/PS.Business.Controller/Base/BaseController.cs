﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Model.Base;
using PS.Core.Model.Entity.Base;
using PS.Core.Model.Entity.POCO;
using PS.Emitter.Ligand.Emit;

namespace PS.Business.Controller.Base {
    public class BaseController<TEntity> 
        where TEntity : BaseModel {


        #region Methods

        public async Task<ICollection<TEntity>> All() {

            var found = await Repository.All();
            return found.ToArray();

        }

        public async Task<TEntity> Insert(TEntity entity) {

            return await Repository.With(entity).Insert();

        }

        public async Task<TEntity> Update(TEntity entity) {

            return await Repository.With(entity).Update();

        }

        public async Task Remove(TEntity entity) {

            await Repository.With(entity).Delete();

        }

        public async Task ResetTable() {

            var list = await All();
            foreach (var e in list) {
                await Repository.With(e).Delete();
            }

        }

        public BaseController<TEntity> AddSelectQuery(Expression<Func<TEntity, Boolean>> filter) {

            Repository.Filter(filter);
            return this;

        }

        public TEntity Representative {
            get {
                return Repository.Representative;
            }
        }

        public async Task<TEntity> ForKey(Int64 ID) {

            return await Repository.Key(ID).One();

        }

        #endregion

        #region Inherited

        private IRepository<TEntity> _Repository;
        protected IRepository<TEntity> Repository {
            get {
                return _Repository ??
                     (_Repository = SLigand.Attach<IRepository<TEntity>>());
            }
        }

        public async Task SeedDatabase() {

            var seed  = _Repository.Seed();
            foreach (var e in seed) {
                await _Repository.With(e).Insert();
            }

        }

        #endregion
    }
}
