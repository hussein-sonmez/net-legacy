﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Business.Controller.Base;
using PS.Business.Extensions.Instance;
using PS.Business.Model.Base;
using PS.Core.Definitions.Web;
using PS.Core.Model.Entity.POCO;
using PS.Emitter.Ligand.Emit;
using PS.Presentation.UnitTest.Base;

namespace PS.Presentation.UnitTest.ApiTests {
    [TestClass]
    public class UserInRoleApiTest : ApiTestBase {

        #region Implemented

        protected override string ControllerRoutePrefix {
            get {
                return SControllerDefinitions.UserInRoleRoute;
            }
        }

        #endregion

        #region TestMethods For Post Actions

        [TestMethod]
        public async Task UserInRoleInsertCallRunsProperly() {

            var controller = SLigand.Attach<BaseController<UserInRole>>();
            Assert.IsNotNull(controller);

            var uir = controller.Representative;
            uir = await ClientCallPostAsync<UserInRole>("insert", uir);

            var filtered = await controller.ForKey(uir.ID);
            Assert.IsFalse(uir.InstanceEqual(filtered));

            await controller.Remove(filtered);
        }

        [TestMethod]
        public async Task UserInRoleInsertRunsProperly() {

            var controller = SLigand.Attach<BaseController<UserInRole>>();
            Assert.IsNotNull(controller);

            var uir = controller.Representative;
            uir = await controller.Insert(uir);

            var filtered = await controller.ForKey(uir.ID);
            Assert.IsTrue(uir.InstanceEqual(filtered));

            await controller.Remove(filtered);

        }

        #endregion

        #region TestMethods For Get Actions

        [TestMethod]
        public async Task UserInRoleAllClientCallRunsProperly() {

            var uirs = await ClientCallGetAsync<IEnumerable<UserInRole>>("all");
            Assert.IsNotNull(uirs);
            Assert.AreNotEqual(0, uirs.Count());

        }

        [TestMethod]
        public async Task UserInRoleControllerGetsAllMessagesProperly() {

            var controller = SLigand.Attach<BaseController<UserInRole>>();
            Assert.IsNotNull(controller);
            var gotten = await controller.All();
            Assert.IsNotNull(gotten);
            Assert.AreNotEqual(0, gotten.Count);

        }

        #endregion

    }

}
