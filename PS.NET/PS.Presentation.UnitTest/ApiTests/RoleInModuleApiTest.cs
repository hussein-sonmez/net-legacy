﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Business.Controller.Base;
using PS.Business.Extensions.Instance;
using PS.Business.Model.Base;
using PS.Core.Definitions.Web;
using PS.Core.Model.Entity.POCO;
using PS.Emitter.Ligand.Emit;
using PS.Presentation.UnitTest.Base;

namespace PS.Presentation.UnitTest.ApiTests {
    [TestClass]
    public class RoleInModuleApiTest : ApiTestBase {

        #region Implemented

        protected override string ControllerRoutePrefix {
            get {
                return SControllerDefinitions.RoleInModuleRoute;
            }
        }

        #endregion

        #region TestMethods For Post Actions

        [TestMethod]
        public async Task RoleInModuleInsertCallRunsProperly() {

            var controller = SLigand.Attach<BaseController<RoleInModule>>();
            Assert.IsNotNull(controller);

            var rim = controller.Representative;
            rim = await ClientCallPostAsync<RoleInModule>("insert", rim);

            var filtered = await controller.ForKey(rim.ID);
            Assert.IsFalse(rim.InstanceEqual(filtered));

            await controller.Remove(filtered);
        }

        [TestMethod]
        public async Task RoleInModuleInsertRunsProperly() {

            var controller = SLigand.Attach<BaseController<RoleInModule>>();
            Assert.IsNotNull(controller);

            var rim = controller.Representative;
            rim = await controller.Insert(rim);

            var filtered = await controller.ForKey(rim.ID);
            Assert.IsTrue(rim.InstanceEqual(filtered));

            await controller.Remove(filtered);

        }

        #endregion

        #region TestMethods For Get Actions

        [TestMethod]
        public async Task RoleInModuleAllClientCallRunsProperly() {

            var rims = await ClientCallGetAsync<IEnumerable<RoleInModule>>("all");
            Assert.IsNotNull(rims);
            Assert.AreNotEqual(0, rims.Count());

        }

        [TestMethod]
        public async Task RoleInModuleControllerGetsAllMessagesProperly() {

            var controller = SLigand.Attach<BaseController<RoleInModule>>();
            Assert.IsNotNull(controller);
            var gotten = await controller.All();
            Assert.IsNotNull(gotten);
            Assert.AreNotEqual(0, gotten.Count);

        }

        #endregion

    }

}
