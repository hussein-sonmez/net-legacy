﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using PS.Business.Api.Configuration;
using PS.Business.Api.Formatters;
using PS.Core.Definitions.Web;
using PS.Emitter.Ligand.Emit;

namespace PS.Presentation.UnitTest.Base {

    public abstract class ApiTestBase {

        #region Inherited

        protected async Task<TResponse> ClientCallGetAsync<TResponse>(String path)
            where TResponse : class {

            using (var client = new HttpClient()) {

                var uri = String.Format("http://localhost/{2}/{3}/{0}/{1}"
                    , ControllerRoutePrefix, path, SWebDefinitions.ApiVirtualDirectory
                    , SControllerDefinitions.ApiRoute);
                var responseMessage = await client.GetAsync(uri);
                responseMessage.EnsureSuccessStatusCode();
                if (responseMessage.IsSuccessStatusCode) {
                    var response =
                        await responseMessage.Content.ReadAsAsync<TResponse>();
                    return response;
                } else {
                    return default(TResponse);
                }
            }

        }

        protected async Task<TEntity> ClientCallPostAsync<TEntity>(String path
            , TEntity entity)
            where TEntity : class {

            using (var client = new HttpClient()) {

                var uri = String.Format("http://localhost/{2}/{3}/{0}/{1}"
                    , ControllerRoutePrefix, path, SWebDefinitions.ApiVirtualDirectory
                    , SControllerDefinitions.ApiRoute);
                var response = await client.PostAsync<TEntity>(uri, entity
                    , SLigand.Attach<JsonMediaTypeFormatter>());
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode) {
                    return await response.Content.ReadAsAsync<TEntity>();
                } else {
                    return default(TEntity);
                }
            }

        }

        #endregion

        #region Abstract

        protected abstract String ControllerRoutePrefix { get; }

        #endregion

    }

}
