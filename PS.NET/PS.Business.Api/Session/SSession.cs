﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace PS.Business.Api.Session {
    public static class SSession {

        private static ISessionProvider _Provider;
        public static ISessionProvider Provider() {

            return _Provider ?? (_Provider = new SessionProvider());

        }

    }
}