﻿
using System;
using System.Web;
using System.Linq;
using System.Web.Http;
using System.Web.SessionState;
using System.Web.Http.Controllers;
using System.Collections;
using System.Collections.Generic;

namespace PS.Business.Api.Session {

    internal class SessionProvider : ISessionProvider {

        #region Fields

		private String _Key;
		private IDictionary _IDictionary;
        protected IDictionary Session {
            get {
				return HttpContext.Current != null
				? HttpContext.Current.Items
				: (_IDictionary = new Dictionary<String, Object>());
            }
        }
        #endregion

        #region  Implementation

        public void Apply<TItem>(TItem item) {

            if (Session.Keys.OfType<String>().Count(k=> k == _Key) == 1) {
                Session[_Key] = item;
            } else {
                Session.Add(_Key, item);
            }

        }

        public TItem Get<TItem>() {
			if (Session.Keys.OfType<String>().Count(k => k == _Key) == 1) {
                return (TItem)Session[_Key];
            } else {
                return default(TItem);
            }
        }

        public ISessionProvider OfKey(String key) {

            _Key = key;
            return this;

        }

        #endregion

    }

}