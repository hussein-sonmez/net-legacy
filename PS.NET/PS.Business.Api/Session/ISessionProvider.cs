﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS.Business.Api.Session {

    public interface ISessionProvider {

        ISessionProvider OfKey(String key);
        void Apply<TItem>(TItem item);
        TItem Get<TItem>();

    }
}
