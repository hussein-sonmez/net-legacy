﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Routing;
using System.Web.SessionState;
using Newtonsoft.Json;
using PS.Business.Api.Formatters;
using PS.Business.Api.Handlers;
using PS.Business.Api.Session;

namespace PS.Business.Api.Configuration {
    public static class GenericConfig {

		public static void Formatters(HttpConfiguration config) {

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.Remove(config.Formatters.JsonFormatter);
            config.Formatters.Add(new BrowserJsonFormatter());
        }

		public static void Session(HttpConfiguration config) {

			SSession.Provider().OfKey("AuthenticatedUserKey").Apply(1L);

		}

		public static void Routes(HttpConfiguration config) {

			// Web API routes
			config.MapHttpAttributeRoutes();

        }

		public static void PostAuthenticateRequest(Object s, EventArgs e) {

			System.Web.HttpContext.Current
				.SetSessionStateBehavior(SessionStateBehavior.Required);

		}

    }
}
