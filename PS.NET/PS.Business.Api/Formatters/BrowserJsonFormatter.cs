﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace PS.Business.Api.Formatters {

    internal class BrowserJsonFormatter : JsonMediaTypeFormatter {

        public BrowserJsonFormatter() {
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            this.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            this.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }

        public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType) {
            headers.ContentType = new MediaTypeHeaderValue("application/json");
            base.SetDefaultContentHeaders(type, headers, mediaType);
        }
    }

}
