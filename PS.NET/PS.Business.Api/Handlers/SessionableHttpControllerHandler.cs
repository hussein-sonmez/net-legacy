﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.WebHost;
using System.Web.Routing;
using System.Web.SessionState;

namespace PS.Business.Api.Handlers {

    internal class SessionableHttpControllerHandler : 
        HttpControllerHandler, IRequiresSessionState {
        public SessionableHttpControllerHandler(RouteData routeData)
            : base(routeData) { }
    }


}
