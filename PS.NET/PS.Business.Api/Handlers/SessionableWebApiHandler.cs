﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.WebHost;
using System.Web.Routing;
using System.Web.SessionState;

namespace PS.Business.Api.Handlers {
    internal class SessionableWebApiHandler : DelegatingHandler {

        public SessionableWebApiHandler(HttpConfiguration httpConfiguration) {

            InnerHandler = new HttpControllerDispatcher(httpConfiguration);

        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken) {

            return await base.SendAsync(request, cancellationToken);

        }

    }
}
