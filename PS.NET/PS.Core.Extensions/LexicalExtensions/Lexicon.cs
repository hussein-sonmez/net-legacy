﻿using PS.Core.Extensions.LexicalExtensions.Dictionary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace PS.Core.Extensions.LexicalExtensions {
    public static class Lexicon {

        #region Fields

        private static IEnumerable<String> Faust { get; set; }

        #endregion

        #region Public Static Methods

        public static String Pluralise(this String name) {
            return name.EndsWith("y") ? name.Substring(0, name.Length - 1) + "ies" : name.EndsWith("s") || name.EndsWith("z") ? name + "es" : name + "s";
        }

        public static String CapitalLetterToLower(this String name) {
            return name[0].ToString().ToLowerInvariant() + name.Substring(1);
        }

        public static String GenerateIdentifier(this String suffix) {

            SetFaust();

            var randomizer = new Func<Int32, Int32>((length) =>
            {
                return Convert.ToInt32(new Random().NextDouble() * length);
            });

            var identifier = new StringBuilder("");
            for (int i = 0; i < 4; i++) {
                identifier.Append(Faust.ElementAt(randomizer(Faust.Count())));
            }

            return identifier.ToString() + suffix;

        }

        public static String GenerateNumber(this String prefix) {

            prefix = "";
            return new Random().Next(1024 * 1024).ToString();

        }
        #endregion

        #region Fields

        private static void SetFaust() {
            if (Faust == null) {
                var text = Membrane.ResourceManager.GetObject("faust", Thread.CurrentThread.CurrentCulture).ToString();
                Faust = Regex.Matches(text, @"([a-zA-Z]{4,})").OfType<Match>()
                    .Select<Match, String>((m) => m.Value.ToUpperInvariant()[0] + m.Value.ToLowerInvariant().Substring(1));
            }
        }

        #endregion

    }

}
