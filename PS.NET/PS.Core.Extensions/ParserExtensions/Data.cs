﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS.Core.Extensions.ParserExtensions {
    public static class Data {

        public static Int32 Magnitude<TEntity>(this DbSet<TEntity> set)
                where TEntity : class {

            return (from e in set select e).Count();

        }

    }
}
