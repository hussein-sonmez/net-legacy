﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PS.Core.Extensions.ParserExtensions {
    public static class Cryptographic {

        public static String ComputeHash(this String value) {

            var md5 = new MD5CryptoServiceProvider();

            var result = md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(value));
            var strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++) {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();

        }


    }
}
