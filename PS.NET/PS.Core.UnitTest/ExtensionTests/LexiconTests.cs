﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Core.Extensions.LexicalExtensions;
using System.Linq;

namespace PS.Core.UnitTest.ExtensionTests {

    [TestClass]
    public class LexiconTests {

        [TestMethod]
        public void GenerateIdentifierWorksProperly() {

            var identifier = "".GenerateNumber().GenerateIdentifier();
            Assert.IsTrue(identifier.Length > 0, identifier);
            Assert.AreNotEqual(Enumerable.Count(identifier, (ch) => Char.IsUpper(ch)), 0);

        }
    }
}
