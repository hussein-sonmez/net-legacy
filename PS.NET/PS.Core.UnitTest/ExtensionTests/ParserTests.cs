﻿using System;
using System.Dynamic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Core.Extensions.ParserExtensions;

namespace PS.Core.UnitTest.ExtensionTests {

    [TestClass]
    public class ParserTests {

        [TestMethod]
        public void TransferToWorksProperly() {

            MyClass1 mc1 = new MyClass1("myField1")
            {
                MyProperty = 1
            };
            dynamic expando = new ExpandoObject();
            expando.MyProperty = mc1.MyProperty;
            expando.MyField = mc1.MyField;

            mc1.TransferTo(expando as Object);
            Assert.AreEqual(mc1.MyProperty, expando.MyProperty);
            Assert.AreEqual(mc1.MyField, expando.MyField);

        }

        [TestMethod]
        public void ImportWorksProperly() {

            MyClass1 mc1 = new MyClass1("myField1");

            dynamic expando = new ExpandoObject();
            expando.MyProperty = 1;
            expando.MyField = "myField11";

            mc1.Import<MyClass1>(expando as Object);
            Assert.AreEqual(mc1.MyProperty, expando.MyProperty);
            Assert.AreEqual("myField11", expando.MyField);

        }

    }

    class MyClass1 {

        public int MyProperty { get; set; }
        internal String MyField;

        public MyClass1(String myField) {
            MyField = myField;
        }

        public MyClass1() {

        }

    }
}
