﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PS.Business.Extensions.Data {
	public static class RepositoryExtensions {

		#region Extensions

		public static void Collection<TElement, TEntity, TContext>(this TContext ctx,
			Expression<Func<TEntity, ICollection<TElement>>> query)
			where TEntity : class
			where TElement : class
			where TContext : DbContext {

			var set = ctx.Set<TEntity>();
            var list = set.ToList();
            foreach (var e in list) {
				var collection = ctx.Entry(e).Collection(query);
                if (!collection.IsLoaded) {
                    collection.Load();
                }
			}

		}

		public static void Reference<TElement, TEntity, TContext>(this TContext ctx,
			Expression<Func<TEntity, TElement>> query)
			where TEntity : class
			where TElement : class
			where TContext : DbContext {

            var set = ctx.Set<TEntity>();
            var list = set.ToList();
			foreach (var e in list) {
				var reference = ctx.Entry(e).Reference(query);
                if (!reference.IsLoaded) {
                    reference.Load();
                }
			}

		}

		#endregion

	}
}
