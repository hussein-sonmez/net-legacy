﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS.Emitter.Markups.Attributes {
    [System.AttributeUsage(AttributeTargets.Assembly
        , Inherited = false, AllowMultiple = false)]
    public sealed class EmitAttribute : Attribute {

        #region Ctor

        public EmitAttribute() {
        }

        #endregion

    }
}
