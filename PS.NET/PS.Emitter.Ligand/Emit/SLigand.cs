﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Emitter.Receptor.Static;

namespace PS.Emitter.Ligand.Emit {

    public static class SLigand {

        public static TItem Attach<TItem>(params Object[] args) {

            return SReceptor.Emit<TItem>(args);
        }

    }
}
