﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS.Core.Model.Entity.Base {
    public class BaseModel {

        public Int64 ID { get; set; }
        public Boolean IsDeleted { get; set; }

    }
}
