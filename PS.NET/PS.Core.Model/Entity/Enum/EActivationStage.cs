﻿

using System.ComponentModel.DataAnnotations;

namespace PS.Core.Model.Entity.Enum {

    public enum EActivationStage {

        [Display(Description="Bekliyor")]
        Pending = 1,

        [Display(Description="Aktive Edildi")]
        Activated,

        [Display(Description="İptal Edildi")]
        Cancelled

    }
}