﻿

using System.ComponentModel.DataAnnotations;

namespace PS.Core.Model.Entity.Enum {
    public enum EDelayKind {

        [Display(Description = "Days")]
        FromDays = 1,
        [Display(Description = "Hours")]
        FromHours,
        [Display(Description = "Seconds")]
        FromSeconds,
        [Display(Description = "Minutes")]
        FromMinutes,
        [Display(Description = "Milliseconds")]
        FromMilliseconds,
        [Display(Description = "Ticks")]
        FromTicks

    }
}