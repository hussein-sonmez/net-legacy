﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Core.Model.Entity.Base;
using PS.Core.Model.Entity.POCO;

namespace PS.Core.Model.Entity.Map {
    public class TextMap : BaseMap<Text> {

        #region ctor

        public TextMap() {

            #region Properties

            this.Property(t => t.Content)
                .HasColumnName("Content")
                .HasMaxLength(8000)
                .IsRequired();

            this.Property(t => t.Dismissed)
                .HasColumnName("Dismissed")
                .IsRequired();

            this.Property(t => t.PublishedOn)
                .HasColumnName("PublishedOn")
                .IsOptional();

            #endregion

            #region Navigation Properties

            this.HasRequired(t => t.Message)
                .WithMany(t => t.Texts)
                .Map(x => x.MapKey("MessageID"));

            #endregion

            this.ToTable("Texts");
        }

        #endregion


    }
}
