﻿using PS.Core.Model.Entity.Base;
using PS.Core.Model.Entity.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS.Core.Model.Entity.Map {
    public class MessageMap : BaseMap<Message> {

        #region ctor

        public MessageMap() {

            #region Properties

            this.Property(t => t.DelayKind)
                .HasColumnName("DelayKind")
                .IsRequired();

            this.Property(t => t.DelayValue)
                .HasColumnName("DelayValue")
                .IsRequired();

            this.Property(t => t.PostDate)
                .HasColumnName("PostDate")
                .IsRequired();

            #endregion

            #region Navigation Properties

            this.HasOptional(t => t.Sender)
                .WithMany(t => t.SentMessages)
                .Map(x => x.MapKey("SenderID"))
                .WillCascadeOnDelete(false);

            this.HasOptional(t => t.Receiver)
                .WithMany(t => t.RecievedMessages)
                .Map(x => x.MapKey("ReceiverID"))
                .WillCascadeOnDelete(false);


            #endregion

            this.ToTable("Messages");
        }

        #endregion

    }
}
