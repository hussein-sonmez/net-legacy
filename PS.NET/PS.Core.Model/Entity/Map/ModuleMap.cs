﻿using PS.Core.Model.Entity.Base;
using PS.Core.Model.Entity.POCO;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;

namespace PS.Core.Model.Entity.Map {
    public class ModuleMap : BaseMap<Module> {

        public ModuleMap() {

            #region Properties

            this.Property(t => t.Name)
                .HasColumnName("Name")
                .HasMaxLength(255)
                .IsRequired();

            #endregion

            #region Navigation Properties

            #endregion

            this.ToTable("Modules");

        }

    }
}
