﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Core.Model.Entity.Base;

namespace PS.Core.Model.Entity.POCO {
    public class Function : BaseModel {

        #region Properties

        public String Code { get; set; }
        public String Description { get; set; }

        #endregion

        #region Navigation Properties

        public virtual Module Module { get; set; }

        #endregion

    }
}
