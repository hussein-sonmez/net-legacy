﻿using PS.Core.Model.Entity.Base;
using PS.Core.Model.Entity.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS.Core.Model.Entity.POCO {
    public class Message : BaseModel {

        #region Properties

        public Int64 DelayValue { get; set; }
        public EDelayKind DelayKind { get; set; }
        public DateTime PostDate { get; set; }

        #endregion

        #region Navigation Properties

        public virtual User Sender { get; set; }
        public virtual User Receiver { get; set; }
        public virtual ICollection<Text> Texts { get; set; }

        #endregion
    }
}
