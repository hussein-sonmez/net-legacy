﻿using PS.Core.Model.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS.Core.Model.Entity.POCO {
    public class Role : BaseModel {

        #region Properties

        public String Code { get; set; }

        #endregion

        #region Navigation Properties

        public virtual ICollection<UserInRole> UsersInRoles { get; set; }
        public virtual ICollection<RoleInModule> RolesInModules { get; set; }

        #endregion

    }
}
