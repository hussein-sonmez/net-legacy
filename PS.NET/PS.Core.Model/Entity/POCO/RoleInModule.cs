﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Core.Model.Entity.Base;

namespace PS.Core.Model.Entity.POCO {
    public class RoleInModule : BaseModel {

        #region Navigation Properties

        public virtual Role Role { get; set; }
        public virtual Module Module { get; set; }

        #endregion

    }
}
