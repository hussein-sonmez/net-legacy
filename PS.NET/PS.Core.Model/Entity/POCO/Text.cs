﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Core.Model.Entity.Base;

namespace PS.Core.Model.Entity.POCO {
    public class Text : BaseModel {

        #region Properties

        public String Content { get; set; }
        public DateTime PublishedOn { get; set; }
        public Boolean Dismissed { get; set; }

        #endregion

        #region Navigation Properties

        public virtual Message Message { get; set; }

        #endregion

    }
}
