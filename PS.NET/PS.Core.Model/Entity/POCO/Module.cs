﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Core.Model.Entity.Base;

namespace PS.Core.Model.Entity.POCO {
    public class Module : BaseModel {

        #region Properties

        public String Name { get; set; }

        #endregion

        #region Navigation Properties

        public virtual ICollection<Function> Functions { get; set; }
        public virtual ICollection<RoleInModule> RolesInModules { get; set; }

        #endregion

    }
}
