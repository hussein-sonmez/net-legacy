﻿using PS.Core.Model.Entity.Map;
using PS.Core.Model.Entity.POCO;
using System;
using System.Data.Entity;

namespace PS.Core.Model.Context {

    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class PSDataContext : DbContext, IDisposable {

		#region Ctors

		public PSDataContext()
			: base(SConnectionProvider.CurrentConnection, false) {

			this.Configuration.ProxyCreationEnabled = false;
			this.Configuration.LazyLoadingEnabled = false;

        }

        #endregion

        #region Sets

        public DbSet<Function> Functions { get; set; }
		public DbSet<Message> Messages { get; set; }
		public DbSet<Text> Texts { get; set; }
		public DbSet<Module> Modules { get; set; }
		public DbSet<RoleInModule> RolesInModules { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<UserInRole> UsersInRoles { get; set; }
		public DbSet<User> Users { get; set; }

		#endregion

		#region Overrides
		protected override void OnModelCreating(DbModelBuilder modelBuilder) {

			base.OnModelCreating(modelBuilder);
			modelBuilder.Configurations.Add(new FunctionMap());
			modelBuilder.Configurations.Add(new MessageMap());
			modelBuilder.Configurations.Add(new TextMap());
			modelBuilder.Configurations.Add(new ModuleMap());
			modelBuilder.Configurations.Add(new RoleInModuleMap());
			modelBuilder.Configurations.Add(new RoleMap());
			modelBuilder.Configurations.Add(new UserInRoleMap());
			modelBuilder.Configurations.Add(new UserMap());

        }
        #endregion
    }
}
