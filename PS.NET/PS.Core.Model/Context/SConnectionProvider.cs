﻿using System.Data.Common;
using MySql.Data.MySqlClient;

namespace PS.Core.Model.Context {
    internal static class SConnectionProvider {

        internal static DbConnection CurrentConnection {
            get {
                var connStr = @"Server=localhost;Database=psdb;Uid=psadmin;Pwd=1q2w3e4r5t;";
                var conn = new MySqlConnection(connStr);
                return conn;
            }
        }

    }
}
