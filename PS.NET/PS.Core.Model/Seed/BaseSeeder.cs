﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Core.Extensions.ParserExtensions;

namespace PS.Core.Model.Seed {
    public class BaseSeeder<TEntity> : ISeeder<TEntity>
        where TEntity : class {

        public ISeeder<TEntity> SetSeeder(Func<Int64, TEntity> seeder) {
            _Seeder = seeder;
            return this;
        }
        private Func<Int64, TEntity> _Seeder;
        protected Int64 N = 30;
        public IEnumerable<TEntity> Seed() {
            var seed = new List<TEntity>();
            foreach (var index in Enumerable.Range(1, N.ParseToInt32())) {
                seed.Add(_Seeder(index));
            }

            return seed.ToArray();
        }

        private TEntity _Representative;
        public TEntity Representative {
            get {
                return _Representative ?? (_Representative = _Seeder(N + 1));
            }
        }

    }
}
