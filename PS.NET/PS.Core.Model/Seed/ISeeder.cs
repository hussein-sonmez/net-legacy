﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS.Core.Model.Seed {
    public interface ISeeder<TEntity>
        where TEntity : class {

        ISeeder<TEntity> SetSeeder(Func<Int64, TEntity> seeder);
        IEnumerable<TEntity> Seed();
        TEntity Representative { get; }
    }
}
