﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Business.UnitTest.Base;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.UnitTest.CrudTests {
    [TestClass]
    public class UserInRoleCrudTests : GenericCrudTest<UserInRole> {

        [TestMethod]
        public async Task CrudUserInRoleWorksProperly() {

            var dict = await base.CrudSuite();
            foreach (var kv in dict) {
                Assert.IsTrue(kv.Value, kv.Key);
            }

        }

        protected override UserInRole UpdatedEntity(UserInRole seededEntity) {

            seededEntity.IsDeleted = true;
            return seededEntity;

        }

    }
}
