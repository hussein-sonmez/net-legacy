﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Business.Model.Base;
using PS.Core.Model.Entity.POCO;
using PS.Emitter.Ligand.Emit;
using PS.Core.Extensions.ParserExtensions;
using PS.Business.UnitTest.Base;

namespace PS.Business.UnitTest.CrudTests {
    [TestClass]
    public class UserCrudTests : GenericCrudTest<User> {


        [TestMethod]
        public async Task CrudUserWorksProperly() {

            var dict = await base.CrudSuite();
            foreach (var kv in dict) {
                Assert.IsTrue(kv.Value, kv.Key);
            }
            
        }

        protected override User UpdatedEntity(User seededEntity) {

            seededEntity.IsDeleted = true;
            return seededEntity;

        }
    }
}
