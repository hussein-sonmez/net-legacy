﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.WebHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Emitter.Ligand.Emit;
using PS.Business.Api.Configuration;


namespace PS.Business.UnitTest.EmitTests {

    [TestClass]
    public class EmitterTests {

        #region Test Methods

        [TestMethod]
        public void EmitterEmitsHttpMessageHandler() {

			var config = new HttpConfiguration();
			GenericConfig.Routes(config);
			GenericConfig.Formatters(config);
			GenericConfig.Session(config);
			var handler = SLigand.Attach<DelegatingHandler>(config);
            Assert.IsNotNull(handler);

        }

        #endregion

    }
}
