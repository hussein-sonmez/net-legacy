﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS.Core.Definitions.Web {
    public static class SWebDefinitions {

        public const String ApiVirtualDirectory = "psapi";
        public const String WebVirtualDirectory = "psweb";

    }
}
