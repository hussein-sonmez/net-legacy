﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS.Core.Definitions.Web {
    public static class SControllerDefinitions {

        public const String ApiRoute = "api";
        public const String MessageRoute = "messages";
        public const String RoleInModuleRoute = "rolesinmodules";
        public const String UserRoute = "users";
        public const String UserInRoleRoute = "usersinroles";

    }
}
