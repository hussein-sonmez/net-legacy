﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using PS.Emitter.Markups.Attributes;

namespace PS.Emitter.Receptor.Static {

	internal static class SReceptor {

		#region Public Static
		public static TItem Emit<TItem>(params Object[] args) {

			ScanForCatalog();
			var itemType = typeof(TItem);
			IList<AssemblyName> asmNames = new List<AssemblyName>();
			if (itemType.Assembly
				.GetCustomAttribute<EmitAttribute>() != null) {
				asmNames.Add(itemType.Assembly.GetName());
			} else {
				asmNames = itemType.Assembly
			   .GetReferencedAssemblies().Where(asmn => Assembly.Load(asmn).GetCustomAttribute<EmitAttribute>() != null).ToList();
			}
			typeof(SReceptor).Assembly.GetReferencedAssemblies()
				.ToList().ForEach(asmn => asmNames.Add(asmn));

			AssemblyCatalog.ToList().ForEach(asm => asmNames.Add(asm.GetName()));

			Type foundType = null;

			foreach (var asmName in asmNames) {
				var assembly = Assembly.Load(asmName);
				if (itemType.IsInterface) {
					foundType = assembly.GetTypes()
					.SingleOrDefault(t =>
					t.ContainsGenericParameters ?
					   t.GetInterface(itemType.Name) != null
					: t.GetInterface(itemType.FullName) != null
					);
				} else {
					try {
						foundType = assembly.GetTypes()
									  .Where(t => !t.IsInterface 
										  && t.IsSubclassOf(itemType))
									  .FirstOrDefault();
					} catch (ReflectionTypeLoadException ex) {
						foundType = ex.Types
							.Where(t => !t.IsInterface 
								&& t.IsSubclassOf(itemType)).FirstOrDefault();
					}
				}
				if (foundType == null) {
					continue;
				}
				if (foundType.ContainsGenericParameters) {
					itemType.GenericTypeArguments.ToList()
						.ForEach(ga => foundType = foundType.MakeGenericType(ga));
				}
                if (foundType.GetCustomAttribute<OmitAttribute>() != null) {
                    foundType = assembly.GetTypes().Where(t => t.IsSubclassOf(foundType))
                        .SingleOrDefault();
                }
                return (TItem)Activator.CreateInstance(foundType, args);
			}

			return default(TItem);

		}

		#endregion

		#region Private
		private static HashSet<Assembly> AssemblyCatalog = new HashSet<Assembly>();
		private static void ScanForCatalog() {

			var assemblyBase = new FileInfo(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", @"\"));

			var dlls = Directory.EnumerateFiles(assemblyBase.DirectoryName, "PS*.dll");
			foreach (var dll in dlls) {
				EmitAttribute attr = null;
				var assembly = Assembly.LoadFrom(dll);
				if ((attr = assembly.GetCustomAttribute<EmitAttribute>()) != null) {
					if (!AssemblyCatalog.Contains(assembly)) {
						AssemblyCatalog.Add(assembly);
					}
				}
			}

		}
		#endregion

	}
}
