﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.Sheet {
    public interface IMark {
        INumber Correct { get; set; }
        INumber Wrong { get; set; }
        INumber Blank { get; set; }
    }
}
