﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.Sheet {
    public interface IGrade {
        UInt32 Value { get; set; }
    }
}
