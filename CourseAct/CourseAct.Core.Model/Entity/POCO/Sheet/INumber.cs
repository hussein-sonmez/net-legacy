﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.Sheet {
    public interface INumber {
        UInt32 Count { get; set; }
    }
}
