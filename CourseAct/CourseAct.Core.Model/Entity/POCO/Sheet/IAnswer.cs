﻿using CourseAct.Model.Script;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseAct.Model.Sheet {
    public interface IAnswer : IScripture {
        Boolean IsTrue { get; set; }
    }
}
