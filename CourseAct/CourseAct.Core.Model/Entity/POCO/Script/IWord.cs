﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.Script {
    public interface IWord {
        ICollection<ILanguage> Languages { get; set; }
        String Value { get; set; }
        Char Punctuation { get; set; }
    }
}
