﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.Script {
    public interface ITranslation {
        ICollection<IWord> Words { get; set; }
        ICollection<IWord> Translations { get; set; }
    }
}
