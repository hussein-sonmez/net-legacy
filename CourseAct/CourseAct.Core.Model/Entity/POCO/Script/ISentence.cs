﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.Script {
    public interface ISentence {
        ICollection<IWord> Words { get; set; }
    }
}
