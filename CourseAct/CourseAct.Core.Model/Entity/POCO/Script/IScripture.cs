﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.Script {
    public interface IScripture {
        ICollection<IDrawing> Drawings { get; set; }
        ICollection<ISentence> Sentences { get; set; }
    }
}
