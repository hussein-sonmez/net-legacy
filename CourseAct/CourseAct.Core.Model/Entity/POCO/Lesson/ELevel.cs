﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseAct.Model.Lesson {
    public enum ELevel {
        Beginner = 1,
        Elementary,
        PreIntermediate,
        Intermediate,
        UpperIntermediate,
        Advanced,
        Proficient
    }
}
