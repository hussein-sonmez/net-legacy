﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.Case;

using CourseAct.Model.Sheet;

namespace CourseAct.Model.Lesson {
    public interface ILesson{
        ITopic Topic { get; set; }

        ICollection<IWorksheet> WorkSheets { get; set; }
    }
}
