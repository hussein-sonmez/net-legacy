﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.Sheet;
using CourseAct.Model.Department;
using CourseAct.Model.User;

namespace CourseAct.Model.Case {
    public interface IExam : ICase<ITrainee, IQuiz> {
    }
}
