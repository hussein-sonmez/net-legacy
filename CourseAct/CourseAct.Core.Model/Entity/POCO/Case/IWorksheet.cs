﻿using CourseAct.Model.Script;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.Lesson;
using CourseAct.Model.Sheet;
using CourseAct.Model.Department;



namespace CourseAct.Model.Case {
    public interface IWorksheet {
        ICollection<IProblem> Problems { get; set; }

        IScripture Foreword { get; set; }
        ILesson Lesson { get; set; }
        IMark Mark { get; set; }

        Boolean IsDraft { get; set; }
        DateTime DatePublished { get; set; }

        IClassRoom ClassRoom { get; set; }
    }
}
