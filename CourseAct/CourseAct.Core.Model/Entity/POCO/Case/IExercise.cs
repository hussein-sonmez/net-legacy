﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseAct.Model.User;

namespace CourseAct.Model.Case {
    public interface IExercise : ICase<ITrainee, IHomework> {
    }
}
