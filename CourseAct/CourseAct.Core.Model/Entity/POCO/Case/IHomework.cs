﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseAct.Model.Case {
    public interface IHomework : IWorksheet {
        DateTime DueDate { get; set; }
    }
}
