﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.User;

namespace CourseAct.Model.Case {
    public interface IDemonstration : ICase<IDemonstrator, IQuiz> {
    }
}
