﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.User;
using CourseAct.Model.Lesson;
using CourseAct.Model.Case;
using CourseAct.Model.Sheet;


namespace CourseAct.Model.Department {
    public interface IClassRoom{
        ITitle NamePlate { get; set; }

        ILecturer Lecturer { get; set; }
        ICollection<ITrainee> Trainees { get; set; }
        ICollection<ILesson> Lessons { get; set; }

        ICollection<IExam> Exams { get; set; }
        ICollection<IExercise> Exercises { get; set; }
    }
}
