﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.User.Client {
    public interface IPersonalInfo {
        String Name { get; set; }
        String LastName { get; set; }
        String TCNumber { get; set; }
        String MobilePhone { get; set; }
        String MailAddress { get; set; }
    }
}
