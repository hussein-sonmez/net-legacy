﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseAct.Model.User.Client {
    public enum EActivity {
        RegisteredAccount = 1,
        AccountActivated,
        AccountInactivated,
        LoggedIn,
        RegisteredExam,
        RegisteredHomework,
        CompletedExam,
        LoggedOut
    }
}
