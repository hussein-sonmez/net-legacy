﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.User.Client {
    public interface IAddressInfo {
        String Definition { get; set; }
        String Value { get; set; }
        IAddressInfo Container { get; set; }
    }
}
