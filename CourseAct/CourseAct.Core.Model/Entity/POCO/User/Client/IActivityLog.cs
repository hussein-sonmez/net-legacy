﻿using CourseAct.Model.Department;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.User.Client {
    public interface IActivityLog {
        DateTime LastActivityTime { get; set; }
        EActivity Activity { get; set; }
        IClient Client { get; set; }
    }
}
