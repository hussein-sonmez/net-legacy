﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseAct.Model.User {
    public interface IInstitution : IClient {
        ICollection<IGeneralManager> GeneralManagers { get; set; }
    }
}
