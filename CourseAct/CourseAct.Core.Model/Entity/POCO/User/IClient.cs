﻿using CourseAct.Model.User.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.Role;



namespace CourseAct.Model.User {
    public interface IClient{
        IAddressInfo AddressInfo { get; set; }
        IPersonalInfo PersonalInfo { get; set; }
        ICredentialInfo CredentialInfo { get; set; }

        ICollection<IActivityLog> ActivityLogs { get; set; }
        ICollection<IGrant> Grants { get; set; }
    }
}
