﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.Department;

namespace CourseAct.Model.User {
    public interface IDepartmentManager : IClient {
        IDepartment Department { get; set; }
    }
}
