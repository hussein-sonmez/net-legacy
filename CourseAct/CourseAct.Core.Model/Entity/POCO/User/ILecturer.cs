﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.Lesson;
using CourseAct.Model.Department;

namespace CourseAct.Model.User {
    public interface ILecturer : ITrainee {
        IDepartmentManager DepartmentManager { get; set; }
        ICollection<ITrainee> Trainees { get; set; }
    }
}
