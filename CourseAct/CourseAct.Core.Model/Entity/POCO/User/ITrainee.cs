﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.Lesson;
using CourseAct.Model.Department;

namespace CourseAct.Model.User {
    public interface ITrainee : IClient {
        IClassRoom ClassRoom { get; set; }
        ICollection<ILesson> Lessons { get; set; }
    }
}
