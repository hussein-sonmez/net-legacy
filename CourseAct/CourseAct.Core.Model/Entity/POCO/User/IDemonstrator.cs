﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.Case;
using CourseAct.Model.Sheet;

namespace CourseAct.Model.User {
    public interface IDemonstrator : IClient {
        DateTime DueDateMembership  { get; set; }
    }
}
