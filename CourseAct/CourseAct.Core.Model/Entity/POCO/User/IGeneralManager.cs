﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseAct.Model.Department;

namespace CourseAct.Model.User {
    public interface IGeneralManager : IClient {
        IInstitution Institution { get; set; }
        ICollection<IDepartment> Departments { get; set; }
    }
}
