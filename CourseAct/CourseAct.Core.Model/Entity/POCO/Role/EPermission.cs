﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseAct.Model.Role {
    public enum EPermission {
        UserCRUD = 1,
        SheetCRUD,
        UnitCRUD,
        LessonCRUD,
        CaseCRUD,
        TakeExam,
        TakeDemonstration,
        Logon,

    }
}
