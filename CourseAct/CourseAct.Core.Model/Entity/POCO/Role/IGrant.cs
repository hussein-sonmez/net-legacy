﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.Role {
    public interface IGrant {
        IRole Role { get; set; }
        IPermission Permission { get; set; }
    }
}
