﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseAct.Model.Role {
    public interface IRole : IPermission {
        ERole Role { get; set; }
    }
}
