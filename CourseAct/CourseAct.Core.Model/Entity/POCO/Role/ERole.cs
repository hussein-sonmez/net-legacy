﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseAct.Model.Role {
    public enum ERole {
        Manager = 1,
        Developer,
        Lecturer,
        Trainee
    }
}
