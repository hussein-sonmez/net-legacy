﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public enum EConstraintType {

		PrimaryKey = 1,
		ForeignKey,
		Default,
		Unique,
		Check

	}
}
