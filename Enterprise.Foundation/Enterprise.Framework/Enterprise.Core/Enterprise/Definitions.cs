﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {

	internal class Definitions : IDefinitions {

		#region String Typed Definitions

		public String ConnectionStringFormats(EProviderType providerType, ESecurityMode securityMode) {

			switch (providerType) {
				case EProviderType.MsSql:
					switch (securityMode) {
						case ESecurityMode.StandartSecurity:
							return @"Server={0};Database={3};User Id={1};Password={2};";
						case ESecurityMode.LocalDb:
							return @"Data Source=np:\\.\pipe\LOCALDB{0}\tsql\query;Integrated Security=True";
						case ESecurityMode.SqlExpress:
							return @"Data Source={0};Initial Catalog={3};Trusted_Connection=Yes";
						default:
							throw new ArgumentException("ESecurityMode");
					}
				case EProviderType.MySql:
					return securityMode == ESecurityMode.StandartSecurity ?
						@"Server={0};Database={3};Uid={1};Pwd={2};" :
						@"Server={0};Database={3};";
				case EProviderType.PostgreSql:
					return securityMode == ESecurityMode.StandartSecurity ?
						@"Data Source={0};User Id={2};Password={3};Integrated Security=no;" :
						@"Data Source={0};Integrated Security=yes;";
				default:
					return String.Empty;
			}

		}

		public String VirtualDirectory {
			get {
				return "enterprise";
			}
		}
		public String Virtualize(string path) {

			return String.Format("/{0}{1}", VirtualDirectory, path);

		}
		public IConnectionCredential GetCredential() {

			var cred = SEnterprise
				.GetImplementor<IConnectionCredential>().Construct(EProviderType.MsSql);
			cred.DataSource.Set(@".");
			cred.DatabaseName.Set("EnterpriseDB");
            cred.UserName.Set("epadmin");
            cred.Password.Set("1q2w3e4r5t");
            cred.SecurityMode.Set(ESecurityMode.StandartSecurity);
			return cred;

		}

		public String GetLoginCommandString(IConnectionCredential credential) {

			return String.Format(@"
If not Exists (select loginname from master.dbo.syslogins 
    where name='{1}' and dbname='{0}')
BEGIN
CREATE LOGIN {1} WITH PASSWORD = '{2}',CHECK_POLICY = OFF,DEFAULT_DATABASE = {0};USE {0};CREATE USER [{1}] FOR LOGIN {1};exec sp_addrolemember 'db_owner', '{1}';
END", credential.DatabaseName.Item, credential.UserName.Item, credential.Password.Item);
		}

		#endregion

	}
}
