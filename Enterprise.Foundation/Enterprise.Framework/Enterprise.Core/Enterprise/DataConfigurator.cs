﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.SessionState;

namespace Enterprise.Core {
	class DataConfigurator<TConnection> : IDataConfigurator<TConnection>
		where TConnection : DbConnection {

		#region Ctor

		public DataConfigurator(IConnectionCredential credential) {
			this.dataStore = new Lazy<IDataStore<TConnection>>(() => SEnterprise.GetImplementor<IDataStore<TConnection>>().Construct(credential));
		}

		#endregion

		#region IDataConfigurator Members

		public IDataConfigurator<TConnection> RegisterModel<TModel>()
			where TModel : class, IGenericModel<TModel>, new() {
			this.DataStore.RegisterDataClass<TModel>();
			return this;
		}

		public IDataStore<TConnection> DataStore {
			get {
				return this.dataStore.Value;
			}
		}

		#endregion

		#region Properties

			private Lazy<IDataStore<TConnection>> dataStore;

		#endregion

	}
}
