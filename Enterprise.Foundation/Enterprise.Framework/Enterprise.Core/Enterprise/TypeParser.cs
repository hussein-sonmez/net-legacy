﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core.Decorators;

namespace Enterprise.Core {
	internal class TypeParser : Singularity<ITypeParser>, ITypeParser{

		#region ITypeParser Members

		public Object GetValueOf(Object item, String property, object[] index = null) {

			var value = item.GetType().GetProperty(property).GetValue(item, index);
			return value;

		}

		public IEnumerable<dynamic> AnalyzeParameters<TItem>(TItem item) {

			return AnalyzeParameters<TItem>(item, (pi) => pi.GetCustomAttributes<ColumnAttribute>().Count() > 0);

		}

		public IEnumerable<dynamic> SelectParameters<TItem>(dynamic item) {

			return AnalyzeParameters<TItem>(item, new Func<PropertyInfo, Boolean>(pi => !IsUnassigned(pi.GetValue(item, null))));

		}

		public dynamic AnalyzeEnumValues<TEnum>() {
			var locals = typeof(TEnum).GetCustomAttribute<LocalsAttribute>();
			var lang = Activator.CreateInstance(locals.Language);
			var title = locals.Language.GetProperty(locals.Title ?? typeof(TEnum).Name).GetValue(lang).ToString();
			dynamic options = typeof(TEnum).GetEnumValues().OfType<TEnum>().Select(en => new {
				TextValue = locals.Language.GetProperty(Enum.GetName(typeof(TEnum), en)).GetValue(lang).ToString(),
				DataValue = ParseType<Int32>(en)
			});
			return new {
				OptionTitle = String.Format("{0}&nbsp;<span class='caret'></span>", title),
				OptionID = "Option-" + Guid.NewGuid().ToString("N"),
				Options = options
			};
		}

		public IEnumerable<dynamic> FilterParameters<TItem, TKeyAttribute, TValueAttribute>(Func<TKeyAttribute, TValueAttribute, Boolean> query, Func<TKeyAttribute, TValueAttribute, dynamic> selector)
			where TKeyAttribute : Attribute
			where TValueAttribute : Attribute {

			var q = typeof(TItem).GetProperties()
				.Where<PropertyInfo>(pi => query(pi.GetCustomAttribute<TKeyAttribute>(), pi.GetCustomAttribute<TValueAttribute>()))
				.Select<PropertyInfo, dynamic>(pi =>
					selector(pi.GetCustomAttribute<TKeyAttribute>(), pi.GetCustomAttribute<TValueAttribute>()));
			return q.ToArray();

		}

		public Object Assign(Object instance, Object value, String propertyName) {

			instance.GetType().GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public).SetValue(instance, value);
			return value;

		}

		public IEnumerable<TAttribute> GetCustomPropertyAttribute<TItem, TAttribute>()
		where TAttribute : System.Attribute {

			return typeof(TItem).GetProperties().SelectMany<PropertyInfo, TAttribute>(pi => pi.GetCustomAttributes<TAttribute>());

		}
		public TAttribute GetCustomClassAttribute<TItem, TAttribute>()
			where TAttribute : Attribute {
			return typeof(TItem).GetCustomAttribute<TAttribute>();
		}

		public IEnumerable<KeyValuePair<TAttribute1, TAttribute2>> GetCustomPropertyAttributePair<TItem, TAttribute1, TAttribute2>(TItem to)
			where TAttribute1 : System.Attribute
			where TAttribute2 : System.Attribute {

			return typeof(TItem).GetProperties(BindingFlags.FlattenHierarchy).Select<PropertyInfo, KeyValuePair<TAttribute1, TAttribute2>>(pi =>
				new KeyValuePair<TAttribute1, TAttribute2>(pi.GetCustomAttributes<TAttribute1>().FirstOrDefault()
					, pi.GetCustomAttributes<TAttribute2>().FirstOrDefault()));

		}

		public String TakeDescription<TEnum>(Object value) {
			var q = from TEnum en in Enum.GetValues(typeof(TEnum)).AsQueryable()
					where ParseCode(en) == ParseCode(value)
					select typeof(TEnum).GetFields().Where<FieldInfo>((fi) =>
						fi.GetCustomAttributes(true).Where(
					(ca) => ca is DescriptionAttribute && ParseType<Int32>(fi.GetValue(en)) == ParseType<Int32>(value))
					.FirstOrDefault() != null)
						.Select<FieldInfo, String>((fi) =>
							fi.GetCustomAttributes(true).OfType<DescriptionAttribute>().First().Description).FirstOrDefault();

			return q.FirstOrDefault();
		}

		public TItem ParseType<TItem>(dynamic instance) {
			if (instance.GetType().GetInterface("IConvertible") == null || typeof(TItem).Equals(typeof(ValueType))) {
				return instance != null ? (TItem)instance : default(TItem);
			} else {
				if (instance.GetType().IsEnum) {
					return (TItem)Enum.Parse(instance.GetType(), instance.ToString());
				} else {
					TypeConverter conv;
					conv = TypeDescriptor.GetConverter(typeof(TItem));
					return (TItem)(conv.CanConvertFrom(instance.GetType()) == true ? conv.ConvertFrom(instance) : instance);
				}
			}
		}

		public Int32 ParseCode(Object instance) {

			if (instance.GetType().IsEnum) {
				return Convert.ToInt32(instance);
			} else {
				throw new ArgumentException("Enum Required");
			}

		}

		public Boolean IsNumberType(dynamic item) {

			TypeInfo t = item.GetType();
			var repr = item.ToString();
			Int64 rLong;
			Decimal rDecimal;
			if (Int64.TryParse(repr, out rLong)) {
				return true;
			} else if (Decimal.TryParse(repr, out rDecimal)) {
				return true;
			} else {
				return false;
			}

		}

		public Boolean IsUnassigned(dynamic item) {
			DateTime reponse1;
			Guid response2;

			var itemNull = item == null;
			if (itemNull) {
				return true;
			} else {
				var itemIsNumber = this.IsNumberType(item);
				var itemIsDate = DateTime.TryParse(item.ToString(), out reponse1);
				var itemIsGuid = Guid.TryParse(item.ToString(), out response2);
				var itemIsEnum = item.GetType().IsEnum;

				return (itemIsNumber && (item == 0 || item == 0.0))
					|| (itemIsDate && item == default(DateTime))
					|| (itemIsGuid && item == default(Guid))
					|| (itemIsEnum && item == 0);
			}
		}

		public void Copy<TItem>(TItem source, TItem destination)
			where TItem : class, new() {
			foreach (PropertyInfo pi in typeof(TItem).GetProperties(BindingFlags.Public | BindingFlags.Instance)) {
				Object set = null;
				try {
					set = pi.GetValue(source, null);
					if ((set is ValueType && set.Equals(Activator.CreateInstance(pi.PropertyType)))
						|| (true == String.IsNullOrEmpty(set.ToString()))
						|| (set.ToString() == Guid.Empty.ToString())) {
						continue;
					} else {
						pi.SetValue(destination, set, null);
					}
				} catch (Exception) {
					continue;
				}
			}
		}

		public dynamic DynamicCopy(dynamic source, String entityModelAssembly, String entityModelNamespace, string typeIdentifier) {
			var destinationType = AssemblyBuilder.Load(entityModelAssembly)
				.GetType(entityModelNamespace + "." + typeIdentifier.Split(',')[0]);
			dynamic copy = Activator.CreateInstance(destinationType);
			Type sourceType = source.GetType();
			foreach (var sp in (source as IDictionary<String, Object>)) {
				var target = destinationType.GetProperties().SingleOrDefault(pi => pi.Name == sp.Key);
				if (null != target) {
					if (false == sp.Value is ExpandoObject) {
						target.SetValue(copy, sp.Value, null);
					} else {
						var derived = DynamicCopy(sp.Value, entityModelAssembly, entityModelNamespace, typeIdentifier.Split(',')[1]);
						target.SetValue(copy, derived, null);

					}
				}
			}
			return copy;
		}

		public void HollowMembers(dynamic source, dynamic destination) {
			var props = source.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
			foreach (var prop in props) {
				var v = prop.GetValue(source, null);
				if (!IsUnassigned(v) && prop.CanWrite) {
					prop.SetValue(destination, v, null);
				}
			}
		}


		#endregion

		#region  Private

		private IEnumerable<dynamic> AnalyzeParameters<TItem>(TItem item, Func<PropertyInfo, Boolean> selector) {

			var q = typeof(TItem).GetProperties().Where<PropertyInfo>(pi => selector(pi))
				.Select<PropertyInfo, dynamic>(pi => {

					var constraint = pi.GetCustomAttribute<ConstraintAttribute>();
					dynamic t = new ExpandoObject();
					t.Key = pi.Name;
					t.Value = pi.GetValue(item, null);
					t.Column = pi.GetCustomAttribute<ColumnAttribute>();
					t.PropertyType = pi.PropertyType;
					if (constraint != null && constraint.ConstraintType == EConstraintType.ForeignKey) {
						t.RelatedModel = constraint.ReferencingType.Name;
					}
					return t;

				});
			return q;

		}

		#endregion

		#region Overrides

		public override ITypeParser Me {
			get { return this; }
		}

		#endregion

	}
}
