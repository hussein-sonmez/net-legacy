﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core.Decorators;

namespace Enterprise.Core {
    internal class Implementor<TInterface> : IImplementor<TInterface> {

        #region Private

        private static HashSet<Assembly> AssemblyCatalog = new HashSet<Assembly>();
        private IImplementor<TInterface> ScanForCatalog() {

            var assemblyBase = new FileInfo(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", @"\"));
            var parser = new SentenceParser();

            var dlls = Directory.EnumerateFiles(assemblyBase.DirectoryName, "*.dll");
            foreach (var dll in dlls) {
                MarkAsImplementorAttribute attr = null;
                var assembly = Assembly.LoadFrom(dll);
                if ((attr = assembly.GetCustomAttribute<MarkAsImplementorAttribute>()) != null) {
                    if (!AssemblyCatalog.Contains(assembly)) {
                        AssemblyCatalog.Add(assembly);
                    }
                }
            }

            return this;

        }

        #endregion

        #region Methods

        public TInterface Construct(params object[] args) {
            ScanForCatalog();
            if (typeof(TInterface).IsInterface) {
                Func<Assembly, Type, TInterface> search = (asm, tif) =>
                {
                    var found = asm.GetTypes().Where(t =>
                        t.GetCustomAttribute<MarkAsBaseAttribute>() != null)
                        .SingleOrDefault(t=>
                        t.GetCustomAttribute<MarkAsBaseAttribute>().BaseFor.Name.StartsWith(tif.Name));
                    if (found == null) {
                        found = asm.GetTypes()
                      .FirstOrDefault(t => t.IsInterface == false && t.GetInterfaces()
                          .Any(ti => t.GetInterface(tif.Name) != null &&
                              t.GetInterface(tif.Name).GenericTypeArguments
                              .All(gta => gta.IsGenericParameter ? true
                                  : tif.GenericTypeArguments.Any(tifgta => tifgta.IsAssignableFrom(gta)))));
                    }

                    if (found != null) {
                        var generics = tif.GetGenericArguments();
                        if (generics.Count() > 0) {
                            found = found.MakeGenericType(generics);
                        }
                        return (TInterface)Activator.CreateInstance(found, args);
                    } else {
                        return default(TInterface);
                    }

                };

                TInterface type = default(TInterface);
                foreach (var assembly in AssemblyCatalog) {
                    if ((type = search(assembly, typeof(TInterface))) != null) {
                        return type;
                    }
                }
                if (type == null) {
                    type = search(typeof(TInterface).Assembly, typeof(TInterface));
                }
                if (type == null) {
                    type = search(Assembly.GetCallingAssembly(), typeof(TInterface));
                }
                if (type == null) {
                    var asms = Assembly.GetCallingAssembly().GetReferencedAssemblies();
                    foreach (var asm in asms) {
                        type = search(Assembly.Load(asm), typeof(TInterface));
                        if (type != null) {
                            return type;
                        }
                    }
                }
                return type;
            } else {
                throw new Exception("Not an interface: " + typeof(TInterface).Name);
            }

        }

        public TResponse Construct<TResponse>(params object[] args)
            where TResponse : class {
            return Construct(args) as TResponse;
        }

        #endregion

    }
}
