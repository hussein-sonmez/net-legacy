﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;
using System.Web.SessionState;
using Enterprise.Core.Decorators;

namespace Enterprise.Core {
	public static class SEnterprise {

		public static IImplementor<TInterface> GetImplementor<TInterface>() {
			return new Implementor<TInterface>();
		}

		public static ISentenceParser GetSentenceParser() {
			return new SentenceParser();
		}

		public static ITypeParser GetTypeParser() {
			return new TypeParser();
		}

		public static IDefinitions GetDefinitions() {
			return new Definitions();
		}


		public static void ConfigureRouteTable(RouteCollection routeCollection, Func<IDataStore<SqlConnection>> configurator) {

			var virdir = GetDefinitions().VirtualDirectory;

			var getRelatedRoute = new Func<Type, IEnumerable<String>>(
				(model)=> model.GetProperties().SelectMany(prop=>prop.GetCustomAttributes<ConstraintAttribute>()
					.Where(constraint=>constraint.ConstraintType == EConstraintType.ForeignKey)
					.Select<ConstraintAttribute, String>(constraint => String.Format("{0}/{{id}}/{1}"
						, model.Name, constraint.ReferencingType.Name))).AppendElement(model.Name).ToArray()
				);

			routeCollection.Ignore("Default.aspx");
			routeCollection.MapPageRoute(routeName: "Main-Page-" + Guid.NewGuid().ToString("N"),
					routeUrl: String.Format("localhost/{0}/", virdir),
					physicalFile: "~/Default.aspx",
					checkPhysicalUrlAccess: false);
			var list = configurator().GetRegisteredDataClasses().ToList();
			list.ForEach(kv =>
				getRelatedRoute(kv.Key).ToList().ForEach(relatedRoute =>
				routeCollection.MapPageRoute(
					routeName: "Detail-" + relatedRoute.Replace("{id}", "-").Replace(@"/", ""),
					routeUrl: relatedRoute,
					physicalFile: "~/Default.aspx",
					checkPhysicalUrlAccess: false))
					);

		}

		public static IReflectionManager GetReflectionManager() {
			return new ReflectionManager();
		}

		public static IDataConfigurator<TConnection> GetDataConfigurator<TConnection>(IConnectionCredential credential)
		where TConnection : DbConnection {

			return new DataConfigurator<TConnection>(credential);

		}


		public static TItem SessionItem<TItem>(String key, Func<TItem> generator = null)
		where TItem : class {

			if (HttpContext.Current != null) {
				var item = HttpContext.Current.Session[key] as TItem;
				var response = generator != null ? item ?? generator() : item;
				HttpContext.Current.Session[key] = response;
				return item;
			} else {
				return null;
			}

		}
	}
}
