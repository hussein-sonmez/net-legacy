﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public abstract class Singularity<TSingular> : ISingularity<TSingular>
	where TSingular : ISingularity<TSingular> {

		#region ISingularity Members

		private IReflectionManager reflectionManager = SEnterprise.GetReflectionManager();

		public IReflectionManager ReflectionManager {
			get {
				return this.reflectionManager;
			}
		}

		public TResponse ExecuteByReflection<TResponse>(string methodName, Type[] genericTypes, params object[] parameters)
		where TResponse : class {

			return this.ReflectionManager.ExecuteMethod(Me, methodName, genericTypes, parameters) as TResponse;

		}

		public Object ExecuteByReflection(string methodName, Type[] genericTypes, params object[] parameters) {

			return this.ExecuteByReflection<Object>(methodName, genericTypes, parameters);

		}

		public abstract TSingular Me { get; }
		#endregion

	}
}
