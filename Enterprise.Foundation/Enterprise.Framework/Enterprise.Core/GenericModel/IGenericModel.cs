﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {

	public interface IGenericModel<TModel>
	where TModel : class, IGenericModel<TModel>, new() {
		
		#region Methods

		IGenericModel<TModel> Populate(Action<TModel> enzyme);

		TModel Seed<TConnection>(IDataStore<TConnection> dataStore)
			where TConnection : DbConnection;

		TStrongModel As<TStrongModel>()
			where TStrongModel : class, IGenericModel<TModel>;

		IGenericModel<TRelatedModel> RelatedModel<TRelatedModel>()
			where TRelatedModel : class, IGenericModel<TRelatedModel>, new();
		#endregion

	}
}
