﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Routing;
using Enterprise.Core.Decorators;

namespace Enterprise.Core {
	public abstract class GenericModel<TModel> : IGenericModel<TModel>
	 where TModel : class, IGenericModel<TModel>, new() {

		[Identity, Constraint(EConstraintType.PrimaryKey)]
		[Column("ID", EDataType.Integer)]
		public Int32 ID { get; set; }

		#region IGenericModel<TModel> Members

		public IGenericModel<TModel> Populate(Action<TModel> enzyme) {

			enzyme((TModel)(this as IGenericModel<TModel>));
			return this;

		}

		public IGenericModel<TRelatedModel> RelatedModel<TRelatedModel>()
			where TRelatedModel : class, IGenericModel<TRelatedModel>, new() {

			var typeParser = SEnterprise.GetTypeParser();
			var relatedConstraint = typeParser.GetCustomPropertyAttribute<TModel, ConstraintAttribute>()
				.SingleOrDefault(constraint => constraint.ConstraintType == EConstraintType.ForeignKey && constraint.ReferencingType.Equals(typeof(TRelatedModel)));
			if (relatedConstraint != null) {
				return Activator.CreateInstance(relatedConstraint.ReferencingType) as IGenericModel<TRelatedModel>;
			} else {
				throw new ArgumentException(String.Format("Type<{0}> is needed to have a foreign key relation to Type<{1}>"
					, typeof(TModel).Name, typeof(TRelatedModel).Name));
			}
		}

		public TStrongModel As<TStrongModel>()
			where TStrongModel : class, IGenericModel<TModel> {

			return (this as TStrongModel);

		}

		public abstract TModel Seed<TConnection>(IDataStore<TConnection> dataStore)
			where TConnection : DbConnection;

		#endregion

		#region Protected Members

		protected static ILexerService LexerServices = SEnterprise.GetImplementor<ILexerService>().Construct();
		protected static ICryptoService CryptoServices = SEnterprise.GetImplementor<ICryptoService>().Construct();

		#endregion

	}
}
