﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface IConnectionService<TConnection> : IService<TConnection>
		where TConnection : DbConnection {

		#region Properties

		IConnectionCredential Credential { get; }

		#endregion

		#region Methods

		IConfig<TConnection> Construct();

		#endregion

	}
}
