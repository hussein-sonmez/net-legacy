﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {

	public interface IStatementService<TModel> : IService<TModel> {

		#region Methods

		SqlCommand GetCommand();
		IList<IDataColumn<TModel>> ListColumns();

		#endregion

	}
}
