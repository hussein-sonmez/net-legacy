﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface IDataParameter<TParameter> : IService<TParameter> {

		#region Properties

		IConfig<String> Title { get; }

		#endregion

	}
}
