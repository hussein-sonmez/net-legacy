﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface IDataConfigurator<TConnection>
		where TConnection : DbConnection {

		#region Methods

		IDataConfigurator<TConnection> RegisterModel<TModel>()
			where TModel : class, IGenericModel<TModel>, new();

		#endregion

		#region Properties

		IDataStore<TConnection> DataStore { get; }

		#endregion
	}
}
