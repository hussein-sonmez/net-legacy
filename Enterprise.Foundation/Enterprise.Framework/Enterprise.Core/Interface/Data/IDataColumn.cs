﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core.Decorators;

namespace Enterprise.Core {
	public interface IDataColumn<TModel> {

		IService<String> Title { get; }
		IService<EDataType> DataType { get; }
		IService<Boolean> AllowNull { get; }
		IService<Int32> UpperLimit { get; }
		IService<Int32> LowerLimit { get; }
		Boolean IsIdentityColumn { get; }

		IEnumerable<ConstraintAttribute> DataConstraints { get; }
		IService<ShallRenderAttribute> ShallRenderInfo { get; }
	}
}
