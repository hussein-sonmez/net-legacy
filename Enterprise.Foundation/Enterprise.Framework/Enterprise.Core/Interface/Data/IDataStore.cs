﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core.Decorators;

namespace Enterprise.Core {
	public interface IDataStore<TConnection> : ISingularity<IDataStore<TConnection>>
		where TConnection : DbConnection {

		#region Method

		IDataStore<TConnection> SeedDatabase();

		IDataStore<TConnection> RebuildDatabase();

		IDataStore<TConnection> RegisterDataClass<TModel>() where TModel : new();

		IDataStore<TConnection> ExecuteDDL(EDDLType ddlType);

		TModel ExecuteDML<TModel>(TModel guideModel, EDMLType dmlType) where TModel : class, new();

		IEnumerable<IModelService<TModel>> ExecuteSelect<TModel>(dynamic guideModel) where TModel : class, new();

		IEnumerable<KeyValuePair<Type, dynamic>> GetRegisteredDataClasses();

		IConnectionService<TConnection> ConnectionService(IConnectionCredential credential);

		#endregion

	}
}
