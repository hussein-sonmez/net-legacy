﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Enterprise.Core {
	public interface IModelService<TModel> : IService<TModel>
	where TModel : new() {

		#region Methods

		IStatementService<TModel> DdlStatementService(EDDLType ddlType);
		IStatementService<TModel> DmlStatementService(EDMLType dmlType);
		IEnumerable<IModelService<TModel>> DigestReader(DbDataReader reader);

		#endregion

		#region Properties

		IStatementService<TModel> SelectStatementService { get; }
		
		#endregion

	}
}
