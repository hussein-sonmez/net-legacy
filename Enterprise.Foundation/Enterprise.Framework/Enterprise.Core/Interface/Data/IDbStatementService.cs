﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {


	public interface IDbStatementService<TCommand>
	where TCommand : DbCommand {

		#region Methods

		TCommand GetDbCommand(IConnectionCredential credential);

		#endregion

	}

}
