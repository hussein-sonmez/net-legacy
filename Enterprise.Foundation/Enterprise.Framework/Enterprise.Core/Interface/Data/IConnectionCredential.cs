﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface IConnectionCredential {
		
		#region Properties

		IService<IPAddress> ServerIP { get; }
		IService<String> DataSource { get; }
		IService<String> LocalDbName { get; }
		IService<String> DatabaseName { get; }
		IService<String> UserName { get; }
		IService<String> Password { get; }
		IService<ESecurityMode> SecurityMode { get; }
		EProviderType ProviderType { get; }

		#endregion

		#region Methods

		String GetConnectionString();
		TConnection GetConnection<TConnection>() where TConnection : DbConnection;

		#endregion

	}
}
