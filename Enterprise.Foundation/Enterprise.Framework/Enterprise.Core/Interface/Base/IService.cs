﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface IService<TItem> : IConfig<TItem> {

		#region Members

		IService<TItem> Set(TItem item);
		TResponse As<TResponse>()
			where TResponse : class, IService<TItem>;

		#endregion

		#region Methods

		IConfig<TResponse> Translate<TResponse>(Func<TItem, TResponse> engine);

		#endregion
	}
}
