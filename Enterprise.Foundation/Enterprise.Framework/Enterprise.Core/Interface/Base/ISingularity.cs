﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface ISingularity<TSingular>
	where TSingular : ISingularity<TSingular> {

		IReflectionManager ReflectionManager { get; }
		TSingular Me { get; }
		
		TResponse ExecuteByReflection<TResponse>(string methodName, Type[] genericTypes, params object[] parameters)
			where TResponse : class;
		Object ExecuteByReflection(string methodName, Type[] genericTypes, params object[] parameters);


	}
}
