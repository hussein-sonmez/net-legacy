﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Enterprise.Core {
	public interface ITransformerService {

		dynamic GenerateFormData(ITypeParser typeParser, Object sourceObject);
		dynamic GenerateMutationList(ITypeParser typeParserService, Type sourceType, IEnumerable<dynamic> list);
	}
}
