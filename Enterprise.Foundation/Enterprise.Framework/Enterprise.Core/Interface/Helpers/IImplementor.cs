﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface IImplementor<TInterface> {

		TInterface Construct(params object[] args);
		TResponse Construct<TResponse>(params object[] args)
			where TResponse : class;

	}
}
