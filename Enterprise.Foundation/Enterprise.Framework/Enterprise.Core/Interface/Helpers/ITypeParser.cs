﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface ITypeParser : ISingularity<ITypeParser> {

		IEnumerable<dynamic> AnalyzeParameters<TItem>(TItem item);

		dynamic AnalyzeEnumValues<TEnum>();

		Object GetValueOf(Object item, String property, object[] index = null);

		IEnumerable<dynamic> SelectParameters<TItem>(dynamic item);

		IEnumerable<dynamic> FilterParameters<TItem, TKeyAttribute, TValueAttribute>(Func<TKeyAttribute, TValueAttribute, Boolean> query, Func<TKeyAttribute, TValueAttribute, dynamic> selector)
			where TKeyAttribute : Attribute
			where TValueAttribute : Attribute;

		Object Assign(Object instance, Object value, String propertyName);

		IEnumerable<TAttribute> GetCustomPropertyAttribute<TItem, TAttribute>()
			where TAttribute : System.Attribute;

		IEnumerable<KeyValuePair<TAttribute1, TAttribute2>> GetCustomPropertyAttributePair<TItem, TAttribute1, TAttribute2>(TItem to)
			where TAttribute1 : System.Attribute
			where TAttribute2 : System.Attribute;

		String TakeDescription<TEnum>(Object value);

		TItem ParseType<TItem>(Object instance);
		Int32 ParseCode(Object instance);

		Boolean IsNumberType(dynamic item);
		Boolean IsUnassigned(dynamic item);

		TAttribute GetCustomClassAttribute<TItem, TAttribute>() where TAttribute : Attribute;

		void Copy<TItem>(TItem source, TItem destination)
			where TItem : class, new();

		dynamic DynamicCopy(dynamic source, String entityModelAssembly, String entityModelNamespace, string typeIdentifier);

		void HollowMembers(dynamic source, dynamic destination);
	}
}
