﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core  {
	public interface IDefinitions {

		#region Methods

		String ConnectionStringFormats(EProviderType providerType, ESecurityMode securityMode);
		IConnectionCredential GetCredential();
		String GetLoginCommandString(IConnectionCredential credential);

		#endregion

		#region Properties

		String VirtualDirectory { get; }

		String Virtualize(string path);

		#endregion

	}
}
