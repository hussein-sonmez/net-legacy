﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {

	public static class EnumerableExtensions {

		public static IEnumerable<T> AppendElement<T>(this IEnumerable<T> array, T element) {

			var list = new List<T>(array);
			list.Add(element);
			return list.ToArray();

		}

	}

}
