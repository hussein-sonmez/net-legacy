﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core.Decorators {

	[AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
	public sealed class IdentityAttribute : Attribute {

		public IdentityAttribute(EIdentityMode generationType = EIdentityMode.Int32Generation) {
			this.Start = 1;
			this.Seed = 1;
		}

		#region Overrides

		public override string ToString() {

			return String.Format("IDENTITY({0},{1})", this.Start, this.Seed);

		}

		#endregion

		#region Named Arguments

		public Int32 Start { get; set; }
		public Int32 Seed { get; set; }

		#endregion

	}

}
