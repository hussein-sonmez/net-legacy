﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core.Decorators {

	[AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
	public sealed class ConstraintAttribute : Attribute {

		#region Attribute Specs

		readonly EConstraintType constraintType;

		public ConstraintAttribute(EConstraintType constraintType) {

			this.constraintType = constraintType;

		}

		public EConstraintType ConstraintType {
			get { return this.constraintType; }
		}


		public Type ReferencingType { get; set; }
		public String ReferencingColumn { get; set; }
		public Object DefaultValue { get; set; }
		public String CheckStatement { get; set; }

		#endregion

		#region Overrides

		public override string ToString() {

			var repr = "";
			switch (this.constraintType) {
				case EConstraintType.PrimaryKey:
					repr = "PRIMARY KEY";
					break;
				case EConstraintType.ForeignKey:
					var referencingMapping = this.ReferencingType.GetCustomAttributes(typeof(TableMappingAttribute), false).First() as TableMappingAttribute;
					repr = String.Format("REFERENCES {0}({1}) ON DELETE CASCADE", referencingMapping.TableName, this.ReferencingColumn);
					break;
				case EConstraintType.Default:
					repr = String.Format("DEFAULT {0}", DefaultValue.ToString());
					break;
				case EConstraintType.Unique:
					repr = "UNIQUE";
					break;
				case EConstraintType.Check:
					repr = String.Format("CHECK {0}", CheckStatement);
					break;
				default:
					break;
			}
			return repr;

		}

		#endregion

	}
}
