﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class MarkAsBaseAttribute : Attribute {

        readonly Type _BaseFor;

        public MarkAsBaseAttribute(Type baseFor) {

            this._BaseFor = baseFor;

        }

        public Type BaseFor {
            get { return this._BaseFor; }
        }

    }
}
