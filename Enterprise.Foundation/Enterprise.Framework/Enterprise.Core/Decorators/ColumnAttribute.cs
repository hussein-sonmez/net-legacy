﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core.Decorators {

	[AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
	public sealed class ColumnAttribute : Attribute {

		#region Attribute Specs

		readonly String title;
		readonly EDataType dataType;

		public ColumnAttribute(String Title, EDataType DataType) {
			this.title = Title;
			this.dataType = DataType;
		}

		public String Title {
			get { return this.title; }
		}

		public EDataType DataType {
			get { return this.dataType; }
		}

		// These are named arguments
		public Int32 UpperLimit { get; set; }
		public Int32 LowerLimit { get; set; }

		#endregion

	}

}
