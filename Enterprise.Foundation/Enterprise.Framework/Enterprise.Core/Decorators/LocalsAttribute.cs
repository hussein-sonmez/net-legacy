﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core.Decorators {
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum, Inherited = false, AllowMultiple = true)]
	public sealed class LocalsAttribute : Attribute {

		readonly Type language;

		public LocalsAttribute(Type language) {
			this.language = language;
		}

		public Type Language {
			get { return language; }
		}

		public String Title { get; set; }

	}
}
