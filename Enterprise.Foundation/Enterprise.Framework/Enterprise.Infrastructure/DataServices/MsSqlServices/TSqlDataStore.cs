﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal class TSqlDataStore : DataStore<SqlConnection>, ITSqlDataStore {

		#region Ctor

		public TSqlDataStore(IConnectionCredential credential)
			: base(credential) {

		}

		#endregion

	}
}
