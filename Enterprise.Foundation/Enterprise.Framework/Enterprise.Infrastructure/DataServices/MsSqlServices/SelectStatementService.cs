﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal class SelectStatementService<TModel> : StatementService<TModel>, IStatementService<TModel> {

		#region Methods

		public override SqlCommand GetCommand() {

			var dbParameters = new List<SqlParameter>();
			var tableName = GetTableName();

			var selectList = new StringBuilder();
			var columns = base.ListColumns();

			columns.ToList().ForEach(dc => selectList.AppendFormat("{0}.[{1}], ", tableName, dc.Title.Item));

			var typeParser = SEnterprise.GetTypeParser();
			var whereList = new StringBuilder("1=1");

			var parameters = typeParser.SelectParameters<TModel>(this.Item);

			parameters = parameters.Select(val => {
				var isEnum = val.Column != null && val.Column.DataType == EDataType.Enumeration;
				dynamic t = new ExpandoObject();
				t.Key = val.Key;
				t.Value = isEnum ? typeParser.ParseType<Int32>(val.Value) : val.Value;
				t.Column = val.Column;
				dbParameters.Add(new SqlParameter(t.Key, t.Value));
				return t;
			});

			parameters.ToList().ForEach(prm => whereList.AppendFormat(" AND {0}.[{1}]=@{1} "
				, tableName, prm.Key));

			var commandText = String.Format("SELECT {0} FROM [{1}] WHERE ({2});"
				, selectList.ToString().TrimEnd(' ', ','), tableName, whereList.ToString().TrimEnd(' ', ','));
			var command = new SqlCommand(commandText);
			command.Parameters.AddRange(dbParameters.ToArray());
			return command;
		}

		#endregion

	}
}
