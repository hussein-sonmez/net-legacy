﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal class DeleteStetementService<TModel> : StatementService<TModel>, IStatementService<TModel> {

		#region Overrides

		public override SqlCommand GetCommand() {
			
			var tableName = GetTableName();
			var dbParameters = new List<SqlParameter>();

			var whereColumnsList = new StringBuilder("1=1");
			var columns = base.ListColumns();
			
			var typeParser = SEnterprise.GetTypeParser();
			var parameters = typeParser.AnalyzeParameters<TModel>(this.Item);

			var identityColumns = columns.Where(col => col.DataConstraints.Any(c => c.ConstraintType == EConstraintType.PrimaryKey));

			identityColumns.ToList().ForEach(col => {
				var parameter = parameters.Single(p => p.Key == col.Title.Item);
				whereColumnsList.AppendFormat(" AND [{0}]=@{0} ", col.Title.Item);
				dbParameters.Add(new SqlParameter(parameter.Key, parameter.Value));
			});

			var commandText = String.Format("DELETE FROM {0} WHERE ({1});", tableName
				, whereColumnsList.ToString().TrimEnd(' '));
			var command = new SqlCommand(commandText);
			command.Parameters.AddRange(dbParameters.ToArray());
			return command;

		}

		#endregion

	}
}
