﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;
using Enterprise.Core.Decorators;

namespace Enterprise.Infrastructure {
	internal class CreateTableService<TModel> : StatementService<TModel>, IStatementService<TModel> {

		#region Overrides

		public override SqlCommand GetCommand() {

			var tableFormat = @"CREATE TABLE {0} ({1});"; ;
			var commandText = String.Format(tableFormat, GetTableName()
				, base.ListColumns().Aggregate<IDataColumn<TModel>, String>("", (repr, col) =>
					String.Format("{0}{2} {1}", repr, col.ToString(), String.IsNullOrEmpty(repr) ? "" : ",")));
			var command = new SqlCommand(commandText);
			return command;

		}

		#endregion

	}
}
