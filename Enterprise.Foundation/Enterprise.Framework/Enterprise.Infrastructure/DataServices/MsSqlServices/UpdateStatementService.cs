﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal class UpdateStatementService<TModel> : StatementService<TModel>, IStatementService<TModel> {

		#region Overrides

		public override SqlCommand GetCommand() {

			var typeParser = SEnterprise.GetTypeParser();
			var tableName = GetTableName();
			var whereColumnsList = new StringBuilder("1=1");
			var setValuesList = new StringBuilder();
			var parameters = typeParser.AnalyzeParameters<TModel>(this.Item);
			var columns = base.ListColumns().Where(col => parameters.Single(p => p.Key == col.Title.Item).Value != null);
			var dbParameters = new List<SqlParameter>();

			var identityColumns = columns.Where(col => col.DataConstraints.Any(c => c.ConstraintType == EConstraintType.PrimaryKey));
			var identityStrippedColumns = parameters.Where(val => !identityColumns.Any(icol => icol.Title.Item == val.Key));

			identityColumns.ToList().ForEach(col => {
				var key = col.Title.Item;
				var param = parameters.Single(p => p.Key == col.Title.Item);
				whereColumnsList.AppendFormat(" AND [{0}]=@{1} ", key, param.Key);
				dbParameters.Add(new SqlParameter(key, param.Value));
			});

			identityStrippedColumns.ToList().ForEach(val => {
				setValuesList.AppendFormat(" [{0}]=@{0}, ", val.Key);
				dbParameters.Add(new SqlParameter(val.Key, val.Value));
			});

			var commandText = String.Format("UPDATE {0} SET {1} WHERE ({2});", tableName
				, setValuesList.ToString().TrimEnd(' ', ','), whereColumnsList.ToString().TrimEnd(' '));
			var command = new SqlCommand(commandText);
			command.Parameters.AddRange(dbParameters.ToArray());
			return command;
		}

		#endregion

	}
}
