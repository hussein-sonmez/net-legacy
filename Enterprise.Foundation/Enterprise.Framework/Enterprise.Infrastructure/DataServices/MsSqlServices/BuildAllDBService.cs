﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal class BuildAllDBService : DbStatementService<SqlCommand> {

		public override SqlCommand GetDbCommand(IConnectionCredential credential) {

			var definitions = SEnterprise.GetDefinitions();
			var databaseName = credential.DatabaseName.Item;
			var login = credential.UserName.Item;
			var password = credential.Password.Item;

			switch (credential.ProviderType) {
				case EProviderType.MsSql:
					return new SqlCommand(definitions.GetLoginCommandString(credential));
				case EProviderType.MySql:
					throw new NotImplementedException("EProviderType.MySql");
				case EProviderType.PostgreSql:
					throw new NotImplementedException("EProviderType.PostgreSql");
				default:
					throw new NotImplementedException("default");
			}

		}

	}
}
