﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal class DropCreateTableService<TModel> : StatementService<TModel>, IStatementService<TModel> {

		#region Overrides

		public override SqlCommand GetCommand() {

			var tableFormat = @"IF OBJECT_ID('{0}', 'U') IS NOT NULL DROP TABLE {0};";
			var commandText = String.Format(tableFormat, GetTableName());
			var command = new SqlCommand(commandText);
			return command;

		}

		#endregion

	}
}
