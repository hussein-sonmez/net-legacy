﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal class TSqlModelService<TModel> : Service<TModel>, ITSqlModelService<TModel>
	where TModel : new() {

		#region ITSqlModelService<TModel> Members

		public IStatementService<TModel> DdlStatementService(EDDLType ddlType) {

			switch (ddlType) {
				case EDDLType.Create:
					return new CreateTableService<TModel>();
				case EDDLType.Drop:
					return new DropTableService<TModel>();
				case EDDLType.Alter:
					throw new NotImplementedException();
				case EDDLType.DropCreate:
					return new DropCreateTableService<TModel>();
				default:
					throw new NotImplementedException();
			}

		}
		public IStatementService<TModel> DmlStatementService(EDMLType dmlType) {

			switch (dmlType) {
				case EDMLType.Insert:
					return new InsertStatementService<TModel>().Set(this.Item).As<IStatementService<TModel>>();
				case EDMLType.Update:
					return new UpdateStatementService<TModel>().Set(this.Item).As<IStatementService<TModel>>();
				case EDMLType.Delete:
					return new DeleteStetementService<TModel>().Set(this.Item).As<IStatementService<TModel>>();
				default:
					throw new NotImplementedException();
			}

		}
		public IEnumerable<IModelService<TModel>> DigestReader(DbDataReader reader) {

			var typeParser = SEnterprise.GetTypeParser();
			var models = new List<IModelService<TModel>>();
			while (reader.Read()) {
				var model = new TModel();
				var keys = SelectStatementService.ListColumns()
					.Where(col => reader[col.Title.Item].ToString() != "" || !col.AllowNull.Item)
					.Select<IDataColumn<TModel>, String>(dc => dc.Title.Item);
				keys.ToList().ForEach(key => model.GetType().GetProperty(key).SetValue(model, typeParser.ExecuteByReflection("ParseType"
					, new Type[] { model.GetType().GetProperty(key).PropertyType }, reader[key])));
				models.Add(new TSqlModelService<TModel>().Set(model).As<IModelService<TModel>>());
			}
			return models.ToArray();
		}

		#endregion

		#region Overrides

		public override IConfig<TModel> Me {
			get {
				return this;
			}
		}

		#endregion

		#region IModelService<TModel> Members


		public IStatementService<TModel> SelectStatementService {
			get {
				return new SelectStatementService<TModel>().Set(this.Item).As<IStatementService<TModel>>();
			}
		}

		#endregion
	}
}
