﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal abstract class ConnectionService<TConnection> : Service<TConnection>, IConnectionService<TConnection>
		where TConnection : DbConnection {

		#region Ctor

		public ConnectionService(IConnectionCredential credential) {

			this.credential = credential;

		}

		#endregion

		#region Private

		private IConnectionCredential credential;

		#endregion

		#region IConnectionCredential Members

		public IConfig<TConnection> Construct() {

			var connStr = credential.GetConnectionString();
			switch (credential.ProviderType) {
				case EProviderType.MsSql:
					this.Set(new SqlConnection(connStr) as TConnection)
						.Translate<TConnection>(conn => (TConnection)Convert.ChangeType(conn, typeof(TConnection)));
					break;
				case EProviderType.MySql:
					throw new NotImplementedException("EProviderType.MySql");
				case EProviderType.PostgreSql:
					throw new NotImplementedException("EProviderType.PostgreSql");
				default:
					throw new NotImplementedException("default");
			}
			return this;
		}

		public IConnectionCredential Credential {
			get { return this.credential; }
		}

		#endregion

	}
}
