﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;
using Enterprise.Core.Decorators;

namespace Enterprise.Infrastructure {
	internal class Column<TModel> : IDataColumn<TModel> {

		#region Ctor

		public Column(IEnumerable<Attribute> attrs) {

			this.title = new Service<String>();
			this.dataType = new Service<EDataType>();
			this.upperLimit = new Service<Int32>();
			this.lowerLimit = new Service<Int32>();
			this.allowNull = new Service<Boolean>();
			this.identityColumn = new Service<IdentityAttribute>();
			this.shallRenderInfo = new Service<ShallRenderAttribute>();

			var column = attrs.OfType<ColumnAttribute>().First();
			this.Title.Set(column.Title);
			this.DataType.Set(column.DataType);
			this.UpperLimit.Set(column.UpperLimit);
			this.LowerLimit.Set(column.LowerLimit);
			this.AllowNull.Set(attrs.OfType<AllowNullAttribute>().Count() == 1);
			
			this.identityColumn.Set(attrs.OfType<IdentityAttribute>().FirstOrDefault());
			this.shallRenderInfo.Set(attrs.OfType<ShallRenderAttribute>().FirstOrDefault());
			
			this.dataConstraints = attrs.OfType<ConstraintAttribute>();
		}

		#endregion

		#region Private

		private IService<String> title;
		private IService<EDataType> dataType;
		private IService<Int32> upperLimit;
		private IService<Int32> lowerLimit;
		private IService<Boolean> allowNull;
		private IService<IdentityAttribute> identityColumn;
		private IEnumerable<ConstraintAttribute> dataConstraints;
		private IService<ShallRenderAttribute> shallRenderInfo;

		#endregion

		#region IDataColumn Members

		public IService<String> Title {
			get { return this.title; }
		}

		public IService<EDataType> DataType {
			get { return this.dataType; }
		}

		public IService<Int32> UpperLimit {
			get { return this.upperLimit; }
		}

		public IService<Int32> LowerLimit {
			get { return this.lowerLimit; }
		}

		public IService<Boolean> AllowNull {
			get { return this.allowNull; }
		}

		public Boolean IsIdentityColumn {
			get { return this.identityColumn.Item != null; }
		}

		public IEnumerable<ConstraintAttribute> DataConstraints {
			get {
				return this.dataConstraints;
			}
		}

		public IService<ShallRenderAttribute> ShallRenderInfo {
			get { return this.shallRenderInfo; }
		}

		#endregion

		#region Overrides

		public override String ToString() {
			var typeParser = SEnterprise.GetTypeParser();
			return String.Format(@"[{0}] [{1}]{2} {3} {4} {5}",
				this.Title.Item
				, typeParser.TakeDescription<EDataType>(this.DataType.Item)
				, (this.DataType.Item == EDataType.NVarChar
					|| this.DataType.Item == EDataType.VarChar)
				? (this.UpperLimit.Item != 0 ? "(" + this.UpperLimit.Item + ")" : "(MAX)") : ""
				, this.IsIdentityColumn ? this.identityColumn.Item.ToString() : ""
				, this.AllowNull.Item ? "NULL" : "NOT NULL"
				, this.DataConstraints != null
				? this.DataConstraints.Aggregate<ConstraintAttribute, String>("", (prev, next) => prev + " " + next.ToString())
				: "");
		}

		#endregion

	}
}
