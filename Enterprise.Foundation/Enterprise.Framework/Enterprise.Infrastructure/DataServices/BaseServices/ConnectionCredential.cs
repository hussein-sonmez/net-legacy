﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal class ConnectionCredential : IConnectionCredential {

		#region Ctor

		public ConnectionCredential(EProviderType providerType) {

			this.providerType = providerType;
			this.serverIP = new Service<IPAddress>();
			this.dataSource = new Service<String>();
			this.localDbName = new Service<String>();
			this.databaseName = new Service<String>((name) => name ?? "master");
			this.userName = new Service<String>();
			this.password = new Service<String>();
			this.securityMode = new Service<ESecurityMode>();

		}

		#endregion

		#region Fields
		private IService<IPAddress> serverIP;
		private IService<String> dataSource;
		private IService<String> localDbName;
		private IService<String> databaseName;
		private IService<String> userName;
		private IService<String> password;
		private IService<ESecurityMode> securityMode;
		private EProviderType providerType;
		#endregion

		#region IDataConnection Members

		public EProviderType ProviderType {
			get { return this.providerType; }
		}

		public IService<IPAddress> ServerIP {
			get { return this.serverIP; }
		}

		public IService<String> DataSource {
			get { return this.dataSource; }
		}

		public IService<String> LocalDbName {
			get { return this.localDbName; }
		}

		public IService<String> DatabaseName {
			get { return this.databaseName; }
		}

		public IService<String> UserName {
			get { return this.userName; }
		}

		public IService<String> Password {
			get { return this.password; }
		}

		public IService<ESecurityMode> SecurityMode {
			get { return this.securityMode; }
		}

		public String GetConnectionString() {

			var connectionSentenceFormat = SEnterprise.GetDefinitions().ConnectionStringFormats(this.providerType, this.securityMode.Item);

			return SEnterprise.GetSentenceParser().Construct(connectionSentenceFormat)
				.From(this.ServerIP.Item == null ? (this.DataSource.Item ?? this.localDbName.Item) : this.ServerIP.Translate<String>(ip => ip.ToString()).Item,
				this.UserName.Item,
				this.Password.Item,
				this.DatabaseName.Item);

		}
		public TConnection GetConnection<TConnection>() where TConnection : DbConnection {

			if (typeof(TConnection) == typeof(SqlConnection)) {
				return new SqlConnection(this.GetConnectionString()) as TConnection;
			} else {
				throw new NotImplementedException(typeof(TConnection).Name);
			}

		}

		#endregion
	}
}
