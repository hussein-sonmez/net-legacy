﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal abstract class DbStatementService<TCommand> : IDbStatementService<TCommand>
		where TCommand : DbCommand {

		#region IDbStatementService<TCommand> Members

		public abstract TCommand GetDbCommand(IConnectionCredential credential);

		#endregion

	}
}
