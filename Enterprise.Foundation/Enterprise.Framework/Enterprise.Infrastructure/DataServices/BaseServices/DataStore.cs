﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;
using Enterprise.Core.Decorators;

namespace Enterprise.Infrastructure {
	internal class DataStore<TConnection> : Singularity<IDataStore<TConnection>>, IDataStore<TConnection>
	where TConnection : DbConnection {

		#region Ctor

		public DataStore(IConnectionCredential credential) {

			this.registeredServices = new List<KeyValuePair<Type, dynamic>>();
			this.sortedServicesForDrop = new List<KeyValuePair<Type, dynamic>>();
			this.credential = credential;
		}

		#endregion

		#region Private

		private List<KeyValuePair<Type, dynamic>> registeredServices;
		private List<KeyValuePair<Type, dynamic>> sortedServicesForDrop;
		private IConnectionCredential credential;

		private void RecursiveConstraintSearch(Type modelType, ref List<Type> list) {

			var typeParser = SEnterprise.GetTypeParser();

			var constraints = typeParser.ExecuteByReflection<IEnumerable<ConstraintAttribute>>(
					"GetCustomPropertyAttribute"
					, new Type[] { modelType, typeof(ConstraintAttribute) }).Where(ct => ct.ConstraintType == EConstraintType.ForeignKey);
			foreach (var constraint in constraints) {
				RecursiveConstraintSearch(constraint.ReferencingType, ref list);
				if (list.Count(t => t.Equals(constraint.ReferencingType)) == 0) {
					list.Add(constraint.ReferencingType);
				}
			}
			if (list.Count(t => t.Equals(modelType)) == 0) {
				list.Add(modelType);
			}
		}

		private IEnumerable<Type> EvaluateSortedServicesForInsert() {

			var list = new List<Type>();
			registeredServices.ToList().ForEach(kv => RecursiveConstraintSearch(kv.Key, ref list));
			return list.ToArray();

		}

		private IEnumerable<SqlCommand> GenerateCommands(EDDLType ddlType) {

			var insertList = this.EvaluateSortedServicesForInsert();

			var services = (from t in insertList
							join rs in registeredServices on t.FullName equals rs.Key.FullName
							select rs.Value.GetType().GetMethod("DdlStatementService")
							 .Invoke(rs.Value, new Object[] { ddlType }));
			var statements = (from ss in services
							  select ss.GetType().GetMethod("GetCommand")
								  .Invoke(ss, null))
						  .Cast<SqlCommand>().ToArray();
			return statements;

		}

		#endregion

		#region Overrides

		public override IDataStore<TConnection> Me {
			get { return this; }
		}

		#endregion

		#region IDataStore<TConnection> Members

		public IDataStore<TConnection> RebuildDatabase() {

			var requiredCredential = SEnterprise.GetDefinitions().GetCredential();

			var builder = new BuildAllDBService();
			var command = builder.GetDbCommand(this.credential);
			command.Connection = this.ConnectionService(requiredCredential).Construct().Item as SqlConnection;
			command.Connection.Open();
			try {
				command.ExecuteNonQuery();
			} catch {

			} finally {
				command.Connection.Close();
			}
			return this;

		}


		public IDataStore<TConnection> SeedDatabase() {

			var typeParser = SEnterprise.GetTypeParser();
			var modelList = this.EvaluateSortedServicesForInsert().Reverse();
			var connectionType = new Type[] { this.ConnectionService(this.credential).Construct().Item.GetType() };
			modelList.ToList().ForEach(model =>
				typeParser.ReflectionManager.ExecuteMethod(Activator.CreateInstance(model), "Seed", connectionType, this));
			return this;

		}

		public IDataStore<TConnection> RegisterDataClass<TModel>()
		where TModel : new() {

			var tSqlService = SEnterprise.GetImplementor<ITSqlModelService<TModel>>().Construct();
			registeredServices.Add(new KeyValuePair<Type, dynamic>(typeof(TModel), tSqlService));
			return this;

		}

		public IEnumerable<KeyValuePair<Type, dynamic>> GetRegisteredDataClasses() {

			return this.registeredServices
				.Select(kv => new KeyValuePair<Type, dynamic>(kv.Key, Activator.CreateInstance(kv.Key)));

		}

		public IDataStore<TConnection> ExecuteDDL(EDDLType ddlType) {

			IConfig<TConnection> connectionConfig = default(IConfig<TConnection>);
			var statements = GenerateCommands(ddlType).ToArray();

			if (ddlType == EDDLType.DropCreate) {
				statements = statements.Reverse().ToArray();
			}
			connectionConfig = ConnectionService(this.credential).Construct();
			connectionConfig.Item.Open();

			for (int i = 0; i < statements.Length; i++) {
				statements[i].Connection = connectionConfig.Item as SqlConnection;
				statements[i].ExecuteNonQuery();
			}

			if (ddlType == EDDLType.DropCreate) {
				this.ExecuteDDL(EDDLType.Create);
			}
			connectionConfig.Item.Close();


			return this;

		}

		public TModel ExecuteDML<TModel>(TModel guideModel, EDMLType dmlType)
			where TModel : class, new() {

				var typeParser = SEnterprise.GetImplementor<ITypeParser>().Construct();

			var connectionConfig = ConnectionService(this.credential).Construct();
			var theModel = registeredServices.Single<KeyValuePair<Type, dynamic>>(rs => rs.Key.Equals(typeof(TModel))).Value as ITSqlModelService<TModel>;
			if (guideModel == null || guideModel.Equals(default(TModel))) {
				guideModel = Activator.CreateInstance(typeof(TModel)) as TModel;
			}
			IModelService<TModel> response;

			var statementService = theModel.Set(guideModel).As<ITSqlModelService<TModel>>().DmlStatementService(dmlType);
			var command = statementService.GetCommand();
			command.Connection = connectionConfig.Item as SqlConnection;
			command.Connection.Open();

			if (dmlType == EDMLType.Insert) {
				command.ExecuteNonQuery();
				var id = new SqlCommand("SELECT @@IDENTITY;", connectionConfig.Item as SqlConnection).ExecuteScalar().ToString();
				var idCol = statementService.ListColumns()
					.Single(col => col.DataConstraints.Any(dc => dc.ConstraintType == EConstraintType.PrimaryKey));
				var idInt32 = 0;
				typeParser.Assign(theModel.Item, Int32.TryParse(id.ToString(), out idInt32) ? idInt32 as Object : id.ToString()
					, idCol.Title.Item);
				response = theModel;
			} else if (dmlType == EDMLType.Update) {
				command.ExecuteNonQuery();
				var returnedModel = this.ExecuteSelect<TModel>(theModel.Item).FirstOrDefault();
				response = returnedModel;
			} else {
				command.ExecuteNonQuery();
				response = theModel;
			}
			command.Connection.Close();

			return response.Item;
		}
		public IEnumerable<IModelService<TModel>> ExecuteSelect<TModel>(dynamic guideModel) where TModel : class, new() {

			var connectionConfig = ConnectionService(this.credential).Construct();
			var theModel = registeredServices.Single<KeyValuePair<Type, dynamic>>(rs => rs.Key.Equals(typeof(TModel))).Value as ITSqlModelService<TModel>;
			if (guideModel == null || guideModel.Equals(default(TModel))) {
				throw new ArgumentNullException("guideModel");
			}

			IEnumerable<IModelService<TModel>> modelsFound;
			TModel m = new TModel();
			SEnterprise.GetTypeParser().HollowMembers(guideModel, m);

			var statementService = theModel.Set(m).As<ITSqlModelService<TModel>>().SelectStatementService;
			var command = statementService.GetCommand();
			command.Connection = connectionConfig.Item as SqlConnection;
			command.Connection.Open();

			var reader = command.ExecuteReader();
			modelsFound = theModel.DigestReader(reader);
			command.Connection.Close();
			return modelsFound;
		}
		public IConnectionService<TConnection> ConnectionService(IConnectionCredential credential) {
			switch (credential.ProviderType) {
				case EProviderType.MsSql:
					return (new TSqlConnectionService(credential) as IConnectionService<TConnection>);
				case EProviderType.MySql:
					return (new TSqlConnectionService(credential) as IConnectionService<TConnection>);
				case EProviderType.PostgreSql:
					return (new TSqlConnectionService(credential) as IConnectionService<TConnection>);
				default:
					return (new TSqlConnectionService(credential) as IConnectionService<TConnection>);
			}
		}


		#endregion
	}
}
