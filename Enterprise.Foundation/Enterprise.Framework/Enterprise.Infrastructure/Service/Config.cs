﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
    [MarkAsBase(typeof(IConfig<>))]
	internal class Config<TItem> : Singularity<IConfig<TItem>>, IConfig<TItem> {

		#region Fields

		protected TItem item;
		private Func<TItem, TItem> generator;

		#endregion

		#region Ctor

		public Config() {

		}

		public Config(TItem item) {

			this.item = item;

		}

		public Config(Func<TItem, TItem> generator) {

			this.generator = generator;

		}

		#endregion

		#region IConfig<TItem> Members

		public TItem Item {
			get {
				var response = this.generator != null ? (this.item = this.generator(this.item)): this.item;
				this.generator = null;
				return response;
			}
		}

		#endregion

		#region Overrides

		public override IConfig<TItem> Me {
			get {
				return this;
			}
		}

		public override string ToString() {
			
			return this.item.ToString();

		}

		#endregion

	}
}
