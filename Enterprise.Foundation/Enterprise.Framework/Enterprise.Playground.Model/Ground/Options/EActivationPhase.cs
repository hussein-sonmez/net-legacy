﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core.Decorators;
using Enterprise.Playground.Model.Localization.Titles;

namespace Enterprise.Playground.Model {
	[Locals(typeof(Titles), Title = "EActivationPhase")]
	public enum EActivationPhase {

		CreatedRecently = 1,

		MailInvitationSent,

		MailActivationCompleted,

		PurchaseInformationCompleted,

		CycleCompleted,

	}
}
