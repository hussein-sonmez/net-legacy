﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Enterprise.Core.Decorators;
using Enterprise.Playground.Model.Localization.Titles;



namespace Enterprise.Playground.Model {


	[Locals(typeof(Titles), Title = "EFeels")]
	public enum EFeels {

		Wonderful = 1,

		Excited,

		Georgeous,

		Fascinated,

		Proud,

		Great,

		HappiestPersonInTheWorld,

	}
}
