﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;
using Enterprise.Core.Decorators;
using Enterprise.Playground.Model.Localization.Titles;

namespace Enterprise.Playground.Model {

	[TableMapping("Achievements"), Locals(typeof(Titles))]
	public class Achievement : GenericModel<Achievement> {

		#region Properties

		[ShallRender, Column("How", EDataType.Enumeration)]
		public EHow How { get; set; }

		[ShallRender, Column("What", EDataType.Enumeration)]
		public EWhat What { get; set; }

		[ShallRender, Column("Where", EDataType.Enumeration)]
		public EWhere Where { get; set; }

		[ShallRender, Column("When", EDataType.Date)]
		public DateTime When { get; set; }

		[ShallRender, Column("Feels", EDataType.Enumeration)]
		public EFeels Feels { get; set; }

		#endregion

		#region Overrides

		public override Achievement Seed<TConnection>(IDataStore<TConnection> dataStore) {

			var that = this.Populate(ach => ach.How = EHow.Completing)
				.Populate(ach => ach.What = EWhat.NextLevel)
				.Populate(ach => ach.Where = EWhere.AtWork)
				.Populate(ach => ach.When = DateTime.Now)
				.Populate(ach => ach.Feels = EFeels.Georgeous).As<Achievement>();
			return dataStore.ExecuteDML<Achievement>(that, EDMLType.Insert);

		}

		#endregion
		
	}
}
