﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;
using Enterprise.Core.Decorators;
using Enterprise.Playground.Model.Localization.Titles;


namespace Enterprise.Playground.Model {

	[TableMapping("Gratitudes"), Locals(typeof(Titles))]
	public class Gratitude : GenericModel<Gratitude> {

		#region Properties

		[Column("IsPassive", EDataType.Boolean), ShallRender]
		public Boolean IsPassive { get; set; }

		#endregion

		#region Overrides

		public override Gratitude Seed<TConnection>(IDataStore<TConnection> dataStore) {

			var that = this.Populate(model=>model.IsPassive = false).As<Gratitude>();
			return dataStore.ExecuteDML<Gratitude>(that, EDMLType.Insert);
		}
	
		#endregion

	}
}
