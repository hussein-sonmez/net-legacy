﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;
using Enterprise.Core.Decorators;
using Enterprise.Playground.Model.Localization.Titles;


namespace Enterprise.Playground.Model {

	[TableMapping("Societies"), Locals(typeof(Titles))]
	public class Society : GenericModel<Society>{

		#region Properties

		[ShallRender, Column("PublicIdentifier", EDataType.VarChar, UpperLimit=255)]
		public String PublicIdentifier { get; set; }

		#endregion

		#region Overrides

		public override Society Seed<TConnection>(IDataStore<TConnection> dataStore) {

			var that = this.Populate(model => model.PublicIdentifier = LexerServices.GenerateIdentifier("Society-")).As<Society>();
			return dataStore.ExecuteDML<Society>(that, EDMLType.Insert);

		}

		#endregion


	}
}
