﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Enterprise.Core;
using Enterprise.Core.Decorators;
using Enterprise.Playground.Model.Localization.Titles;

namespace Enterprise.Playground.Model {

	[TableMapping("Credentials"), Locals(typeof(Titles))]
	public class Credential : GenericModel<Credential> {

		#region Properties

		[ShallRender, Column("Locator", EDataType.NVarChar)]
		public String  Locator { get; set; }

		[Column("Cipher", EDataType.VarChar)]
		public String Cipher { get; set; }

		[ShallRender, Column("ActivationPhase", EDataType.Enumeration)]
		public EActivationPhase ActivationPhase { get; set; }

		#endregion

		#region Overrides

		public override Credential Seed<TConnection>(IDataStore<TConnection> dataStore) {

			var that = this.Populate(model => model.Locator = LexerServices.GenerateIdentifier("Credential-"))
				.Populate(model => model.Cipher = CryptoServices.GetMD5String("1q2w3e4r5t"))
				.Populate(model => model.ActivationPhase = EActivationPhase.CreatedRecently).As<Credential>();
			return dataStore.ExecuteDML<Credential>(that, EDMLType.Insert);

		}

		#endregion

	}
}
