﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;
using Enterprise.Core.Decorators;
using Enterprise.Playground.Model.Localization.Titles;


namespace Enterprise.Playground.Model {

	[TableMapping("Greetings"), Locals(typeof(Titles))]
	public class Greeting : GenericModel<Greeting> {

		#region Reference Properties

		[Constraint(EConstraintType.ForeignKey, ReferencingType = typeof(Gratitude), ReferencingColumn = "ID")]
		[Column("GratitudeID", EDataType.Integer)]
		public Int32 GratitudeID { get; set; }
		
		[Constraint(EConstraintType.ForeignKey, ReferencingType = typeof(Achievement), ReferencingColumn = "ID")]
		[Column("AchievementID", EDataType.Integer)]
		public Int32 AchievementID { get; set; }
		
		[Constraint(EConstraintType.ForeignKey, ReferencingType = typeof(Society), ReferencingColumn = "ID")]
		[Column("SocietyID", EDataType.Integer)]
		public Int32 SocietyID { get; set; }

		#endregion

		#region Overrides

		public override Greeting Seed<TConnection>(IDataStore<TConnection> dataStore) {

			var that = this
				.Populate(model => model.GratitudeID = model.RelatedModel<Gratitude>().Seed<TConnection>(dataStore).ID)
				.Populate(model => model.SocietyID = model.RelatedModel<Society>().Seed<TConnection>(dataStore).ID)
				.Populate(model => model.AchievementID = model.RelatedModel<Achievement>().Seed<TConnection>(dataStore).ID).As<Greeting>();
			return dataStore.ExecuteDML<Greeting>(that, EDMLType.Insert);

		}

		#endregion

	}
}
