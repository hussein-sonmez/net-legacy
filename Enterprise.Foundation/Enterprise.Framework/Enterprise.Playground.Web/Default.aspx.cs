﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Enterprise.Core;

namespace Enterprise.Playground.Web {
	public partial class Default : System.Web.UI.Page {

		#region Overrides

		protected override void OnLoad(EventArgs e) {

			var path = Request.RawUrl.ToString();
			if(Regex.IsMatch(path, String.Format(@"\/{0}\/?$", SEnterprise.GetDefinitions().VirtualDirectory))) {
				Response.RedirectToRoute("Detail-Achievement");
			}
			base.OnLoad(e);
		}

		#endregion

	}
}