﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using Enterprise.Core;
using Enterprise.Playground.Model;

namespace Enterprise.Playground.Web {
	public class Global : System.Web.HttpApplication {

		#region Private

		private Func<IDataStore<SqlConnection>> totalConfigurator = () => SEnterprise.GetDataConfigurator<SqlConnection>(
				SEnterprise.GetImplementor<IDefinitions>().Construct().GetCredential())
				.RegisterModel<Achievement>()
				.RegisterModel<Credential>()
				.RegisterModel<Fellow>()
				.RegisterModel<Gratitude>()
				.RegisterModel<Greeting>()
				.RegisterModel<Society>().DataStore;


		#endregion

		protected void Application_Start(object sender, EventArgs e) {

			SEnterprise.ConfigureRouteTable(RouteTable.Routes, totalConfigurator);

		}

		protected void Session_Start(object sender, EventArgs e) {

			SEnterprise.SessionItem<IDataStore<SqlConnection>>("DataStore", () => 
				totalConfigurator().RebuildDatabase().ExecuteDDL(EDDLType.DropCreate).SeedDatabase());
		}

		protected void Application_BeginRequest(object sender, EventArgs e) {

		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e) {

		}

		protected void Application_Error(object sender, EventArgs e) {

		}

		protected void Session_End(object sender, EventArgs e) {

		}

		protected void Application_End(object sender, EventArgs e) {

		}
	}
}