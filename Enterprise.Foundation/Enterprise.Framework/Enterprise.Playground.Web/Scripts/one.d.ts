/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="typings/knockout/knockout.d.ts" />
/// <reference path="typings/knockout.viewmodel/knockout.viewmodel.d.ts" />
interface ConfigSeen {
    TransitionLength: number;
    VirtualDirectory: string;
    ServiceHosts: {
        [x: string]: () => string;
    };
    Patterns: {
        [x: string]: RegExp;
    };
    Messages: {
        [x: string]: string;
    };
    Constants: {
        [x: string]: any;
    };
    Captions: {
        [x: string]: string;
    };
    WebFontConfig: {
        google: {
            families: string[];
        };
    };
    AjaxTransform: (key: string, genome: Genome<any>, done: (data: any) => void) => void;
}
interface AppTask {
    Stream: () => MainStream;
    Glyph: () => Glyph;
    SiteMaster: () => SiteMaster;
    Playground: () => Playground;
    NewItemModal: () => NewItemModal;
    Core: {
        ExtractID: (from: string) => string;
        IDExists: (from: string) => boolean;
        Union: (first: any, second: any) => any;
    };
}
declare class App {
    static run(): void;
    static Task: AppTask;
    static Config: ConfigSeen;
}
declare enum AsyncMode {
    WebSocket = 1,
    jQueryAjax = 2,
}
declare enum AsyncMethod {
    Get = 1,
    Post = 2,
}
declare class Asyncll<T> {
    private asyncMode;
    private host;
    private method;
    constructor(asyncMode: AsyncMode, host: string, method?: AsyncMethod);
    transmit(genome: Genome<T>, action: string, seeder: (data: any) => void, check: (err: ErrorEvent, genome: Genome<T>) => void, always?: (genome: Genome<T>) => void): void;
}
declare class Test {
    static make(): void;
}
declare class Code<T> {
    code: string;
    decode: T;
    constructor();
    static Transform(code: string): string;
    static Construct<TCode>(code: string, decode: TCode): Code<TCode>;
    assign(code: string, decode: T): Code<T>;
    equals(c: Code<T>): boolean;
}
declare class Genome<T> {
    private _sequence;
    private count;
    private max;
    private indeedSubscriber;
    constructor();
    Swap(code: string, decode: T): Genome<T>;
    Take(code: string, that: (c: Code<T>) => void): Genome<T>;
    ForEach(that: (code: Code<T>, index?: number, genome?: Genome<T>) => void): Genome<T>;
    Remove(code: Code<T>): void;
    Magnitude(which: (code: Code<T>) => boolean): number;
    Last(which?: (code: Code<T>) => boolean): Code<T>;
    SubscribeIndeeder(max: number, subscriber: (genome: Genome<T>) => any): Genome<T>;
    Indeed(): Genome<T>;
    Add(code: Code<T>): Genome<T>;
    Extend(genome: Genome<T>): Genome<T>;
    Width: number;
    toJSONString(): string;
    toJSONObject(): {
        data: {};
    };
}
declare class Act<T extends Neuron> {
    verb: (done: boolean, t: T, next: (done: boolean, bus?: Genome<string>) => void) => void;
    constructor(verb: (done: boolean, t: T, next: (done: boolean, bus?: Genome<string>) => void) => void);
}
declare class Neuron {
    private thresholder;
    private threshold;
    private act;
    private bus;
    private soma;
    Threshold: number;
    State: boolean;
    Memory: Genome<string>;
    Value: any;
    Affirmative: boolean;
    Identity: any;
    private invoke(state);
    constructor(thresholder: (t: Neuron) => boolean, threshold?: number);
    actify(verb: (done: boolean, t: Neuron, next: (done: boolean, bus?: Genome<string>) => void) => void): Neuron;
    modify(that: (corpus: Soma) => void): Neuron;
    somatize(soma: Soma): Neuron;
    memorize(bus: Genome<string>): Neuron;
    Forget(...exclude: string[]): Neuron;
    innervate(that: Neuron): Neuron;
    charge<E extends Event>(type: string, charger: (e: E, neuron: Neuron, fire: () => void) => void): Neuron;
    private cascade(step);
    static test(): void;
}
declare class BipolarNeuron extends Neuron {
    private excitator;
    private inhibitor;
    constructor(excitator: Neuron, inhibitor: Neuron, threshold: (n: Neuron) => boolean);
    resolve(resolver: (resolution: boolean, n: Neuron) => void): BipolarNeuron;
}
declare class Axon {
    synapse: Synapse;
    constructor(synapse?: Synapse);
}
declare class Soma extends Axon {
    private _body;
    private invoked;
    body: ObjectModel;
    state: boolean;
    constructor(body: ObjectModel);
    invoke(state: boolean): Soma;
}
declare class Synapse {
    private _binded;
    nexus: Neuron;
    constructor();
    bind(to: Neuron): void;
    jumps(then: (to: Neuron) => void): void;
}
declare class NewItemModal {
    static ConstructUI(config: ConfigSeen): typeof NewItemModal;
    static TransformArrived(data: any): void;
    static Check(err: ErrorEvent, genome: Genome<Object>): void;
    static Always(genome: Genome<Object>): void;
}
declare class Playground {
    private static AsyncPost;
    static ConstructUI(config: ConfigSeen): typeof Playground;
    static GenerateMarkup(config: ConfigSeen): void;
    static GenerateDetailMarkup(config: ConfigSeen): void;
    static ModelListed(data: any): void;
    static ModelDeleted(data: any): void;
    static ModelUpdated(data: any): void;
    static ModelDetailed(data: any): void;
    static Check(err: ErrorEvent, genome: Genome<Object>): void;
    static Always(genome: Genome<Object>): void;
}
declare class Lobby {
    private static Async;
    static ConstructUI(config: ConfigSeen): void;
    private static SetNet(config);
    private static SubmitLocate(preprotein, n, config);
    private static SetAnimation(net);
    private static SetSlider(config);
    private static Handshake(config, golgi);
    private static Seed(preprotein);
}
declare class SiteMaster {
    static AsyncGenome: Genome<any>;
    static DataArray: {
        [x: string]: any;
    };
    static ConstructUI(config: ConfigSeen): typeof SiteMaster;
    static SubscribeIndeeder(k: string, data: any): void;
    static SelectModelsForDetail(config: ConfigSeen): typeof SiteMaster;
    static SelectModel(config: ConfigSeen): typeof SiteMaster;
}
declare class Glyph {
    static morph(config: ConfigSeen): typeof Glyph;
}
declare var WebFontConfig: {
    google: {
        families: string[];
    };
};
declare class ObjectModel {
    private element;
    private anime;
    constructor(element?: HTMLElement, anime?: Anime);
    static that(the: string): HTMLElement;
    static those(having: string, step: (elem: HTMLElement, index?: number, all?: number) => void): typeof ObjectModel;
    static wipe(the: string): void;
    static contains(selector: string): boolean;
    static documentTop: number;
    static documentLeft: number;
    static documentWidth: number;
    get(key: string): any;
    constructTable(columnMapping: {
        [x: string]: string;
    }, genome: Genome<any>, constructed: (omodel: ObjectModel, table: string) => void): ObjectModel;
    take(elem: HTMLElement): ObjectModel;
    mark(by: {
        [x: string]: any;
    }): ObjectModel;
    classify(...classes: string[]): ObjectModel;
    append(the: ObjectModel): ObjectModel;
    include(the: ObjectModel): ObjectModel;
    create(tag: string): ObjectModel;
    hide(): ObjectModel;
    stylize(styler: (elem: ObjectModel) => {
        [x: string]: any;
    }[]): ObjectModel;
    size: {
        height: number;
        width: number;
        offset: {
            left: number;
            top: number;
        };
        margin: {
            left: number;
            right: number;
            top: number;
            bottom: number;
        };
    };
    put(text: string): ObjectModel;
    show(): ObjectModel;
    terminate(): void;
    commit(to?: HTMLElement): ObjectModel;
    animate(keyFrames: {
        [x: number]: string[];
    }, config?: AnimeConfig, newAnimation?: boolean): ObjectModel;
    wait(by: number, what: (elem: ObjectModel) => void): ObjectModel;
    on<E extends Event>(type: string, handler: (elem: ObjectModel, e: E) => void): ObjectModel;
    run(type: string): void;
}
declare class UIEngine {
    private static registeredMessengerTop;
    static Progress<T extends Neuron>(n: T): void;
    static EndProgress<T extends Neuron>(n: T, fadeout?: boolean): void;
    static Phantom<T extends Neuron>(n: T): void;
    static AreIdentical<T extends Neuron>(n: T, o: T): boolean;
    static GenerateCode<T extends Neuron>(from: T): Code<string>;
    static CreateMessenger(message: string, withLink?: ObjectModel): void;
    static LinkToStream(streamIndex: number, action: (elem: ObjectModel, ev: MouseEvent) => void): ObjectModel;
    static PaintRainbow(to: HTMLElement): void;
}
declare class KeyFrame {
    percent: number;
    patterns: string[];
    constructor(percent: number, patterns: string[]);
    toString(designator: string): string;
}
declare class AnimeConfig {
    ease: string;
    fillmode: string;
    delay: number;
    transition: number;
    constructor(ease: string, fillmode: string, delay: number, transition: number);
}
declare class Anime {
    private name;
    private keyframes;
    static Config: AnimeConfig;
    constructor(name: string);
    keyframe(percent: number, patterns: string[]): Anime;
    apply(to: HTMLElement, config?: AnimeConfig): Anime;
    monaic(to: HTMLElement, pattern: string, config?: AnimeConfig): void;
    toString(designator: string): string;
    static test(): void;
}
declare class MainStream {
    private anime;
    private clickRegister;
    private distRegister;
    constructor(transition: number, anime?: Anime, clickRegister?: number, distRegister?: number);
    static generate(config: ConfigSeen): MainStream;
    SwitchTo(i: number): void;
    registerFlow(): MainStream;
    private a(count, a);
    private d(count, dist);
}
