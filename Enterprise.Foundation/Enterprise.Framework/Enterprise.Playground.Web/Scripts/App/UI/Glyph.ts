class Glyph {

	static morph(config: ConfigSeen) {

		WebFontConfig = config.WebFontConfig;
		var wf = document.createElement('script');
		wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		wf.type = 'text/javascript';
		wf.async = true;
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.appendChild(wf);

		return this;
        //UIEngine.PaintRainbow(<HTMLElement>document.querySelector('#h1-greetings'));
        //UIEngine.PaintRainbow(<HTMLElement>document.querySelector('#p-greetings'));
	}
} 
declare var WebFontConfig: { google: { families: string[] } };