class ObjectModel {

	constructor(private element?: HTMLElement, private anime?: Anime) {

	}

	static that(the: string) {

		return <HTMLElement>document.querySelector(the);

	}

	static those(having: string, step: (elem: HTMLElement, index?: number, all?: number) => void) {

		var all = document.querySelectorAll(having);
		for (var i = 0; i < all.length; i++) {
			step(<HTMLElement>all.item(i), i, all.length);
		}
		return this;

	}

	static wipe(the: string) {

		var that = ObjectModel.that(the);
		that.parentElement.removeChild(that);

	}

	static contains(selector: string) {

		return !!this.that(selector);

	}

	static get documentTop() {
		var doc = document.documentElement;
		var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
		return top;
	}

	static get documentLeft() {
		var doc = document.documentElement;
		var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
		return left;
	}

	static get documentWidth() {
		var doc = document.documentElement;
		var width = (window.outerWidth || doc.clientWidth || doc.scrollWidth);
		return width;
	}


	get(key: string) {

		if (key in this.element) {
			return this.element[key];
		} else if (key in this.element.attributes) {
			return this.element.attributes[key].nodeValue;
		} else if (key in this.element.style) {
			return this.element.style[key];
		}

	}

	constructTable(columnMapping: { [key: string]: string }, genome: Genome<any>, constructed: (omodel: ObjectModel, table: string) => void) {

		App.Config.AjaxTransform("CommandList", genome, (data) => {
			var thead = '<thead><tr>';
			var tr = '<tr>';
			for (var col in columnMapping) {
				if (col in columnMapping) {
					thead += '<th class="text-center">' + columnMapping[col] + '</th>';
					tr += '<td data-bind="text: ' + col + '"></td>';
				}
			}
			thead += '<th class="text-center">' + App.Config.Captions["CommandsColumn"] + '</th>';
			tr += '<td>' + data.response.Template + '</td>';
			thead += '</tr></thead>';
			tr += '</tr>';
			var tbody = '<tbody data-bind="foreach: DataList">';
			tbody += tr;
			tbody += '</tbody>';

			var table = '<table class="table table-hover table-bordered">' + thead + tbody + '</table>';
			constructed(this, table);
		});
		return this;
	}

	take(elem: HTMLElement) {

		this.element = elem;
		return this;

	}

	mark(by: { [key: string]: any }) {

		for (var b in by) {
			if (b in this.element) {
				this.element[b] = by[b];
			} else if (b in this.element.attributes) {
				this.element.attributes[b].value = by[b];
			} else if (b.indexOf('data') === 0) {
				var ds = b.replace("data", "");
				ds = ds[0].toLowerCase() + ds.slice(1);
				this.element.dataset[ds] = by[b];
			}
		}
		return this;

	}

	classify(...classes: string[]) {

		classes.filter(cls=> !this.element.classList.contains(cls)).forEach(cls=> this.element.classList.add(cls));
		return this;

	}

	append(the: ObjectModel) {

		this.element.parentNode.insertBefore(the.element, this.element.nextSibling);
		return this;

	}

	include(the: ObjectModel) {

		this.element.appendChild(the.element);
		return this;
	}

	create(tag: string) {

		this.element = document.createElement(tag);
		return this;

	}

	hide() {

		this.element.hidden = true;
		return this;

	}

	stylize(styler: (elem: ObjectModel) => Array<{ [key: string]: any }>) {

		var styles = styler(this);
		for (var i = 0; i < styles.length; i++) {
			var style = styles[i];
			for (var s in style) {
				this.element.style[s] = style[s];
			}
		}
		return this;

	}

	get size() {

		return {
			height: this.element.clientHeight || this.element.style.pixelHeight,
			width: this.element.clientWidth || this.element.style.pixelWidth,
			offset: {
				left: this.element.clientLeft || this.element.scrollLeft || this.element.style.pixelLeft || this.element.offsetLeft,
				top: this.element.clientTop || this.element.scrollTop || this.element.style.pixelTop || this.element.offsetTop,
			},
			margin: {
				left: parseInt(this.element.style.marginLeft, 10),
				right: parseInt(this.element.style.marginRight, 10),
				top: parseInt(this.element.style.marginTop, 10),
				bottom: parseInt(this.element.style.marginBottom, 10)
			}
		}

	}

	put(text: string) {

		if ('value' in this.element) {
			this.element['value'] = text;
		} else {
			this.element.innerHTML = text;
		}
		return this;

	}

	show() {

		this.element.style.display = 'block';
		this.element.hidden = false;
		return this;

	}

	terminate() {

		setTimeout(() => this.element.parentElement.removeChild(this.element), 2000);

	}

	commit(to?: HTMLElement) {

		to ? to.appendChild(this.element) : document.body.appendChild(this.element);
		return this;

	}

	animate(keyFrames: { [key: number]: string[] }, config?: AnimeConfig, newAnimation?: boolean) {

		var an = new Anime('anime' + (Math.random() * 6666).toString().replace('.', '-'));
		this.anime = newAnimation ? an : this.anime || an;
		for (var key in keyFrames) {
			this.anime.keyframe(key, keyFrames[key]);
		}
		this.anime.apply(this.element, config);
		return this;

	}

	wait(by: number, what: (elem: ObjectModel) => void) {

		setTimeout(() => what(this), by);
		return this;

	}

	on<E extends Event>(type: string, handler: (elem: ObjectModel, e: E) => void) {

		this.element.addEventListener(type, (e) => {
			handler(this, <E>e);
		}, false);
		return this;

	}

	run(type: string) {

		var ev = new Event();
		ev.type = type;
		this.element.dispatchEvent(ev);

	}

}
