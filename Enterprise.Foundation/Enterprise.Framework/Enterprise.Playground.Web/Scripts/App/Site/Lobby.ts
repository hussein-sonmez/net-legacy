class Lobby {

    private static Async: Asyncll<string>;

    static ConstructUI(config: ConfigSeen) {

        Lobby.Async = new Asyncll<string>(AsyncMode.WebSocket, '/');
        Lobby.Handshake(config, cfg=> {
            Lobby.SetSlider(cfg);
            Lobby.SetNet(cfg);
        });

    }

    private static SetNet(config: ConfigSeen) {

        var net = new Array<Neuron>();
        for (var i = 0; i < config.Constants['LocatorRangeCount']; ++i) {
            var input = new ObjectModel(document.getElementById('input-range-' + i));
            var neuron = new Neuron(n=> n.Value > 0).somatize(new Soma(input)).actify((done, n, next) => {
                next(done, new Genome<string>().Add(UIEngine.GenerateCode(n)));
            }).charge<MouseEvent>('change', (e, n, fire) => fire());
            net.push(neuron);
        }

        var submit = new ObjectModel().create('input').commit();
        var submitter = new Neuron(n=> n.Memory.Magnitude(c=> c.code != Code.Transform('who')) === n.Threshold)
            .somatize(new Soma(submit)).actify((done, n, next) => {
                if (done) {
                    Lobby.SetAnimation(net);
                    var code = new Code<string>().assign('type', 'type.action');
                    Lobby.Async.transmit(n.Memory.Add(code), 'locate',
                        (preprotein) => Lobby.SubmitLocate(preprotein, n, config),
                        (mutation) => UIEngine.CreateMessenger(mutation.message || config.Messages['PleaseTryAgain']),
                        (genome) => {
                            n.Forget('who');
                            Lobby.SetAnimation(net);
                        });
                }
            });
        for (var i = 0; i < net.length; ++i) {
            net[i].innervate(submitter);
        }

    }

    private static SubmitLocate(preprotein: any, n: Neuron, config: ConfigSeen) {

        if (!preprotein.CycleCompleted) {
            var who = parseInt(preprotein.Am, 10) * parseInt(preprotein.I, 10);
            n.Memory.Take('who', cd => cd.decode = who.toString());
            UIEngine.CreateMessenger(preprotein.Message);
        } else if (preprotein.Located) {
            UIEngine.CreateMessenger(preprotein.Message);
        } else {
            UIEngine.CreateMessenger(config.Messages['PleaseTryAgain']);
        }

    }

    private static SetAnimation(net: Array<Neuron>) {

        for (var i = 0; i < net.length; ++i) {
            UIEngine.Phantom(net[i]);
        }

    }

    private static SetSlider(config: ConfigSeen) {

        var history = (elem: ObjectModel, t: number) => {
            if (elem.get('data-slider-track')) {
                var track = new ObjectModel(document.getElementById('figure-locator-' + elem.get('data-slider-track')));
                track.animate({
                    0: ['scaleX(1.0)', 'opacity: 1.0'],
                    100: ['scaleX(0.0)', 'opacity: 0.0']
                });
            }
            var pos = parseInt(elem.get('value'), 10);
            var floct = config.Constants['LocatorSlideCount'] * t + pos;
            var floc = new ObjectModel(document.getElementById('figure-locator-' + floct));
            floc.animate({
                0: ['scaleX(0.0)', 'opacity: 0.0'],
                100: ['scaleX(1.0)', 'opacity: 1.0']
            });
            elem.mark({ dataSliderTrack: floct });
        }

        var floccer = (elem: ObjectModel, t: number) => {
            var pos = parseInt(elem.get('value'), 10);
            for (var i = 0; i < config.Constants['LocatorSlideCount']; ++i) {
                var floct = config.Constants['LocatorSlideCount'] * t + i;
                var floc = new ObjectModel(document.getElementById('figure-locator-' + floct))
                    .stylize(fl=> [{
                        position: 'absolute',
                        right: '25%',
                        zIndex: -1
                    }]);
                if (pos === i) {
                    elem.mark({ dataSliderTrack: floct });
                    floc.animate({
                        0: ['scaleX(0.0)', 'opacity: 0.0'],
                        100: ['scaleX(1.0)', 'opacity: 1.0']
                    });
                } else {
                    floc.animate({
                        0: ['scaleX(1.0)', 'opacity: 1.0'],
                        100: ['scaleX(0.0)', 'opacity: 0.0']
                    });
                }
            }
        };

        for (var i = 0; i < config.Constants['LocatorRangeCount']; ++i) {
					var input = new ObjectModel(document.getElementById('input-range-' + i));
            input.stylize(el=> [{ paddingBottom: '10px', marginTop: '20px', marginRight: '2px', width: '35%' }]);
            floccer(input, i);
            input.on<MouseEvent>('change', (elem, e) => {
                history(elem, parseInt(elem.get('id').replace('input-range-', ''), 10));
            });
        }

    }

    private static Handshake(config: ConfigSeen, golgi: (config: ConfigSeen) => void) {

		Lobby.Async.transmit(new Genome<string>().Add(new Code<string>().assign('type', 'type.handshake')), 'locate',
            (preprotein) => { Lobby.Seed(preprotein); golgi(config); },
            (mutation) => { UIEngine.CreateMessenger(mutation.message || config.Messages['HandshakeRejected']); },
            (genome) => { });

    }

    private static Seed(preprotein: any) {

        new ObjectModel(document.getElementById('content')).put(preprotein.Seed);

    }

}