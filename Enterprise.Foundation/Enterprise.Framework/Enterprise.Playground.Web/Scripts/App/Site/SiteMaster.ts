﻿
class SiteMaster {

	static AsyncGenome: Genome<any>;
	static DataArray: { [key: string]: any };

	static ConstructUI(config: ConfigSeen) {

		SiteMaster.AsyncGenome = new Genome<string>();
		SiteMaster.DataArray = <{ [key: string]: any }>{};
		return SiteMaster;

	}
	static SubscribeIndeeder(k: string, data: any) {
		SiteMaster.DataArray[k] = data;
		SiteMaster.AsyncGenome.SubscribeIndeeder(2, genome => {
			SiteMaster.AsyncGenome.Take('PlaygroundViewModel', kovm1=> {
				SiteMaster.AsyncGenome.Take('NewItemModalViewModel', kovm2=> {

					var template = SiteMaster.DataArray['NewItemModalViewModel'].response.Template;
					var om = new ObjectModel(ObjectModel.that('#section-absolute-elements')).put(template);

					var columnMap: { [key: string]: string } = {};
					SiteMaster.DataArray['PlaygroundViewModel'].response.ColumnList.forEach(ci=> columnMap[ci.Title] = ci.Label);
					var form = ObjectModel.that('#form-authenticate');
					new ObjectModel(form)
						.constructTable(columnMap, SiteMaster.AsyncGenome, (omodel, table) => {
							omodel.put(table);
							ko.cleanNode(form);
							ko.applyBindings(App.Task.Core.Union(new kovm1.decode(), new kovm2.decode()));
							ObjectModel.that('#buttonModalNewItem').click();
						});

				});
			});
		});
	}
	static SelectModelsForDetail(config: ConfigSeen) {

		var match = document.location.pathname.match(/\/([^\d]+)\/?/g);
		var requestedModel = match[0].replace(config.VirtualDirectory, '').replace(/(\/)/g, '');
		var relatedModel = match[1].replace(/(\/)+/g, '');
		var itemID = document.location.pathname.match(/\d+/g)[0];

		SiteMaster.AsyncGenome.Add(Code.Construct("RequestedModel", requestedModel))
			.Add(Code.Construct("RelatedModel", relatedModel))
			.Add(Code.Construct("ItemID", itemID));
		return SiteMaster;

	}

	static SelectModel(config: ConfigSeen) {

		var match = document.location.pathname.replace(/\/(\w+)\//g, '').match(/\w+/g);
		var requestedModel = match[0];
		SiteMaster.AsyncGenome.Add(Code.Construct("RequestedModel", requestedModel));
		return SiteMaster;

	}

}