/// <reference path="../../typings/knockout/knockout.d.ts" />
/// <reference path="../../typings/knockout.viewmodel/knockout.viewmodel.d.ts" />

class Playground {

	private static AsyncPost: Asyncll<any>;

	static ConstructUI(config: ConfigSeen) {

		Playground.AsyncPost = new Asyncll<string>(AsyncMode.jQueryAjax, config.ServiceHosts['Generic'](), AsyncMethod.Post);
		SiteMaster.AsyncGenome = new Genome<string>();
		if (App.Task.Core.IDExists(document.location.pathname)) {
			Playground.GenerateDetailMarkup(config);
		} else {
			Playground.GenerateMarkup(config);
		}
		return Playground;

	}

	static GenerateMarkup(config: ConfigSeen) {

		SiteMaster.SelectModel(config);

		Playground.AsyncPost.transmit(SiteMaster.AsyncGenome, 'List'
			, Playground.ModelListed, Playground.Check, Playground.Always);

	}
	static GenerateDetailMarkup(config: ConfigSeen) {

		SiteMaster.SelectModelsForDetail(config);

		Playground.AsyncPost.transmit(SiteMaster.AsyncGenome, 'List'
			, Playground.ModelListed, Playground.Check, Playground.Always);

	}
	
	static ModelListed(data: any) {

		var komodels = {};

		var koRowGenerator = (row) => {
			var korow = {};
			var relatedModels = [];
			for (var i = 0; i < row.Columns.length; i++) {
				var col = row.Columns[i];
				korow[col.Key] = ko.observable(col.Value);
				if (col.RelatedModel) {
					relatedModels.push({
						ID: ko.observable(row.RowID),
						Model: ko.observable(col.RelatedModel)
					});
				}
			}
			korow["RelatedModels"] = ko.observableArray(relatedModels);
			korow["CommandButtonExists"] = ko.observable(relatedModels.length > 0);
			return korow;
		};

		function PlaygroundViewModel() {
			var self = this;
			self.DataList = ko.observableArray(data.response['DataList'].Rows.map(row=> koRowGenerator(row)));

			self.removeItem = function (row) {
				Playground.AsyncPost.transmit(SiteMaster.AsyncGenome.Add(Code.Construct("ItemID", row.ID())), 'Delete', Playground.ModelDeleted, Playground.Check, Playground.Always);
				self.DataList.remove(row);
			};

			self.updateItem = function (row) {
				Playground.AsyncPost.transmit(SiteMaster.AsyncGenome.Add(Code.Construct("ItemID", row.ID())), 'Update', Playground.ModelUpdated, Playground.Check, Playground.Always);
			};

			self.detailsItem = function (row) {
				Playground.AsyncPost.transmit(SiteMaster.AsyncGenome.Add(Code.Construct("ItemID", row.ID()))
					.Add(Code.Construct("RelatedModel", row.Model())), 'Details', Playground.ModelDetailed, Playground.Check, Playground.Always);
			};

		};

		SiteMaster.AsyncGenome.Add(Code.Construct('PlaygroundViewModel', PlaygroundViewModel));

		SiteMaster.SubscribeIndeeder('PlaygroundViewModel', data);

		SiteMaster.AsyncGenome.Indeed();

	}

	static ModelDeleted(data: any) {

		console.log("deleted successfully: ");
		console.log(data);
	}

	static ModelUpdated(data: any) {

		console.log("updated successfully: ");
		console.log(data);
	}

	static ModelDetailed(data: any) {

		var response = data["response"];
		var form = <HTMLFormElement>document.forms[0];
		form.action = response["DetailsUrl"];
		form.submit();
	}

	static Check(err: ErrorEvent, genome: Genome<Object>) {

		genome.Add(Code.Construct('StatusText', err['statusText']));

	}

	static Always(genome: Genome<Object>) {

		App.Task.Stream().SwitchTo(1);

	}
}