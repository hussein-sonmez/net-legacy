﻿/// <reference path="../../typings/jquery/jquery.d.ts" />
enum AsyncMode {
	WebSocket= 1,
	jQueryAjax
}
enum AsyncMethod {
	Get= 1,
	Post
}

class Asyncll<T> {

	constructor(private asyncMode: AsyncMode, private host: string, private method: AsyncMethod = AsyncMethod.Get) {

		if (this.asyncMode == AsyncMode.WebSocket) {
			this.host = "ws://" + document.domain + App.Config.VirtualDirectory + "Hub/Handlers/GenericWebSocketHandler.ashx";
		}
	}

	transmit(genome: Genome<T>, action: string, seeder: (data: any) => void, check: (err: ErrorEvent, genome: Genome<T>) => void, always?: (genome: Genome<T>) => void) {

		if (this.asyncMode == AsyncMode.WebSocket) {
			var ws = new WebSocket(this.host + '?sw=' + action);
			ws.onerror = (ev) => {
				check(ev, genome);
				always(genome);
			};

			ws.onopen = (ev) => {
				if (ws.readyState === WebSocket.OPEN) {
					ws.send(genome.toJSONString());
				}
			};

			ws.onmessage = (ev) => {
				seeder(JSON.parse(ev.data));
				always(genome);
			};
		} else {
			var jqxhr;
			if (this.method == AsyncMethod.Get) {
				jqxhr = $.ajax({
					url: this.host + '/' + action,
					type: "GET",
					contentType: "application/json; charset=utf-8",
					dataType: 'json',
					cache: false,
					success: (data, textStatus, jqXHR) => {
						seeder(JSON.parse(data.d));
					},
					async: true
				});
			} else if (this.method == AsyncMethod.Post) {
				jqxhr = $.ajax({
					url: this.host + '/' + action,
					type: "POST",
					contentType: "application/json; charset=utf-8",
					data: genome.toJSONString(),
					dataType: 'json',
					cache: false,
					success: (data, textStatus, jqXHR) => {
						seeder(JSON.parse(data.d));
					},
					async: true
				});
			}

			jqxhr
				.fail((jqXHR, textStatus, errorThrown) => {
					check(errorThrown, genome);
				})
				.always(() => {
					always && always(genome);
				});
		}

	}
}