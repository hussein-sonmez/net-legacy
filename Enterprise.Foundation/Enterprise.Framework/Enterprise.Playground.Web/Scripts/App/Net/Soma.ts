class Axon {

	constructor(public synapse?: Synapse) {

		this.synapse = new Synapse();

	}

}

class Soma extends Axon {

	private _body: ObjectModel;
    private invoked: boolean;

	get body() {
		return this._body;
	}

    get state() {
        return this.invoked;
    }

    constructor(body: ObjectModel) {

        super();
        this.invoked = true;
		this._body = body;

	}

    invoke(state: boolean) {

        this.invoked = state;
        return this;

    }

} 