class Synapse {
	private _binded: Neuron;

	get nexus() {
		return this._binded;
	}

	constructor() {

	}

	bind(to: Neuron) {

		this._binded = to;

	}

	jumps(then: (to: Neuron) => void) {
		if (this._binded) {
			then(this._binded);
		}
	}

}