﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Enterprise.Playground.Web.Default" %>

<%@ Import Namespace="Enterprise.Core" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="description" content="Greetings | A new way to show gratitude!" />
	<meta name="keywords" content="greetings, thanks, thank you, love, affect" />
	<meta itemprop="og:title" content="Greetings" />
	<meta itemprop="og:description" content="World is worth to greet!" />
	<meta itemprop="og:url" content="http://lampiclobe.com/greetings" />
	<meta itemprop="og:image" content="/images/ta.png" />
	<meta itemprop="og:site_name" content="lampiclobe" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

	<title>Enterprise | Playground!</title>

	<%--<link rel="icon" href="/images/favicon.ico" type="image/x-icon">--%>
	<link rel="apple-touch-icon" href="/images/touch-icon-iphone.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/images/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/images/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/images/touch-icon-ipad-retina.png">

	<link href='<%=SEnterprise.GetDefinitions().Virtualize("/Content/bootstrap.min.css") %>' rel="stylesheet" />
	<link href='<%=SEnterprise.GetDefinitions().Virtualize("/Content/app.css") %>' rel="stylesheet" />
</head>
<body>
	<header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
				<ul class="nav navbar-nav">
					<li>
						<a href="#" data-stream-index="0">Playground<span class="underline"></span></a>

					</li>
					<li>
						<a href="#" data-stream-index="1">Let's Introduce<span class="underline"></span></a>
					</li>
					<li>
						<a href="#" data-stream-index="2">Join Us<span class="underline"></span></a>
					</li>
					<li>
						<a href="#" data-stream-index="3">What We Do<span class="underline"></span></a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="#" data-stream-index="4">lampiclobe&copy;2014<span class="underline"></span></a>
					</li>
				</ul>
			</nav>
		</div>
	</header>
	<section role="main" id="content" class="main-stream" id="mainSection">
		<section id="stream-0" class="stream outstand">
			<h1 id="h1-greetings">Greetings!</h1>
			<p id="p-greetings" class="lead">
				You Have Started Your Happiest Journey!
			</p>
		</section>
		<section id="stream-1" class="stream text-center outstand">
			<p class="lead">
				Let's Introduce!
			</p>
			<p class="outstand">
				Now Let's Find Out Whether You're A Human Or Not!
			</p>
			<form role="form" id="form-authenticate">
			</form>
		</section>
		<section id="stream-2" class="stream text-center outstand">
			<p class="lead">
				Join Us!
			</p>
			<form role="form" id="form-joint">
			</form>
		</section>
		<section id="stream-3" class="stream outstand">
			<p class="lead">
				What We Do!
			</p>

		</section>
		<section id="stream-4" class="stream outstand">
			<p class="lead">
				Who We're!
			</p>

		</section>
	</section>
	<section id="section-absolute-elements">
		
	</section>

	<script src='<%=SEnterprise.GetDefinitions().Virtualize("/Scripts/jquery-1.9.0.min.js") %>' type="text/javascript"></script>
	<script src='<%=SEnterprise.GetDefinitions().Virtualize("/Scripts/bootstrap.min.js") %>' type="text/javascript"></script>
	<script src='<%=SEnterprise.GetDefinitions().Virtualize("/Scripts/knockout-3.1.0.js") %>' type="text/javascript"></script>
	<script data-main='<%=SEnterprise.GetDefinitions().Virtualize("/Scripts/acquired.js") %>' 
		src='<%=SEnterprise.GetDefinitions().Virtualize("/Scripts/require.js") %>'></script>
</body>
</html>
