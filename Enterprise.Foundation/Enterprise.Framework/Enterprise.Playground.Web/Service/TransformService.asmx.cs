﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Script.Services;
using System.Web.Services;

namespace Enterprise.Playground.Web.Service {
	[WebService(Namespace = "Enterprise.Playground.Web")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]

	[System.Web.Script.Services.ScriptService]
	public class TransformService : GenericService {

		#region Private

		private FileInfo Template(String key) {
			return new FileInfo(HostingEnvironment.MapPath(String.Format("~/Content/Template/{0}.html", key)));
		}

		#endregion

		#region Verbs

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public virtual String GetTemplate(dynamic data) {

			AssignCorrectListings(data);
			var key = data["key"].ToString();
			FileInfo template = Template(key);

			var queryObject = Activator.CreateInstance(RelatedModelType ?? ModelType);
			if (template.Exists) {
				return GenerateJson("", new {
					Template = File.ReadAllText(template.FullName),
					FormData = TransformService.GenerateFormData(TypeParserService, queryObject),
				});
			} else {
				throw new ArgumentException(key);
			}

		}

		#endregion

	}
}
