﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.Script.Services;
using System.Web.Services;
using Enterprise.Core;
using Enterprise.Core.Decorators;
using Enterprise.Playground.Model;

namespace Enterprise.Playground.Web.Service {
	[WebService(Namespace = "Enterprise.Playground.Web")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]

	[System.Web.Script.Services.ScriptService]
	public class GenericService : BaseService {

		#region Verbs

		// List: api/Default
		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public virtual String List(dynamic data) {

			this.AssignCorrectListings(data);

			var model = (data as Dictionary<string, object>).Keys.Contains("ItemID")
				? GetIDAssignedRelatedModel(data["ItemID"]) : Activator.CreateInstance(ModelType);
			return SelectModel(data, model);

		}

		// DELETE:
		[ScriptMethod, WebMethod(EnableSession = true)]
		public virtual String Delete(dynamic data) {

			this.AssignCorrectListings(data);

			dynamic toBeDeleted = this.SingleSelect(data["ItemID"]);
			var deleted = this.DataStore.ExecuteDML(toBeDeleted, EDMLType.Delete);
			return GenerateJson("", deleted);

		}

		// Details:
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[WebMethod(EnableSession = true)]
		public virtual String Details(dynamic data) {

			this.AssignCorrectListings(data);

			var response = new {
				DetailsUrl = RelatedModelType != null 
				? String.Format("{0}/{1}/{2}", ModelType.Name, data["ItemID"], RelatedModelType.Name)
				: "#"
			};
			return GenerateJson("", response);

		}

		// New: api/Default
		[ScriptMethod, WebMethod(EnableSession = true)]
		public virtual String New(dynamic data) {

			this.AssignCorrectListings(data);

			var added = this.DataStore.ExecuteDML(data, EDMLType.Insert).FirstOrDefault();
			return GenerateJson("", added);

		}

		// Modify: api/Default/5
		[ScriptMethod, WebMethod(EnableSession = true)]
		public virtual String Modify(dynamic data) {

			this.AssignCorrectListings(data);

			var response = this.DataStore.ExecuteDML(data, EDMLType.Update);
			return GenerateJson("", response);

		}

		#endregion

		#region Private

		private string SelectModel(dynamic data, Object queryObject) {

			var type = RelatedModelType ?? ModelType;
			var list = this.DataStore.ExecuteByReflection<IEnumerable<dynamic>>("ExecuteSelect", new Type[] { type }, queryObject);

			var mutationList = TransformService.GenerateMutationList(TypeParserService, type, list);

			var json = GenerateJson("", mutationList);
			return json;
		}

		private dynamic SingleSelect(Object id) {

			var selected = (this.DataStore as ISingularity<IDataStore<SqlConnection>>).ExecuteByReflection("ExecuteSelect", new Type[] { ModelType }, GetIDAssignedModel(id, ModelType)) as IEnumerable<dynamic>;

			return TypeParserService.GetValueOf(selected.FirstOrDefault(), "Item");

		}

		private dynamic GetIDAssignedModel(Object id, Type modelType) {

			var idColumns = GetIDColumns(ModelType);

			var model = Activator.CreateInstance(modelType);
			TypeParserService.Assign(model, Int32.Parse(id.ToString()), idColumns.First().Title);
			return model;

		}

		private dynamic GetIDAssignedRelatedModel(Object id) {

			var model = Activator.CreateInstance(ModelType);
			dynamic relatedModel = TypeParserService.ReflectionManager.ExecuteMethod(model, "RelatedModel", new Type[] { RelatedModelType });

			var idColumns = GetIDColumns(RelatedModelType);

			TypeParserService.Assign(relatedModel, Int32.Parse(id.ToString()), idColumns.First().Title);
			return relatedModel;

		}

		#endregion

	}
}
