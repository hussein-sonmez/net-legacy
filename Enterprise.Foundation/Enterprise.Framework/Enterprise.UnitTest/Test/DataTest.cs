﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Enterprise.Core;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using Enterprise.Playground.Model;

namespace Enterprise.UnitTest {

	[TestClass]
	public class DataTest {

		#region Private

		private static IDataStore<SqlConnection> DataStore;

		private static IConnectionCredential GetRightCredentials(Boolean local = true) {

			var def = SEnterprise.GetImplementor<IDefinitions>().Construct();
			return def.GetCredential();

		}
		
		private static IConnectionCredential InsertAndReturn(out Fellow returnedModel) {
			var cred = GetRightCredentials(true);

			var boundModel1 = new Society() {
				PublicIdentifier = "public-iden-society"
			};

			var boundModel2 = new Credential() {
				ActivationPhase = EActivationPhase.CreatedRecently,
				Cipher = "cipher-1234",
				Locator = "locatorvdss-21"
			};

			var mainModel = new Fellow() {
				PublicIdentifier = "publiciden-1",
				SocietyID = DataStore.ExecuteDML<Society>(boundModel1,EDMLType.Insert).ID,
				CredentialID = DataStore.ExecuteDML<Credential>(boundModel2, EDMLType.Insert).ID
			};
			returnedModel = DataStore.ExecuteDML<Fellow>(mainModel, EDMLType.Insert); ;
			return cred;
		}

		#endregion

		#region Test Methods

		[ClassInitialize]
		public static void Init(TestContext context) {

			Func<IDataStore<SqlConnection>> totalConfigurator = () => SEnterprise.GetDataConfigurator<SqlConnection>(
				SEnterprise.GetImplementor<IDefinitions>().Construct().GetCredential())
				.RegisterModel<Achievement>()
				.RegisterModel<Credential>()
				.RegisterModel<Fellow>()
				.RegisterModel<Gratitude>()
				.RegisterModel<Greeting>()
				.RegisterModel<Society>().DataStore.RebuildDatabase().ExecuteDDL(EDDLType.DropCreate).SeedDatabase();
			DataStore = totalConfigurator();
			Assert.IsNotNull(DataStore);
		}

		[TestInitialize]
		public void Init() {

			Assert.IsNotNull(DataStore.ExecuteDDL(EDDLType.DropCreate));

		}

		[TestMethod]
		public void DataChannelWellBuilt() {

			var ce = GetRightCredentials(false);
			var conn = ce.GetConnection<SqlConnection>();
			conn.Open();
			Assert.AreEqual(conn.State, ConnectionState.Open);
			conn.Close();
		}

		[TestMethod]
		public void ConnectionBoundToIP() {

			var cred = GetRightCredentials();
			var sqlServicesConnection = DataStore.ConnectionService(cred).Construct();
			Assert.IsNotNull(sqlServicesConnection);
			Assert.IsNotNull(sqlServicesConnection.Item);
			Assert.IsNotNull(sqlServicesConnection.Item);

			try {
				sqlServicesConnection.Item.Open();
				Assert.AreEqual<ConnectionState>(ConnectionState.Open, sqlServicesConnection.Item.State);
			} catch (Exception ex) {
				Assert.Fail(ex.Message);
			} finally {
				sqlServicesConnection.Item.Close();
			}

		}
		[TestMethod]
		public void ConnectionBoundToLocal() {

			var cred = GetRightCredentials();
			var sqlServicesConnection = DataStore.ConnectionService(cred).Construct();

			Assert.IsNotNull(sqlServicesConnection);
			Assert.IsNotNull(sqlServicesConnection.Item);
			Assert.IsNotNull(sqlServicesConnection.Item);

			try {
				sqlServicesConnection.Item.Open();
				Assert.AreEqual<ConnectionState>(ConnectionState.Open, sqlServicesConnection.Item.State);
			} catch (Exception ex) {
				Assert.Fail(ex.Message);
			} finally {
				sqlServicesConnection.Item.Close();
			}

		}

		[TestMethod]
		public void DataCommandWellBuilt() {

			var model = SEnterprise.GetImplementor<ITSqlModelService<Credential>>().Construct();
			var command = model.DdlStatementService(EDDLType.Create).GetCommand();
			Assert.IsNotNull(command);

		}

		[TestMethod]
		public void DataSelectIsSuccessful() {

			var model = new Credential() {
				ID = 0,
				ActivationPhase = EActivationPhase.CreatedRecently,
				Cipher = "1q2w3e4r5t",
				Locator = "locate-me"
			};
			var found = DataStore.ExecuteSelect<Credential>(model);

			Assert.AreEqual<Int32>(0, found.Count());
			Fellow fellow;
			InsertAndReturn(out fellow);

			Assert.AreNotEqual<Int32>(0, DataStore.ExecuteSelect<Fellow>((Fellow)Activator.CreateInstance(typeof(Fellow))).Count());
			var selected = DataStore.ExecuteSelect<Fellow>(new {
				ID=fellow.ID
			}).FirstOrDefault();
			Assert.IsNotNull(selected);
			Assert.AreEqual(fellow.PublicIdentifier, selected.Item.PublicIdentifier);
		}

		[TestMethod]
		public void DataInsertIsSuccessful() {

			Fellow returnedModel;
			InsertAndReturn(out returnedModel);

			Assert.IsNotNull(returnedModel);
			Assert.AreNotEqual<Int32>(0, returnedModel.ID);

		}


		[TestMethod]
		public void DataUpdateIsSuccessful() {

			Fellow returnedModel;
			var cred = InsertAndReturn(out returnedModel);


			//Update Should Complete
			returnedModel.PublicIdentifier = "reserved";
			var updated = DataStore.ExecuteDML<Fellow>(returnedModel, EDMLType.Update);
			Assert.AreEqual<String>("reserved", updated.PublicIdentifier);

		}

		[TestMethod]
		public void DataDeleteIsSuccessful() {

			Fellow returnedModel;
			var cred = InsertAndReturn(out returnedModel);

			//Delete Should Complete
			DataStore.ExecuteDML<Fellow>(returnedModel, EDMLType.Delete);
			var shouldBeDeleted = DataStore.ExecuteSelect<Fellow>(returnedModel);

			Assert.AreEqual<Int32>(0, shouldBeDeleted.Count());

		}

		[TestMethod]
		public void DataSeedIsSuccessful() {

			DataStore.SeedDatabase();

			var models = DataStore.ExecuteSelect<Fellow>(new { });

			Assert.AreNotEqual<Int32>(0, models.Count());

		}

		#endregion

	}
}
