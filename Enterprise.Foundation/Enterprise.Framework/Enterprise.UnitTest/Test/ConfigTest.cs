﻿using System;
using System.Reflection;
using Enterprise.Core;
using Enterprise.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Enterprise.UnitTest {

    [TestClass]
    public class ConfigTest {
        [TestMethod]
        public void ConfigsMadeProperly() {

            var cfg = SEnterprise.GetImplementor<IConfig<String>>().Construct("cfg");
            Assert.IsNotNull(cfg);

            var service = SEnterprise.GetImplementor<IService<String>>()
                .Construct()
                .Set("cfg");
            Assert.IsNotNull(service);

            Assert.AreEqual<String>("cfg", service.Item);

            var translated = service.Translate<String>(str => str.TrimEnd('g'));
            Assert.AreEqual<String>("cf", translated.Item);

        }
    }
}
