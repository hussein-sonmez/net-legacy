﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using System.Web.Routing;
using Oneness.Definitions.Configurations;

namespace Oneness.WebApi {
    public class WebApiApplication : System.Web.HttpApplication {
        protected void Application_Start() {

            GlobalConfiguration.Configure(CustomRouteConfig.Routes);
            GlobalConfiguration.Configure(CustomWebApiConfig.Formatters);
            GlobalConfiguration.Configure(CustomWebApiConfig.HttpConfig);

        }
    }
}
