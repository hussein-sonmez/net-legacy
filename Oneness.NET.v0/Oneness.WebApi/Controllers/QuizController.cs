﻿using System.Threading.Tasks;
using System.Web.Routing;
using Ninject;
using Oneness.IoC.Core;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.Definitions.WebConfig;
using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Oneness.ServiceModel.EntityModels;
using System.Net.Http;
using System.Net;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Oneness.ViewModel.EntityViewModels.Extensions;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.CoreData.Entity.POCO;

namespace Oneness.WebApi.Controllers {

    [RoutePrefix(SControllerConfig.QuizConfig.Prefix) ]
    public class QuizController : ApiController {

        #region Get Actions

        [Route(SControllerConfig.QuizConfig.HttpGet.Index), HttpGet]
        public async Task<HttpResponseMessage> Index() {

            var quiz = await SInstanceProvider.Container.Get<IRepositoryBaseAsync<Quiz>>().TopAsync(e => e.ID);
            var response = Request.CreateResponse<QuizViewModel>(
                HttpStatusCode.OK, quiz.ToViewModel());
            return response;

        }

        #endregion

    }
}