﻿using Oneness.CoreData.Entity.POCO;
using Oneness.Mocking.Base;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Mocking.Mocks.TranslationScheme {
    internal class ManifestMock : MockBase<Manifest>, IEntityMock<Manifest> {

        protected override Func<long, Manifest> Seeder {
            get {
                return (id) => new Manifest() {
                    ID = id,
                    Language = new LanguageMock().ShuffleAny(),
                    Sentence = new SentenceMock().ShuffleAny()
                };
            }
        }

    }
}
