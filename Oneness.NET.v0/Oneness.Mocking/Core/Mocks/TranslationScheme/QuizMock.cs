﻿using Oneness.CoreData.Entity.POCO;
using Oneness.Mocking.Base;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Mocking.Mocks.TranslationScheme {
    internal class QuizMock : MockBase<Quiz>, IEntityMock<Quiz> {

        #region Inherited

        protected override Func<long, Quiz> Seeder {
            get {
                return (id) => new Quiz() {
                    ID = id,
                    Questions = new QuestionMock().TakeSome(_Magnitude).As<ICollection<Question>>(),
                    Sentence = new SentenceMock().ShuffleAny()
                };
            }
        }
        #endregion
    }

}
