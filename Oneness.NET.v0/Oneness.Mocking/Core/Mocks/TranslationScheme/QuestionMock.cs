﻿using Oneness.CoreData.Entity.POCO;
using Oneness.Mocking.Base;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Mocking.Mocks.TranslationScheme {
    internal class QuestionMock : MockBase<Question>, IEntityMock<Question> {

        #region Inherited

        protected override Func<long, Question> Seeder {
            get {
                return (id) => new Question() {
                    ID = id,
                    Answers =  new AnswerMock().TakeSome(_Magnitude).As<ICollection<Answer>>(),
                    Sentence = new SentenceMock().ShuffleAny()
                };
            }
        }

        #endregion
    }

}
