﻿using Oneness.Mocking.Base;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oneness.CoreData.Entity.POCO;

namespace Oneness.Mocking.Mocks.TranslationScheme {
    internal class LanguageMock : MockBase<Language>, IEntityMock<Language> {

        protected override Func<long, Language> Seeder {
            get {
                return (id) => new Language() {
                    ID = id,
                    Description = "Language-{0}-Description".GenerateIdentifier(),
                    Name = "Name of Language<{0}>".GenerateIdentifier()
                };
            }
        }
    }

}
