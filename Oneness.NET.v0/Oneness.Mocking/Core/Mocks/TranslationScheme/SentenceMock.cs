﻿using Oneness.CoreData.Entity.POCO;
using Oneness.Mocking.Base;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Mocking.Mocks.TranslationScheme {
    internal class SentenceMock : MockBase<Sentence>, IEntityMock<Sentence> {

        #region Inherited

        protected override Func<long, Sentence> Seeder {
            get {
                return (id) => new Sentence() {
                    ID = id,
                    Text = DataStore[_Magnitude % DataStore.Count()]
                };
            }
        }

        protected String[] DataStore {
            get {

                return new String[] {
                        "Ak Akçe Kara Gün İçindir",
                        "Ak Akçe Kara Gün İçindir" ,
                        "Aslan Gibi Ye Aslan Gibi Çalış",
                        "Üzüm Üzüme Baka Baka Kararır",
                        "White fox is running away",
                        "Green fox is at home",
                        "Horses run along parking statations" ,
                        "Delikanlı adam öfkesini yenen kişiye denir",
                        "İlim ilim bilmentir ilim kendini bilmektir",
                        "Yalnızlık Allah'a mahsustur",
                        "Parking is a crucial problem for butterflies",
                        "House taxes inflated enormously" ,
                        "Love is affection",
                        "Beyaz para kararır",
                        "Helal Kazanılmış Para Zor Zamanlarda Kullanılır",
                        "Çok çalışan yemeği hakkıyla yer",
                        "Çalışmakla yemek yemek aynı anda olmaz",
                        "İki üzüm aynı olmaz",
                        "Beraber bulunan kişilerin huyları benzeşir",

                        "Snow white runs like a fox",
                        "An animal with a long tail goes away",
                        "A living creature with a color of a leaf stays home",
                        "Green hair is a best fit for a fox",
                        "Running animals take a trip near parking facilities",
                        "4 legged animals run like a charm",

                        "Er kişi gazabına üstün gelir",
                        "Adam olan sinirlenince delirir",
                        "İlim ilmek ilmektir",
                        "Gerçek ilim öze ilişkin olandır",
                        "Allah-ü Teala yalnızlığı kendisine özgü kılmıştır",
                        "Allah-ü Teala hakkında yalnızca izin verildiği kadar bilebiliriz",

                        "Butterflies has a huge problem, like cars",
                        "Butterflies die when they broke apart",
                        "Government payment policies increased payings of properties",
                        "We should use wax in houses everytime",
                        "When we love somebody we get effected from him or her",
                        "Love is like a touch of mother to her child"
                    };
            }
        }

        #endregion
    }

}
