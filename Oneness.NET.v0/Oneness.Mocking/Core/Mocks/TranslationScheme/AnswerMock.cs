﻿using Oneness.Mocking.Base;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oneness.CoreData.Entity.POCO;

namespace Oneness.Mocking.Mocks.TranslationScheme {
    internal class AnswerMock : MockBase<Answer>, IEntityMock<Answer> {

        #region Inherited

        protected override Func<long, Answer> Seeder {
            get {
                return (id) => new Answer() {
                    ID = id,
                    IsCorrectMatch = _Magnitude % id % 17 % 3 == 0,
                    Sentence = new SentenceMock().ShuffleAny()
                };
            }
        }

        #endregion
    }

}
