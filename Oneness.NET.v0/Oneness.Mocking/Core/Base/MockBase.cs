﻿using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.Base;
using Oneness.Definitions.CoreDataDefinitions;
using Oneness.Protocols.RepositoryProtocols.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using Shared.Extensions.Core;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Reflection;
using System.Collections;
using Oneness.Repository.Core.Base;

namespace Oneness.Mocking.Base {
    internal abstract class MockBase<TEntity> : IEntityMock<TEntity>
            where TEntity : BaseModel, new() {

        public MockBase(Int32 magnitude = SCoreDataConfig.PredefinedMagnitude) {
            this._Magnitude = magnitude;
        }

        public IRepositoryBaseAsync<TEntity> GenerateMock() {

            var list = new List<TEntity>();

            //var entitySubstitute = Substitute.For<TEntity>();
            //entitySubstitute.When(x => x.Collect(Arg.Any<Action<RepositoryAsyncBase<TEntity>, TEntity, PropertyInfo, Boolean>>()))
            //    .Do(x => x.Arg<Action<RepositoryAsyncBase<TEntity>, TEntity, PropertyInfo, Boolean>>().Returns((rp, e, mp, enumerable) => {
            //        if (enumerable) {
            //            var elementType = mp.PropertyType.GetGenericArguments().FirstOrDefault();
            //            mp.SetValue(e, SCoreDataConfig.PredefinedMagnitude.Times<Object>(() => Activator.CreateInstance(elementType)));
            //        } else {
            //            mp.SetValue(e, Activator.CreateInstance(mp.PropertyType));
            //        }
            //    }));

            for (int i = 1; i <= _Magnitude; i++) {
                list.Add(Seeder(i));
            }

            var mock = Substitute.For<IRepositoryBaseAsync<TEntity>>();
            mock.When(x => x.Delete(Arg.Any<TEntity>())).Do(x => list.Remove(x.Arg<TEntity>()));

            mock.When(x => x.Update(Arg.Any<TEntity>(), Arg.Any<Func<TEntity, TEntity>>()))
                .Do(x => list.Remove(x.Arg<Func<TEntity, TEntity>>()(x.Arg<TEntity>())));

            mock.Insert(Arg.Any<TEntity>())
                .Returns(x => list.Assign(li => li.Insert(list.Count - 1, x.Arg<TEntity>().Assign(e => e.ID = list.OrderBy(l => l.ID).Last().ID + 1)))
                    .OrderBy(li => li.ID).LastOrDefault());

            mock.Top(Arg.Any<Func<TEntity, Int64>>())
                .Returns(x => list.OrderBy(x.Arg<Func<TEntity, Int64>>()).FirstOrDefault());

            mock.The(Arg.Any<Int64>()).Returns(x => list.SingleOrDefault(l => l.ID == x.Arg<Int64>()));

            mock.One(Arg.Any<Expression<Func<TEntity, Boolean>>>()).Returns(x => list.SingleOrDefault(x.Arg<Expression<Func<TEntity, Boolean>>>().Compile()));

            mock.Some(Arg.Any<Expression<Func<TEntity, Boolean>>>())
                .Returns(x => list.Where(x.Arg<Expression<Func<TEntity, Boolean>>>().Compile()));

            mock.All().Returns(list);

            mock.When(x => x.DeleteAsync(Arg.Any<TEntity>())).Do(x => list.Remove(x.Arg<TEntity>()));

            mock.When(x => x.UpdateAsync(Arg.Any<TEntity>(), Arg.Any<Func<TEntity, TEntity>>()))
                .Do(x => x.Arg<Func<TEntity, TEntity>>()(x.Arg<TEntity>()));

            mock.InsertAsync(Arg.Any<TEntity>())
                .Returns(x => Task.FromResult(list.Assign(li => li.Insert(list.Count - 1, x.Arg<TEntity>().Assign(e => e.ID = list.OrderBy(l => l.ID).Last().ID + 1)))
                    .OrderBy(li => li.ID).LastOrDefault()));

            mock.TopAsync(Arg.Any<Func<TEntity, Int64>>())
                .Returns(x => Task.FromResult(list.OrderBy(x.Arg<Func<TEntity, Int64>>()).First()));

            mock.OneAsync(Arg.Any<Expression<Func<TEntity, Boolean>>>()).Returns(x => Task.FromResult(
                list.SingleOrDefault(x.Arg<Expression<Func<TEntity, Boolean>>>().Compile())));

            mock.TheAsync(Arg.Any<Int64>()).Returns(x => Task.FromResult(list.SingleOrDefault(l => l.ID == x.Arg<Int64>())));

            mock.SomeAsync(Arg.Any<Expression<Func<TEntity, Boolean>>>())
                .Returns(x => Task.FromResult(list.Where(x.Arg<Expression<Func<TEntity, Boolean>>>().Compile())));

            mock.AllAsync().Returns(x => Task.FromResult(list.AsEnumerable()));

            mock.Context.Returns(x => new EntityContext());

            mock.Query.Returns(list.AsQueryable());

            mock.GetFault().Returns<Exception>(e => null);
            mock.GetFaultMessage().Returns("Success");
            mock.HasNoFault().Returns(true);
            mock.InsertedEntity.Returns(x => list.OrderBy(e => e.ID).Last());
            return mock;

        }


        public TEntity ShuffleAny() {

            var r = new Random();
            var id = r.Next(1, _Magnitude);
            return Seeder(id);

        }

        public IEnumerable<TEntity> TakeSome(Int32 magnitude) {

            for (int id = _Magnitude; id < _Magnitude + magnitude; id++) {
                yield return Seeder(id);
            }

        }

        #region Abstract

        protected abstract Func<Int64, TEntity> Seeder { get; }

        #endregion

        #region Protected

        protected int _Magnitude;

        protected String Resolve(Expression<Func<TEntity, Object>> expr) {

            var repr = expr.ToString();
            return repr.Substring(repr.LastIndexOf(".") + 1);

        }

        #endregion

    }

}
