﻿using Oneness.Protocols.RepositoryProtocols.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Mocking.Base {
    public interface IEntityMock<TEntity> {

        IRepositoryBaseAsync<TEntity> GenerateMock();
         TEntity ShuffleAny();
        IEnumerable<TEntity> TakeSome(Int32 magnitude);
    }
}
