﻿
using NSubstitute;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Oneness.Test.MVC.Extensions {

    public static class MvcMockHelpers {
        public static HttpContextBase FakeHttpContext() {
            var context = Substitute.For<HttpContextBase>();
            var request = Substitute.For<HttpRequestBase>();
            var response = Substitute.For<HttpResponseBase>();
            var session = Substitute.For<HttpSessionStateBase>();
            var server = Substitute.For<HttpServerUtilityBase>();

            return context;
        }

        public static void MakeAjaxRequest(this Controller controller, params KeyValuePair<String, Object>[] requestParameters) {

            var httpRequest = Substitute.For<HttpRequestBase>();
            httpRequest.Headers.Returns(
                new WebHeaderCollection() { { "X-Requested-With", "XMLHttpRequest" } }
            );
            var collection = new NameValueCollection();
            requestParameters.Enumerate(rp => collection.Add(rp.Key, rp.Value.ToType<String>()));

            var httpContext = Substitute.For<HttpContextBase>();
            httpRequest.Params.Returns(collection);
            httpContext.Request.Returns(httpRequest);
            controller.ControllerContext = new ControllerContext(httpContext, new RouteData(), controller);

        }

        public static HttpContextBase FakeHttpContext(string url) {
            HttpContextBase context = FakeHttpContext();
            context.Request.SetupRequestUrl(url);
            return context;
        }

        public static void SetFakeControllerContext<TController>(this TController controller)
                where TController : Controller {
            var httpContext = FakeHttpContext();
            var request = new RequestContext(httpContext, new RouteData());
            var context = new ControllerContext(request, controller);
            controller.ControllerContext = context;
        }

        static string GetUrlFileName(string url) {
            if (url.Contains("?"))
                return url.Substring(0, url.IndexOf("?"));
            else
                return url;
        }

        static NameValueCollection GetQueryStringParameters(string url) {
            if (url.Contains("?")) {
                var parameters = new NameValueCollection();

                string[] parts = url.Split("?".ToCharArray());
                string[] keys = parts[1].Split("&".ToCharArray());

                foreach (string key in keys) {
                    string[] part = key.Split("=".ToCharArray());
                    parameters.Add(part[0], part[1]);
                }

                return parameters;
            } else {
                return null;
            }
        }

        public static void SetHttpMethodResult(this HttpRequestBase request, string httpMethod) {

            Substitute.For<HttpRequestBase>(request).HttpMethod.Returns(httpMethod);

        }

        public static void SetupRequestUrl(this HttpRequestBase request, string url) {
            if (url == null)
                throw new ArgumentNullException("url");

            if (!url.StartsWith("~/"))
                throw new ArgumentException("Sorry, we expect a virtual url starting with \"~/\".");

            var mock = Substitute.For<HttpRequestBase>(request);

            mock.QueryString
                .Returns(GetQueryStringParameters(url));
            mock.AppRelativeCurrentExecutionFilePath
                .Returns(GetUrlFileName(url));
            mock.PathInfo
                .Returns(string.Empty);
        }
    }
}
