﻿

using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Shared.Extensions.Core;
using System;
using System.Linq;
using System.Threading;

namespace Oneness.ViewModel.EntityViewModels.Extensions {

    public static class QuizViewModelExtensions {

        public static QuizViewModel ToViewModel(this Quiz entity) {

            var vm = new QuizViewModel();
            entity.MapTo(ref vm);
            vm.SentenceViewModel = entity.Sentence.ToViewModel();
            vm.QuestionViewModels = entity.Questions
                .Select(sm => sm.ToViewModel()).ToArray();
            return vm;

        }
    }


}