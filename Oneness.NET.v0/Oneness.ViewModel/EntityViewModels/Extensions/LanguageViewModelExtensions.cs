﻿

using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Shared.Extensions.Core;
using System;
using System.Linq;
using System.Threading;

namespace Oneness.ViewModel.EntityViewModels.Extensions {

    public static class LanguageViewModelExtensions {

        public static LanguageViewModel ToViewModel(this Language entity) {

            var vm = new LanguageViewModel();
            entity.MapTo(ref vm);
            return vm;

        }

        public static LanguageListViewModel GetLanguageList(Int64 selectedLanguageID) {

            using (var context = new EntityContext()) {
                var all = context.Languages.ToList();
                var vmList = new LanguageListViewModel();
                vmList.SelectedLanguageID = selectedLanguageID;
                vmList.LanguageViewModels = all.Select(lang => lang.ToViewModel());
                return vmList;
            }

        }

    }
}