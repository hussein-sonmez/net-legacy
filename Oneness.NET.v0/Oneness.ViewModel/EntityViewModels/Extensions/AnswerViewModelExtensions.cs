﻿

using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Shared.Extensions.Core;
using System;
using System.Linq;
using System.Threading;

namespace Oneness.ViewModel.EntityViewModels.Extensions {


    public static class AnswerViewModelExtensions {

        public static AnswerViewModel ToViewModel(this Answer entity) {

            using (var context = new EntityContext()) {
                var vm = new AnswerViewModel();
                entity.MapTo(ref vm);
                vm.SentenceViewModel = entity.Sentence.ToViewModel();
                return vm;
            }

        }
    }
}