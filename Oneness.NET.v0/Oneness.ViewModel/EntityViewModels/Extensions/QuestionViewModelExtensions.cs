﻿

using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Shared.Extensions.Core;
using System;
using System.Linq;
using System.Threading;

namespace Oneness.ViewModel.EntityViewModels.Extensions {


    public static class QuestionViewModelExtensions {

        public static QuestionViewModel ToViewModel(this Question entity) {

            var vm = new QuestionViewModel();
            entity.MapTo(ref vm);
            vm.SentenceViewModel = entity.Sentence.ToViewModel();
            vm.AnswerViewModels = entity
                .Answers.Select(sm => sm.ToViewModel()).ToArray();
            return vm;

        }
    }


}