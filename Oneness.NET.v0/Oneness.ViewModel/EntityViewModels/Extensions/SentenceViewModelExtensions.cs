﻿

using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Shared.Extensions.Core;
using System;
using System.Linq;
using System.Threading;

namespace Oneness.ViewModel.EntityViewModels.Extensions {

    public static class SentenceViewModelExtensions {

        public static SentenceViewModel ToViewModel(this Sentence entity) {

            var vm = new SentenceViewModel();
            entity.MapTo(ref vm); ;
            return vm;

        }

        public static SentenceListViewModel GetSelectList(Int64 selectedSentenceID) {

            using (var context = new EntityContext()) {
                var all = context.Sentences.ToList();
                var vmList = new SentenceListViewModel();
                vmList.SentenceViewModels = all.Select(a => a.ToViewModel()).ToArray();
                vmList.SelectedSentenceID = selectedSentenceID;
                return vmList;
            }

        }

    }
}