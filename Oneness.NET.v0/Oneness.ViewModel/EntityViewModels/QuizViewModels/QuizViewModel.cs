﻿using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oneness.ViewModel.EntityViewModels.QuizViewModels {
    public class QuizViewModel : GenericViewModel {

        public SentenceViewModel SentenceViewModel { get; set; }

        public IEnumerable<QuestionViewModel> QuestionViewModels { get; set; }

    }

}