﻿using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oneness.ViewModel.EntityViewModels.QuizViewModels {

    public class AnswerViewModel : GenericViewModel {

        public Boolean IsCorrectMatch { get; set; }

        public SentenceViewModel SentenceViewModel { get; set; }

    }


}