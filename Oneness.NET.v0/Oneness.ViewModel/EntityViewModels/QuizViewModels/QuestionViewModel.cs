﻿using Oneness.Localization.Resources;
using Oneness.Localization.Static;
using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oneness.ViewModel.EntityViewModels.QuizViewModels {
    public class QuestionViewModel : GenericViewModel {

        private String _SelectedAnswer;
        [Required(ErrorMessageResourceType = typeof(Dialogs)
            , ErrorMessageResourceName = SLocalization.AnAnswerShouldBeSelected)]
        public String SelectedAnswer {
            get {
                return _SelectedAnswer ?? (_SelectedAnswer = "");
            }
            set {
                _SelectedAnswer = value;
            }
        }

        public SentenceViewModel SentenceViewModel { get; set; }

        public IEnumerable<AnswerViewModel> AnswerViewModels { get; set; }

    }

}