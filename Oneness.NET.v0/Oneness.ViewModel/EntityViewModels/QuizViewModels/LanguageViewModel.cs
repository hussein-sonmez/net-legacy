﻿using Oneness.Localization.Resources;
using Oneness.Localization.Static;
using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.ViewModel.EntityViewModels.QuizViewModels {
    public class LanguageViewModel : GenericViewModel {
        [Required(ErrorMessageResourceType = typeof(ValidationErrors)
            , ErrorMessageResourceName = SLocalization.NameCannotBeNull)]
        public String Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationErrors)
            , ErrorMessageResourceName = SLocalization.DescriptionCannotBeNull)]
        public String Description { get; set; }

       
    }


}
