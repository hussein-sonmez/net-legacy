﻿
using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.GenericBase;
using Oneness.ViewModel.EntityViewModels.Extensions;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Oneness.ViewModel.EntityViewModels.QuizViewModels {
    public class ManifestViewModel : GenericViewModel {

        public LanguageViewModel LanguageViewModel { get; set; }

        public SentenceViewModel SentenceViewModel { get; set; }



    }
    public static class ManifestViewModelExtensions {

        public static ManifestViewModel ToViewModel(this Manifest entity) {

            var vm = new ManifestViewModel();
            entity.MapTo(ref vm);
            vm.LanguageViewModel = entity.Language.ToViewModel();
            vm.SentenceViewModel = entity.Sentence.ToViewModel();
            return vm;

        }

    }

}