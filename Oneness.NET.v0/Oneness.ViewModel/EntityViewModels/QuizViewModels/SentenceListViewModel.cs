﻿
using Oneness.Localization.Resources;
using Oneness.Localization.Static;
using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Web;

namespace Oneness.ViewModel.EntityViewModels.QuizViewModels {
    public class SentenceListViewModel {

        [Required(ErrorMessageResourceType = typeof(ValidationErrors)
           , ErrorMessageResourceName = SLocalization.SentenceSelectionCannotBeNull)]
        public IEnumerable<SentenceViewModel> SentenceViewModels { get; set; }

        public Int64 SelectedSentenceID { get; set; }

    }


}