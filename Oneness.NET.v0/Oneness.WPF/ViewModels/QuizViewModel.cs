﻿using Microsoft.Practices.Prism.Commands;
using Oneness.WPF.ServiceReferenceQuiz;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Oneness.WPF.ViewModels {
    public class QuizViewModel : Microsoft.Practices.Prism.Mvvm.BindableBase {

        #region ctors
        public QuizViewModel() {
            var quizwcfClient = new QuizWCFClient();
            this.Quizzes = new ObservableCollection<QuizServiceModel>(
                quizwcfClient.Assign(qwcf => qwcf.Open()).GetAllQuizzes()
                );
            quizwcfClient.Close();
        }

        #endregion


        #region Properties


        public String QuizTitle {
            get {
                return _SelectedQuiz != null ?
                    _SelectedQuiz.Manifest.Sentences
                        .Single(s => s.Language.Name == Thread.CurrentThread.CurrentUICulture.Name).Text
                        : "No Title";
            }
            set {
                var sentence = _SelectedQuiz.Manifest.Sentences
                    .Single(s => s.Language.Name == Thread.CurrentThread.CurrentUICulture.Name);
                SetProperty<SentenceServiceModel>(ref sentence, new SentenceServiceModel() { Text = value, Language = sentence.Language });
            }
        }

        #endregion

        #region Observables

        public ObservableCollection<QuizServiceModel> Quizzes { get; set; }
        public ObservableCollection<QuestionServiceModel> Questions { get; set; }

        private QuizServiceModel _SelectedQuiz;
        public QuizServiceModel SelectedQuiz {
            get {
                return _SelectedQuiz;
            }
            set {
                SetProperty<QuizServiceModel>(ref this._SelectedQuiz, value);
                this.Questions = new ObservableCollection<QuestionServiceModel>(value.Questions);
            }
        }
        #endregion

        #region Commands


        #endregion

    }
}
