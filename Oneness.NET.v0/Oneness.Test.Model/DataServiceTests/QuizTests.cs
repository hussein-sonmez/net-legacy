﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Oneness.IoC.Core;
using Ninject;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.CoreData.Entity.POCO;

namespace Oneness.Test.Model.DataServiceTests {
    [TestClass]
    public class QuizTests {

        [TestMethod]
        public async Task ShuffleNewQuizProcedureOkay() {

            var quiz = await SInstanceProvider.Container.Get<IRepositoryBaseAsync<Quiz>>().TopAsync(e => e.ID);
            Assert.IsNotNull(quiz);

        }


    }
}
