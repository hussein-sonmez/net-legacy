﻿using Oneness.Definitions.WebConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using System.Web.Routing;

namespace Oneness.Definitions.Configurations {
    public static class CustomWebApiConfig
    {

        public static void Formatters(HttpConfiguration config) {

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.Remove(config.Formatters.JsonFormatter);
            config.Formatters.Add(SControllerConfig.GenerateFormatter());

        }

        public static void HttpConfig(HttpConfiguration config) {

            config.EnsureInitialized();

        }

    }
}
