﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using Shared.Extensions.Core;
using System.Web.Http;
using System.Web.Mvc;

namespace Oneness.Definitions.Configurations {

    public class CustomRouteConfig {
        public static void Routes(HttpConfiguration config) {

            config.MapHttpAttributeRoutes();
        }

        public static void RegisterMVCRoutes(RouteCollection routes) {

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

        }
    }
}
