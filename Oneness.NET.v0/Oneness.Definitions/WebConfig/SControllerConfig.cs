﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Oneness.Definitions.WebConfig {
    public static class SControllerConfig {

        public static class QuizConfig {

            public const String Prefix = "quiz";

            public static class HttpGet {
                public const String Index = "index";
                public const String Sentence = "sentence";
            }
            public static class HttpPost {
                
                public const String Question = "question";

            }

        }
        public static class ManifestConfig {

            public const String Prefix = "manifest";
            public static class HttpGet {
                public const String List = "list";
            }

        }
        public static class LanguageConfig {

            public const String Prefix = "language";
            public static class HttpGet {
                public const String List = "list";
            }
            public static class HttpPost {
                
                public const String NewLanguage = "new-language";

            }

        }

        public static class SentenceConfig {

            public const String Prefix = "sentence";
            public static class HttpGet {
                public const String Index = "index";
            }
            public static class HttpPost {
                
                public const String NewSentence = "new-sentence";

            }

        }

        public static JsonMediaTypeFormatter GenerateFormatter() {
           
            var ft = new JsonMediaTypeFormatter();
            ft.SerializerSettings.PreserveReferencesHandling =
                PreserveReferencesHandling.None;
            ft.SerializerSettings.ObjectCreationHandling =
                ObjectCreationHandling.Auto;
            ft.SerializerSettings.ReferenceLoopHandling =
                ReferenceLoopHandling.Ignore;
            return ft;
        }
    }
}
