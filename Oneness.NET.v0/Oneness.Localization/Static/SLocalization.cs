﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Localization.Static {
    public static class SLocalization {

        public const String AnAnswerShouldBeSelected= "AnAnswerShouldBeSelected";
        public const String NameCannotBeNull = "NameCannotBeNull";
        public const String DescriptionCannotBeNull = "DescriptionCannotBeNull";
        public const String TextCannotBeNull = "TextCannotBeNull";
        public const String LanguageSelectionCannotBeNull = "LanguageSelectionCannotBeNull";
        public const String SentenceSelectionCannotBeNull = "SentenceSelectionCannotBeNull";
    }
}
