﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Oneness.Localization.Resources {
    public static class TranslationExtensions {

        public static String Translate(this String key) {

            var resources = new Object[] { new Dialogs(), new Forms(), new ValidationErrors() };
            var translations = resources.Where(
                src => src.GetType().GetProperties()
                    .Where(pi => pi.Name == key).FirstOrDefault() != null)
                    .Select(src=>
                        src.GetType().GetProperties().Where(pi => pi.Name == key).First()
                            .GetValue(src, null).ToString());

            var translation = translations.FirstOrDefault();
            if (translation == null) {
                throw new KeyNotFoundException(key);
            }
            return translation;
        }

    }
}
