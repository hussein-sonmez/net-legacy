﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oneness.Definitions.Configurations;
using Oneness.WebApi.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace Oneness.Test.WebApi.ControllerTests {
    [TestClass]
    public class QuizControllerTest {

        [TestMethod]
        public async Task QuizControllerShuffles() {
            // arrange
            var controller = new QuizController();

            controller.Request = new HttpRequestMessage {
                RequestUri = new Uri("http://localhost/onapi/quiz/index")
            };
            controller.Configuration = new HttpConfiguration();
            CustomRouteConfig.Routes(controller.Configuration);
            CustomWebApiConfig.Formatters(controller.Configuration);
            CustomWebApiConfig.HttpConfig(controller.Configuration);

            // act
            //product product = new product() { ıd = 42, name = "product1" };
            //var response = await controller.shufflenew();

            var response = await controller.Index();
            Assert.IsNotNull(response);
        }

    }
}
