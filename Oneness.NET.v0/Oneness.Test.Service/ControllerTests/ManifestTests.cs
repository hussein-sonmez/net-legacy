﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oneness.IoC.Core;
using Oneness.Protocols.RepositoryProtocols;
using Ninject;
using System.Threading.Tasks;
using Shared.Extensions.Core;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.CoreData.Entity.POCO;

namespace Oneness.Test.Service.ControllerTests {
    [TestClass]
    public class ManifestTests {
        [TestMethod]
        public async Task ManifestCollectsProperly() {

            var engine = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Manifest>>();
            var response = await engine.AllAsync();

            response.Enumerate(e => Assert.IsNotNull(e.Language)).Enumerate(e => Assert.IsNotNull(e.Sentence));

        }
    }
}
