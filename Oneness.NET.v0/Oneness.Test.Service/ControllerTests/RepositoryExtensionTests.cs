﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oneness.IoC.Core;
using Ninject;
using Oneness.Protocols.RepositoryProtocols;
using Shared.Extensions.Core;
using System.Threading.Tasks;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.CoreData.Entity.POCO;

namespace Oneness.Test.Service.ControllerTests {
    [TestClass]
    public class RepositoryExtensionTests {
        [TestMethod]
        public async Task CollectRunsProperly() {

            var quizService = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Quiz>>();

            var quiz = await quizService.TopAsync(e=>e.ID);
            Assert.IsNotNull(quiz.Sentence);
            Assert.IsNotNull(quiz.Questions);
            quiz.Questions.Enumerate(qs => Assert.IsNotNull(qs.Sentence))
                .Enumerate(qs => Assert.IsNotNull(qs.Answers))
                .Enumerate(qs => Assert.AreNotEqual(0, qs.Answers.Count));
        }
    }
}
