﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oneness.IoC.Core;
using Oneness.Protocols.RepositoryProtocols;
using Ninject;
using System.Threading.Tasks;
using Shared.Extensions.Core;
using Oneness.CoreData.Entity.POCO;
using Oneness.Protocols.RepositoryProtocols.Base;

namespace Oneness.Test.Service.ControllerTests {
    [TestClass]
    public class QuizTests {
        [TestMethod]
        public async Task CollectsAllTests() {

            var engine = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Quiz>>();
            var collected = await engine.AllAsync();
            collected
                .Enumerate(qz => Assert.IsNotNull(qz.Questions, "qz.Questions"))
                .Enumerate(qz => qz.Questions.Enumerate(q => Assert.IsNotNull(q, "q")));

        }

        [TestMethod]
        public async Task CollectsTheTests() {

            var engine = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Sentence>>();
            var first = await engine.TopAsync(m => m.ID);
            var c = await engine.TheAsync(first.ID);
            c.InstanceEqual(first).Assert<InvalidCastException>("{0} not equal to {1}".PostFormat(c.ID, first.ID));
        }

        [TestMethod]
        public async Task DeletesProperlyTests() {

            var engine = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Sentence>>();
            var quiz1 = await engine.InsertAsync(new Sentence() {
                Text = "Quiz-{0}".PostFormat(Guid.NewGuid().ToString("N"))
            });
            var first = await engine.OneAsync(s => s.Text == quiz1.Text);
            await engine.DeleteAsync(first);

            var shouldBeNull = await engine.TheAsync(first.ID);
            Assert.IsNull(shouldBeNull);

        }
    }
}
