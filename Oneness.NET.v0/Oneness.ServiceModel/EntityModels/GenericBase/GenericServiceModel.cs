﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.ServiceModel.EntityModels.GenericBase {
    [DataContract]
    public class GenericServiceModel {

        [DataMember]
        public Int64 ID { get; set; }

    }
}
