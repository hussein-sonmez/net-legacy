﻿using Oneness.CoreData.Entity.POCO;
using Oneness.ServiceModel.EntityModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Oneness.ServiceModel.EntityModels {
    [DataContract]
    public class ManifestServiceModel : GenericServiceModel {

        [DataMember]
        public SentenceServiceModel SentenceServiceModel { get; set; }
        [DataMember]
        public LanguageServiceModel LanguageServiceModel { get; set; }


    }

    public static class ManifestServiceModelExtensions {

        public static ManifestServiceModel ToServiceModel(this Manifest manifest) {


            var sm = new ManifestServiceModel();
            manifest.MapTo(ref sm);
            sm.SentenceServiceModel = manifest.Sentence.ToServiceModel();
            sm.LanguageServiceModel = manifest.Language.ToServiceModel();
            return sm;

        }

    }
    
}
