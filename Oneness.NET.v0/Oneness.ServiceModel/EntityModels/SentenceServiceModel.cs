﻿using Oneness.CoreData.Entity.POCO;
using Oneness.ServiceModel.EntityModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.ServiceModel.EntityModels {
    [DataContract]
    public class SentenceServiceModel : GenericServiceModel {

        [DataMember]
        public String Text { get; set; }

    }

    public static class SentenceServiceModelExtensions {
        public static SentenceServiceModel ToServiceModel(this Sentence sentence) {

            var sm = new SentenceServiceModel();
            sentence.MapTo(ref sm);
            return sm;

        }

    }

}
