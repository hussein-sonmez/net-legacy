﻿using Oneness.CoreData.Entity.POCO;
using Oneness.ServiceModel.EntityModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.ServiceModel.EntityModels {
    [DataContract]
    public class QuestionServiceModel : GenericServiceModel {

        [DataMember]
        public Boolean IsCorrectMatch { get; set; }

        [DataMember]
        public SentenceServiceModel Sentence { get; set; }

        [DataMember]
        public IEnumerable<AnswerServiceModel> Answers { get; set; }

        [DataMember]
        public Int64 QuizID { get; set; }


    }

    public static class QuestionServiceExtensions {
        public static QuestionServiceModel ToServiceModel(this Question question) {

            var sm = new QuestionServiceModel();
            question.MapTo(ref sm);

            sm.Sentence = question.Sentence.ToServiceModel();
            sm.Answers = question.Answers.Select<Answer, AnswerServiceModel>(aw => aw.ToServiceModel());

            return sm;

        }
    }
    
}
