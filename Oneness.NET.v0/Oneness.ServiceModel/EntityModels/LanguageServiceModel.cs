﻿using Oneness.CoreData.Entity.POCO;
using Oneness.ServiceModel.EntityModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.ServiceModel.EntityModels {
    [DataContract]
    public class LanguageServiceModel : GenericServiceModel {

        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public String Description { get; set; }

    }

    public static class LanguageServiceModelExtensions {

        public static LanguageServiceModel ToServiceModel(this Language lang) {

            var sm = new LanguageServiceModel();
            lang.MapTo(ref sm);
            return sm;

        }
    }

}
