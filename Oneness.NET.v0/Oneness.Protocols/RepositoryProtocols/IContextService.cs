﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Protocols.RepositoryProtocols {
    public interface IContextService {

        Task<IContextService> SeedDatabase();
        IContextService TruncateDatabase();
        Boolean Succeeded { get; }
        Exception ThrownException { get; }

    }
}
