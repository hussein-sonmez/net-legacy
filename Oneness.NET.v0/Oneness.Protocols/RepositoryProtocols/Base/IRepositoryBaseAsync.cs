﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Protocols.RepositoryProtocols.Base {
    public interface IRepositoryBaseAsync<TEntity> : IRepositoryBase<TEntity> {

        Task<TEntity> TopAsync<TProperty>(Func<TEntity, TProperty> orderSelector);
        Task<IEnumerable<TEntity>> AllAsync();
        Task<TEntity> TheAsync(Int64 key);
        Task<IEnumerable<TEntity>> SomeAsync(Expression<Func<TEntity, Boolean>> selector);
        Task<TEntity> OneAsync(Expression<Func<TEntity, Boolean>> selector);
        Task DeleteAsync(TEntity entity);
        Task<TEntity> InsertAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity, Func<TEntity, TEntity> modifier);

        #region Parameters

        TEntity InsertedEntity { get; }

        #endregion

    }
}
