﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Protocols.RepositoryProtocols.Base {
    public interface IRepositoryBase<TEntity> {

        TEntity Top<TProperty>(Func<TEntity, TProperty> orderSelector);
        IEnumerable<TEntity> All();
        TEntity The(Int64 key);
        IEnumerable<TEntity> Some(Expression<Func<TEntity, Boolean>> selector);
        TEntity One(Expression<Func<TEntity, Boolean>> selector);
        void Delete(TEntity entity);
        TEntity Insert(TEntity entity);
        TEntity Update(TEntity entity, Func<TEntity, TEntity> modifier);

        #region Parameters
        DbContext Context { get; }
        IQueryable Query { get; }
        Boolean HasNoFault();
        Exception GetFault();
        String GetFaultMessage();
        #endregion

    }
}
