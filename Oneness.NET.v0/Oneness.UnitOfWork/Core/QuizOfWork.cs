﻿using System;
using System.Threading.Tasks;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.Protocols.RepositoryProtocols;
using Ninject;
using Oneness.UnitOfWork.Base;
using Shared.Extensions.Core;
using System.Collections.Generic;
using Oneness.CoreData.Entity.POCO;
using Oneness.IoC.Core;
using Oneness.Mocking.Base;
using Oneness.UnitOfWork.Protocols;

namespace Oneness.UnitOfWork.Core {
    internal class QuizOfWork : BaseUnitOfWork<Quiz>, IQuizOfWork {


        public async Task<Quiz> TopQuiz<TProperty>(Func<Quiz, TProperty> orderby) {

            var response = await this.RunWorker<Task<Quiz>>(async (qz, qs, qw, snt) => {
                var quiz = await qz.TopAsync<TProperty>(orderby);
                return quiz;
            });
            return response;

        }

        public async Task<Quiz> QuizByID(Int64 ID) {

            var response = await this.RunWorker<Task<Quiz>>(async (qz, qs, qw, snt) => {
                var quiz = await qz.TheAsync(ID);
                return quiz;
            });
            return response;

        }

        public TResponse RunWorker<TResponse>(Func<IRepositoryBaseAsync<Quiz>, IRepositoryBaseAsync<Question>, IRepositoryBaseAsync<Answer>, IRepositoryBaseAsync<Sentence>, TResponse> worker) {

            var qzobj = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Quiz>>();
            var qskobj = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Question>>();
            var awobj = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Answer>>();
            var sntobj = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Sentence>>();

            return worker(qzobj, qskobj, awobj, sntobj);

        }

        public async Task<IRepositoryBaseAsync<Quiz>> DeleteQuiz(Int64 quizID) {

            quizID.AssertNotNull();

            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Quiz>>>(async (qz, qs, qw, snt) => {
                var quiz = await qz.TheAsync(quizID);
                await qz.DeleteAsync(quiz);
                return qz;
            });
            return response;

        }

        public async Task<IRepositoryBaseAsync<Quiz>> UpdateQuiz(Int64 quizID, Func<Quiz, Quiz> modifier) {

            quizID.AssertNotNull();

            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Quiz>>>(async (qz, qs, qw, snt) => {
                var quiz = await qz.TheAsync(quizID);
                await qz.UpdateAsync(quiz, modifier);
                return qz;
            });
            return response;
        }

        public async Task<IRepositoryBaseAsync<Quiz>> InsertQuiz(Quiz quiz) {

            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Quiz>>>(async (qz, qs, qw, snt) => {
                await qz.InsertAsync(quiz);
                return qz;
            });
            return response;
        }

        public async Task<IEnumerable<Quiz>> AllQuizzes() {

            var response = await this.RunWorker<Task<IEnumerable<Quiz>>>(async (qz, qs, qw, snt) => {
                var all = await qz.AllAsync();
                return all;
            });
            return response;

        }
    }
}
