﻿
using Ninject;
using Oneness.UnitOfWork.Core;
using Oneness.UnitOfWork.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.UnitOfWork.Base {
    public static class SUnitProvider {

        static SUnitProvider() {
            Container.Bind<IManifestOfWork>().To<ManifestOfWork>();
            Container.Bind<IQuizOfWork>().To<QuizOfWork>();

        }

        private static Lazy<IKernel> _KernelProvider = new Lazy<IKernel>(() => new StandardKernel());
        public static IKernel Container {
            get {
                return _KernelProvider.Value;
            }
        }

    }
}
