﻿
using Oneness.CoreData.Entity.POCO;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.Protocols.RepositoryProtocols.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.UnitOfWork.Protocols {
    public interface IManifestOfWork : IUnitOfWork {

        Task<IRepositoryBaseAsync<Manifest>> DeleteManifest(Int64 manifestID);
        Task<IRepositoryBaseAsync<Sentence>> UpdateSentence(Int64 sentenceID, Func<Sentence, Sentence> modifier);
        Task<IRepositoryBaseAsync<Sentence>> InsertSentence(Sentence sentence);
        Task<IRepositoryBaseAsync<Language>> UpdateLanguage(Int64 languageID, Func<Language, Language> modifier);
        Task<IRepositoryBaseAsync<Language>> InsertLanguage(Language language);

        TResponse RunWorker<TResponse>(Func<IRepositoryBaseAsync<Manifest>, IRepositoryBaseAsync<Sentence>, IRepositoryBaseAsync<Language>, TResponse> worker);
        Task<IEnumerable<Sentence>> AllSentences();
        Task<IEnumerable<Language>> AllLanguages();
        Task<IEnumerable<Manifest>> AllManifests();
        Task<IRepositoryBaseAsync<Sentence>> DeleteSentence(Int64 sentenceID);
        Task<IRepositoryBaseAsync<Language>> DeleteLanguage(Int64 languageID);
        Task<IRepositoryBaseAsync<Manifest>> UpdateManifest(Int64 manifestID, Int64 languageID, Int64 sentenceID, Func<Manifest, Manifest> modifier );
        Task<IRepositoryBaseAsync<Manifest>> InsertManifest(Int64 languageID, Int64 sentenceID);
    }
}
