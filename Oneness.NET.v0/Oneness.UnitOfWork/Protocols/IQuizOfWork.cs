﻿
using Oneness.CoreData.Entity.POCO;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.Protocols.RepositoryProtocols.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.UnitOfWork.Protocols {
    public interface IQuizOfWork : IUnitOfWork {

        Task<IRepositoryBaseAsync<Quiz>> DeleteQuiz(Int64 quizID);
        Task<IRepositoryBaseAsync<Quiz>> UpdateQuiz(Int64 quizID, Func<Quiz, Quiz> modifier);
        Task<IRepositoryBaseAsync<Quiz>> InsertQuiz(Quiz quiz);
        TResponse RunWorker<TResponse>(Func<IRepositoryBaseAsync<Quiz>, IRepositoryBaseAsync<Question>, IRepositoryBaseAsync<Answer>, IRepositoryBaseAsync<Sentence>, TResponse> worker);
        Task<IEnumerable<Quiz>> AllQuizzes();
        Task<Quiz> QuizByID(Int64 ID);
        Task<Quiz> TopQuiz<TProperty>(Func<Quiz, TProperty> orderby);
    }
}
