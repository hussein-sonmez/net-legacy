﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Oneness.IoC.Core;
using Oneness.Protocols.RepositoryProtocols;

namespace Oneness.Test.CoreData.Persistence {
    [TestClass]
    public class SeedDatabaseTests {
        [TestMethod]
        public async Task SeedDatabaseRunsOkay() {

            var context = SInstanceProvider.Container.Get<IContextService>();
            await context.SeedDatabase();
            Assert.IsTrue(context.Succeeded);

        }
    }
}
