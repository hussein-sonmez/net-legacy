﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Oneness.CoreData.Entity.POCO;
using Oneness.Definitions.CoreDataDefinitions;
using Oneness.IoC.Core;
using Oneness.Mocking.Base;
using Oneness.Mocking.Mocks.TranslationScheme;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.Protocols.RepositoryProtocols.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Test.CoreData.EntityMocks {

    [TestClass]
    public class LanguageMockTests {

        [TestMethod]
        public void MockInitsProperly() {

            var langMock = SInstanceProvider.Container.Get<IEntityMock<Language>>().GenerateMock();
            var all = langMock.All();
            Assert.AreEqual(SCoreDataConfig.PredefinedMagnitude, all.Count());

        }

        [TestMethod]
        public void MockInjectsProperly() {

            var quizService = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Quiz>>();
            var all = quizService.All();
            Assert.AreNotEqual(0, all.Count());

        }

    }
}
