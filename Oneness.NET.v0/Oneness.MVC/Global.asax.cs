﻿using Oneness.Definitions.Configurations;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Oneness.MVC {

    public class MvcApplication : HttpApplication {

        protected void Application_Start() {

            BeginRequest += OnBeginRequest;
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            CustomRouteConfig.RegisterMVCRoutes(RouteTable.Routes);

        }
        protected void OnBeginRequest(object sender, System.EventArgs e) {
            if (Request.UserLanguages != null && Request.UserLanguages.Any()) {
                var cultureInfo = new CultureInfo(Request.UserLanguages[0]);
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
            }
        }
    }
}
