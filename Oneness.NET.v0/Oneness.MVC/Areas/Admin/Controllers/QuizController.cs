﻿
using Oneness.CoreData.Entity.POCO;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.UnitOfWork.Core;
using Oneness.UnitOfWork.Protocols;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oneness.ViewModel.EntityViewModels.Extensions;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.UnitOfWork.Base;
using Ninject;

namespace Oneness.MVC.Areas.Admin.Controllers {
    public class QuizController : Controller {

        #region Actions

        // GET: Admin/Language
        [HttpGet]
        public async Task<ActionResult> Index() {

            var response = await UnitOfWork.AllQuizzes();

            return View(response.Select(m => m.ToViewModel()));

        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public async Task<JsonResult> Add() {

            if (Request.IsAjaxRequest()) {

                IRepositoryBaseAsync<Quiz> response;
                String text = Convert.ToString(Request.Params["Text"]);

                Int64 quizID = Convert.ToInt64(Request.Params["CurrentQuizID"]);
                if (quizID != default(Int64)) {
                    response = await UnitOfWork.UpdateQuiz(quizID, qz => qz.Assign(qzz => qzz.Sentence.Text = text));
                } else {
                    response = await UnitOfWork.InsertQuiz(new Quiz() {
                        Sentence = new Sentence() {
                            Text = text
                        }
                    });
                }
                return Json(new {
                    faultMessage = !response.HasNoFault() ? response.GetFaultMessage() : "Success",
                    done = response.HasNoFault()
                });
            } else {
                return Json(new {
                    fullPostback = true
                });
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public async Task<JsonResult> Delete() {

            if (Request.IsAjaxRequest()) {

                Int64 quizID = Convert.ToInt64(Request["QuizIDToBeDeleted"]);

                var response = await UnitOfWork.DeleteQuiz(quizID);

                return Json(new {
                    faultMessage = response.HasNoFault() ? "Success" : response.GetFaultMessage(),
                    done = response == null
                });

            } else {
                return Json(new {
                    fullPostback = true
                });
            }
        }


        #endregion

        #region UnitOfWork

        private IQuizOfWork UnitOfWork {
            get {
                return SUnitProvider.Container.Get<IQuizOfWork>();
            }
        }

        #endregion


    }
}