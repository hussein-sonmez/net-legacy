﻿using Oneness.CoreData.Entity.POCO;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.UnitOfWork.Core;
using Oneness.UnitOfWork.Protocols;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oneness.UnitOfWork.Base;
using Ninject;

namespace Oneness.MVC.Areas.Admin.Controllers {
    [OutputCache(Duration = 60)]
    public class ManifestController : Controller {

        #region Actions

        // GET: Admin/Manifest
        [HttpGet]
        public async Task<ActionResult> Index() {

            var response = await UnitOfWork.AllManifests();

            return View(response.Select(m => m.ToViewModel()).ToArray());

        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public async Task<ActionResult> Add() {

            if (Request.IsAjaxRequest()) {
                IRepositoryBaseAsync<Manifest> response;

                Int64 manifestID = Convert.ToInt64(Request["CurrentManifestID"]);

                Int64 languageID = Convert.ToInt64(Request["SelectedLanguageID"]);
                Int64 sentenceID = Convert.ToInt64(Request["SelectedSentenceID"]);

                if (manifestID != default(Int64)) {
                    response = await UnitOfWork.UpdateManifest(manifestID, languageID, sentenceID, mf => mf);
                } else {
                    response = await UnitOfWork.InsertManifest(languageID, sentenceID);
                }
                return View("Index");

            } else {
                return View("Index");
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public async Task<ActionResult> Delete() {

            if (Request.IsAjaxRequest()) {

            Int64 manifestID = Convert.ToInt64(Request["ManifestIDToBeDeleted"]);
                manifestID.AssertNotNull();
                await UnitOfWork.DeleteManifest(manifestID);

                return View("Index");

            } else {
                return View("Index");
            }
        }


        #endregion

        #region UnitOfWork

        private IManifestOfWork UnitOfWork {
            get {
                return SUnitProvider.Container.Get<IManifestOfWork>();
            }
        }

        #endregion


    }
}