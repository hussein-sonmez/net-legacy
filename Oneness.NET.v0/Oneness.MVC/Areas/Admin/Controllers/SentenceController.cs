﻿using Oneness.CoreData.Entity.POCO;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.UnitOfWork.Core;
using Oneness.UnitOfWork.Protocols;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oneness.ViewModel.EntityViewModels.Extensions;
using Oneness.UnitOfWork.Base;
using Ninject;

namespace Oneness.MVC.Areas.Admin.Controllers {
    [OutputCache(Duration = 60)]
    public class SentenceController : Controller {


        #region Actions

        // GET: Admin/Sentence
        [HttpGet]
        public async Task<ActionResult> Index() {

            var response = await UnitOfWork.AllSentences();

            return View(response.Select(m => m.ToViewModel()));

        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public async Task<ActionResult> Add() {

            if (Request.IsAjaxRequest()) {

                var text = Request.Form["Text"].ToType<String>();
                var sentenceID = Request.Form["CurrentSentenceID"].ToType<Int64>();

                if (sentenceID != default(Int64)) {
                    var response = await UnitOfWork.UpdateSentence(sentenceID, sent => sent.Assign(snt => snt.Text = text));
                } else {
                    var response = await UnitOfWork.InsertSentence(new Sentence() {
                        Text = text
                    });
                }

                return View("Index");

            } else {
                return View("Index");
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public async Task<ActionResult> Delete() {

            if (Request.IsAjaxRequest()) {
                var sentenceID = Request["SentenceIDToBeDeleted"].ToType<Int64>();

                var response = await UnitOfWork.DeleteSentence(sentenceID);

                return View("Index");

            } else {
                return View("Index");
            }
        }


        #endregion


        #region UnitOfWork

        private IManifestOfWork UnitOfWork {
            get {
                return SUnitProvider.Container.Get<IManifestOfWork>();
            }
        }

        #endregion

    }
}