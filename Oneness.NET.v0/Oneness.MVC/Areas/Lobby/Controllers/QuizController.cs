﻿using Ninject;
using Oneness.IoC.Core;
using Oneness.Localization.Resources;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oneness.ViewModel.EntityViewModels.Extensions;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.CoreData.Entity.POCO;

namespace Oneness.MVC.Areas.Lobby.Controllers
{
    [OutputCache(Duration = 60)]
    public class QuizController : Controller
    {

        [HttpGet]
        public async Task<ActionResult> Index() {

            var response = await SInstanceProvider.Container.Get<IRepositoryBaseAsync<Quiz>>().TopAsync(e => e.ID);
            return View(response.ToViewModel());

        }

        [HttpPost]
        public async Task<JsonResult> PostAnswer(QuestionViewModel viewModel) {

            if (Request.IsAjaxRequest()) {
                var qsid = Request["CurrentQuestionID"].ToType<Int64>();
                var qzid = Request["CurrentQuizID"].ToType<Int64>();
                var selectedAnswerID = Request["SelectedAnswer"].ToType<Int64>();
                var done = (await SInstanceProvider.Container
                    .Get<IRepositoryBaseAsync<Answer>>().TheAsync(selectedAnswerID)) != null;
                return await Task.Factory.StartNew<JsonResult>(() =>
                    Json(new {
                        qzid = qzid, qsid = qsid, done = done,
                        ModalTitle = "{0}ModalTitle".PostFormat(done ? "Done" : "Failed").Translate(),
                        ModalBody = "{0}ModalBody".PostFormat(done ? "Done" : "Failed").Translate()
                    }));
            } else {
                return await Task.Factory.StartNew<JsonResult>(() =>
                    Json(new { awid = Request["SelectedAnswer"] }));
            }

        }

    }
}