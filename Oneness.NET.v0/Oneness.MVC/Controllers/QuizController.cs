﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using Ninject;
using Oneness.IoC.Core;
using Oneness.Localization.Resources;
using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.Protocols.RepositoryProtocols;
using Shared.Extensions.Core;
using Oneness.Definitions.WebConfig;
using System.Web.UI.WebControls;
using System.Dynamic;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Oneness.ViewModel.EntityViewModels.Extensions;
using Oneness.Protocols.RepositoryProtocols.Base;

namespace Oneness.MVC.Controllers {

    //[OutputCache(Duration = 60)]
    public class QuizController : Controller {

        #region Actions

        [HttpGet]
        public async Task<ActionResult> Index() {

            var response = await SInstanceProvider.Container.Get<IRepositoryBaseAsync<Quiz>>().TopAsync(e => e.ID);
            return View(response.ToViewModel());

        }

        [HttpPost]
        public async Task<JsonResult> PostAnswer(QuestionViewModel viewModel) {

            if (Request.IsAjaxRequest()) {
                var qsid = Request["CurrentQuestionID"].ToType<Int64>();
                var qzid = Request["CurrentQuizID"].ToType<Int64>();
                var selectedAnswerID = Request["SelectedAnswer"].ToType<Int64>();
                var done = (await SInstanceProvider.Container
                    .Get<IRepositoryBaseAsync<Answer>>().TheAsync(selectedAnswerID)) != null;
                return await Task.Factory.StartNew<JsonResult>(() =>
                    Json(new {
                        qzid = qzid, qsid = qsid, done = done,
                        ModalTitle = "{0}ModalTitle".PostFormat(done ? "Done" : "Failed").Translate(),
                        ModalBody = "{0}ModalBody".PostFormat(done ? "Done" : "Failed").Translate()
                    }));
            } else {
                return await Task.Factory.StartNew<JsonResult>(() =>
                    Json(new { awid = Request["SelectedAnswer"] }));
            }

        }

        #endregion

    }
}