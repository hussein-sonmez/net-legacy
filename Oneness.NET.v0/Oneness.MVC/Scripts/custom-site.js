﻿$body = $("body");

$(document).on({
    ajaxStart: function () { $body.addClass("loading"); },
    ajaxStop: function () { $body.removeClass("loading"); }
});

function ShowForm(diff) {

    diff = diff || 0;
    $("#modal-body-" + diff)
        .addClass('text-success')
        .height(140);
    $("#img-modal-body-" + diff).attr("src", "../Images/verified.png");
    var $trigger = $('#bootstrap-modal-trigger-' + diff);
    $("#modal-body-save-" + diff).click(function (e) {
        $("body-" + diff).submit();
    })
    $trigger.click();
    return false;

}