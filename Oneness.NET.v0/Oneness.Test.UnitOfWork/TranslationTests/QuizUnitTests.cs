﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oneness.UnitOfWork.Core;
using System.Collections.Generic;
using Oneness.Protocols.RepositoryProtocols;
using System.Threading.Tasks;
using Shared.Extensions.Core;
using Oneness.CoreData.Entity.POCO;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.UnitOfWork.Protocols;
using Oneness.UnitOfWork.Base;
using Ninject;

namespace Oneness.Test.UnitOfWork.TranslationTests {
    [TestClass]
    public class QuizUnitTests {
        [TestMethod]
        public async Task QuizInsertWorksProperly() {

            var quizUnit = SUnitProvider.Container.Get<IQuizOfWork>();

            var response = await quizUnit.InsertQuiz(new Quiz() {
                Sentence = new Sentence() {
                    Text = "inserted by test<{0}>".GenerateIdentifier()
                }
            });
            response.HasNoFault().Assert<InvalidOperationException>("quiz insert failed: {0}".PostFormat(response.GetFaultMessage()));

            var existing = await quizUnit.QuizByID(response.InsertedEntity.ID);

          
            Assert.AreEqual(response.InsertedEntity.Sentence.Text, existing.Sentence.Text);
            await response.DeleteAsync(response.InsertedEntity);
            response.HasNoFault().Assert<InvalidOperationException>("Delete not succeeded");

        }
        [TestMethod]
        public async Task QuizUpdateWorksProperly() {

            var quizUnit = SUnitProvider.Container.Get<IQuizOfWork>();

            var quiz = await quizUnit.TopQuiz(e => e.ID);

            string text = "updated by test".GenerateIdentifier();

            IRepositoryBaseAsync<Quiz> response = await quizUnit.UpdateQuiz(quiz.ID, qz => qz.Assign(qzz => qzz.Sentence.Text = text));
            var changed = await quizUnit.QuizByID(quiz.ID);

            Assert.AreEqual(text, changed.Sentence.Text);
        }
    }
}
