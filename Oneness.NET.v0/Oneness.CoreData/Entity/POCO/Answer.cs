﻿using Oneness.CoreData.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.CoreData.Entity.POCO {
    
    public class Answer : BaseModel {

        #region Properties

        
        public Boolean IsCorrectMatch { get; set; }

        #endregion

        #region Navigation Properties


        public virtual Sentence Sentence { get; set; }

        public virtual Question Question { get; set; }

        #endregion

    }


}
