﻿using Oneness.CoreData.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.CoreData.Entity.POCO {
    
    public class Manifest : BaseModel {

        #region Properties

        #endregion

        #region Navigation Properties

        public virtual Language Language { get;  set; }
        public virtual Sentence Sentence { get; set; }

        #endregion

    }
}
