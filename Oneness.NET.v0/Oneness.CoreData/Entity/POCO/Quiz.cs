﻿using Oneness.CoreData.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.CoreData.Entity.POCO {
    
    public class Quiz : BaseModel {

        #region Ctor

        public Quiz() {

            Questions = new List<Question>();

        }

        #endregion

        #region Properties

        #endregion

        #region Navigation Properties

        public virtual ICollection<Question> Questions { get; set; }

        public virtual Sentence Sentence { get; set; }

        #endregion

    }
}
