﻿
using Oneness.CoreData.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.CoreData.Entity.POCO {
    
    public class Question : BaseModel {

        #region Ctor

        public Question() {
            Answers = new List<Answer>();
        }

        #endregion

        #region Properties

        #endregion

        #region Navigation Properties

        public Quiz Quiz { get; set; }
        
        public virtual Sentence Sentence { get; set; }
        
        public virtual ICollection<Answer> Answers { get; set; }

        #endregion

    }
}
