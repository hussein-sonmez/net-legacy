﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oneness.CoreData.Entity.Base;
using Oneness.CoreData.Entity.POCO;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oneness.CoreData.Entity.Mapping {
    public class QuestionMapping : BaseMapping<Question> {

        public QuestionMapping() {

            #region Properties

            #endregion

            #region Navigation Properties


            this.HasOptional(t => t.Sentence).WithMany(t=>t.Questions).Map(x => x.MapKey("SentenceID")).WillCascadeOnDelete(false);

            this.HasOptional(t => t.Quiz).WithMany(t => t.Questions).Map(x => x.MapKey("QuizID"));

            #endregion

            this.ToTable("Questions", "Translation");

        }

    }
}
