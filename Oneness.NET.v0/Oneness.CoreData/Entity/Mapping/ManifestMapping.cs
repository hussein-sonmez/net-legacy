﻿using Oneness.CoreData.Entity.Base;
using Oneness.CoreData.Entity.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.CoreData.Entity.Mapping {
    public class ManifestMapping : BaseMapping<Manifest> {

        #region Ctor

        public ManifestMapping() {

            #region Properties

            #endregion

            #region Relations

            this.HasOptional(t => t.Sentence).WithMany(r => r.Manifests).Map(x => x.MapKey("SentenceID")).WillCascadeOnDelete(false);


            this.HasOptional(t => t.Language).WithMany(r => r.Manifests).Map(x => x.MapKey("LanguageID"));

            #endregion

            this.ToTable("Manifests", "Translation");

        }

        #endregion

    }
}
