﻿using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.Base;
using Oneness.Protocols.RepositoryProtocols.Base;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Repository.Core.Base {
    public class RepositoryAsyncBase<TEntity> : RepositoryBase<TEntity>, IRepositoryBaseAsync<TEntity>
        where TEntity : BaseModel {

        #region Private

        private async Task Persist() {
            try {
                await Context.SaveChangesAsync();
                _Faulted = false;
            } catch (Exception ex) {
                _Fault = ex;
                _Faulted = true;
            }
        }

        #endregion

        #region Implementation of IServiceBase<TModel>
        
        public async Task<TEntity> TopAsync<TKey>(Func<TEntity, TKey> keySelector) {

            var all = await AllAsync();
            var top = all.OrderBy(keySelector).FirstOrDefault();
            return top;

        }
        public async Task<IEnumerable<TEntity>> AllAsync() {

            var all = await Query.ToListAsync();
            return all.Select(a => a as TEntity);

        }

        public async Task<TEntity> TheAsync(Int64 key) {

            var the = await Query.OfType<TEntity>().SingleOrDefaultAsync(e => e.ID == key);
            return the;

        }

        public async Task<IEnumerable<TEntity>> SomeAsync(Expression<Func<TEntity, Boolean>> selector) {

            return await Query.OfType<TEntity>().Where(selector).ToListAsync();

        }

        public async Task<TEntity> OneAsync(Expression<Func<TEntity, Boolean>> selector) {

            var one = await Query.OfType<TEntity>().SingleOrDefaultAsync(selector);
            return one;

        }

        public async Task DeleteAsync(TEntity entity) {

            Context.Set<TEntity>().Remove(entity);
            await Persist();

        }


        public async Task<TEntity> InsertAsync(TEntity entity) {

            Context.Set<TEntity>().Add(entity);
            await Persist();
            return (_InsertedEntity = entity);

        }

        public async Task<TEntity> UpdateAsync(TEntity entity, Func<TEntity, TEntity> modifier) {

            entity = modifier(entity);
            if (Context.Entry(entity).State == EntityState.Detached) {
                Context.Set<TEntity>().Attach(entity);
            }
            await Persist();
            return entity;

        }

        #endregion

    }

}
