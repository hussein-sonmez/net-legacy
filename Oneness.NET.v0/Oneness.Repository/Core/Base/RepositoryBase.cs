﻿using Moq;
using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.Base;
using Oneness.Protocols.RepositoryProtocols.Base;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Repository.Core.Base {
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity>
        where TEntity : BaseModel {

        #region Private

        protected Boolean _Faulted;
        protected Exception _Fault;
        private EntityContext _Context;

        public DbContext Context {
            get {
                return _Context ?? (_Context = new EntityContext());
            }
        }
        protected TEntity _InsertedEntity;
        public TEntity InsertedEntity {
            get {
                return _InsertedEntity;
            }
        }

        private void Persist() {
            try {
                Context.SaveChanges();
                _Faulted = false;
            } catch (Exception ex) {
                _Fault = ex;
                _Faulted = true;
            }
        }

        #endregion

        #region Implementation of IServiceBase<TModel>
        private IQueryable _Query;
        public IQueryable Query {
            get {
                return _Query ?? (_Query = Context.Set(typeof(TEntity)));
            }
        }

        public Exception GetFault() {

            return _Faulted ? _Fault : null;

        }
        public Boolean HasNoFault() {

            return !_Faulted;

        }
        public String GetFaultMessage() {

            return _Faulted ? _Fault.Message : "Success";

        }
        
        public TEntity Top<TKey>(Func<TEntity, TKey> keySelector) {

            var all = All();
            var top = all.OrderBy(keySelector).FirstOrDefault();
            return top;

        }
        public IEnumerable<TEntity> All() {

            var all = Query.OfType<TEntity>().ToList();
            return all;

        }

        public TEntity The(Int64 key) {

            var the = Query.OfType<TEntity>().SingleOrDefault(e => e.ID == key);
            return the;

        }

        public IEnumerable<TEntity> Some(Expression<Func<TEntity, Boolean>> selector) {

            return Query.OfType<TEntity>().Where(selector).ToList();

        }

        public TEntity One(Expression<Func<TEntity, Boolean>> selector) {

            var one = Query.OfType<TEntity>().SingleOrDefault(selector);
            return one;

        }

        public void Delete(TEntity entity) {

            Context.Set<TEntity>().Remove(entity);
            Persist();

        }


        public TEntity Insert(TEntity entity) {

            Context.Set<TEntity>().Add(entity);
            Persist();
            return (_InsertedEntity = entity);

        }

        public TEntity Update(TEntity entity, Func<TEntity, TEntity> modifier) {

            entity = modifier(entity);
            if (Context.Entry(entity).State == EntityState.Detached) {
                Context.Set<TEntity>().Attach(entity);
            }
            Persist();
            return entity;

        }

        #endregion

    }

}
