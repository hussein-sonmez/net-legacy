﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Brut.Core.Read {
    internal class Reader : IReader {

        #region ctor
        internal Reader() {
            Instrument = new Unrar();
        }
        #endregion

        #region Internal Methods
        public bool Make(FileInfo target, DirectoryInfo destination, String password = null) {

            FileIOPermission fioPermission;
            fioPermission = new FileIOPermission(FileIOPermissionAccess.AllAccess, destination.FullName);
            fioPermission.Demand();

            Instrument.DestinationPath = destination.FullName;
            Instrument.Open(target.FullName, Unrar.OpenMode.Extract);
            Instrument.PasswordRequired += (s, e) => {
                e.Password = password;
                e.ContinueOperation = true;
            };

            try {
                while (Instrument.ReadHeader()) {
                    Instrument.Extract();
                }
            } catch {
                return false;
            }

            return true;
        }
        #endregion

        #region Properties
        internal Unrar Instrument { get; private set; }
        #endregion
    }
}
