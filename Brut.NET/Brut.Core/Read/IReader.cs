﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brut.Core.Read {
    public interface IReader {
        bool Make(FileInfo target, DirectoryInfo destination, String password = null);
    }
}
