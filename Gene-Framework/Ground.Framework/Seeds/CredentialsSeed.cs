﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gene.Framework;

namespace Ground.Framework.Seeds {
    internal class CredentialsSeed : ISeeder<ETCredential> {
        #region ISeeder<ETCredential> Üyeleri

        public ICollection<ETCredential> GetSeed(int N = 10) {

            var seed = new List<ETCredential>();
            for (int i = 0; i < N; i++) {
                var cr = new ETCredential().Replicate<ETCredential>();
                cr.Login = "Login" + i;
                seed.Add(cr);
            }
            return seed;

        }

        #endregion
    }
}
