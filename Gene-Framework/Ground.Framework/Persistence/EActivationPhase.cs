﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ground.Framework {
  public enum EActivationPhase {

    MailInvitationSent = 1,
    MailActivationCompleted,
    ContactDetailsSupplied,
    PurchaseInformationCompleted

  }
}
