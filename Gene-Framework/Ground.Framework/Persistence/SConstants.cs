﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gene.Framework;

namespace Ground.Framework {
	internal static class SConstants {

		internal static IPort Port {
			get {
				var nc = SFactory.Object().Generate<INetworkCredentials>();
				nc.DataSource = @".";
				nc.InitialCatalog = "FrameworkDB";
				nc.UserID = "fmadmin";
				nc.Password = "1q2w3e4r5t";

				var port = SFactory.Object().Generate<IPort>();
				port.NetworkCredentials = nc;
				port.Porter = typeof(SConstants).Assembly;
				return port;
			}
		}

	}
}
