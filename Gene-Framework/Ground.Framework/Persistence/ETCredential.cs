﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Gene.Framework;

namespace Ground.Framework {
    [Table("Credentials")]
    public class ETCredential : Genome {

        #region Properties
        [MaxLength(255)]
        public String Login { get; set; }
        [MaxLength(255)]
        public String Cipher { get; set; }
        public EActivationPhase ActivationPhase { get; set; }
        #endregion

        #region Overrides

        public override TGene Replicate<TGene>() {
            ActivationPhase = EActivationPhase.ContactDetailsSupplied;
            Cipher = "1q2w3e4r5t";
            return base.Replicate<TGene>();
        }

        #endregion

    }
}
