﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Gene.Framework {
	[AttributeUsage(AttributeTargets.Enum, Inherited = false, AllowMultiple = true)]
	public sealed class OptionTranslationAttribute : Attribute {
		readonly Type _ResourceType;

		public OptionTranslationAttribute(Type resourceType) {
			this._ResourceType = resourceType;
			Culture = Thread.CurrentThread.CurrentUICulture;
		}

		public Type ResourceType {
			get { return _ResourceType; }
		}

		public CultureInfo Culture { get; set; }
	}
}
