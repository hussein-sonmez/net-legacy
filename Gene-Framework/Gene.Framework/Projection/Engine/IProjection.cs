﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gene.Framework {
  public interface IProjection {

    #region Properties

    dynamic CompositeEntity { get; }

    #endregion

  }
}
