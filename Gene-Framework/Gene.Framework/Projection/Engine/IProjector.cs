﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Gene.Framework {
    public interface IProjector {

        #region Methods

        IProjector Map(params Action<dynamic, dynamic>[] mappers);
        IProjector Reduce(dynamic entity);

        #endregion

        #region Properties

        IProjection Projection { get; }

        #endregion

    }
}
