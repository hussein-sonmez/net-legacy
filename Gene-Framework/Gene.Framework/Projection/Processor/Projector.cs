﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Gene.Framework;

namespace Gene.Framework {
    internal class Projector : IProjector {

        #region Fields

        private List<Action<dynamic, dynamic>> _Mappers;
        private IProjection projection;

        #endregion

        #region IProjector Members

        public IProjector Map(params Action<dynamic, dynamic>[] mappers) {
            _Mappers = _Mappers ?? new List<Action<dynamic, dynamic>>();
            foreach (var mr in mappers) {
                _Mappers.Add(mr);
            }
            return this;
        }

        public IProjector Reduce(dynamic entity) {
            
            var compositeEntity = new ExpandoObject();
            foreach (var mr in _Mappers) {
                mr(entity, compositeEntity);
            }

            projection = SFactory.Object().Generate<IProjection>(null, compositeEntity);
            return this;

        }

        public IProjection Projection {
            get {
                return projection;
            }
        }

        #endregion

    }
}
