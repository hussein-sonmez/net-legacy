﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gene.Framework {
  internal class Projection : IProjection {

    #region Fields

    private ExpandoObject _CompositeEntity;    

    #endregion

    #region ctors
    public Projection(ExpandoObject compositeEntity) {
      _CompositeEntity = compositeEntity;
    }
    #endregion

    #region IProjection Members

    public dynamic CompositeEntity {
      get { return _CompositeEntity; }
    }

    #endregion

  }
}
