﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gene.Framework {
	public interface ITransactor : IDisposable {

		void Commit();
		void Rollback();

	}
}
