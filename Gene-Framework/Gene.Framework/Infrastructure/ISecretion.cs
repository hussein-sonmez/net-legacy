﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Gene.Framework {
  public interface ISecretion : ITranslation {

		#region Methods
		ISecretion Each(Action<Int32, dynamic> @for);
		ICollection All() ;
		ICollection Some(Func<dynamic, Boolean> selector) ;
		ICollection Sorted(Func<dynamic, bool> selector, Func<dynamic, object> keySelector) ;
		dynamic The(dynamic idRepr);
		dynamic One(Func<dynamic, Boolean> selector) ;
		#endregion

	}
}
