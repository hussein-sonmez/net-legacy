﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gene.Framework {
    public interface IObjectFactory {

        #region Methods

        TInfrastructure Generate<TInfrastructure>(Func<Type, Boolean> typeSelector = null, params Object[] args);

        #endregion

    }
}
