﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gene.Framework {
	public interface ICryptoService {

		#region Methods

		String GetMD5String(String value);

		byte[] Encrypt(string plainText, byte[] key, byte[] iv);
		string Decrypt(byte[] cipher, byte[] key, byte[] iv);

		void GenerateSymmetricAlgorithms(out byte[] key, out byte[] iv);

		#endregion

	}
}
