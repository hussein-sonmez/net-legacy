﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gene.Framework {
	public interface ILexicon {

		#region Methods

		String Pluralise(string name);
		String CapitalLetterToLower(String name);
		String GenerateIdentifier(Int32 magnitude = 4);

		#endregion

	}
}
