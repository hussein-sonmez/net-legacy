﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gene.Framework {
    public interface ISeeder<Model> {

        ICollection<Model> GetSeed(Int32 N = 10);

    }
    public interface ISeeder<Model, Relation> {

        ICollection<Model> GetSeed(Relation relation, Int32 N = 10);

    }
}
