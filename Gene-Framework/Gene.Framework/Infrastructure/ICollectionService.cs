﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gene.Framework {
	public interface ICollectionService<TItem> {

		ICollection<TItem> Populate(Int32 N, Func<TItem> populator=null);

	}
}
