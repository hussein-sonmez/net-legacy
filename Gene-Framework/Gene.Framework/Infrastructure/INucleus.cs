﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Gene.Framework {
    public interface INucleus {

        #region Methods

        ITranscriptor Activate(String promotor);
        IEnumerable<dynamic> Incarnate(DbModelBuilder modelBuilder);
        ITransactor BeginTransaction();

        #endregion

    }
}
