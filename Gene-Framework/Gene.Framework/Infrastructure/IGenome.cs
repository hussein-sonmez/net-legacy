﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gene.Framework {
    public interface IGenome {

        #region Properties

        Guid Epitope { get; }

        #endregion

        #region Methods

        IGenome Primase<TGene>(Action<TGene> enzyme)
            where TGene : IGenome;

        TGene TerminateSequence<TGene>()
            where TGene : IGenome;

        TGene Replicate<TGene>()
            where TGene : class, IGenome;

        #endregion

    }
}
