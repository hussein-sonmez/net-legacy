﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Gene.Framework {
	public interface IReflexor {

		Type FindTopMost(IEnumerable<Type> models);
		Type GetRootType(Type genericType);
		PropertyInfo[] GetPublicProperties(Type type);
		Type GetElementType(Type attribute);

		Boolean IsOrCapsulatesNested(Type attribute);
		Type GetBaseInterface(Type genre);
		MethodInfo GetMethod(String methodName, Type type, Type[] genericArguments, params dynamic[] parameters);
		Boolean IsCollection(Type attribute);

	}
}
