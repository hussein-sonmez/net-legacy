﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Gene.Framework {
    public interface IGolgi {

        #region Methods
        ISecretion Secrete();
        IGolgi Reincarnate();
        #endregion

    }
}
