﻿using System;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Gene.Framework {
    public interface ITranscriptor : IGolgi {

        #region Methods
        ITranscriptor Insert(dynamic item);
        ITranscriptor Swap(dynamic id, dynamic item);
        ITranscriptor Delete(dynamic item);
        #endregion

        #region Properties
        String GeneIdentifier { set; }
        INucleus Nucleus { set; }
        #endregion

    }
}
