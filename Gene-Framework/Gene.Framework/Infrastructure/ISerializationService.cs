﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gene.Framework {
    public interface ISerializationService {

        #region Methods
        TJSO GenerateJSOFrom<TJSO>(String jsonString);
        String GenerateJSONFrom(Object jsObject);
        #endregion

    }
}
