﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gene.Framework {
	public interface INetworkCredentials {

		#region Properties

		String DataSource { get; set; }
		String InitialCatalog { get; set; }

		String UserID { get; set; }
		String Password { get; set; }

        Boolean PersistSecurityInfo { set; }
		String ConnectorFormat { set; }
        String AttachDbFileName { set; }
        
		#endregion

		#region Methods

		String GeneratePassthrough();

		#endregion

	}
}
