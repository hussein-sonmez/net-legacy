﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Gene.Framework {
	internal class ObjectFactory : IObjectFactory {

		#region IObjectFactory Members

		public TInfrastructure Generate<TInfrastructure>(Func<Type, Boolean> typeSelector = null, params Object[] args) {
			if (typeSelector == null) {
				typeSelector = (t) => true;
			}
			if (!typeof(TInfrastructure).IsInterface) {
				throw new InvalidOperationException("interface needed");
			}
			Func<Assembly, Type, TInfrastructure> search = (asm, tif) => {
				var found = asm.GetTypes()
				.SingleOrDefault(t => t.IsInterface == false && t.GetInterfaces()
					.Any(ti => t.GetInterface(tif.Name) != null &&
						t.GetInterface(tif.Name).GenericTypeArguments
						.All(gta => tif.GenericTypeArguments.Any(tifgta => tifgta.IsAssignableFrom(gta)))));
				if (found != null) {
					if (found.ContainsGenericParameters) {
						found = found.MakeGenericType(tif.GetGenericArguments());
					}
					if (typeSelector(found)) {
						return (TInfrastructure)Activator.CreateInstance(found, args);
					}
				}
				return default(TInfrastructure);
			};
			var f = search(typeof(TInfrastructure).Assembly, typeof(TInfrastructure));
			if (f == null) {
				f = search(Assembly.GetCallingAssembly(), typeof(TInfrastructure));
			}
			if (f == null) {
				var asms = Assembly.GetCallingAssembly().GetReferencedAssemblies();
				foreach ( var asm in asms) {
					f = search(Assembly.Load(asm), typeof(TInfrastructure));
					if (f != null) {
						return f;
					}
				}
			}
			return f;
		}

		#endregion

	}
}
