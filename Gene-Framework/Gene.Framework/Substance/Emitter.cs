﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using Gene.Framework;

namespace Gene.Framework {
	internal class Emitter : IEmitter {
	
		#region Public static classes

		public PropertyBuilder CreateProperty(TypeBuilder generator, Type type, Type propertyType, Type fieldType, String propertyName, MethodAttributes attributes) {
			var fieldBuilder = generator.DefineField("m_" + SFactory.Object().Generate<ILexicon>().CapitalLetterToLower(propertyName), propertyType, FieldAttributes.Private);
			MethodInfo getMethod, setMethod;
			var baseInterface = type;
			do {
				getMethod = baseInterface.GetMethod("get_" + propertyName);
				setMethod = baseInterface.GetMethod("set_" + propertyName);
				baseInterface = SFactory.Object().Generate<IReflexor>().GetBaseInterface(baseInterface);
				if (baseInterface == null) {
					break;
				}
			} while (getMethod == null || setMethod == null);

			var getSetAttr = MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.HideBySig | attributes;
			var propertyBuilder = generator.DefineProperty(
					propertyName,
					PropertyAttributes.HasDefault,
					propertyType, null);

			var getBuilder = generator.DefineMethod(
					"get_" + propertyName,
					getSetAttr, propertyType, Type.EmptyTypes);
			var getIL = getBuilder.GetILGenerator();
			getIL.Emit(OpCodes.Ldarg_0);
			getIL.Emit(OpCodes.Ldfld, fieldBuilder);
			getIL.Emit(OpCodes.Ret);

			var setBuilder = generator.DefineMethod(
					"set_" + propertyName,
					getSetAttr, typeof(void), new Type[] { propertyType });

			var setIL = setBuilder.GetILGenerator();
			setIL.Emit(OpCodes.Ldarg_0);
			setIL.Emit(OpCodes.Ldarg_1);
			setIL.Emit(OpCodes.Stfld, fieldBuilder);
			setIL.Emit(OpCodes.Ret);
			if (getMethod != null && setMethod != null) {
				generator.DefineMethodOverride(getBuilder, getMethod);
				generator.DefineMethodOverride(setBuilder, setMethod);
			}
			propertyBuilder.SetGetMethod(getBuilder);
			propertyBuilder.SetSetMethod(setBuilder);
			return propertyBuilder;
		}
		
		public TypeBuilder CreateTypeBuilder(AssemblyName name, string typeName, AssemblyBuilder assemblyBuilder) {
			var moduleBuilder = assemblyBuilder.GetDynamicModule(name.FullName) ??
					assemblyBuilder.DefineDynamicModule(name.FullName, name.FullName + ".dll");

			var typeBuilder = moduleBuilder.DefineType(typeName, TypeAttributes.Public | TypeAttributes.Class);
			return typeBuilder;
		}

		public AssemblyBuilder CreateAssemblyBuilder(AssemblyName name) {
			var assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(name, AssemblyBuilderAccess.RunAndSave);
			return assemblyBuilder;
		}

		public void SetAttribute<TAttribute>(dynamic builder, Type[] argTypes, Object[] args) {
			var constructorInfo = typeof(TAttribute).GetConstructor(argTypes);
			var customAttributeBuilder = new CustomAttributeBuilder(constructorInfo, args);
			builder.SetCustomAttribute(customAttributeBuilder);
		}

		#endregion

	}
}
