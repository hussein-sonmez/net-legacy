﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Gene.Framework {
	internal class Reflexor : IReflexor {

		#region Public Static Methods

		public Type FindTopMost(IEnumerable<Type> models) {
			if (Enumerable.Count(models) > 0) {
				Type topMost = models.First();
				foreach (var m in models) {
					if (m.GetInterfaces().Length == 0) {
						topMost = m;
						break;
					}
					while (topMost.GetInterfaces().Length > 0) {
						topMost = FindTopMost(topMost.GetInterfaces());
					}
				}
				return topMost;
			} else {
				return null;
			}
		}
		public Type GetRootType(Type genericType) {
			return Type.GetType(String.Format("{0}.{1}", genericType.Namespace, genericType.Name));
		}
		public PropertyInfo[] GetPublicProperties(Type type) {
			if (type.IsInterface) {
				var properties = new List<PropertyInfo>();
				var baseInterface = type;
				do {
					baseInterface.GetProperties().ToList().ForEach((p) => {
						if (properties.Contains(p) == false) {
							properties.Add(p);
						}
					});
					baseInterface = GetBaseInterface(baseInterface);
				} while (baseInterface != null);
				return properties.ToArray();

			}

			return type.GetProperties(BindingFlags.FlattenHierarchy
					| BindingFlags.Public | BindingFlags.Instance);
		}
		public Type GetElementType(Type attribute) {
			var elementType = IsCollection(attribute) == true
					 ? (attribute.GetGenericArguments().FirstOrDefault() ?? attribute.GetElementType()) : attribute;
			return elementType;
		}

		public Boolean IsOrCapsulatesNested(Type attribute) {
			return attribute.IsNested == true || attribute.GetGenericArguments().Any((ga) => ga.IsNested == true) == true;
		}
		public Type GetBaseInterface(Type genre) {
			return genre.GetInterfaces().OrderBy<Type, Int32>((i) => i.GetInterfaces().Count()).LastOrDefault();
		}
		public MethodInfo GetMethod(String methodName, Type type, Type[] genericArguments, params dynamic[] parameters) {
			var method = type.GetMethods()
											 .Where(m => m.Name == methodName)
											 .Select(m => new {
												 Method = m,
												 Params = m.GetParameters(),
												 GenericArgs = m.GetGenericArguments()
											 })
											 .Where(x => x.Params.Length == parameters.Length
																	 && x.GenericArgs.Length == genericArguments.Length)
											 .Select(x => x.Method)
											 .First();
			
			foreach (var garg in genericArguments) {
				method = method.MakeGenericMethod(garg);
			}

			return method;
		}
		public Boolean IsCollection(Type attribute) {
			return (attribute.GetInterface("IEnumerable`1") != null || attribute.GetInterface("ICollection`1") != null) && attribute.IsGenericType;
		}

		#endregion

	}

}
