﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Gene.Framework {
    public static class SFactory {

        #region Methods

        public static IObjectFactory Object() {
            return new ObjectFactory();
        }

        #endregion

    }

}
