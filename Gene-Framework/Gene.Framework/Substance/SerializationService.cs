﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Gene.Framework {
    internal class SerializationService : ISerializationService {

        #region ISerializationService Üyeleri

        public TJSO GenerateJSOFrom<TJSO>(String jsonString) {
            return JsonConvert.DeserializeObject<TJSO>(jsonString);
        }

        public String GenerateJSONFrom(Object jsObject) {
            return JsonConvert.SerializeObject(jsObject);
        }

        #endregion

    }
}
