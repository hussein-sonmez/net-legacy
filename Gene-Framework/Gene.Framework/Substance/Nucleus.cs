﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Gene.Framework {
	public class Nucleus : DbContext, INucleus {

		#region ctor

		public Nucleus(IPort port)
			: base(port.Connection, true) {

            Nucleus.Port = port;

		}

        public Nucleus() : this(Nucleus.Port) {

        }

        private static IPort _Port;
        internal static IPort Port {
            get {
                var nc = new NetworkCredentials();
                nc.DataSource = @".";
                nc.InitialCatalog = "FrameworkDB";
                nc.UserID = "fmadmin";
                nc.Password = "1q2w3e4r5t";

                var port = SFactory.Object().Generate<IPort>();
                port.NetworkCredentials = nc;
                port.Porter = Assembly.LoadFile(@"G:\Source\Workspaces\CSharp\Gene-Framework\Ground.Framework\bin\Debug\Gene.Framework.dll");
                return _Port = port;
            } 
            set {
                _Port = value;
            }
        }

        #endregion

        #region Overrides

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {

			base.OnModelCreating(modelBuilder);
			Incarnate(modelBuilder);

		}

		#endregion

		#region INucleus Members

		public IEnumerable<dynamic> Incarnate(DbModelBuilder modelBuilder) {

			var rx = SFactory.Object().Generate<IReflexor>();
			Database.SetInitializer<Nucleus>(new MigrateDatabaseToLatestVersion<Nucleus, Mitochondria>());

			var entities =
                    Nucleus.Port.Porter.GetTypes().Where(tp =>
							tp.GetInterface(typeof(IGenome).Name) != null && !tp.IsAbstract);

			var types = new List<dynamic>();
			foreach (var en in entities) {
				var entifier = rx.GetMethod("Entity", typeof(DbModelBuilder), new Type[] { en });
				types.Add(entifier.Invoke(modelBuilder, null));
			}

			return types.ToArray();
		}

		public ITranscriptor Activate(String entityIdentifier) {
			var t = SFactory.Object().Generate<ITranscriptor>();
			t.GeneIdentifier = entityIdentifier;
			t.Nucleus = this;
			return t;
		}

		public ITransactor BeginTransaction() {
			return new Transactor(this.Database.BeginTransaction());
		}

        #endregion

    }
}
