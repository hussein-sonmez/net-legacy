﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using Gene.Framework;

namespace Gene.Framework {
    internal class Port : IPort {

        #region IPort Members

        public Assembly Porter { get; set; }

        public DbConnection Connection {
            get { return new SqlConnection(NetworkCredentials.GeneratePassthrough()); }
        }
       
        public INetworkCredentials NetworkCredentials { get; set; }

        #endregion

    }
}
