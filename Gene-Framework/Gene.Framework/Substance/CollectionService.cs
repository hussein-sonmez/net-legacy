﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gene.Framework {
	internal class CollectionService<TItem> : ICollectionService<TItem> {
		
		#region ICollectionService<TItem> Members

		public ICollection<TItem> Populate(Int32 N, Func<TItem> populator = null) {

			if (populator == null) {
				populator = ()=> Activator.CreateInstance<TItem>();
			}

			return Enumerable.Repeat(new Func<TItem>(() => populator()), N).Select((fi)=>fi()).ToList();

		}

		#endregion
	
	}
}
