﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Text;
using Gene.Framework.Properties;

namespace Gene.Framework.Substance {
	internal class Secretion : ISecretion {

		#region Fields
		private ICollection<Exception> _ErrorCascade;
		private EGain _Gain;
		private DbSet _DataSet;
		#endregion

		#region ctor
		public Secretion(DbSet dataSet) {
			_DataSet = dataSet;
		}
		#endregion

		#region ISecretion Members

		private IEnumerable Each() {
            var set = _DataSet;
			foreach (var item in set) {
				yield return item as dynamic;
			}
		}
		public ISecretion Each(Action<int, dynamic> @for) {

			var i = 0;
			foreach (var item in Each()) {
				@for(i++, item);
			}

			return this;
		}
		public ICollection All(){
			var all = new List<dynamic>();
			foreach (var e in Each()) {
				all.Add(e);
			}
			return all;
		}

		public ICollection Some(Func<dynamic, bool> selector) {
			
			return (from object a in Each() where selector(a) select a).ToList<dynamic>();
		}

		public ICollection Sorted(Func<dynamic, bool> selector, Func<dynamic, object> keySelector) {
			return (from object a in Each() where selector(a) orderby keySelector(a) select a).ToList<dynamic>();
		}

		public dynamic The(dynamic idRepr) {
			dynamic id = idRepr;
			dynamic dbItem = default(dynamic);
			var idPropertyInfo = id.GetType().GetProperties()[0];
			foreach (var item in _DataSet) {
				var idOfItem = item.GetType().GetProperties()
								.Single(pi => pi.Name == idPropertyInfo.Name).GetValue(item, null);
				if (idOfItem.Equals(idPropertyInfo.GetValue(id, null))) {
					return item as dynamic;
				}
			}
			return dbItem;
		}

		public dynamic One(Func<dynamic, bool> selector) {
			dynamic one = default(dynamic);
			foreach (var e in Each()) {
				if (selector(e)) {
					one = e;
					break;
				}
			}
			return one;
    }

		#endregion

		#region ITranslation Members

		public String Information {
			get {
				var errors = _ErrorCascade ?? (_ErrorCascade = new List<Exception>());
				return errors.Aggregate<Exception, String>("!:", (soFar, error) =>
						soFar + ExtractErrorInformationCascade(error).Aggregate((sf, e) => sf + e));
			}
		}
		public EGain Gain {
			get {
				return _Gain;
			}
			set {
				_Gain = value;
			}
		}

		public ITranslation Append(Exception exception) {
			var errors = _ErrorCascade ?? (_ErrorCascade = new List<Exception>());
			errors.Add(exception);
			return this;
		}

		#endregion

		#region Private Helpers

		private IEnumerable<String> ExtractErrorInformationCascade(Exception error) {
			var infos = new List<String>();
			if (error.InnerException == null) {
				infos.Add(error.Message + Settings.Default.ErrorSeperator);
				return infos;
			} else {
				infos.AddRange(ExtractErrorInformationCascade(error.InnerException));
			}
			return infos;
		}

		#endregion

	}
}
