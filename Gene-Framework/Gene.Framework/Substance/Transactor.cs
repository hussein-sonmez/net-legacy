﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gene.Framework.Substance;

namespace Gene.Framework {

	internal class Transactor : ITransactor {

		#region Fields

		private DbContextTransaction transaction;

		#endregion

		#region Ctor

		public Transactor(DbContextTransaction transaction) {

			this.transaction = transaction;

		}

		#endregion

		#region ITransactor Members

		public void Commit() {

			this.transaction.Commit();

		}

		public void Rollback() {

			this.transaction.Commit();

		}

		#endregion

		#region IDisposable Members

		public void Dispose() {
			this.transaction = null;
		}

		#endregion
	}

}
