﻿using System;
using System.Dynamic;
using Gene.Framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ground.Framework;

namespace UnitTest.Framework.Tests {
	[TestClass]
	public class ProjectionTests {
		[TestMethod]
		public void ProjectorMakesProcess() {

			var cryptoService = SFactory.Object().Generate<ICryptoService>();
			var projector = SFactory.Object().Generate<IProjector>();
			var entity = new ETCredential() {
				Login = "architect@ll.com",
				Cipher = "97db1846570837fce6ff62a408f1c26a",
				ActivationPhase = EActivationPhase.MailActivationCompleted
			};

			var reduced = projector.Map(
					  (e, cpzt) => cpzt.Authenticated = e.Login == "architect@ll.com" && cryptoService.GetMD5String("1q2w3e4r5t") == e.Cipher,
			  (e, cpzt) => cpzt.PhaseInformation = e.ActivationPhase == EActivationPhase.MailActivationCompleted
				? "Mail Sending Process Completed" : "Mail Not Sent Yet",
			  (e, cpzt) => cpzt.Message = cpzt.Authenticated ? String.Format("Good to see you {0}!", e.Login) : "We're missing you!").Reduce(entity);

			Assert.IsNotNull(reduced.Projection);
			Assert.IsNotNull(reduced.Projection.CompositeEntity.Authenticated);
			Assert.IsNotNull(reduced.Projection.CompositeEntity.PhaseInformation);
			Assert.IsNotNull(reduced.Projection.CompositeEntity.Message);
			Assert.AreEqual(true, reduced.Projection.CompositeEntity.Authenticated);
		}
	}
}
