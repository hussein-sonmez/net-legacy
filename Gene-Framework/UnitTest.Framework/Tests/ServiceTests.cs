﻿using System;
using System.Linq;
using Gene.Framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Framework.Tests {
	[TestClass]
	public class ServiceTests {

		#region Fields

		private ILexicon Lexer;
		private ICollectionService<dynamic> DynamicCollectionService;

		#endregion

		[TestInitialize]
		public void TestSuiteInitialize() {

			Lexer = SFactory.Object().Generate<ILexicon>();
			DynamicCollectionService = SFactory.Object().Generate<ICollectionService<dynamic>>();

		}

		[TestMethod]
		public void LexerGeneratesIdentifier() {

			var magnitude = 45;

			var identifier = Lexer.GenerateIdentifier(magnitude);
			Assert.IsTrue(identifier.Length > magnitude, identifier);

			Assert.AreEqual<Int32>(Enumerable.Count(identifier,
				(ch) => Char.IsUpper(ch)), magnitude);
		}

		[TestMethod]
		public void DynamicCollectionServiceEmerges() {

			Assert.IsNotNull(DynamicCollectionService);
		}
	}
}
