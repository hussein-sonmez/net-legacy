﻿using System;
using System.IO;
using System.Linq;
using Gene.Framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ground.Framework;

namespace UnitTest.Framework {
	[TestClass]
	public class GeneTests {

		private INucleus ETNucleus { get; set; }

		[TestInitialize]
		public void TestSuite() {

			ETNucleus = SFactory.Object().Generate<INucleus>(null, SConstants.Port);
			Assert.IsNotNull(ETNucleus);

		}

		[TestMethod]
		public void TranscriptorSecretesAll() {

			var credentials = SFactory.Object().Generate<ISeeder<ETCredential>>().GetSeed();

			var transcriptor = ETNucleus.Activate(typeof(ETCredential).FullName);
			Assert.IsNotNull(transcriptor);

			var secretion = transcriptor.Secrete();
			Assert.AreNotEqual<EGain>(EGain.Fail, secretion.Gain, secretion.Information);

			foreach (var cr in credentials) {
				var processed = transcriptor.Insert(cr);
			}

			Assert.AreNotEqual<EGain>(EGain.Fail, secretion.Gain, secretion.Information);

			var elements = secretion.All();
			Assert.AreEqual<Int32>(credentials.Count(), elements.OfType<ETCredential>().Count());

			var someElements = secretion.Some(el => el.ActivationPhase == EActivationPhase.ContactDetailsSupplied);
			Assert.IsNotNull(someElements);
			Assert.AreEqual<Int32>(elements.Count, someElements.Count);

			secretion = transcriptor.Reincarnate().Secrete();
			Assert.AreNotEqual<EGain>(EGain.Fail, secretion.Gain, secretion.Information);

		}

	}
}
