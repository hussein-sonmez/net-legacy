﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using ShareVerSln.WinUI.ProxyClone;

namespace UzakNesne
{
    //.Net remoting'de aradaki nesnenin MarshalByRefObject den miras alması gerekir
    public class ProxyNesne
    {
        //ekran genişliği
        public int EkranGenisligi
        {
            get
            {
                return Screen.PrimaryScreen.Bounds.Width;
            }
        }
        public int EkranYuksekligi
        {
            get
            {
                return Screen.PrimaryScreen.Bounds.Height;
            }
        }

        public byte[] EkranGoruntusu()
        {
            //ekran görüntüsünün 
            Rectangle alan = Screen.PrimaryScreen.Bounds;
            //geri döndüreceğim resim
            //bu stream e kaydediyor
            Bitmap bmp = new Bitmap(alan.Width, alan.Height);
            //bu bitmap üzerine çizim yapmamızı sağlıyor
            Graphics grafik = Graphics.FromImage(bmp);
            //bu ekran görüntüsünü  çekiyor
            grafik.CopyFromScreen(0, 0, 0, 0, bmp.Size);
            //bu an itibariyle ekran görüntüsü bitmap üzerinde
            //stream e aktar
            MemoryStream ms = new MemoryStream();
            //resmin ms e gitmesi lazım
            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            //stream içerisindeki byte[] sini almam gerek.
            byte[] data = ms.GetBuffer();
            return data;
        }
        public void FareKiprastir(Point nokta)
        {
            Cursor.Position = nokta;
        }

        public void FareClick()
        {
            Mouse.SendClick();
        }

        public void Tusla(Keyboard.klavye k)
        {
            Keyboard.SendKey(k);
        }
    }
}
