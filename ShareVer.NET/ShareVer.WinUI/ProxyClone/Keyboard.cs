﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace ShareVerSln.WinUI.ProxyClone
{
    public class Keyboard
    {
        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);
        [Flags]
        public enum klavye
        {
            VK_KEY_R = 0x00000052
        }

        public static void SendKey(klavye k)
        {
            keybd_event((byte)k, 0, 0, new UIntPtr());
        }
    }
}
