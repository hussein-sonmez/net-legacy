﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace ShareVerSln.WinUI.ProxyClone
{
    class Mouse
    {
        //dllimport
        //windows'un kullandığı dll leri kullanmak için yaparız.
        [DllImport("user32.dll")]
        static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint dwData, UIntPtr dwExtraInfo);
        [Flags]
        public enum MouseEventFlags
        {
            LEFTDOWN = 0x00000002,
            LEFTUP = 0x00000004,
            MIDDLEDOWN = 0x00000020,
            MIDDLEUP = 0x00000040,
            MOVE = 0x00000001,
            ABSOLUTE = 0x00008000,
            RIGHTDOWN = 0x00000008,
            RIGHTUP = 0x00000010
        }

        public static void SendClick()
        {
            mouse_event((uint)MouseEventFlags.LEFTDOWN, 0, 0, 0, new UIntPtr());
            mouse_event((uint)MouseEventFlags.LEFTUP, 0, 0, 0, new UIntPtr());
        }

        public static void SendMouseDown()
        {
            mouse_event((uint)MouseEventFlags.LEFTDOWN, 0, 0, 0, new UIntPtr());
        }
    }
}
