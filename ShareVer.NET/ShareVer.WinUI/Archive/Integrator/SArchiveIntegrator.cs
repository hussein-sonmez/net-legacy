﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace ShareVerSln.WinUI.Archive.Integrator
{
    public static class SArchiveIntegrator
    {
        public static string CompressFolder(string zipPath, string compressTarget)
        {
            //"C:\Program Files\7-Zip\7z.exe" a -r "C:\new.7z" "C:\dirdemo\*.*"
            string compressedZip = String.Format(@"{0}", zipPath);
            string args = String.Format(@" a -r ""{0}"" ""{1}\*.*"" ",
                 zipPath, compressTarget);
            string zip7exe = Path.Combine(Application.StartupPath, "7z.exe");
            Process zipper = Process.Start(zip7exe, args);
            while (!zipper.HasExited)
            {

            }
            if (zipper.ExitCode != 0)
                throw new Exception("exit code for compressor: " + zipper.ExitCode);
            return compressedZip;
        }
        private static void UnCompressZip(string zipPath, string outputDir)
        {
            //"C:\Program Files\7-Zip\7z.exe" x -oC:\clone -y "C:\new.7z"
            string args = String.Format(@" x -o""{0}"" ""{1}""", outputDir, zipPath);
            string zip7exe = Path.Combine(Application.StartupPath, "7z.exe");
            Process zipper = Process.Start(zip7exe, args);
            while (!zipper.HasExited)
            {
                
            }
            if (zipper.ExitCode != 0)
               throw new Exception("exit code for decompressor: "+zipper.ExitCode);
        }
        public static void ExtractData(byte[] data, string extractTarget)
        {
            string tempDir = Path.Combine(extractTarget, Guid.NewGuid().ToString("N"));
            Directory.CreateDirectory(tempDir);
            string zipPath = Path.Combine(tempDir, "temp.7z");
            FileStream zipFile = new FileStream(zipPath, FileMode.CreateNew, FileAccess.ReadWrite);
            zipFile.Write(data, 0, data.Length);
            zipFile.Flush();
            zipFile.Close();
            UnCompressZip(zipPath, extractTarget);
            Directory.Delete(tempDir, true);
        }
    }
}
