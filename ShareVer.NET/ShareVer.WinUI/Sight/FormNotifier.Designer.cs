﻿namespace ShareVerSln.WinUI.Sight
{
    partial class FormNotifier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNotifier));
            this.niMain = new System.Windows.Forms.NotifyIcon(this.components);
            this.cmsMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiAddShareVer = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiShareVerEntries = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiBindEntity = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiInjectEntity = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiServerList = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.fbdAddShareVer = new System.Windows.Forms.FolderBrowserDialog();
            this.cmsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // niMain
            // 
            this.niMain.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.niMain.ContextMenuStrip = this.cmsMain;
            this.niMain.Icon = ((System.Drawing.Icon)(resources.GetObject("niMain.Icon")));
            // 
            // cmsMain
            // 
            this.cmsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAddShareVer,
            this.tsmiShareVerEntries,
            this.tsmiBindEntity,
            this.tsmiInjectEntity,
            this.tsmiServerList,
            this.tsmiExit});
            this.cmsMain.Name = "cmsMain";
            this.cmsMain.Size = new System.Drawing.Size(159, 136);
            // 
            // tsmiAddShareVer
            // 
            this.tsmiAddShareVer.Enabled = false;
            this.tsmiAddShareVer.Name = "tsmiAddShareVer";
            this.tsmiAddShareVer.Size = new System.Drawing.Size(158, 22);
            this.tsmiAddShareVer.Text = "Add ShareVer..";
            this.tsmiAddShareVer.Click += new System.EventHandler(this.tsmiAddShareVer_Click);
            // 
            // tsmiShareVerEntries
            // 
            this.tsmiShareVerEntries.Enabled = false;
            this.tsmiShareVerEntries.Name = "tsmiShareVerEntries";
            this.tsmiShareVerEntries.Size = new System.Drawing.Size(158, 22);
            this.tsmiShareVerEntries.Text = "ShareVer Entries";
            // 
            // tsmiBindEntity
            // 
            this.tsmiBindEntity.Enabled = false;
            this.tsmiBindEntity.Name = "tsmiBindEntity";
            this.tsmiBindEntity.Size = new System.Drawing.Size(158, 22);
            this.tsmiBindEntity.Text = "Bind Entity..";
            this.tsmiBindEntity.Click += new System.EventHandler(this.tsmiBindEntity_Click);
            // 
            // tsmiInjectEntity
            // 
            this.tsmiInjectEntity.Enabled = false;
            this.tsmiInjectEntity.Name = "tsmiInjectEntity";
            this.tsmiInjectEntity.Size = new System.Drawing.Size(158, 22);
            this.tsmiInjectEntity.Text = "Inject Entity";
            this.tsmiInjectEntity.Click += new System.EventHandler(this.tsmiInjectEntity_Click);
            // 
            // tsmiServerList
            // 
            this.tsmiServerList.Name = "tsmiServerList";
            this.tsmiServerList.Size = new System.Drawing.Size(158, 22);
            this.tsmiServerList.Text = "Server List";
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(158, 22);
            this.tsmiExit.Text = "Exit";
            this.tsmiExit.Click += new System.EventHandler(this.tsmiExit_Click);
            // 
            // FormNotifier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(10, 10);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormNotifier";
            this.Opacity = 0D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.cmsMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon niMain;
        private System.Windows.Forms.ContextMenuStrip cmsMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddShareVer;
        private System.Windows.Forms.ToolStripMenuItem tsmiShareVerEntries;
        private System.Windows.Forms.FolderBrowserDialog fbdAddShareVer;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
        private System.Windows.Forms.ToolStripMenuItem tsmiBindEntity;
        private System.Windows.Forms.ToolStripMenuItem tsmiServerList;
        private System.Windows.Forms.ToolStripMenuItem tsmiInjectEntity;
    }
}