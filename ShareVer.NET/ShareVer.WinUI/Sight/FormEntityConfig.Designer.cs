﻿namespace ShareVerSln.WinUI.Sight
{
    partial class FormEntityConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxConfigureEntity = new System.Windows.Forms.GroupBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtEntityIP = new System.Windows.Forms.TextBox();
            this.lblEntityIPCaption = new System.Windows.Forms.Label();
            this.gbxConfigureEntity.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxConfigureEntity
            // 
            this.gbxConfigureEntity.Controls.Add(this.btnOK);
            this.gbxConfigureEntity.Controls.Add(this.txtEntityIP);
            this.gbxConfigureEntity.Controls.Add(this.lblEntityIPCaption);
            this.gbxConfigureEntity.Location = new System.Drawing.Point(21, 17);
            this.gbxConfigureEntity.Name = "gbxConfigureEntity";
            this.gbxConfigureEntity.Size = new System.Drawing.Size(227, 107);
            this.gbxConfigureEntity.TabIndex = 0;
            this.gbxConfigureEntity.TabStop = false;
            this.gbxConfigureEntity.Text = "Configure Entity";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.FlatAppearance.BorderSize = 0;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.Location = new System.Drawing.Point(130, 61);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "Approve";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtEntityIP
            // 
            this.txtEntityIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEntityIP.Location = new System.Drawing.Point(61, 27);
            this.txtEntityIP.Name = "txtEntityIP";
            this.txtEntityIP.Size = new System.Drawing.Size(144, 20);
            this.txtEntityIP.TabIndex = 1;
            // 
            // lblEntityIPCaption
            // 
            this.lblEntityIPCaption.AutoSize = true;
            this.lblEntityIPCaption.Location = new System.Drawing.Point(6, 30);
            this.lblEntityIPCaption.Name = "lblEntityIPCaption";
            this.lblEntityIPCaption.Size = new System.Drawing.Size(49, 13);
            this.lblEntityIPCaption.TabIndex = 0;
            this.lblEntityIPCaption.Text = "Entity IP:";
            // 
            // FormEntityConfig
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(269, 140);
            this.Controls.Add(this.gbxConfigureEntity);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormEntityConfig";
            this.Opacity = 0.9D;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configure Entity";
            this.TopMost = true;
            this.gbxConfigureEntity.ResumeLayout(false);
            this.gbxConfigureEntity.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxConfigureEntity;
        private System.Windows.Forms.TextBox txtEntityIP;
        private System.Windows.Forms.Label lblEntityIPCaption;
        private System.Windows.Forms.Button btnOK;
    }
}