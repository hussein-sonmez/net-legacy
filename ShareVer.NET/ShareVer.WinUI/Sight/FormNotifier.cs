﻿using System;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using Entity.Genetics.Afferent;
using Entity.Proxy.Integrator;
using System.Net;
using Entity.Proxy.Integrator.Base;
using Entity.Proxy.NetworkEntity;
using ShareVerSln.WinUI.Archive.Integrator;
using System.Drawing;
using UzakNesne;
using Entity.Proxy.NetworkData;

namespace ShareVerSln.WinUI.Sight
{
    public partial class FormNotifier : Form
    {
        TEntityIntegrator MainEntityIntegrator;
        TEntityIntegrator RemoteDTEntityIntegrator;
        FormPictureArea pictureBox;
        #region ctors
        public FormNotifier()
        {
            InitializeComponent();
            string svListPath = Directory.GetCurrentDirectory() + ConfigurationManager.AppSettings["svListPath"];
            MainEntityIntegrator = new TEntityIntegrator();
            SGeneModifier.Init(svListPath);
            InitNetwork();
            InitLocal();
        }
        private void InitLocal()
        {
            MainEntityIntegrator.Server.NetworkData.ActionPerformed += MainProxyActionHandle;
            MainEntityIntegrator.Server.NetworkData.EntityConnected += ProxyReceivedServerConnectionHandle;
            MainEntityIntegrator.Server.NetworkData.EntitySignalled += EntitySignalledHandle;
            MainEntityIntegrator.Server.NetworkData.EntityDisconnected += EntityDisconnectedHandle;
            pictureBox = new FormPictureArea();
            UpdateEntries(SGeneTranscriptor.ReadLines());
        }
        private void InitNetwork()
        {
            UpdateServerList();
            niMain.Visible = true;
        }

        private void UpdateServerList()
        {
            tsmiServerList.DropDownItems.Clear();
            foreach (IPAddress ip in MainEntityIntegrator.GetHostIPList(Dns.GetHostName()))
            {
                ToolStripMenuItem tsmiServer = new ToolStripMenuItem(ip.ToString());
                tsmiServer.Tag = ip;
                tsmiServer.Click += new EventHandler(tsmiServer_Click);
                tsmiServer.ForeColor = Color.FromKnownColor(KnownColor.Black);
                tsmiServerList.DropDownItems.Add(tsmiServer);
            }
        }

        void tsmiServer_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = sender as ToolStripMenuItem;
            IPAddress ip = mi.Tag as IPAddress;
            MainEntityIntegrator.DisconnectServer();
            MainEntityIntegrator.InitService("shareVer", 112, ip);
            MainEntityIntegrator.ConnectServer();

            mi.ForeColor = Color.FromKnownColor(KnownColor.Red);
            foreach (ToolStripMenuItem item in tsmiServerList.DropDownItems)
            {
                if (item == mi)
                    continue;
                item.ForeColor = Color.FromKnownColor(KnownColor.Black);
            }
            niMain.ShowBalloonTip(1000, "Server Started", "Your IP is: " + MainEntityIntegrator.Server.NetworkData.IP, ToolTipIcon.Info);
            tsmiBindEntity.Enabled = true;
        }
        private void EntityDisconnectedHandle()
        {
            MessageBox.Show(String.Format("Entity<{0}> disconnected", MainEntityIntegrator.Entity.NetworkData.IP));
        }
        private void EntitySignalledHandle(TNetworkData signal)
        {

            niMain.ShowBalloonTip(1000, "Server Signalled You.", "SVlist Updated..", ToolTipIcon.Info);
            string[] lines = signal.HandBag["lines"] as string[];
            UpdateEntries(lines);
        }
        private void ProxyReceivedServerConnectionHandle(string serverInfo)
        {
            niMain.ShowBalloonTip(1000, "Connection Handled", serverInfo, ToolTipIcon.Info);
        }
        private TNetworkData MainProxyActionHandle(TNetworkData data)
        {
            //save as remote
            string extractTarget = Path.Combine(
                Application.StartupPath,
                MainEntityIntegrator.SharedProxyFolderName,
                data.HandBag["SVName"] as string,
                data.HandBag["NowMark"] as string);
            Directory.CreateDirectory(extractTarget);
            if (DialogResult.Cancel != MessageBox.Show(
                    String.Format("localZip will be extracted to <{0}>", extractTarget),
                    "Warning.",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Warning))
                SArchiveIntegrator.ExtractData(data.Data, extractTarget);
            return data;
        }
        delegate void invoke_delegate();
        private void UpdateEntries(string[] lines)
        {
            invoke_delegate inv = () =>
            {
                tsmiShareVerEntries.DropDownItems.Clear();
                foreach (string entry in lines)
                {
                    string name = entry.Split(SGeneTranslator.Seperator)[0];
                    string path = entry.Split(SGeneTranslator.Seperator)[1];
                    ToolStripMenuItem tsmi = new ToolStripMenuItem(name);
                    tsmi.Click += new EventHandler(tsmi_Click);
                    tsmi.Tag = path;
                    tsmiShareVerEntries.DropDownItems.Add(tsmi);
                }
            };
            inv.Invoke();
        }
        void tsmi_Click(object sender, EventArgs e)
        {
            using (TNetworkData data = new TNetworkData())
            {
                ToolStripMenuItem s = sender as ToolStripMenuItem;
                //Save locally first
                string nowMark = DateTime.Now.ToString("yyyy.MM.dd.HH.mm.ss");
                string zipFolder = Path.Combine(s.Tag as string, nowMark);
                string zipPath = Path.Combine(zipFolder, "temp.7z");
                string compressTarget = SArchiveIntegrator.CompressFolder(zipPath, s.Tag as string);

                //args receives data
                FileStream zip = new FileStream(zipPath, FileMode.Open, FileAccess.Read);
                MemoryStream ms = new MemoryStream();
                zip.CopyTo(ms);
                data.Data = ms.ToArray();
                ms.Close();
                zip.Close();
                Directory.Delete(zipFolder, true);

                //add keys to HandBag of arg for PerformAction
                data.HandBag["NowMark"] = nowMark;
                data.HandBag["SVName"] = s.Text;

                //perform action
                MainEntityIntegrator.Entity.ProxyObject.PerformAction(data);
            }
        }
        #endregion

        #region event handler
        private void tsmiAddShareVer_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == fbdAddShareVer.ShowDialog())
            {
                string fullPath = fbdAddShareVer.SelectedPath;
                string pathName = fullPath.Substring(fullPath.LastIndexOf('\\') + 1);
                SGeneModifier.GenerateCodon(pathName, fullPath);
                UpdateEntries(SGeneTranscriptor.ReadLines());
                TNetworkData data = new TNetworkData();
                data.HandBag["lines"] = SGeneTranscriptor.ReadLines();
                MainEntityIntegrator.Entity.ProxyObject.SignalEntity(data);
            }
        }
        private void tsmiExit_Click(object sender, EventArgs e)
        {
            MainEntityIntegrator.DisconnectEntity();
            MainEntityIntegrator.DisconnectServer();
            niMain.Visible = false;
            niMain.Dispose();
            this.Close();
        }
        #endregion

        private void tsmiBindEntity_Click(object sender, EventArgs e)
        {
            using (FormEntityConfig fec = new FormEntityConfig())
            {
                if (DialogResult.OK == fec.ShowDialog())
                {
                    if (!MainEntityIntegrator.InitEntity(fec.EntityIP))
                    {
                        MessageBox.Show("Connection Refused.");
                        return;
                    }
                    tsmiAddShareVer.Enabled = true;
                    tsmiShareVerEntries.Enabled = true;
                    tsmiInjectEntity.Enabled = true;
                }
            }
        }

        private void tsmiInjectEntity_Click(object sender, EventArgs e)
        {

            RemoteDTEntityIntegrator = new TEntityIntegrator();
            RemoteDTEntityIntegrator.InitService("shareVer", 112, MainEntityIntegrator.Server.NetworkData.IP);
            RemoteDTEntityIntegrator.ConnectServer();
            pictureBox.Show();
            RemoteDTEntityIntegrator.Entity.NetworkData.ActionPerformed += new ActionPerformedDelegate(NetworkData_ActionPerformed);
            RemoteDTEntityIntegrator.InitEntity(MainEntityIntegrator.Entity.NetworkData.IP);

            //fill data
            TNetworkData data = RemoteDTEntityIntegrator.Entity.NetworkData;
            data.Data = pictureBox.prx.EkranGoruntusu();

            //perform action.
            RemoteDTEntityIntegrator.Entity.ProxyObject.PerformAction(data);

            //Close Remote Entity
            RemoteDTEntityIntegrator.DisconnectServer();
            RemoteDTEntityIntegrator.DisconnectEntity();
        }

        TNetworkData NetworkData_ActionPerformed(TNetworkData data)
        {
            MemoryStream ms = new MemoryStream(data.Data, true);
            pictureBox.pbxRemoteScreen.Image = Image.FromStream(ms, false);
            return data;
        }
    }
}
