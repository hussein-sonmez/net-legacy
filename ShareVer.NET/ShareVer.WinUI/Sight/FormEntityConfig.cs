﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace ShareVerSln.WinUI.Sight
{
    public partial class FormEntityConfig : Form
    {
        public FormEntityConfig()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            EntityIP = IPAddress.Parse(txtEntityIP.Text);
        }

        public IPAddress EntityIP { get; set; }
    }
}
