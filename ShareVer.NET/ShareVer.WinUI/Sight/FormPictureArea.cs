﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UzakNesne;
using System.IO;
using ShareVerSln.WinUI.ProxyClone;

namespace ShareVerSln.WinUI.Sight
{
    public partial class FormPictureArea : Form
    {
        public FormPictureArea()
        {
            InitializeComponent();
        }
        internal ProxyNesne prx;
        private void pbxRemoteScreen_Click(object sender, EventArgs e)
        {
            prx.FareClick();
        }

        private void pbxRemoteScreen_MouseMove(object sender, MouseEventArgs e)
        {
            prx.FareKiprastir(e.Location);
        }

        private void tmrMain_Tick(object sender, EventArgs e)
        {
            byte[] data = prx.EkranGoruntusu();
            MemoryStream ms = new MemoryStream(data);
            pbxRemoteScreen.Image = Image.FromStream(ms);
        }

        private void FormPictureArea_KeyPress(object sender, KeyPressEventArgs e)
        {
            prx.Tusla(Keyboard.klavye.VK_KEY_R);
        }

        private void FormPictureArea_Load(object sender, EventArgs e)
        {
            tmrMain.Start();
            prx = new ProxyNesne();
        }

    }
}
