﻿namespace ShareVerSln.WinUI.Sight
{
    partial class FormPictureArea
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PnlMain = new System.Windows.Forms.Panel();
            this.pbxRemoteScreen = new System.Windows.Forms.PictureBox();
            this.tmrMain = new System.Windows.Forms.Timer(this.components);
            this.PnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRemoteScreen)).BeginInit();
            this.SuspendLayout();
            // 
            // PnlMain
            // 
            this.PnlMain.Controls.Add(this.pbxRemoteScreen);
            this.PnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlMain.Location = new System.Drawing.Point(0, 0);
            this.PnlMain.Name = "PnlMain";
            this.PnlMain.Size = new System.Drawing.Size(564, 370);
            this.PnlMain.TabIndex = 0;
            // 
            // pbxRemoteScreen
            // 
            this.pbxRemoteScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxRemoteScreen.Location = new System.Drawing.Point(0, 0);
            this.pbxRemoteScreen.Name = "pbxRemoteScreen";
            this.pbxRemoteScreen.Size = new System.Drawing.Size(564, 370);
            this.pbxRemoteScreen.TabIndex = 0;
            this.pbxRemoteScreen.TabStop = false;
            this.pbxRemoteScreen.Click += new System.EventHandler(this.pbxRemoteScreen_Click);
            this.pbxRemoteScreen.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbxRemoteScreen_MouseMove);
            // 
            // tmrMain
            // 
            this.tmrMain.Tick += new System.EventHandler(this.tmrMain_Tick);
            // 
            // FormPictureArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 370);
            this.Controls.Add(this.PnlMain);
            this.Name = "FormPictureArea";
            this.Opacity = 0.9D;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormPictureArea";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FormPictureArea_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FormPictureArea_KeyPress);
            this.PnlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxRemoteScreen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PnlMain;
        internal System.Windows.Forms.PictureBox pbxRemoteScreen;
        private System.Windows.Forms.Timer tmrMain;
    }
}