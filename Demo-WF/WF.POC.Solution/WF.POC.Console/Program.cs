﻿using System;
using System.Linq;
using System.Activities;
using System.Activities.Statements;
using System.Dynamic;

namespace WF.POC.Console {

	class Program {
		static void Main(string[] args) {
			var activity = new GuessNumberWF();
			var invoker = new WorkflowInvoker(activity);

			dynamic invokerArguments = new ExpandoObject();
			invokerArguments.upperLimit = 15;
			invokerArguments.lowerLimit = 1;
			invoker.Invoke(invokerArguments);
		}
	}
}
