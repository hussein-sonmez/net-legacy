﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO {
	public class Function : BaseModel {

		#region Properties

		public String Name { get; set; }
		public String Description { get; set; }
		public String FunctionCode { get; set; }
		public Boolean IsActive { get; set; }
		public EFunctionType FunctionType { get; set; }
		public EModuleType ModuleType { get; set; }

		#endregion

		#region Navigation Properties

		public virtual ICollection<RoleFunction> RoleFunctions { get; set; }

		#endregion


	}
}
