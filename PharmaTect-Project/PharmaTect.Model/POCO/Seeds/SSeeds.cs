﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PharmaTect.Model.EntityWrappers;
using PharmaTect.Core.Extensions;

namespace PharmaTect.Model.POCO.Seeds {
	public static class SSeeds {
		public static IEnumerable<User> ForUsers(Int32 N = 100) {

			var seed = new BaseSeeder<User>()
				.SetSeeder(index => new User() {
					ID = index,
					FullName = "Neriman Ayvacı",
					Description = "Kullanıcı Neriman Ayvacı-" + index,
					EMail = "neriman@ayvaci.com-" + index,
					IsActive = true,
					Password = ("1q2w3e4r5t-" + index),
					PasswordNeverExpires = true,
					CanChangePassword = false,
					MustChangePassword = false,
					UserName = "nerimana-" + index,
                    SecurityAnswer = "Güvenlik Sorunu-" + index,
                    SecurityQuestion = "Güvenlik Cevabı-" + index,
                    MembershipStatus = EMembershipStatus.Pending
				}).Seed(N);
			return seed;

		}

	}
}
