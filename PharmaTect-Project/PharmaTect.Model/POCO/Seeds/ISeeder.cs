﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO.Seeds {
	public interface ISeeder<TEntity>
		where TEntity : class {

		ISeeder<TEntity> SetSeeder(Func<Int32, TEntity> seeder);
		IEnumerable<TEntity> Seed(Int32 N = 10);

	}
}
