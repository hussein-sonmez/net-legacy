﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO.Seeds {
	internal class BaseSeeder<TEntity> : ISeeder<TEntity> 
		where TEntity: class{
		public ISeeder<TEntity> SetSeeder(Func<int, TEntity> seeder) {
			_Seeder = seeder;
			return this;
		}
		private Func<int, TEntity> _Seeder;

		public IEnumerable<TEntity> Seed(int N = 10) {
			var seed = new List<TEntity>();
			foreach (var index in Enumerable.Range(1, N)) {
				seed.Add(_Seeder(index));
			}

			return seed.ToArray();
		}
	}
}
