﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO
{
    public class AssignedGenericType : BaseModel
    {
        public virtual GenericCodeType ParentGenericCodeType { get; set; }
        public virtual GenericCodeType SubGenericCodeType { get; set; }
    }
}
