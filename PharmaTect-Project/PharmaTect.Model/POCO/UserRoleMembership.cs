﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO {
	public class UserRoleMembership : BaseModel {

		#region Properties
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
        #endregion

        #region Navigation Properties

        public User User { get; set; }
		public Role Role { get; set; }

		#endregion

	}
}
