﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO {
	public class RoleFunction : BaseModel {

		#region Navigation Properties

		public virtual Role Role { get; set; }
		public virtual Function Function { get; set; }

		#endregion

	}
}
