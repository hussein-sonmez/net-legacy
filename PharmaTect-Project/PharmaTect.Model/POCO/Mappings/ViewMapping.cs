﻿using PharmaTect.Model.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model {
	public class ViewMapping : BaseMapping<View> {

		#region Ctors
		public ViewMapping() {

            #region PropertiesMapping

            this.Property(t => t.Name)
                .HasColumnName("Name").IsRequired();

            this.Property(t => t.UriType)
                .HasColumnName("UriType").IsRequired();

            this.Property(t => t.Height)
				.HasColumnName("Height");

			this.Property(t => t.Width)
				.HasColumnName("Width");

			this.Property(t => t.Uri)
				.HasColumnName("Uri").IsRequired();

			this.Property(t => t.Code).HasMaxLength(64)
				.HasColumnName("Code");

			this.Property(t => t.Target)
				.HasColumnName("Target");

			this.Property(t => t.ShowInMenu)
				.HasColumnName("ShowInMenu");

			this.Property(t => t.Order)
				.HasColumnName("Order");

			this.Property(t => t.IsDefaultView)
				.HasColumnName("IsDefaultView");

			#endregion

			#region Navigation Properties

			this.HasOptional(t => t.Parent)
				.WithOptionalDependent()
				.Map(x => x.MapKey("ParentID"));

			this.HasOptional(t => t.DefaultFunction)
				.WithOptionalDependent()
				.Map(x => x.MapKey("DefaultFunctionID"));

			#endregion

			this.ToTable("View", "Infrastructure");

		}

		#endregion

	}
}

