﻿using PharmaTect.Model.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model {
	public class FunctionMapping : BaseMapping<Function> {

		#region Ctors
		public FunctionMapping() {

			#region PropertiesMapping

			this.Property(t => t.FunctionType)
				.HasColumnName("FunctionType").IsRequired();

			this.Property(t => t.IsActive)
				.HasColumnName("IsActive");

            this.Property(t => t.Name)
                .HasColumnName("Name").IsRequired();

            this.Property(t => t.FunctionCode).HasMaxLength(500)
                .HasColumnName("FunctionCode")
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("UQ_FunctionCode", 1) { IsUnique = true }));

            this.Property(t => t.Description)
				.HasColumnName("Description")
				.IsOptional();

			#endregion

			this.ToTable("Function", "Infrastructure");

		}

		#endregion

	}
}

