﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO.Mappings
{
    public class GenericCodeTypeMapping : BaseMapping<GenericCodeType>
    {
        public GenericCodeTypeMapping()
        {
            #region Properties

            this.Property(g => g.IsActive)
                .IsOptional()
                .HasColumnName("IsActive");

            this.Property(g => g.HasRelatedGenericCodeType)
                .IsOptional()
                .HasColumnName("HasRelatedGenericCodeType");

            this.Property(g => g.IsPossibleInRoot)
                .IsOptional()
                .HasColumnName("IsPossibleInRoot");

            this.Property(g => g.Description)
                .IsOptional()
                .HasColumnName("Description");

            this.Property(g => g.Definition)
                .IsRequired()
                .HasColumnName("Definition");

            #endregion

            #region Navigation Properties

            #endregion

            this.ToTable("GenericCodeType", "Infrastructure");

        }
    }
}
