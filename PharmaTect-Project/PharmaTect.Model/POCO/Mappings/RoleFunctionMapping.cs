﻿using PharmaTect.Model.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model {
	public class RoleFunctionMapping : BaseMapping<RoleFunction> {

		#region Ctors
		public RoleFunctionMapping() {

			#region NavigationMapping

			this.HasRequired(t => t.Role)
				.WithMany(t => t.RoleFunctions)
				.Map(x => x.MapKey("RoleID"));

			this.HasRequired(t => t.Function)
				.WithMany(t => t.RoleFunctions)
				.Map(x => x.MapKey("FunctionID"));

			#endregion

			this.ToTable("RoleFunction", "Infrastructure");

		}

		#endregion

	}
}

