﻿using PharmaTect.Model.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model {
	public class ActorMapping : BaseMapping<Actor> {

		#region Ctors
		public ActorMapping() {

            #region PropertiesMapping

            #endregion

            #region Navigation Properties

            this.HasOptional(t => t.UpActor)
                .WithOptionalDependent().Map(x => x.MapKey("UpActorID"));

            #endregion

            this.ToTable("Actor", "Enterprise");

		}

		#endregion

	}
}

