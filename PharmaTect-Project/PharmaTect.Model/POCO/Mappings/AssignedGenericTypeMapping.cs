﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO.Mappings
{
    public class AssignedGenericTypeMapping : BaseMapping<AssignedGenericType>
    {
        public AssignedGenericTypeMapping()
        {
            #region Properties

            #endregion

            #region Navigation Properties

            this.HasRequired(t => t.ParentGenericCodeType)
                .WithMany(t=>t.ParentGenericCodeTypes)
                .Map(x => x.MapKey("ParentGenericCodeTypeID"))
                .WillCascadeOnDelete(false);

            this.HasRequired(t => t.SubGenericCodeType)
                .WithMany(t => t.SubGenericCodeTypes)
                .Map(x => x.MapKey("SubGenericCodeTypeID"))
                .WillCascadeOnDelete(false);

            #endregion

            this.ToTable("AssignedGenericType", "Infrastructure");

        }
    }
}
