﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO {
    public class GenericCodeType : BaseModel {

        public bool IsActive { get; set; }
        public bool HasRelatedGenericCodeType { get; set; }
        public bool IsPossibleInRoot { get; set; }
        public string Description { get; set; }
        public string Definition { get; set; }

        public virtual ICollection<AssignedGenericType> ParentGenericCodeTypes { get; set; }
        public virtual ICollection<AssignedGenericType> SubGenericCodeTypes { get; set; }
        public virtual ICollection<GenericLookup> GenericLookups { get; set; }
    }
}
