﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PharmaTect.Model {
	public enum EMaritalStatus {

		[Display(Description="Boşanmış")]
		Divorced,
		[Display(Description = "Evli")]
		Married,
		[Display(Description = "Dul")]
		Widow,
		[Display(Description = "Evli Değil")]
		Single

	}
}
