﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PharmaTect.Model {
	public enum ETarget {

		[Display(Description = "İçeride")]
		Self = 1,
		[Display(Description = "Yeni Sekmede")]
		NewTab,
		[Display(Description="Yeni Pencerede")]
		NewWindow

	}
}
