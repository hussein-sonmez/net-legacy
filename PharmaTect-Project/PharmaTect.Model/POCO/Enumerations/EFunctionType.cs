﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PharmaTect.Model {
	public enum EFunctionType {

		[Display(Description = "Rapor")]
		Report = 1,

		[Display(Description = "Kontrol")]
		Control,

		[Display(Description = "Fonksiyon Yetkisi")]
		Functional
	}
}
