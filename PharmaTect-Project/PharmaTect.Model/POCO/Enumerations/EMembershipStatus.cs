﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model {
    public enum EMembershipStatus {

        [Display(Description="Onaylandı")]
        Approved = 1,
        [Display(Description="Bekliyor")]
        Pending,
        [Display(Description="Yasaklı")]
        Banned

    }
}
