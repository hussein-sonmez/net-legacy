﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model  {
    public enum EViewCategory {

        [Display(Description ="URL")]
        URL=1,

        [Display(Description ="Link")]
        Link,

        [Display(Description ="Ekran")]
        Screen,

        [Display(Description = "Menu Item")]
        MenuItem,

    }
}
