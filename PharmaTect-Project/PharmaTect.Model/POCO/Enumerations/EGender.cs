﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model {
	public enum EGender {

        [Display(Description ="Erkek")]
        Male = 1,
        [Display(Description ="Kadın")]
        Female,
        [Display(Description ="Belirtilmemiş")]
        Unstated

    }
}
