﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model {
	public class BaseMapping<TPOCO> : EntityTypeConfiguration<TPOCO> 
	where TPOCO : BaseModel {
		
		#region Ctor

		public BaseMapping() {
			this.HasKey(t => t.ID).Property(t => t.ID).HasColumnName("ID")
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();

		}

		#endregion

	}
}
