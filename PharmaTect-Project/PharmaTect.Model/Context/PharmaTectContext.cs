﻿using PharmaTect.Model.POCO;
using PharmaTect.Model.POCO.Mappings;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model {

    public class PharmaTectContext : DbContext, IDisposable {

        #region Ctor

        public PharmaTectContext()
			: base(SDatabaseService.LocalConnectionString) {

        }

        #endregion

        #region Sets

		public DbSet<Actor> Actors { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<UserRoleMembership> UserRoleMemberships { get; set; }
		public DbSet<View> Views { get; set; }
		public DbSet<Function> Functions { get; set; }
		public DbSet<RoleFunction> RoleFunctions { get; set; }
		public DbSet<GenericLookup> GenericLookups { get; set; }
		public DbSet<GenericCodeType> GenericCodeTypes { get; set; }
		public DbSet<AssignedGenericType> AssignedGenericTypes { get; set; }

        #endregion

		#region Overrides
		protected override void OnModelCreating(DbModelBuilder modelBuilder) {

			base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new ActorMapping());
			modelBuilder.Configurations.Add(new UserRoleMembershipMapping());
			modelBuilder.Configurations.Add(new RoleMapping());
			modelBuilder.Configurations.Add(new UserMapping());
			modelBuilder.Configurations.Add(new ViewMapping());
			modelBuilder.Configurations.Add(new FunctionMapping());
			modelBuilder.Configurations.Add(new RoleFunctionMapping());
			modelBuilder.Configurations.Add(new GenericLookupMapping());
			modelBuilder.Configurations.Add(new AssignedGenericTypeMapping());
			modelBuilder.Configurations.Add(new GenericCodeTypeMapping());

		}
		#endregion

    }
}
