﻿using System;
using System.Linq;
using System.Data.Entity;
using PharmaTect.Model.Migrations;
using PharmaTect.Model.POCO.Seeds;

namespace PharmaTect.Model {
    public static class SDatabaseService {

		#region ConnectionString

		public static String DevelopmentConnectionString {
			get {
				return SwitchConnectionString("DevelopmentConnectionString");
			}
		}
		public static String LocalConnectionString {
			get {
				return SwitchConnectionString("LocalConnectionString");
			}
		}

		public static String TestsConnectionString {
			get {
				return SwitchConnectionString("TestsConnectionString");
			}
		}

		#endregion

		#region Methods

		private static string SwitchConnectionString(String csKey) {
			return System.Configuration.ConfigurationManager.ConnectionStrings[csKey].ConnectionString;
		}

		public static void InitializeDatabase(PharmaTectContext context) {

			context.Configuration.LazyLoadingEnabled = true;
			var init =
				new MigrateDatabaseToLatestVersion<PharmaTectContext, Configuration>();
			Database.SetInitializer<PharmaTectContext>(init);
			init.InitializeDatabase(context);

		}

		public static void Seed(PharmaTectContext context) {
			var userSeed = SSeeds.ForUsers();
			userSeed.ToList().ForEach(u => context.Users.Add(u));
			context.SaveChanges();
		}

		#endregion

	}
}
