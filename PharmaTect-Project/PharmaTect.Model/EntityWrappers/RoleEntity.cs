﻿using PharmaTect.Model.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PharmaTect.Model.EntityWrappers {
    [NotMapped]
	public class RoleEntity : Role, IDataErrorInfo {

		#region Computeds

		#endregion

		#region IDataErrorInfo Members

		public string Error {
			get { return "Bilinmeyen Hata"; }
		}

		public String this[String columnName] {
			get {
				string result = null;
				if (columnName == "UserName") {
					if (String.IsNullOrEmpty(base.Name)) {
						result = "Lütfen Bir Kullanıcı Adı Giriniz";
					}
				}
				return result;
			}
		}

		#endregion
	}
}
