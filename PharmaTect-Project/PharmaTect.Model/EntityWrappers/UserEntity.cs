﻿using PharmaTect.Model.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PharmaTect.Model.EntityWrappers {

    [NotMapped]
	public class UserEntity : User, IDataErrorInfo {

		#region IDataErrorInfo Members

		public string Error {
			get { return "Bilinmeyen Hata"; }
		}

		public String this[String columnName] {
			get {
				string result = null;
				if (columnName == "UserName") {
					if (String.IsNullOrEmpty(UserName)) {
						result = "Lütfen Bir Kullanıcı Adı Giriniz";
					}
				}
				if (columnName == "EMail") {
					if (String.IsNullOrEmpty(base.EMail)) {
						result = "Lütfen Bir E-Posta Giriniz";
					} else if (!Regex.IsMatch(EMail, @"[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}")) {
						result = "Lütfen E-Posta Adresinizi Gözden Geçiriniz";
					}
				}
				if (columnName == "Password") {
					if (String.IsNullOrEmpty(base.Password)) {
						result = "Lütfen Bir Şifre Giriniz";
					}
				}
				return result;
			}
		}

		#endregion
	}
}
