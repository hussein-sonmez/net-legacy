﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.Contracts.Base {
	public interface IBaseModel {

		Int32 ID { get; set; }

		String Description { get; set; }

		Int32 CreatedBy { get; set; }
		DateTime CreatedOn { get; set; }
		Int32 ModifiedBy { get; set; }
		DateTime ModifiedOn { get; set; }
		Int32 DeletedBy { get; set; }
		DateTime DeletedOn { get; set; }

	}
}
