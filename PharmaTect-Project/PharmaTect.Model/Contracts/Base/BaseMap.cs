﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.Contracts.Base {
	public class BaseMap<TEntity> : EntityTypeConfiguration<TEntity> 
		where TEntity : IBaseModel {

		#region Ctor

		public BaseMap() {
			this.HasKey(t => t.ID).Property(t => t.ID).HasColumnName("ID")
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();

			this.Property(t => t.Description).HasMaxLength(128).HasColumnName("Description");
			this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
			this.Property(t => t.CreatedOn).HasColumnName("CreatedOn")
				.IsOptional().HasColumnType("datetime2");
			this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
			this.Property(t => t.DeletedOn).HasColumnName("DeletedOn")
				.IsOptional().HasColumnType("datetime2");
			this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
			this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn")
				.IsOptional().HasColumnType("datetime2");

		}

		#endregion

	}
}
