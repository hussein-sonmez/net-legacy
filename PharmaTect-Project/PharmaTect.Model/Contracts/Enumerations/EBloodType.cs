﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.Contracts.Enumerations  {
    public enum EBloodType {

        [Display(Description ="0 RH Pozitif")]
        ORHP=1,
        [Display(Description ="0 RH Negatif")]
        ORHN,

        [Display(Description ="A RH Pozitif")]
        ARHP,
        [Display(Description ="A RH Negatif")]
        ARHN,

        [Display(Description ="B RH Pozitif")]
        BRHP,
        [Display(Description ="B RH Negatif")]
        BRHN,

        [Display(Description ="AB RH Pozitif")]
        ABRHP,
        [Display(Description ="AB RH Negatif")]
        ABRHN,
    }
}
