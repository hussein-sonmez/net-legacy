﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PharmaTect.Model.Contracts.Enumerations {
	public enum EMembershipStatus {

		[Display(Description="Onaylı")]
		Approved = 1,
		[Display(Description = "Yasaklı")]
		Banned,
		[Display(Description = "Bekliyor")]
		Pending,

	}
}
