﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PharmaTect.Model.Contracts.Enumerations{

	public enum EModuleType {

		[Display(Description = "Windows Modülü")]
		Windows = 1,
		[Display(Description = "Mobil Modül")]
		Mobile,
		[Display(Description = "Web Modülü")]
		Web,
		[Display(Description = "WPF Modülü")]
		WPF,

	}

}
