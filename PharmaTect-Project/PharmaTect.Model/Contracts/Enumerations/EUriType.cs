﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PharmaTect.Model.Contracts.Enumerations {
	public enum EUriType {

		[Display(Description="Program")]
		Executable,
		[Display(Description = "Web Adresi")]
		Url,
		[Display(Description = "Dosya Yolu")]
		Path

	}
}
