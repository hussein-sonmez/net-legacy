﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PharmaTect.Model.Contracts.Enumerations;

namespace PharmaTect.Model.Contracts {
	public interface IUser {

		#region Properties

		String UserName { get; set; }
		String Password { get; set; }
		String FullName { get; set; }
		String EMail { get; set; }
		EMembershipStatus Status { get; set; }
		Boolean PasswordNeverExpires { get; set; }
		Boolean UserMustChangePassword { get; set; }
		Boolean EnforcePasswordPolicy { get; set; }
		Boolean UserCanChangePassword { get; set; }
		String DefaultCulture { get; set; }
		Boolean Passive { get; set; }
		DateTime LastAccessTime { get; set; }
		DateTime PasswordValidityDate { get; set; }
		DateTime PasswordChangeOn { get; set; }
		String LastAccessIP { get; set; }
		Int32 PasswordChangeBy { get; set; }

		#endregion

		#region Navigation Properties

		ICollection<UserRoleMembership> UserRoleMemberships { get; set; }
		ICollection<Person> Persons { get; set; }

		#endregion

	}
}
