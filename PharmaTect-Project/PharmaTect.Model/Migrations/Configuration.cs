namespace PharmaTect.Model.Migrations {
	using System;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Linq;
	using System.Collections.Generic;

	internal sealed class Configuration : DbMigrationsConfiguration<PharmaTectContext> {
		public Configuration() {
			AutomaticMigrationsEnabled = true;
			AutomaticMigrationDataLossAllowed = true;
		}

		protected override void Seed(PharmaTectContext context) {
			SDatabaseService.Seed(context);
		}
	}
}
