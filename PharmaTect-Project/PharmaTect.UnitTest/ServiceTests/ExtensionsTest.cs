﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PharmaTect.Core.Extensions;
using PharmaTect.Core;
using PharmaTect.Model;

namespace PharmaTect.UnitTest {
    [TestClass]
    public class ExtensionsTest {
        [TestMethod]
        public void GetInt32CodeWorks() {

			var en = EContactType.FaxNumber;
            var retractedIntegerCode = en.GetInt32Code();
            Assert.AreEqual(2, retractedIntegerCode);

        }

        [TestMethod]
        public void GetEnumGenericWorks() {

			var desc = "Cep Telefonu";
			var retractedEnum = desc.GetEnum<EContactType>();
            Assert.AreEqual(3, retractedEnum.GetInt32Code());

        }
    }
}
