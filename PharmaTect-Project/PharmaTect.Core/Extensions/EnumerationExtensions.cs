﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Core.Extensions {
    public static class EnumerationExtensions {

        public static Int32 GetInt32Code(this Enum enumeration) {
            return Convert.ToInt32(enumeration);
        }

        public static TEnum GetEnum<TEnum>(this String enumerationValue) {

            if (typeof(TEnum).IsEnum) {

                try {
                    return (TEnum)Enum.Parse(typeof(TEnum), enumerationValue);
                } catch (Exception) {
                    var descriptionValue = enumerationValue;
                    var fieldName = typeof(TEnum).GetFields()
                        .Where(fi=> fi.GetCustomAttribute<DisplayAttribute>() != null)
                        .Single(fi => 
                        fi.GetCustomAttribute<DisplayAttribute>()
                        .Description == descriptionValue).Name;
                    return (TEnum)Enum.Parse(typeof(TEnum), fieldName);
                }
            } else {
                throw new ArgumentException("Needed Enum Got " + typeof(TEnum).Name);
            }

        }

    }
}
