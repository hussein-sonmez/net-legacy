﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Core.Extensions {
    public static class DataComponentExtensions {

        public static TParsed ParseOrDefault<TParsed>(this DbDataReader reader, String column, Func<String, TParsed> parser = null) {
            if (reader.IsClosed) {
                throw new InvalidOperationException("Connection is Closed");
            } else {

                if (reader[column] != null) {
                    if (parser != null) {
                        return parser(reader[column].ToString());
                    } else {
                        return reader[column].ParseInto<TParsed>();
                    }
                } else {
                    return default(TParsed);
                }
            }
        }

        public static Int32 Magnitude<TEntity>(this DbSet<TEntity> dset, Func<TEntity, Boolean> filterQuery = null)
            where TEntity : class {
            if (filterQuery != null) {
                return (from ent in dset where filterQuery(ent) select ent).Count();
            } else {
                return (from ent in dset select ent).Count();
            }
        }

    }
}
