﻿using DDM.Core.Base.Settings;
using DDM.Core.Base.Classes;
using DDM.Core.Base.Cells;
using System;

namespace DDM.Core.Base.ToolBox
{
    internal class Methods
    {
        internal static void FillRecursive(ref Block parentBlock, ref CellSequence cellSeq)
        {
            SetChildren(ref parentBlock, ref cellSeq);
            int limit = parentBlock.Childs.Count;
            if (limit == 0)
                return;
            Block childBlock = null;
            for (int iBlock = 0; iBlock < limit; iBlock++)
            {
                childBlock = parentBlock.Childs[iBlock];
                FillRecursive(ref childBlock, ref cellSeq);
            }
        }
        internal static void SetChildren(ref Block parentBlock, ref CellSequence cellSeq)
        {
            //Sets Children of current cell sequence curCellSeq
            Cell startCell = parentBlock.StartCell, curCell = null, endCell = null;
            Block curBlock = null;
            
            int endIndex = parentBlock.EndCell.Index;
            for (int i = startCell.Index + 1; i < endIndex; i++)
            {
                curCell = cellSeq[i];
                //find start and end cell of current block:
                if (Methods.IsCellBorder(curCell))
                {
                    curBlock = new Block();
                    endCell = cellSeq.GetCorrespondingCell(curCell);
                    curBlock.StartCell = curCell;
                    curBlock.EndCell = endCell;
                    curBlock.Parent = parentBlock;
                    parentBlock.Childs.Add(curBlock);
                    i = endCell.Index;
                }
                else if (!Methods.IsCellBorder(curCell))
                {
                    //non border cell is found:
                    curBlock = new Block();
                    curBlock.StartCell = curBlock.EndCell = curCell;
                    curBlock.Parent = parentBlock;
                    parentBlock.Childs.Add(curBlock);
                    curBlock = null;
                }
            } //for end
        }

        internal static string GetCellFullName(Cell cell)
        {
            Delimeter delim = Delims.GetDelimeterByType(cell.Type);
            string fullName = GetPureNameOfCell(cell);
            foreach (Cells.Attribute attr in cell.Attrs)
            {
                fullName += String.Format(@" {0}=""{1}""", attr.Name, attr.Value);
            }
            if (cell.Type == Delims.Borders[0].Type)
                fullName =
                    Delims.Standart.Begin + Delims.Borders[0].Begin +
                    fullName +
                    Delims.Borders[0].End + Delims.Standart.End;
            else if (cell.Type == Delims.Borders[1].Type)
                fullName =
                    Delims.Standart.Begin + Delims.Borders[1].Begin +
                    fullName +
                    Delims.Borders[1].End + Delims.Standart.End;
            else
                fullName =
                    Delims.Standart.Begin + delim.Begin + fullName + delim.End + Delims.Standart.End;
            return fullName;
        }
        internal static int GetDepthOfCell(Cell cell)
        {
            if (Methods.IsCellBorder(cell) & Methods.IsCellBalanced(cell))
            {
                if (cell.Type == Delims.Borders[0].Type)
                    return cell.x - cell.y - 1;
                else if (cell.Type == Delims.Borders[1].Type)
                    return cell.x - cell.y;
            }
            return -1;
        }
        internal static string GetPureNameOfCell(Cell cell)
        {
            string name = cell.Name;
            foreach (Delimeter delim in Delims.Extra)
            {
                name =
                    name.TrimStart(delim.Begin.ToCharArray()).TrimEnd(delim.End.ToCharArray());
            }
            return name;
        }
        internal static bool IsCellBalanced(Cell cell)
        {
            if (IsCellBorder(cell))
            {
                if (cell.Type == Delims.Borders[0].Type)
                    return cell.x - cell.y == cell.beta - cell.alpha + 1;
                else if (cell.Type == Delims.Borders[1].Type)
                    return cell.x - cell.y == cell.beta - cell.alpha - 1;
            }
            return true;
        }
        internal static bool IsCellBorder(Cell cell)
        {
            return (cell.Type == Delims.Borders[0].Type | cell.Type == Delims.Borders[1].Type);
        }
        internal static bool IsBlockBottom(Block block)
        {
            return block.Childs.Count == 0;
        }

        internal static string GetBlockContent(Block block, ref string corpus, bool BordersIncluded)
        {
            string text;
            int startIndex, endIndex;
            if (BordersIncluded)
            {
                startIndex = block.StartCell.iBgn;
                endIndex = block.EndCell.iEnd;
            }
            else
            {
                startIndex = block.StartCell.iEnd + 1;
                endIndex = block.EndCell.iBgn - 1;
            }
            text = corpus.Substring(startIndex, endIndex - startIndex + 1);
            return text;
        }
    }
}
