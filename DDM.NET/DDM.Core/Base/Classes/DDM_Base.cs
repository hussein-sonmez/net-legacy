﻿using System.Collections.Generic;
using System.Windows.Forms;
using DDM.Core.Base.ToolBox;
using DDM.Core.Base.Settings;
using DDM.Core.Base.Cells;

namespace DDM.Core.Base.Classes
{
    public abstract class DDM_Base
    {
        public DDM_Base()
        {
            InitializeCustomComponents();
        }
        internal List<Delimeter> Delimeters { get; set; }
        internal string Corpus { get; set; }
        protected CellSequence _seq;
        internal Block rootBlock;
        internal List<Block> ResultBlocks;

        internal delegate void ApplyMethod(Block curBlock, ref CellSequence mainSeq);
        internal delegate List<Block> CollectMethod(Block curBlock, ref CellSequence mainSeq);

        private void ApplyMethodToAllHandle(ref ApplyMethod method, ref Block pBlock)
        {
            Methods.SetChildren(ref pBlock, ref _seq);
            int limit = pBlock.Childs.Count;
            if (limit == 0)
                return;
            Block childBlock = null;
            for (int iBlock = 0; iBlock < limit; iBlock++)
            {
                childBlock = pBlock.Childs[iBlock];
                method(childBlock, ref _seq);
                ApplyMethodToAllHandle(ref method, ref childBlock);
            }
        }
        internal void ApplyMethodToAll(ApplyMethod method, ref Block pBlock)
        {
            if (rootBlock != null)
            {
                ApplyMethodToAllHandle(ref method, ref rootBlock);
            }
            else
                throw new RootBlockNotSetException();
        }
        private void CollectByMethodHandle(ref CollectMethod collect, ref Block pBlock)
        {
            Methods.SetChildren(ref pBlock, ref _seq);
            int limit = pBlock.Childs.Count;
            if (limit == 0)
                return;
            Block childBlock = null;
            for (int iBlock = 0; iBlock < limit; iBlock++)
            {
                childBlock = pBlock.Childs[iBlock];
                this.ResultBlocks.AddRange(collect(childBlock, ref _seq));
                CollectByMethodHandle(ref collect, ref childBlock);
            }
        }
        internal void CollectByMethod(ref CollectMethod collect, ref Block pBlock)
        {
            if (rootBlock != null)
            {
                CollectByMethodHandle(ref collect, ref rootBlock);
            }
            else
                throw new RootBlockNotSetException();
        }

        internal void FillSequence()
        {
            int bgnIndex = 0, endIndex = 0;
            _seq.BeginUpdate();
            while (true)
            {
                if (ParseCell(ref bgnIndex, ref endIndex))
                    continue;
                break;
            }
            _seq.EndUpdate();
        }
        internal void FillBlocks()
        {
            CellSequence mainSeq = _seq;
            //update root close:
            rootBlock.EndCell.Index = _seq[_seq.Count - 2].Index + 1;
            Methods.FillRecursive(ref rootBlock, ref mainSeq);
        }
        internal void FillSequence(ProgressBar pbFill)
        {
            
            pbFill.Value = pbFill.Minimum;
            int bgnIndex = 0, endIndex = 0;
            _seq.BeginUpdate();
            while (true)
            {
                pbFill.PerformStep();
                if (ParseCell(ref bgnIndex, ref endIndex))
                    continue;
                break;
            }
            _seq.EndUpdate();
        }

        private bool ParseCell(ref int bgnIndex, ref int endIndex)
        {
            
            bgnIndex = Corpus.IndexOf(Delims.Standart.Begin, endIndex);
            if (bgnIndex == -1) //if tag is Bottom
                return false;
            endIndex = Corpus.IndexOf(Delims.Standart.End, bgnIndex + 1);
            Cell cell = new Cell(bgnIndex, endIndex, null);
            string cellBody = Corpus.Substring(cell.iBgn, cell.iEnd - cell.iBgn + 1);
            for (int i = 0; i < Delims.Irregulars.Length; i++)
            {
                if (cellBody.StartsWith(Delims.Standart.Begin + Delims.Irregulars[i]))
                {
                    cell.Name = Delims.Irregulars[i];
                    cell.Type = Delims.Borders[0].Type; //opening cell
                    if (cellBody.Contains(Delims.AttrNameValueSeperator))
                        ParseAttributes(ref cell, cellBody);
                    _seq.Add(cell);
                    bgnIndex =
                        Corpus.IndexOf(Delims.Standart.Begin + Delims.Borders[1].Begin + Delims.Irregulars[i], endIndex);
                    if (bgnIndex == -1) //if tag is Bottom
                        throw new IrregularCellNotClosedException();
                    //update endIndex:
                    endIndex =
                        Corpus.IndexOf(Delims.Standart.End, bgnIndex + 1);
                    Cell endCell = new Cell(bgnIndex, endIndex, Delims.Irregulars[i]);
                    endCell.Type = Delims.Borders[1].Type;  //closing cell
                    _seq.Add(endCell);
                    return true;
                }
            }
            foreach (Delimeter delim in Delims.Extra)
            {
                if (cellBody.StartsWith(Delims.Standart.Begin + delim.Begin))
                {   //if extra delims begin found; find end:
                    if (cellBody.EndsWith(delim.End + Delims.Standart.End))
                    {
                        endIndex =
                            Corpus.IndexOf(delim.End + Delims.Standart.End, bgnIndex + 1) +
                            (delim.End + Delims.Standart.End).Length - 1;
                        //update iEnd.
                        cell.iEnd = endIndex;
                        cell.Type = delim.Type;
                        cell.Name = Corpus.Substring(cell.iBgn + 1, cell.iEnd - cell.iBgn - 1);
                        _seq.Add(cell);
                        return true;
                    }
                }
            }
            //stripped name:
            string name = cellBody.
                TrimStart(Delims.Standart.Begin.ToCharArray()).
                TrimEnd(Delims.Standart.End.ToCharArray()).
                TrimStart(Delims.Borders[0].Begin.ToCharArray()).
                TrimStart(Delims.Borders[0].End.ToCharArray()).
                TrimStart(Delims.Borders[1].Begin.ToCharArray()).
                TrimStart(Delims.Borders[1].End.ToCharArray());
            //find if its open or close cell
            if (cellBody.Replace(name, Delims.CellBeginEndSeperator) == Delims.Standart.Begin +
                Delims.Borders[0].Begin + Delims.CellBeginEndSeperator + Delims.Borders[0].End +
                Delims.Standart.End)
            {
                //if open cell:
                cell.Type = Delims.Borders[0].Type;
                cell.Name = name;
                if (!cellBody.Contains(Delims.AttrNameValueSeperator))  //if <html>
                    _seq.Add(cell);
                else
                {
                    ParseAttributes(ref cell, cellBody);
                    _seq.Add(cell);
                    return true;
                }
            }
            else if (cellBody.Replace(name, Delims.CellBeginEndSeperator) == Delims.Standart.Begin +
                Delims.Borders[1].Begin + Delims.CellBeginEndSeperator + Delims.Borders[1].End +
                Delims.Standart.End)
            {
                //if close cell
                cell.Type = Delims.Borders[1].Type;
                cell.Name = name;
                if (!cellBody.Contains(Delims.AttrNameValueSeperator))
                    _seq.Add(cell);
                else
                {
                    ParseAttributes(ref cell, cellBody);
                    _seq.Add(cell);
                    return true;
                }
            }
            return true;
        }
        private void ParseAttributes(ref Cell cell, string cellBody)
        {
            cellBody = cellBody.Replace(Delims.Standart.Begin, "").Replace(Delims.Standart.End, "");
            int _endIndex = 0;
            _endIndex = cellBody.IndexOf(' ');
            cell.Name = cellBody.Substring(0, _endIndex);
            _endIndex = 0;
            int _bgnIndex = 0;
            _bgnIndex = cellBody.IndexOf(cell.Name);
            cellBody = " " + cellBody.Remove(_bgnIndex, cell.Name.Length).Trim();
            // id=f6 style=""
            Cells.Attribute attr;
            //for loop:
            _endIndex = 0;
            while (_endIndex + 1 < cellBody.Length)
            {
                attr = new Cells.Attribute();
                _endIndex = cellBody.IndexOf('=', _endIndex + 1);
                if (_endIndex != -1)
                {
                    int srcInd;
                    for (srcInd = 1; srcInd <= _endIndex; srcInd++)
                    {   //search backwards:
                        if (cellBody[_endIndex - srcInd] == ' ')
                            break;

                    }
                    attr.Name = cellBody.Substring(_endIndex - srcInd + 1, srcInd - 1);
                    if (cellBody[_endIndex + 1] == '"')
                    {   //if: style="..."
                        _bgnIndex = _endIndex + 1;
                        _endIndex = cellBody.IndexOf('"', _bgnIndex + 1);

                    }
                    else if (cellBody[_endIndex + 1] == '\'')
                    {   //if: style='...'
                        _bgnIndex = _endIndex + 1;
                        _endIndex = cellBody.IndexOf('\'', _bgnIndex + 1);

                    }
                    else
                    {   //if: id=f6
                        _bgnIndex = _endIndex;  // to id'='f6
                        _endIndex = cellBody.IndexOf(' ', _bgnIndex + 1);
                        if (_endIndex == -1)
                        {
                            _endIndex = cellBody.Length;
                            attr.Value =
                                cellBody.Substring(_bgnIndex + 1, _endIndex - _bgnIndex - 1);
                            cell.Attrs.Add(attr);
                            continue;
                        }
                    }
                    attr.Value =
                            cellBody.Substring(_bgnIndex + 1, _endIndex - _bgnIndex - 1);
                    cell.Attrs.Add(attr);
                }
                else
                    break;
            }
        }
        private void InitializeCustomComponents()
        {
            _seq = new CellSequence();
            rootBlock = _seq.GetRootBlock();
            ResultBlocks = new List<Block>();
        }
    }
}
