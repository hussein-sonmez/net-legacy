﻿
namespace DDM.Core.Base.Settings
{
    internal class Delimeter
    {
        public Delimeter()
        {
        }
        internal string Begin { get; set; }
        internal string End { get; set; }
        internal string Type { get; set; }
    }

    internal class StandartDelimeter : Delimeter
    {
        public StandartDelimeter()
        {
        }

        public StandartDelimeter(string begin, string end, string type)
        {
            Begin = begin;
            End = end;
            Type = type;
        }
    }
}

