﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DDM.Core.Base.Cells
{
    public class Attribute
    {
        public Attribute(string name, string isWhat)
        {
            this._name = name;
            this._value = isWhat;
        }

        public Attribute() { }

        public Attributes GetSpecialAttrs()
        {
            Attributes attrs = new Attributes();
            string[] pairs = 
                this.Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (pairs.Length < 2)
                throw new ArgumentException("IsWhat is not a special attribute component", "this.IsWhat");
            for (int i = 0; i < pairs.Length; i++)
            {
                Attribute attr = new Attribute();
                string[] pair = pairs[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                if (pair.Length != 2)
                    throw new ArgumentException("IsWhat is not a special attribute component", "this.IsWhat");
                attr.Name = pair[0].Trim();
                attr.Value = pair[1].Trim();
                attrs.Add(attr);
            }
            return attrs;
        }

        private string _name = String.Empty;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _value = String.Empty;
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
