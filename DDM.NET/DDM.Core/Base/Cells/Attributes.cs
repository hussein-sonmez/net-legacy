﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DDM.Core.Base.Cells
{
    public class Attributes : IList<Attribute>
    {
        List<Attribute> _list = new List<Attribute>();

        #region IList<Attribute> Members

        public int IndexOf(Attribute item)
        {
            return _list.IndexOf(item);
        }

        public void Insert(int index, Attribute item)
        {
            _list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _list.RemoveAt(index);
        }

        public Attribute this[string name]
        {
            get
            {
                Attribute retattr = new Attribute();
                foreach (Attribute attr in this._list)
                {
                    if (attr.Name == name)
                    {
                        retattr = attr;
                        break;
                    }
                }
                return retattr;
            }
            set
            {
                int kAttr = 0;
                for (kAttr = 0; kAttr < this._list.Count; kAttr++)
                {
                    if (this._list[kAttr].Name == name)
                    {
                        this._list[kAttr] = value;
                        break;
                    }
                }
            }
        }

        public Attribute this[int index]
        {
            get
            {
                return this._list[index];
            }
            set
            {
                this._list[index] = value;
            }
        }

        #endregion

        #region ICollection<Attribute> Members

        public void Add(Attribute item)
        {
            _list.Add(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(Attribute item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(Attribute[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _list.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Attribute item)
        {
            return _list.Remove(item);
        }

        #endregion

        #region IEnumerable<Attribute> Members

        public IEnumerator<Attribute> GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        #endregion
    }
}
