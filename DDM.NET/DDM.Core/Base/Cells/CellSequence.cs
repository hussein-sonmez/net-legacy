﻿using System;
using System.Collections.Generic;
using System.Collections;
using DDM.Core.Base.ToolBox;
using DDM.Core.Base.Settings;
using DDM.Core.Base.Classes;

namespace DDM.Core.Base.Cells
{
    public class CellSequence : IList<Cell>
    {
        public CellSequence()
        {
            InitializeCustomComponents();
        }

        private void InitializeCustomComponents()
        {
            _seq = new List<Cell>();
        }
        private List<Cell> _seq;
        private bool UpdateStarted = false;
        private bool UpdateInProgress = false;

        //public void SetDefectAndNum(string name, ref DefectType defect, ref int numDefect)
        //{
        //    List<Cell> cellSeq = _seq[name];
        //    int _numDefect =
        //        cellSeq[0].x - cellSeq[0].y + cellSeq[0].alpha - cellSeq[0].beta;
        //    if (!cellSeq[0].IsBalanced())
        //    {
        //        if (cellSeq[0].Type == Delims.Border[0].Type)
        //        {
        //            if (_numDefect > 1)
        //            {
        //                defect = DefectType.Border1;
        //                numDefect = _numDefect - 1;
        //                return;
        //            }
        //            else
        //            {
        //                defect = DefectType.Border0;
        //                numDefect = 1 - _numDefect;
        //                return;
        //            }
        //        }
        //        if (cellSeq[0].Type == Delims.Border[1].Type)
        //        {   //if sequence starts with border last
        //            //it is already border first defect
        //            defect = DefectType.Border0;
        //            numDefect = 1 - _numDefect;
        //            return;
        //        }
        //    }
        //    numDefect = -1;
        //    defect = DefectType.None;
        //}

        internal Cell GetCorrespondingCell(Cell curCell)
        {
            if (!Methods.IsCellBorder(curCell))
                return null;
            Cell returnCell = null;
            if (curCell.Type == Delims.Borders[0].Type)
            {
                for (int iCell = curCell.Index + 1; iCell < Count; iCell++)
                {
                    if (Methods.GetDepthOfCell(_seq[iCell]) == Methods.GetDepthOfCell(curCell))
                    {
                        returnCell = _seq[iCell];
                        break;
                    }
                } 
            }
            else if (curCell.Type == Delims.Borders[1].Type)
            {
                for (int iCell = curCell.Index - 1; iCell >= 0; iCell--)
                {
                    if (Methods.GetDepthOfCell(_seq[iCell]) == Methods.GetDepthOfCell(curCell))
                    {
                        returnCell = _seq[iCell];
                        break;
                    }
                }
            }
            return returnCell;
        }
        private void FillBackAndForth()
        {
            Cell lastCell = _seq[Count - 1];
            Cell curCell = null, nextCell = null, prevCell = null;
            //first set last cell backwards:
            if (lastCell.Type == Delims.Borders[0].Type)
            {
                lastCell.alpha = 1;
                lastCell.beta = 0;
            }
            else if (lastCell.Type == Delims.Borders[1].Type)
            {
                lastCell.alpha = 0;
                lastCell.beta = 1;
            } 
            //then set rest of sequence backwards:
            for (int iCell = Count - 2; iCell >= 0; iCell--)
            {
                curCell = _seq[iCell];
                if (Methods.IsCellBorder(_seq[iCell]))
                {
                    nextCell = NextBorderCell(_seq[iCell]);
                    if (nextCell == null)
                        throw new NoNextBorderCellException();
                    if (curCell.Type == Delims.Borders[0].Type)
                    {
                        curCell.alpha = nextCell.alpha + 1;
                        curCell.beta = nextCell.beta;
                    }
                    else if (curCell.Type == Delims.Borders[1].Type)
                    {
                        curCell.alpha = nextCell.alpha;
                        curCell.beta = nextCell.beta + 1;
                    } 
                }
            }
            //forwards: first set first cell
            Cell firstCell = null;
            foreach (Cell cell in _seq)
            {
                if (cell.Type == Delims.Borders[0].Type)
                {
                    firstCell = cell;
                    break;
                }
            }
            if (firstCell.Type == Delims.Borders[0].Type)
            {
                firstCell.x = 1;
                firstCell.y = 0;
            }
            else if (firstCell.Type == Delims.Borders[1].Type)
            {
                firstCell.x = 0;
                firstCell.y = 1;
            }

            //then fill rest of it forwards:
            for (int iCell = firstCell.Index + 1; iCell < Count; iCell++)
            {
                curCell = _seq[iCell];
                if (Methods.IsCellBorder(_seq[iCell]))
                {
                    prevCell = PreviousBorderCell(_seq[iCell]);
                    if (prevCell == null)
                        throw new NoPreviousBorderCellException();
                    if (curCell.Type == Delims.Borders[0].Type)
                    {
                        curCell.x = prevCell.x + 1;
                        curCell.y = prevCell.y;
                    }
                    else if (curCell.Type == Delims.Borders[1].Type)
                    {
                        curCell.x = prevCell.x;
                        curCell.y = prevCell.y + 1;
                    } 
                }
            }
        }
        public void BeginUpdate()
        {
            if (!UpdateStarted)
            {
                UpdateStarted = UpdateInProgress = true;
            }
            else
                throw new UpdateAlreadyStartedException();
        }
        public void EndUpdate()
        {
            if (UpdateStarted & UpdateInProgress)
            {
                UpdateStarted = false;
                UpdateInProgress = false;
                if (!IsSequenceIntact())
                    throw new DeficientDocParseException();
            }
            else
                throw new EndedUpdateReEndedException();
        }
        private bool IsSequenceIntact()
        {
            FillBackAndForth();
            return Methods.IsCellBalanced(_seq[0]);
        }
        private Cell NextBorderCell(Cell curCell)
        {
            for (int iCell = curCell.Index + 1; iCell < Count; iCell++)
            {
                if (Methods.IsCellBorder(_seq[iCell]))
                    return _seq[iCell];
            }
            return null;
        }
        private Cell PreviousBorderCell(Cell curCell)
        {
            for (int iCell = curCell.Index - 1; iCell >= 0; iCell--)
            {
                if (Methods.IsCellBorder(_seq[iCell]))
                    return _seq[iCell];
            }
            return null;
        }
        internal Block GetRootBlock()
        {
            Block rootBlock = new Block();
            Cell startCell, endCell;
            startCell = new Cell(-1, -1, "Root");
            startCell.Index = 0;
            startCell.x = 1;
            startCell.y = 0;
            startCell.Type = Delims.Borders[0].Type;
            endCell = new Cell(-1, -1, "Root");
            endCell.Index = -1;
            endCell.alpha = 0;
            endCell.beta = 1;
            endCell.Type = Delims.Borders[1].Type;
            rootBlock.StartCell = startCell;
            rootBlock.EndCell = endCell;
            rootBlock.Depth = 0;
            return rootBlock;
        }
        #region IList<Cell> Members

        public int IndexOf(Cell item)
        {
            return item.Index;
        }

        public void Insert(int index, Cell item)
        {
            _seq.Insert(index, item);
            for (int iCell = index + 1; iCell < Count; iCell++)
            {
                _seq[iCell].Index++;
            }
        }

        public void RemoveAt(int index)
        {
            _seq.RemoveAt(index);
            //normalize forwards:
            for (int iCell = index; iCell < Count; iCell++)
            {
                _seq[iCell].Index--;
            }
        }

        public Cell this[int index]
        {
            get
            {
                return _seq[index];
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region ICollection<Cell> Members

        public void Add(Cell item)
        {
            //set item:
            if (Count != 0)
            {
                item.Index = _seq[Count - 1].Index + 1;
            }
            else
                item.Index = 0;
            _seq.Add(item);
        }

        public void Clear()
        {
            _seq.Clear();
        }

        public bool Contains(Cell item)
        {
            return _seq.Contains(item);
        }

        public void CopyTo(Cell[] array, int arrayIndex)
        {
            _seq.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _seq.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Cell item)
        {
            bool res = _seq.Remove(item);
            if (res == false)
                return false;

            _seq.RemoveAt(item.Index);
            //normalize forwards:
            for (int iCell = item.Index; iCell < Count; iCell++)
            {
                _seq[iCell].Index--;
            }
            return true;
        }

        #endregion

        #region IEnumerable<Cell> Members

        public IEnumerator<Cell> GetEnumerator()
        {
            return _seq.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion
    }
}
