﻿
namespace DDM.Core.Base.Cells
{
    public class Cell
    {
        internal Cell(int bgn, int end, string tagName)
        {
            iBgn = bgn;
            iEnd = end;
            Name = tagName;
            InitializeCustomComponents();
        }
        internal Cell()
        {
            InitializeCustomComponents();
        }
        private void InitializeCustomComponents()
        {
            x = y = alpha = beta = 0;
            Index = -1; //single
            Attrs = new Attributes();
        }

        public Attributes Attrs { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int alpha { get; set; }
        public int beta { get; set; }
        public int Index { get; set; }
        internal int iBgn { get; set; }
        internal int iEnd { get; set; }
        internal string Name { get; set; }
        internal string Type { get; set; }
    }
}
