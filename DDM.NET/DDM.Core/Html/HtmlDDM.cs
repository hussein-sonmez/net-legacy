﻿using System.Collections.Generic;
using DDM.Core.Base;
using DDM.Core.Base.ToolBox;
using DDM.Core.Base.Classes;
using DDM.Core.Base.Cells;
using System.Windows.Forms;

namespace DDM.Core.Html
{
    public class HtmlDDM : DDM_Base
    {
        public HtmlDDM()
        {
            InitializeCustomComponents();
        }
        public HtmlDDM(string corpus)
        {
            InitializeCustomComponents();
            Corpus = corpus;
            corpusSet = true;
        }
        private bool corpusSet;

        public bool CorpusSet {
            get {
                return corpusSet;
            }
        }

        public void SetCorpus(string corpus)
        {
            Corpus = corpus;
            corpusSet = true;
        }
        public void Fill(ProgressBar pbFill)
        {
            if (CorpusSet)
            {
                FillSequence(pbFill);
                FillBlocks();
            }
            else
                throw new CorpusNotSetException();
        }
        public List<string> GetContentsByAttribute(string name, string value)
        {
            List<string> texts = new List<string>();
            CollectMethod collectByClass =
                delegate(Block block, ref CellSequence seq) 
                {
                    List<Block> retList = new List<Block>();
                    if (block.StartCell.Attrs[name].Value == value)
                        retList.Add(block);
                    return retList;
                };
            CollectByMethod(ref collectByClass, ref rootBlock);
            string corpus = Corpus;
            foreach (Block block in ResultBlocks)
            {
                texts.Add(Methods.GetBlockContent(block, ref corpus, true));
            }
            return texts;
        }

        private void InitializeCustomComponents()
        {
            corpusSet = false;
        }

    }
}
