﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DDM.Core.Html;
using DDM.Windows.Playground.Properties;

namespace DDM.Windows.Playground.Forms {
    public partial class MainView : Form {
        public MainView() {
            InitializeComponent();
        }

        private void TestDDM() {
            string name = txtName.Text;
            string value = txtValue.Text;
            if (name == "" | value == "") {
                MessageBox.Show("Please fill both Name and Value Fields.");
                return;
            }
            var doc = new HtmlDDM(Resources.page);
            doc.Fill(pbFill);
            //class="category"
            var texts = doc.GetContentsByAttribute(name, value);
            lbResults.DataSource = texts;

        }

        private void btnStart_Click(object sender, EventArgs e) {
            TestDDM();
        }

    }
}
