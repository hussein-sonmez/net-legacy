﻿using Machinery.Extensions.Core;
using System.Web.Mvc;

namespace Oneness.MVC.Areas.Residence
{
    public class ResidenceAreaRegistration : AreaRegistration 
    {

        public override string AreaName {
            get {
                return "residence";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) {
            context.MapRoute(
                "Residence_default",
                "{0}/{{controller}}/{{action}}/{{id}}".PostFormat(AreaName),
                new { action = "Index", id = UrlParameter.Optional }
            );
        }

    }
}