﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Web;
using Oneness.MVC.Areas.Atrium.Persistence;
using Oneness.MVC.Areas.Residence.Models;
using Oneness.MVC.Persistence;

namespace Oneness.MVC.Areas.Residence.Persistence {

    public class MailEntryMapping : MappingProvider<MailEntry> {

        public MailEntryMapping() {

            #region Properties

            this.Property(t => t.To)
                .HasColumnName("To")
                .HasMaxLength(128)
                .IsRequired();

            this.Property(t => t.From)
                .HasColumnName("From")
                .HasMaxLength(128)
                .IsRequired();

            this.Property(t => t.Subject)
                .HasColumnName("Subject")
                .HasMaxLength(256)
                .IsRequired();

            this.Property(t => t.MailBody)
                .HasColumnName("MailBody")
                .HasMaxLength(1024 * 1024 * 8)
                .IsRequired();

            this.Property(t => t.SentOn)
                .HasColumnType("datetime2")
                .HasColumnName("SentOn")
                .IsRequired();

            this.Property(t => t.Unread)
                .HasColumnName("Unread")
                .IsRequired();

            #endregion

            #region Navigation Properties

            this.HasOptional(t => t.Reply).WithOptionalDependent()
                .Map(x => x.MapKey("ReplyID"));

            this.HasRequired(t => t.MailCategory)
                .WithMany(t => t.MailEntries)
                .Map(x => x.MapKey("MailCategoryID"));

            this.HasMany(t => t.MailTags)
                .WithOptional().Map(x => x.MapKey("MailTagID"));

            this.HasMany(t => t.Attachments)
                .WithOptional().Map(x => x.MapKey("AttachmentID"));

            #endregion
            this.ToTable("MailEntries");

        }

    }
    public class MailFolderContainerMapping : MappingProvider<MailFolderContainer> {

        public MailFolderContainerMapping() {

            #region Properties

            #endregion

            #region Navigation Properites

            this.HasOptional(t => t.InboxFolder).WithOptionalDependent()
                .Map(x => x.MapKey("InboxFolderID"));

            this.HasOptional(t => t.SentFolder).WithOptionalDependent()
                .Map(x => x.MapKey("SentFolderID"));

            this.HasOptional(t => t.ImportantFolder).WithOptionalDependent()
                .Map(x => x.MapKey("ImportantFolderID"));

            this.HasOptional(t => t.TrashFolder).WithOptionalDependent()
                .Map(x => x.MapKey("TrashFolderID"));

            this.HasOptional(t => t.DraftFolder).WithOptionalDependent()
                .Map(x => x.MapKey("DraftFolderID"));

            #endregion
            this.ToTable("MailFolderContainers");

        }

    }
    public class MailCategoryItemMapping : MappingProvider<MailCategoryItem> {

        public MailCategoryItemMapping() {

            #region Properties

            this.Property(t => t.InfoClass)
                .HasColumnName("InfoClass")
                .HasMaxLength(64)
                .IsRequired();

            this.Property(t => t.Description)
                .HasColumnName("Description")
                .HasMaxLength(256)
                .IsRequired();

            #endregion

            this.ToTable("MailCategoryItems");
        }

    }

    public class TagItemMapping : MappingProvider<TagItem> {

        public TagItemMapping() {

            #region Properties

            this.Property(t => t.Description)
                .HasColumnName("Description")
                .HasMaxLength(255)
                .IsRequired();

            #endregion

            this.ToTable("TagItems");
        }

    }


    public class TrashFolderMapping : MappingProvider<TrashFolder> {

        public TrashFolderMapping() {

            #region Navigation Properties

            this.HasMany(t => t.MailEntries).WithOptional()
                .Map(x => x.MapKey("TrashFolderID"));

            #endregion

            this.ToTable("TrashFolders");
        }

    }

    public class ImportantFolderMapping : MappingProvider<ImportantFolder> {

        public ImportantFolderMapping() {

            #region Navigation Properties

            this.HasMany(t => t.MailEntries).WithOptional()
                .Map(x => x.MapKey("ImportantFolderID"));

            #endregion

            this.ToTable("ImportantFolders");
        }

    }

    public class SentFolderMapping : MappingProvider<SentFolder> {

        public SentFolderMapping() {


            #region Navigation Properties

            this.HasMany(t => t.MailEntries).WithOptional()
                .Map(x => x.MapKey("SentFolderID"));

            #endregion

            this.ToTable("SentFolders");
        }

    }

    public class InboxFolderMapping : MappingProvider<InboxFolder> {

        public InboxFolderMapping() {

            #region Navigation Properties

            this.HasMany(t => t.MailEntries).WithOptional()
                .Map(x => x.MapKey("InboxFolderID"));

            #endregion

            this.ToTable("InboxFolders");
        }

    }

    public class DraftFolderMapping : MappingProvider<DraftFolder> {

        public DraftFolderMapping() {

            #region Navigation Properties

            this.HasMany(t => t.MailEntries).WithOptional()
                .Map(x => x.MapKey("DraftFolderID"));

            #endregion

            this.ToTable("DraftFolders");
        }

    }


}