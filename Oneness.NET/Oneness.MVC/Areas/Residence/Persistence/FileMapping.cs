﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oneness.MVC.Areas.Atrium.Persistence;
using Oneness.MVC.Areas.Residence.Models;
using Oneness.MVC.Persistence;

namespace Oneness.MVC.Areas.Residence.Persistence {
    public class FileFolderItemMapping : MappingProvider<FileFolderItem> {

        public FileFolderItemMapping() {
            #region Properties

            this.Property(t => t.FolderName)
                .HasColumnName("FolderName")
                .HasMaxLength(255)
                .IsRequired();

            #endregion

            #region Navigation Properties

            this.HasMany(t => t.FileItems).WithOptional(t => t.FileFolder)
                .Map(x => x.MapKey("FileFolderID"));

            #endregion

            this.ToTable("FileFolderItems");
        }

    }

    public class FileItemMapping : MappingProvider<FileItem> {

        public FileItemMapping() {

            #region Properties

            this.Property(t => t.FontAwesomeClass)
                .HasColumnName("FontAwesomeClass")
                .HasMaxLength(64);

            this.Property(t => t.ContainerName)
                .HasColumnName("ContainerName")
                .HasMaxLength(512)
                .IsRequired();

            this.Property(t => t.RealFileName)
                .HasColumnName("RealFileName")
                .HasMaxLength(512)
                .IsRequired();

            this.Property(t => t.GeneratedFileName)
                .HasColumnName("GeneratedFileName")
                .HasMaxLength(512)
                .IsRequired();

            this.Property(t => t.AddedOn)
                .HasColumnName("AddedOn")
                .HasColumnType("datetime2")
                .IsRequired();

            #endregion

            #region Navigation Property

            this.HasOptional(t => t.Thumbnail).WithMany()
                .Map(x => x.MapKey("ThumbnailID"));

            #endregion
            this.ToTable("FileItems");

        }

    }


    public class FileTypeMapping : MappingProvider<FileType> {

        public FileTypeMapping() {

            #region Properties

            this.Property(t => t.Description)
                .HasColumnName("Description")
                .HasMaxLength(64)
                .IsRequired();

            #endregion

            #region Navigation Property

            this.HasMany(t => t.FileItems).WithOptional(t => t.FileType)
                .Map(x => x.MapKey("FileFolderID"));

            #endregion

            this.ToTable("FileTypes");

        }

    }


}