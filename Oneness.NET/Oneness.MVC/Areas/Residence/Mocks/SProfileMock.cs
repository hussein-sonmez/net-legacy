﻿
using Oneness.MVC.Areas.Residence.Models;
using Machinery.Extensions.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Oneness.MVC.Persistence;
using Oneness.MVC.Areas.Atrium.Models;

namespace Oneness.MVC.Areas.Residence.Mocks {

    internal static class SProfileMock {

        static SProfileMock() {
            tumblr1 = (cont) => new PhotoItem() {
                Extension = "jpg",
                ContainerName = cont
            }.Assign(pi => pi.ImportFile("~/Areas/Residence/Images/NewPhotoActivity/tumblr-1.jpg"));
            tumblr2 = (cont) => new PhotoItem() {
                Extension = "jpg",
                ContainerName = cont
            }.Assign(pi => pi.ImportFile("~/Areas/Residence/Images/NewPhotoActivity/tumblr-2.jpg"));
        }

        public static Func<String, PhotoItem> tumblr1 { get; private set; }
        public static Func<String, PhotoItem> tumblr2 { get; private set; }

        internal static ActivityContainer ActivityContainer(LoginModel parentLogin) {

            var leader = SProfileMock.RandomLogin();
            var subject = SProfileMock.RandomLogin();

            var followingActivity = new FollowingActivity();
            followingActivity.FollowType = EFollowType.Follow;
            followingActivity.Leader = leader;
            followingActivity.Subject = subject;
            followingActivity.ActedOn = 12.DaysAgo();

            var followActivity = new FollowerActivity();
            followActivity.FollowType = EFollowType.Follow;
            followActivity.Leader = leader;
            followActivity.Subject = subject;
            followActivity.ActedOn = 12.DaysAgo();

            var newPostActivity = new BlogActivity();
            newPostActivity.BlogActivityType = EBlogActivityType.Post;
            newPostActivity.BlogText =
@"
Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ac magna porta, tristique purus in, porttitor dolor. Nullam congue ipsum in justo molestie, eu pharetra odio molestie. Aenean volutpat sollicitudin luctus. Etiam pretium elementum ligula sed egestas. In tincidunt, justo sed egestas convallis, dui nisi.
";

            newPostActivity.Leader = leader;
            newPostActivity.Subject = subject;
            newPostActivity.ActedOn = 12.DaysAgo();

            var newMessageActivity = new MessageActivity();
            newMessageActivity.MessageText =
@"
Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ac magna porta, tristique purus in, porttitor dolor. Nullam congue ipsum in justo molestie, eu pharetra odio molestie. Aenean volutpat sollicitudin luctus. Etiam pretium elementum ligula sed egestas. In tincidunt, justo sed egestas convallis, dui nisi.
";

            newMessageActivity.Leader = leader;
            newMessageActivity.Subject = subject;
            newMessageActivity.ActedOn = 12.DaysAgo();

            var container = new ActivityContainer();
            container
                .Assign(cont => cont.NewFollower = followActivity)
                .Assign(cont => cont.NewFollowing = followingActivity)
                .Assign(cont => cont.NewMessage = newMessageActivity)
                .Assign(cont => cont.NewPost = newPostActivity)
                .Assign(cont => cont.NewPhoto = new PhotoActivity() {
                    ActedOn = 12.DaysAgo(),
                    Leader = leader,
                    Subject = subject
                }).Assign(cont => cont.NewPhoto.PhotoItems.Extend(tumblr1(parentLogin.ServerContainerName), tumblr2(parentLogin.ServerContainerName)));
            parentLogin.AssociatedProfile.ProfileActivities.Extend(container);
            return container;

        }

        internal static LoginModel RandomLogin(Boolean stop = false) {
            var lm = new LoginModel() {
                Email = "lampic-{0}-lobe@gmail.com".GenerateIdentifier(),
                Password = "1q2w3e4r5t".ComputeHash(),
                ServerContainerName = Guid.NewGuid().ToString("N"),
                ReferredContact = stop ? null : RandomLogin(true),
                AssociatedProfile = new Profile() {
                    AboutMe = " ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat",
                    Address = "Somewhere in the {0}".GenerateIdentifier(),
                    Name = "Martin-{0}".GenerateNumber(),
                    Surname = "Eden",
                    CoorporationName = "Twitter{0}".GenerateNumber(),
                    CoorporateAddressLine1 = "Address line {0}".GenerateNumber(),
                    CoorporateAddressLine2 = "Address line 2-{0}".GenerateNumber(),
                    CoorporatePhone = "(123) {0}-{1}".PostFormat(3.DigitsRandom(), 4.DigitsRandom()),
                    CoorporatePosition = "Manager-{0}".GenerateIdentifier(),
                    MailFolderContainer = new MailFolderContainer() {
                        DraftFolder = new DraftFolder(),
                        ImportantFolder = new ImportantFolder(),
                        InboxFolder = new InboxFolder(),
                        SentFolder = new SentFolder(),
                        TrashFolder = new TrashFolder()
                    },
                }
            };
            lm.AssociatedProfile.FileFolderItem = SFileMock.FileFolderItems(lm).SelectRandom();
            lm.AssociatedProfile.Avatar = tumblr1(lm.ServerContainerName);
            return lm;
        }

    }
}