﻿
using Oneness.MVC.Areas.Residence.Models;
using Machinery.Extensions.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Oneness.MVC.Persistence;
using Oneness.MVC.Areas.Atrium.Models;

namespace Oneness.MVC.Areas.Residence.Mocks {
    public static class SMailMock {

        static SMailMock() {
            attachments = new String[] {
                "~/Areas/Residence/Mocks/Static/loremipsum-2015.docx",
                "~/Areas/Residence/Mocks/Static/p1.jpg",
                "~/Areas/Residence/Mocks/Static/p2.jpg",
                "~/Areas/Residence/Mocks/Static/p3.jpg"
            };
        }

        public static IEnumerable<MailEntry> Mailbox(LoginModel parentLogin) {

            var subjects = new String[] {
                "Lorem ipsum dolor noretek imit set.",
                "Aldus PageMaker including versions of Lorem Ipsum",
                "Many desktop publishing packages and web page editors.",
                "There are many variations of passages of Lorem Ipsum.",
                "Lorem ipsum dolor noretek imit set.",
                "The standard chunk of Lorem Ipsum used.",
                "Contrary to popular belief.",
                "If you are going to use a passage of Lorem",
                "Humour, or non-characteristic words etc.",
                "Oor Lorem Ipsum is that it has a more-or-less normal.",
                "Lorem ipsum dolor noretek imit set."
            };
            var dates = new DateTime[] {
                2.DaysAgo(),
                1.MinutesAgo(),
                19.Randomize().MinutesAgo(),
                36.Randomize().DaysAgo(),
                33.Randomize().HoursAgo(),
                2.DaysAgo(),
                1.MinutesAgo(),
                19.Randomize().MinutesAgo(),
                36.Randomize().DaysAgo(),
                33.Randomize().HoursAgo(),
                11.Randomize().DaysAgo()
            };

            for (int i = 0; i < subjects.Length; i++) {
                yield return new MailEntry() {

                    Attachments = SMailMock.Attachments(parentLogin).Shuffle().ToList(),
                    Subject = subjects[i],
                    From = "anna.smith@from.corporation.com",
                    To = "alex.smith@to.corporation.com",
                    SentOn = dates[i],
                    MailCategory = SMailMock.Categories().SelectRandom(),
                    MailTags = SMailMock.MailTags(),
                    Unread = 19.Randomize() % 2 == 0,
                    MailBody =
@"
<p>
    Hello {0}!
    <br/>
    <br/>
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
    took a galley of type and scrambled it to make a type <strong>specimen book.</strong>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. It has survived not only five centuries, but also the leap into electronic typesetting, remaining
    essentially unchanged.
    </p>
<p>
    It was popularised in the 1960s with the release <a href=""#"" class=""text-navy"">Letraset sheets</a>  containing Lorem Ipsum passages, and more recently with desktop publishing software
    like Aldus PageMaker including versions of Lorem Ipsum.
</p>
<p>
    There are many variations of passages of <strong> Lorem Ipsum </strong> Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of.
</p> 
".PostFormat("Anna Smith")

                };
            }

        }

        public static MailEntry MailItem(LoginModel parentLogin) {

            var mail = new MailEntry {
                Subject = "Aldus PageMaker including versions of Lorem Ipsum",
                From = "alex.smith@from.corporation.com",
                To = "alex.smith@to.corporation.com",
                SentOn = 3.DaysAgo(),
                MailBody =
@"
<p>
    Hello Jonathan!
    <br/>
    <br/>
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
    took a galley of type and scrambled it to make a type <strong>specimen book.</strong>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. It has survived not only five centuries, but also the leap into electronic typesetting, remaining
    essentially unchanged.
    </p>
<p>
    It was popularised in the 1960s with the release <a href=""#"" class=""text-navy"">Letraset sheets</a>  containing Lorem Ipsum passages, and more recently with desktop publishing software
    like Aldus PageMaker including versions of Lorem Ipsum.
</p>
<p>
    There are many variations of passages of <strong> Lorem Ipsum </strong> Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of.
</p> 
",
                Unread = new Random().Next(2007) % 2 == 0
            }
            .Assign(me => me.MailCategory = Categories().SelectRandom())
            .Assign(me => me.MailTags.Extend(MailTags()))
            .Assign(me => me.Attachments.Extend(Attachments(parentLogin).ToArray()));
            return mail;

        }
        private static String[] attachments;
        public static IEnumerable<FileItem> Attachments(LoginModel parentLogin) {

            return new FileItem[] {
                new FileItem() {
                    AddedOn = 3.MinutesAgo(),
                    ContainerName = parentLogin.ServerContainerName,
                    FontAwesomeClass = "fa-file",
                }.Assign(pi => pi.ImportFile(attachments[0])),
                new FileItem() {
                    AddedOn = 2.DaysAgo(),
                    ContainerName = parentLogin.ServerContainerName,
                    Thumbnail = new PhotoItem {
                        ContainerName = parentLogin.ServerContainerName,
                        Extension = "jpg"}.Assign(pi => pi.ImportFile(attachments[1]))
                }.Assign(pi => pi.ImportFile(attachments[1])),
                new FileItem() {
                    AddedOn = 16.MinutesAgo(),
                    ContainerName = parentLogin.ServerContainerName,
                    Thumbnail = new PhotoItem {
                        ContainerName = parentLogin.ServerContainerName,
                        Extension = "jpg"}.Assign(pi => pi.ImportFile(attachments[2]))
                }.Assign(pi => pi.ImportFile(attachments[2])),
                new FileItem() {
                    AddedOn = 5.DaysAgo(),
                    ContainerName = parentLogin.ServerContainerName,
                    Thumbnail = new PhotoItem {
                        ContainerName = parentLogin.ServerContainerName,
                        Extension = "jpg"}.Assign(pi => pi.ImportFile(attachments[3]))
                }.Assign(pi => pi.ImportFile(attachments[3]))
            };

        }
        public static TagItem[] MailTags() {
            return new TagItem[] {
                new TagItem() { Description = "Family" },
                new TagItem() { Description = "Work" },
                new TagItem() { Description = "Home" },
                new TagItem() { Description = "Children" }
            };
        }
        public static TagItem[] ProjectLabels() {
            return new TagItem[] {
                new TagItem() { Description = "Zender" },
                new TagItem() { Description = "Variations" },
                new TagItem() { Description = "Passages" },
                new TagItem() { Description = "Lorem Ipsum" }
            };
        }

        public static IEnumerable<MailCategoryItem> Categories() {

            var categories = new MailCategoryItem[] {
                new MailCategoryItem() {
                    Description = "Work",
                    InfoClass = "navy"
                },
                new MailCategoryItem() {
                    Description = "Documents",
                    InfoClass = "danger"
                },
                new MailCategoryItem() {
                    Description = "Social",
                    InfoClass = "primary"
                },
                new MailCategoryItem() {
                    Description = "Advertising",
                    InfoClass = "info"
                },
                new MailCategoryItem() {
                    Description = "Clients",
                    InfoClass = "warning"
                }
            };
            return categories;

        }

    }
}