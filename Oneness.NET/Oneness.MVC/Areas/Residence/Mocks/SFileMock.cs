﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Oneness.MVC.Areas.Residence.Models;
using Oneness.MVC.Persistence;
using Machinery.Extensions.Core;
using Oneness.MVC.Areas.Atrium.Models;

namespace Oneness.MVC.Areas.Residence.Mocks {
    public static class SFileMock {

        static Func<String, PhotoItem> bigAvatar;
        static Func<String, ICollection<PhotoItem>> avatars;
        static Func<String,ICollection<PhotoItem>> pPhotos;

        static SFileMock() {

            avatars = (cont) => 8.Times(k => new PhotoItem() {
                ContainerName = cont,
                Extension = "jpg"
            }.Assign(pi => pi.ImportFile("~/Areas/Residence/Images/Profile/a{0}.jpg".PostFormat(k))));
            bigAvatar =  (cont) => new PhotoItem() {
                ContainerName = cont,
                Extension = "jpg"
            }.Assign(pi => pi.ImportFile("~/Areas/Residence/Images/Profile/profile_big.jpg"));
            pPhotos =  (cont) => 3.Times(k => new PhotoItem() {
                ContainerName = cont,
                Extension = "jpg"
            }.Assign(pi => pi.ImportFile("~/Areas/Residence/Images/p{0}.jpg".PostFormat(k))));

        }

        public static IEnumerable<FileItem> FileItems(LoginModel parentLogin) {

            var fileNames = new String[] {
                "Document_2014.doc",
                "Italy street.jpg",
                "My feel.png",
                "Michal Jackson.mp3",
                "Document_2014.doc",
                "Monica's birthday.mpg4",
                "Annual report 2014.xls",
                "Document_2014.doc",
                "Italy street.jpg"
            };
            var addeds = new DateTime[] {
                2.DaysAgo(),
                5.MonthsAgo(),
                12.DaysAgo(),
                19.DaysAgo(),
                3.MinutesAgo(),
                6.MonthsAgo(),
                12.DaysAgo(),
                12.DaysAgo(),
                7.MonthsAgo()
            };

            var fontClasses = new String[] {
                "",
                "fa-file",
                "fa-music",
                "",
                "",
                "fa-film",
                "",
                "fa-bar-chart-o",
                ""
            };

            for (int i = 0; i < fileNames.Length; i++) {
                var aPhoto = pPhotos(parentLogin.ServerContainerName).SelectRandom();
                yield return new FileItem() {
                    ContainerName = parentLogin.ServerContainerName,
                    AddedOn = addeds[i],
                    FontAwesomeClass = fontClasses[i],
                    Thumbnail = aPhoto
                }.Assign(pi => pi.ImportFile("~/Areas/Residence/Images/Profile/a{0}.jpg".PostFormat(8.Randomize())));
            }

        }

        public static IEnumerable<FileFolderItem> FileFolderItems(LoginModel parentLogin) {

            var folderNames = new String[] {
                "Files",
                "Pictures",
                "Web pages",
                "Illustrations",
                "Films",
                "Books"
            };

            var files = SFileMock.FileItems(parentLogin);

            for (int i = 0; i < folderNames.Length; i++) {
                yield return new FileFolderItem() {
                    FolderName = folderNames[i]
                }
                .Assign(fl => fl.FileItems.Extend(files.Shuffle().ToArray()));
            }

        }

        public static IEnumerable<FileType> FileTypes() {

            var descriptions = new String[] {
                "Documents", "Audio", "Images"
            };

            for (int i = 0; i < descriptions.Length; i++) {
                yield return new FileType() {
                    Description = descriptions[i]
                };
            }

        }
        public static IEnumerable<TagItem> FileTags() {
            var desc = new String[] {
                "Family",
                "Work",
                "Home",
                "Children",
                "Holidays",
                "Music",
                "Photography"
            };
            for (int i = 0; i < desc.Length; i++) {
                yield return new TagItem() {
                    Description = desc[i]
                };
            }
        }

        internal static ICollection<PhotoItem> Avatars(LoginModel parentLogin) {

            return avatars(parentLogin.ServerContainerName).ToList();

        }
    }
}