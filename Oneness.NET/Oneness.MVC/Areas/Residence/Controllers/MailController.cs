﻿using Oneness.MVC.Areas.Residence.Mocks;
using Oneness.MVC.Areas.Residence.Models;
using Machinery.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oneness.MVC.Persistence;
using Oneness.MVC.Controllers;

namespace Oneness.MVC.Areas.Residence.Controllers {

    [RouteArea("residence")]
    [OutputCache(Duration = 60)]
    public class MailController : BaseController {

        // GET: Residence/Mail
        public ActionResult Index() {

            return AccessGrant<MailEntry>((id, set) => View(set.ToList()));

        }

        public ActionResult Detail(Int32 id) {

            return AccessGrant<MailEntry>((idx, set) => View(set.SingleOrDefault(m => m.ID == idx)));

        }
        [HttpGet]
        public ActionResult Compose() {

            return AccessGrant<MailEntry>((id, set) => View());

        }
        [HttpPost]
        public ActionResult Compose(MailEntry model) {

            return AccessGrant<MailEntry>((id, set) => View());

        }

    }
}