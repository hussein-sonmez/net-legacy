﻿
using Oneness.MVC.Areas.Residence.Mocks;
using Oneness.MVC.Areas.Residence.Models;
using Machinery.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oneness.MVC.Persistence;
using Oneness.MVC.Models;
using System.IO;
using Oneness.MVC.Areas.Atrium.Models;
using System.Data.Entity;
using Oneness.MVC.Controllers;

namespace Oneness.MVC.Areas.Residence.Controllers {

    //[OutputCache(Duration = 60)]
    [RouteArea("residence")]
    public class ProfileController : BaseController {
        // GET: Residence/Profile
        public ActionResult Index() {

            return AccessGrant<LoginModel>((idx, set) => View(
                set.SingleOrDefault(m => m.ID == idx)
                ));

        }

        // GET: Residence/Profile
        public ActionResult Contact(Int32 id) {

            return AccessGrant<LoginModel>((idx, set) => View(set
                .SingleOrDefault(m => m.ID == idx).Contacts
                .SingleOrDefault(m => m.ID == id)));

        }

        public ActionResult Contacts() {

            return AccessGrant<LoginModel>((idx, set) => View(set.SingleOrDefault(m => m.ID == idx).IfPropertyExists(found => found.Contacts)));

        }

        public ActionResult Edit() {

            return AccessGrant<LoginModel>((idx, set) => View(set.SingleOrDefault(m => m.ID == idx).IfPropertyExists(lm => lm.AssociatedProfile)));

        }

        [HttpPost]
        public ActionResult Edit(Profile profile) {

            if (Request.IsAjaxRequest()) {
                var exception = SContextProvider.AuthorizedUser.AssociatedProfile.CommitDatabase((app, prof) => {
                    var path = Directory.EnumerateFiles("~/Images/ClientImages/Temp".MapServerPath(), "{0}-*".PostFormat(prof.ID)).SingleOrDefault();
                    try {
                        prof.Digest(profile);
                        if (!String.IsNullOrEmpty(path)) {
                            prof.Avatar.ImportFile(path, true); 
                        }
                        app.Entry(prof).State = EntityState.Modified;
                        app.SaveChanges();
                        return null;
                    } catch (Exception ex) {
                        return ex;
                    } finally {
                        if (!String.IsNullOrEmpty(path)) {
                            System.IO.File.Delete(path);
                        }
                    }
                });
                return AccessModal(mv => PartialView("_Modal", mv), exception);
            }

            if (!ModelState.IsValid) {
                return View("edit", profile);
            } else {
                return View("edit", profile);
            }

        }

        [HttpPost]
        public JsonResult Upload() {

                Exception exception = null;
            if (Request.IsAjaxRequest()) {
                foreach (string fileName in Request.Files) {
                    var file = Request.Files[fileName];
                    var path = String.Empty;
                    try {
                        if (file != null && file.ContentLength > 0) {
                            path = "~/Images/ClientImages/Temp/{0}-{1}".PostFormat(
                                SContextProvider.AuthorizedUser.AssociatedProfile.ID,
                                file.FileName
                                ).MapServerPath();
                            if (System.IO.File.Exists(path)) {
                                System.IO.File.Delete(path);
                            }
                            file.SaveAs(path);
                        }
                    } catch (Exception ex) {
                        exception = ex;
                        break;
                    }
                }
            }
            var success = exception == null;
            return new JsonResult() {
                Data = new {
                    Done = success,
                    Message = success ? "Photo Updated Successfully" : "An Error Occurred",
                    InfoClass = success ? "success" : "danger"
                }
            };

        }

        [HttpPost]
        public ActionResult Activities(Int32 id) {

            if (Request.IsAjaxRequest()) {
                return PartialView("_Activities", SResidenceProvider.ActivitiesOfProfile(id));
            }

            if (!ModelState.IsValid) {
                return View("index");
            } else {
                return View("index");
            }

        }
    }
}