﻿using Machinery.Extensions.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;
using Oneness.MVC.Persistence;
using Oneness.MVC.Areas.Atrium.Models;

namespace Oneness.MVC.Areas.Residence.Models {

    public class ProjectItem : KeyProvider {

        #region Ctor
        public ProjectItem() {

            this.Participants = new List<LoginModel>();
            this.ProjectActivities = new List<ActivityContainer>();
            this.ProjectTimeline = new List<ProjectTimelineItem>();
            this.ProjectLabels = new List<TagItem>();
            this.ProjectAttachments = new List<FileItem>();

        }
        #endregion

        public Boolean Status { get; set; }

        public String ProjectTitle { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:g}")]
        public DateTime ReleasedOn { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:g}")]
        public DateTime LastUpdatedOn { get; set; }

        public Double CompletionRatio { get; set; }

        [RegularExpression(@"v\d\.\d{1,2}.\d{1,3}")]
        public String Version { get; set; }

        public String ProjectDescription { get; set; }

        public virtual PhotoItem ProjectLogo { get; set; }
        public EPriority Priority { get; set; }
        public virtual Client Client { get; set; }
        public virtual ICollection<FileItem> ProjectAttachments { get; set; }
        public virtual ICollection<TagItem> ProjectLabels { get; set; }
        public virtual ICollection<LoginModel> Participants { get; set; }
        public virtual LoginModel ReleasedBy { get; set; }
        public virtual ICollection<ProjectTimelineItem> ProjectTimeline { get; set; }

        public virtual ICollection<ActivityContainer> ProjectActivities { get; set; }

        [NotMapped]
        public Int32 MessagesCount {
            get {
                return this.ProjectActivities.Count(act => act.NewMessage != null);
            }
        }
        [NotMapped]
        public Int32 CompletionPercent {
            get {
                return Convert.ToInt32(CompletionRatio * 100);
            }
        }

        [NotMapped]
        public String StatusLabelClass {
            get {
                return Status ? "label-primary" : "label-default";
            }
        }
        [NotMapped]
        public String StatusText {
            get {
                return Status ? "Active" : "Not Active";
            }
        }

        [NotMapped]
        public String PriorityText {
            get {
                return Priority.GetDescription();
            }
        }
    }
    public class ProjectTimelineItem : KeyProvider {

        public ETimelineStatus Status { get; set; }

        public String Title { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:g}")]
        public DateTime StartTime { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:g}")]
        public DateTime EndTime { get; set; }

        public String Comment { get; set; }

        [NotMapped]
        public String StatusText {
            get {
                return Status.GetDescription();
            }
        }

    }

    public enum ETimelineStatus {

        [Display(Name = "Completed")]
        Completed = 1,
        [Display(Name = "Accepted")]
        Accepted,
        [Display(Name = "Sent")]
        Sent,
        [Display(Name = "Reported")]
        Reported
    }

    public enum EPriority {

        [Display(Name = "High Priority")]
        High = 1,
        [Display(Name = "Low Priority")]
        Low

    }


    public class Client : KeyProvider {
        [Required, Display(Name = "Title")]
        public String Title { get; set; }

    }
}