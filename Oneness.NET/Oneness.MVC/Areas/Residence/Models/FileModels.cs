﻿using Oneness.MVC.Areas.Atrium.Models;
using Machinery.Extensions.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oneness.MVC.Persistence;
using System.IO;

namespace Oneness.MVC.Areas.Residence.Models {

    public class BaseServerFileItem : KeyProvider {

        private String _ContainerName;
        public String ContainerName {
            get {
                return _ContainerName;
            }
            set {
                _ContainerName = value;
                if (!Directory.Exists(ContainerDirectory)) {
                    Directory.CreateDirectory(ContainerDirectory);
                }
            }
        }
        public String RealFileName { get; internal set; }
        public String GeneratedFileName { get; internal set; }

        private Byte[] _Data;
        [NotMapped]
        public Byte[] Data {
            get {
                return _Data ?? (_Data = File.ReadAllBytes(FullPath));
            }
        }

        public void ImportFile(String virtualPath, Boolean isFullPath = false) {

            var data = File.ReadAllBytes(isFullPath ? virtualPath : virtualPath.MapServerPath());
            var existing = Directory.EnumerateFiles(ContainerDirectory).SingleOrDefault(f => data.SequenceEqual(File.ReadAllBytes(f)));
            RealFileName = virtualPath.Substring(virtualPath.LastIndexOf(isFullPath ? '\\' :  '/') + 1);
            if (existing == null) {
                _Data = data;
                GeneratedFileName = "{0}.{1}".PostFormat(Guid.NewGuid().ToString("N"), RealFileName.Substring(RealFileName.LastIndexOf(".") + 1));
                File.WriteAllBytes(FullPath, Data);
            } else {
                _Data = null;
                GeneratedFileName = existing.Substring(existing.LastIndexOf('\\') + 1);
            }

        }

        [NotMapped]
        public String ContainerDirectory {
            get {
                return @"~/Images/ClientImages/PerProfile/{0}".PostFormat(ContainerName).MapServerPath();
            }
        }
        [NotMapped]
        public String FullPath {

            get {
                ContainerName.AssertNotNull();
                return @"~/Images/ClientImages/PerProfile/{0}/{1}".PostFormat(ContainerName, GeneratedFileName).MapServerPath();
            }
        }
    }

    public class FileFolderItem : KeyProvider {

        public FileFolderItem() {
            this.FileItems = new List<FileItem>();
            this.AssociatedProfiles = new List<Profile>();
        }

        public virtual ICollection<Profile> AssociatedProfiles { get; set; }
        public virtual ICollection<FileItem> FileItems { get; set; }
        public String FolderName { get; internal set; }
    }

    public class FileItem : BaseServerFileItem {

        public String FontAwesomeClass { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime AddedOn { get; set; }

        public virtual FileType FileType { get; set; }
        public virtual FileFolderItem FileFolder { get; set; }
        public virtual PhotoItem Thumbnail { get; set; }

        [NotMapped]
        public Boolean IsFont {
            get {
                return !String.IsNullOrEmpty(FontAwesomeClass);
            }
        }

    }

    public class FileType : KeyProvider {

        public FileType() {
            this.FileItems = new List<FileItem>();
        }

        public String Description { get; set; }

        public virtual ICollection<FileItem> FileItems { get; set; }

    }
}