﻿using Oneness.MVC.Areas.Atrium.Models;
using Machinery.Extensions.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oneness.MVC.Persistence;
using Machinery.Extensions.Definitions;

namespace Oneness.MVC.Areas.Residence.Models {

    public class MailEntry : KeyProvider {

        public MailEntry() {

            this.MailTags = new List<TagItem>();
            this.Attachments = new List<FileItem>();

        }

        [Display(Name = "To"), RegularExpression(SDefinitions.Regex.Email)]
        public String To { get; set; }

        [Display(Name = "From"), RegularExpression(SDefinitions.Regex.Email)]
        public String From { get; set; }

        [Display(Name = "Subject")]
        public String Subject { get; set; }

        [Display(Name = "MailBody")]
        public String MailBody { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:g}")]
        public DateTime SentOn { get; set; }

        public Boolean Unread { get; set; }

        [NotMapped]
        public String TrimmedText {
            get {
                return MailBody.TrimByWords(20);
            }
        }

        [NotMapped]
        public String ReadModeClass {
            get {
                return Unread ? "unread" : "read";
            }
        }

        public virtual MailEntry Reply { get; set; }

        public virtual ICollection<FileItem> Attachments { get; set; }

        public virtual MailCategoryItem MailCategory { get; set; }
        public virtual ICollection<TagItem> MailTags { get; set; }

    }

    public class MailFolderContainer : KeyProvider {

        public virtual DraftFolder DraftFolder { get; set; }
        public virtual InboxFolder InboxFolder { get; set; }
        public virtual SentFolder SentFolder { get; set; }
        public virtual ImportantFolder ImportantFolder { get; set; }
        public virtual TrashFolder TrashFolder { get; set; }

    }

    public class MailCategoryItem : KeyProvider {

        public MailCategoryItem() {
            this.MailEntries = new List<MailEntry>();
        }

        [Required, MaxLength(64)]
        public String InfoClass { get; set; }

        [Required, MaxLength(64)]
        public String Description { get; set; }

        public virtual ICollection<MailEntry> MailEntries { get; set; }

    }

    public class TagItem : KeyProvider {

        [Required, MaxLength(64)]
        public String Description { get; set; }
    }

    #region Folders

    public abstract class FolderBase : KeyProvider {

        public FolderBase() {
            this.MailEntries = new List<MailEntry>();
        }

        public abstract String FontAwesomeClass { get; }
        public abstract String FolderName { get; }

        public virtual ICollection<MailEntry> MailEntries { get; set; }

        [NotMapped]
        public String CountBadge {
            get {
                var count = MailEntries.Count;
                if (count > 0) {
                    return @"<span class=""label label-{1} pull - right"">{0}</span>".PostFormat(count, count < 5 ? "warning" : "danger");
                } else {
                    return String.Empty;
                }
            }
        }

    }
    public class TrashFolder : FolderBase {
        public override string FolderName {
            get {
                return "Trash";
            }
        }

        public override string FontAwesomeClass {
            get {
                return "fa-trash-o";
            }
        }

    }

    public class ImportantFolder : FolderBase {
        public override string FolderName {
            get {
                return "Important";
            }
        }

        public override string FontAwesomeClass {
            get {
                return "fa-certificate";
            }
        }

    }

    public class SentFolder : FolderBase {
        public override string FolderName {
            get {
                return "Sent";
            }
        }

        public override string FontAwesomeClass {
            get {
                return "fa-envelope-o";
            }
        }

    }

    public class InboxFolder : FolderBase {
        public override string FolderName {
            get {
                return "Inbox";
            }
        }

        public override string FontAwesomeClass {
            get {
                return "fa-inbox";
            }
        }

    }

    public class DraftFolder : FolderBase {
        public override string FolderName {
            get {
                return "Draft";
            }
        }

        public override string FontAwesomeClass {
            get {
                return "fa-file-text-o";
            }
        }

    }

    #endregion


}