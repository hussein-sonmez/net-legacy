﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;


namespace Oneness.MVC.Areas.Residence.AppConfig {
    public static class ResidenceConfig {

        public class BundleConfig {

            public static void RegisterBundles(BundleCollection bundles) {


                bundles.Add(new StyleBundle("~/Content/residence/css")
                    .Include("~/Areas/Residence/Content/bootstrap.min.css")
                    .Include("~/Areas/Residence/Content/font-awesome.min.css")
                    .Include("~/Areas/Residence/Content/custom.css")
                    .Include("~/Areas/Residence/Content/summernote.css")
                    .Include("~/Areas/Residence/Content/summernote-bs3.css")
                    .Include("~/Areas/Residence/Content/animate.css")
                    .Include("~/Areas/Residence/Content/style.css")
                    );

                bundles.Add(new ScriptBundle("~/bundles/residence/main")
                      .Include("~/Areas/Residence/Scripts/main/jquery-2.1.3.min.js")
                      .Include("~/Areas/Residence/Scripts/main/bootstrap.min.js")
                      );

                bundles.Add(new ScriptBundle("~/bundles/residence/menu")
                      .Include("~/Areas/Residence/Scripts/menu/jquery.metisMenu.js")
                      .Include("~/Areas/Residence/Scripts/menu/jquery.slimscroll.min.js")
                      );

                bundles.Add(new ScriptBundle("~/bundles/residence/inspinia")
                    .Include("~/Areas/Residence/Scripts/inspinia/inspinia.js")
                    .Include("~/Areas/Residence/Scripts/inspinia/pace.min.js")
                    );

                bundles.Add(new ScriptBundle("~/bundles/residence/peity")
                    .Include("~/Areas/Residence/Scripts/peity/jquery.peity.min.js")
                    .Include("~/Areas/Residence/Scripts/peity/peity-demo.js")
                    );

                bundles.Add(new ScriptBundle("~/bundles/residence/extra")
                    .Include("~/Areas/Residence/Scripts/extra/icheck.min.js")
                    .Include("~/Areas/Residence/Scripts/extra/summernote.min.js")
                    );

                bundles.Add(new ScriptBundle("~/bundles/residence/dropzone")
                    .Include("~/Areas/Residence/Scripts/extra/dropzone.js")
                    );

                bundles.Add(new ScriptBundle("~/bundles/residence/ajax")
                    .Include("~/Areas/Residence/Scripts/ajax/jquery.unobtrusive-ajax.min.js")
                    .Include("~/Areas/Residence/Scripts/ajax/jquery.validate.min.js")
                    .Include("~/Areas/Residence/Scripts/ajax/jquery.validate.unobtrusive.min.js")
                    );

            }
        }

    }
}