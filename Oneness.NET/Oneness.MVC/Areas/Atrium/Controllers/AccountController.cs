﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Oneness.MVC.Areas.Atrium.Models;
using Oneness.MVC.Areas.Residence.Models;
using Oneness.MVC.Models;
using Oneness.MVC.Persistence;
using Machinery.Extensions.Core;
using Oneness.MVC.Controllers;
using System.IO;

namespace Oneness.MVC.Areas.Atrium.Controllers {
    [RouteArea("atrium")]
    [OutputCache(Duration = 120)]
    public class AccountController : BaseController {
        //
        // GET: /Account/Login
        [HttpGet]
        public ActionResult Login() {

            return View();

        }

        // POST: /Account/Login
        [HttpPost]
        public ActionResult Login(LoginModel model) {

            if (Request.IsAjaxRequest()) {
                var cipher = model.Password.ComputeHash();
                var login = model.CommitDatabase((app, m) => app.LoginModels.SingleOrDefault(lm => lm.Email == m.Email && lm.Password == cipher));
                var success = login != null;

                if (success) {
                    SContextProvider.Session("login", login.ID);
                }

                return AnonymousModal(md => PartialView("_Modal", md), success ? null :  new Exception("Invalid Credentials"), "Login was Successful");

            }

            if (!ModelState.IsValid) {
                return View();
            } else {
                return View(model);
            }
        }

        // POST: /Account/Login
        [HttpGet]
        public ActionResult Register() {

            return View();

        }
        // POST: /Account/Login
        [HttpPost]
        public ActionResult Register(RegisterModel model) {

            if (!ModelState.IsValid) {
                return View(model);
            }
            if (Request.IsAjaxRequest()) {
                var validates = false;
                Exception exception = null;
                var message = model.ValidationFor(out validates);
                var lm = new LoginModel();
                model.MapTo(ref lm);
                if (validates) {
                    exception = lm.CommitDatabase((app, m) => {
                        lm.Password = lm.Password.ComputeHash();
                        lm.ServerContainerName = Guid.NewGuid().ToString("N");
                        try {
                            app.LoginModels.Add(lm);
                            app.SaveChanges();
                            return null;
                        } catch (Exception ex) {
                            return ex;
                        }
                    });
                }
                return AnonymousModal(md => PartialView("_Modal", md), exception, "You're one of us");
            }

            if (!ModelState.IsValid) {
                return View("Register");
            } else {
                return View(model);
            }

        }


        [HttpGet]
        public ActionResult Logout() {

            SContextProvider.Session<Int32>("login", 0);

            var message = @"Feel free to login again here: <a href=""{0}"">Log on Again</a>"
                .PostFormat(SContextProvider.Route("login", "account", "atrium"));
            SContextProvider.LogoutUser();
            return AnonymousModal(md => View().Assign(v=>v.ViewData["Modal"] = md), null, message);

        }

    }
}