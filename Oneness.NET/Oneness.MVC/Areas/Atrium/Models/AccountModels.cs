﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Oneness.MVC.Areas.Residence.Models;
using Oneness.MVC.Persistence;
using Machinery.Extensions.Core;
using Machinery.Extensions.Definitions;

namespace Oneness.MVC.Areas.Atrium.Models {

    public class LoginModel : KeyProvider {

        public LoginModel() {
            this.Contacts = new List<LoginModel>();

        }

        [Required]
        [Display(Name = "Email"), RegularExpression(SDefinitions.Regex.Email)]
        public String Email { get; set; }
        [Required]
        public String Password { get; set; }

        private String _ServerContainerName;
        public String ServerContainerName {
            get {
                return _ServerContainerName;
            }
            set {
                _ServerContainerName = value;
                if (!Directory.Exists(FullPath)) {
                    Directory.CreateDirectory(FullPath);
                }
            }
        }


        [NotMapped]
        public String FullPath {
            get {
                return "~/Images/ClientImages/PerProfile/{0}".PostFormat(_ServerContainerName).MapServerPath();
            }
        }
        [NotMapped]
        public Boolean RememberMe { get; set; }

        public virtual LoginModel ReferredContact { get; set; }
        public virtual ICollection<LoginModel> Contacts { get; set; }
        public virtual Profile AssociatedProfile { get; internal set; }

    }

    public class RegisterModel {

        [Required]
        public String Name { get; set; }

        [Required]
        public String Surname { get; set; }

        [Required]
        public String Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public String Password { get; set; }

        [DataType(DataType.Password)]
        public String PasswordConfirm { get; set; }

        [Required]
        public Boolean PolicyAgreed { get; set; }

    }

    public static class RegisterModelExtensions {

        public static String ValidationFor(this RegisterModel rmodel, out Boolean validates) {

            var message = new StringBuilder("");
            if (!rmodel.Email.ValidateEmail()) {
                message.AppendFormat("Please Review Your email Typing / ");
                validates = false;
            }
            if (!rmodel.PolicyAgreed) {
                message.AppendFormat("{0} / ", "You should aggree our participant policy");
                validates = false;
            }
            if (rmodel.Password != rmodel.PasswordConfirm) {
                message.AppendFormat("{0} / ", "You should aggree our participant policy");
                validates = false;
            }
            var response = message.ToString();
            if (String.IsNullOrEmpty(response)) {
                response = "No Validation Error Occured";
                validates = true;
            } else {
                validates = false;
            }
            return response;

        }

    }

}