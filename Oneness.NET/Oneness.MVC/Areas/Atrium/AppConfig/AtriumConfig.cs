﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;


namespace Oneness.MVC.Areas.Atrium.AppConfig {
    public static class AtriumConfig {

        public class BundleConfig {

            public static void RegisterBundles(BundleCollection bundles) {

                bundles.Add(new StyleBundle("~/Content/atrium/css")
                    .Include("~/Areas/Atrium/Content/bootstrap.min.css")
                    .Include("~/Areas/Atrium/Content/font-awesome.min.css")
                    .Include("~/Areas/Atrium/Content/custom.css")
                    .Include("~/Areas/Atrium/Content/toastr.min.css")
                    .Include("~/Areas/Atrium/Content/animate.css")
                    .Include("~/Areas/Atrium/Content/style.css")
                    );

                bundles.Add(new ScriptBundle("~/bundles/atrium/main")
                    .Include("~/Areas/Atrium/Scripts/main/jquery-2.1.3.min.js")
                    .Include("~/Areas/Atrium/Scripts/main/bootstrap.min.js")
                    );

                bundles.Add(new ScriptBundle("~/bundles/atrium/extra")
                    .Include("~/Areas/Atrium/Scripts/extra/icheck.min.js")
                    .Include("~/Areas/Atrium/Scripts/extra/toastr.min.js")
                    );
                bundles.Add(new ScriptBundle("~/bundles/atrium/ajax")
                    .Include("~/Areas/Atrium/Scripts/ajax/jquery.unobtrusive-ajax.min.js")
                    );
            }
        }

    }
}