﻿using System.Web.Mvc;
using Machinery.Extensions.Core;

namespace Oneness.MVC.Areas.Redirect
{
    public class RedirectAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "redirect";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Redirect_default",
                "{0}/{{controller}}/{{action}}/{{id}}".PostFormat(AreaName),
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}