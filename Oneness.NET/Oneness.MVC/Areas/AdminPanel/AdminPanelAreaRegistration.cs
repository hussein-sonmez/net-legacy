﻿using Machinery.Extensions.Core;
using System.Web.Mvc;

namespace Oneness.MVC.Areas.AdminPanel {
    public class AdminPanelAreaRegistration : AreaRegistration {
        public override string AreaName {
            get {
                return "adminpanel";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) {
            context.MapRoute(
                "AdminPanel_default",
                "{0}/{{controller}}/{{action}}/{{id}}".PostFormat(AreaName),
                new { action = "Index", id = UrlParameter.Optional }
            );
        }

    }
}