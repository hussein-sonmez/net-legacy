﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;


namespace Oneness.MVC.Areas.AdminPanel.AppConfig {
    public static class AdminPanelConfig {

        public class BundleConfig {

            public static void RegisterBundles(BundleCollection bundles) {

                bundles.Add(new StyleBundle("~/Content/admin/css")
                    .Include("~/Areas/AdminPanel/Content/bootstrap.min.css")
                    .Include("~/Areas/AdminPanel/Content/font-awesome.min.css")
                    .Include("~/Areas/AdminPanel/Content/toastr.min.css")
                    .Include("~/Areas/AdminPanel/Content/jquery.gritter.css")
                    .Include("~/Areas/AdminPanel/Content/animate.css")
                    .Include("~/Areas/AdminPanel/Content/style.css")
                    );

                bundles.Add(new ScriptBundle("~/bundles/admin/main")
                    .Include("~/Areas/AdminPanel/Scripts/main/jquery-2.1.1.js")
                    .Include("~/Areas/AdminPanel/Scripts/main/bootstrap.min.js")
                    .Include("~/Areas/AdminPanel/Scripts/main/jquery.metisMenu.js")
                    .Include("~/Areas/AdminPanel/Scripts/main/jquery.slimscroll.min.js")
                    );

                bundles.Add(new ScriptBundle("~/bundles/admin/flot")
                    .Include("~/Areas/AdminPanel/Scripts/flot/jquery.flot.js")
                    .Include("~/Areas/AdminPanel/Scripts/flot/jquery.flot.tooltip.min.js")
                    .Include("~/Areas/AdminPanel/Scripts/flot/jquery.flot.spline.js")
                    .Include("~/Areas/AdminPanel/Scripts/flot/jquery.flot.resize.js")
                    .Include("~/Areas/AdminPanel/Scripts/flot/jquery.flot.pie.js")
                    );

                bundles.Add(new ScriptBundle("~/bundles/admin/peity")
                    .Include("~/Areas/AdminPanel/Scripts/peity/jquery.peity.min.js")
                    .Include("~/Areas/AdminPanel/Scripts/peity/peity-demo.js")
                    );

                bundles.Add(new ScriptBundle("~/bundles/admin/inspinia")
                    .Include("~/Areas/AdminPanel/Scripts/inspinia/inspinia.js")
                    .Include("~/Areas/AdminPanel/Scripts/inspinia/pace.min.js")
                    );

                bundles.Add(new ScriptBundle("~/bundles/admin/common")
                    .Include("~/Areas/AdminPanel/Scripts/common/jquery-ui.min.js")
                    .Include("~/Areas/AdminPanel/Scripts/common/jquery.gritter.min.js")
                    .Include("~/Areas/AdminPanel/Scripts/common/jquery.sparkline.min.js")
                    .Include("~/Areas/AdminPanel/Scripts/common/sparkline-demo.js")
                    .Include("~/Areas/AdminPanel/Scripts/common/Chart.min.js")
                    .Include("~/Areas/AdminPanel/Scripts/common/toastr.min.js")
                    );

            }
        }

    }
}