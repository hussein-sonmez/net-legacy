﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Machinery.Extensions.Core;

namespace Oneness.MVC.Models {
    public class GenericModalView {

        private String _ID;
        public String ID {
            get {
                return _ID ?? (_ID = "{0}".GenerateNumber());
            }
        }
        public String AnimationClass { get; set; }
        public String AlertClass { get; set; }
        public String CloseLabel { get; set; }
        public Boolean SaveChangesShown { get; set; }
        public String SaveChangesLabel { get; set; }
        public String Title { get; set; }
        public String Subtitle { get; set; }
        public String Body { get; set; }
        public Boolean OperationSucceeded{ get; set; }
        public String RedirectRoute { get; set; }

    }
}