﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oneness.MVC.Areas.Atrium.Models;
using Oneness.MVC.Models;
using Oneness.MVC.Persistence;
using Machinery.Extensions.Core;

namespace Oneness.MVC.Controllers {
    public class BaseController : Controller {

        protected ActionResult AccessGrant<TModel>(Func<Int32, IQueryable<TModel>, ActionResult> viewer)
            where TModel : KeyProvider {
            var id = SContextProvider.Session<Int32>("login");
            if (id == 0) {
                return Redirect(SContextProvider.Route("login", "account", "atrium"));
            } else {
                var set = SContextProvider.Get<TModel>();
                return viewer(id, set);
            }
        }

        protected ActionResult AccessModal(Func<GenericModalView, ActionResult> viewer, Exception exception, String successMessage = "") {
            var id = SContextProvider.Session<Int32>("login");
            if (id == 0) {
                return Redirect(SContextProvider.Route("login", "account", "atrium"));
            } else {
                var modal = GenerateModal(exception, successMessage);
                return viewer(modal);
            }
        }

        protected ActionResult AnonymousModal(Func<GenericModalView, ActionResult> viewer, Exception exception, String successMessage = "") {

            var modal = GenerateModal(exception, successMessage);
            return viewer(modal);

        }

        private static GenericModalView GenerateModal(Exception exception, string successMessage) {
            var success = exception == null;
            var modal = new GenericModalView() {
                AnimationClass = success ? "bounceInLeft" : "tada",
                CloseLabel = "Close",
                SaveChangesShown = false,
                SaveChangesLabel = "Save Changes",
                Body = success ? "Proceeded Successfully! {0}".PostFormat(successMessage)
                    : "Errors Occurred: {0}".PostFormat(exception.IfPropertyExists(e => e.Message)),
                Title = success ? "Successful Operation" : "Operation failed!",
                Subtitle = success ? "Operation Proceeded" : "Operation Failed",
                OperationSucceeded = success,
                AlertClass = success ? "success" : "danger",
                RedirectRoute = success ? SContextProvider.Route("index", "profile", "residence") : ""
            };
            return modal;
        }

    }
}