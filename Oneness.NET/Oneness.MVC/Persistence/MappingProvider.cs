﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Oneness.MVC.Persistence;

namespace Oneness.MVC.Persistence {

    public class MappingProvider<TEntity> : EntityTypeConfiguration<TEntity>
        where TEntity : KeyProvider {

        #region Ctor

        public MappingProvider() {
            this.HasKey(t => t.ID).Property(t => t.ID).HasColumnName("ID")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();

        }

        #endregion

    }

}