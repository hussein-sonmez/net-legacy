﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using Oneness.MVC.Areas.Atrium.Persistence;
using Oneness.MVC.Areas.Residence.Models;
using Oneness.MVC.Areas.Residence.Persistence;
using Oneness.MVC.Migrations;
using Machinery.Extensions.Core;

namespace Oneness.MVC.Persistence {
    public partial class AppContext : DbContext {

        public AppContext() : base(SConnectionProvider.CurrentConnection, true) {

            this.Configuration.ProxyCreationEnabled = true;
            this.Configuration.LazyLoadingEnabled = true;

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder) {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new LoginModelMapping());

            modelBuilder.Configurations.Add(new ProfileMapping());
            modelBuilder.Configurations.Add(new ActivityContainerMapping());
            modelBuilder.Configurations.Add(new FollowingActivityMapping());
            modelBuilder.Configurations.Add(new FollowerActivityMapping());
            modelBuilder.Configurations.Add(new PhotoActivityMapping());
            modelBuilder.Configurations.Add(new PhotoItemMapping());
            modelBuilder.Configurations.Add(new BlogActivityMapping());
            modelBuilder.Configurations.Add(new MessageActivityMapping());

            modelBuilder.Configurations.Add(new MailEntryMapping());
            modelBuilder.Configurations.Add(new MailFolderContainerMapping());
            modelBuilder.Configurations.Add(new FileItemMapping());
            modelBuilder.Configurations.Add(new MailCategoryItemMapping());
            modelBuilder.Configurations.Add(new TagItemMapping());
            modelBuilder.Configurations.Add(new TrashFolderMapping());
            modelBuilder.Configurations.Add(new ImportantFolderMapping());
            modelBuilder.Configurations.Add(new SentFolderMapping());
            modelBuilder.Configurations.Add(new InboxFolderMapping());
            modelBuilder.Configurations.Add(new DraftFolderMapping());

            modelBuilder.Configurations.Add(new ProjectItemMapping());
            modelBuilder.Configurations.Add(new ClientMapping());
            modelBuilder.Configurations.Add(new ProjectTimelineItemMapping());

        }
        private static AppContext _Context;
        public static AppContext AppInstance {
            get {
                return _Context ?? (_Context = new AppContext());
            }
        }

    }
    public static class AppContextExtensions {

        public static TResponse CommitDatabase<TEntity, TResponse>(this TEntity entity, Func<AppContext, TEntity, TResponse> commit)
                where TEntity : KeyProvider {

            return commit(AppContext.AppInstance, entity);

        }
    }

}