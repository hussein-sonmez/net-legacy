﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Oneness.MVC.Areas.Atrium.Models;
using Oneness.MVC.Areas.Residence.Models;
using Machinery.Extensions.Core;

namespace Oneness.MVC.Persistence {
    public static class SContextProvider {

        public static IQueryable<TEntity> Get<TEntity>()
            where TEntity : KeyProvider {

            var context = AppContext.AppInstance;
            return context.Set<TEntity>().AsQueryable();
        }

        public static String FontAwesomeClassFrom(String contentType) {

            String cls;
            if (contentType.In("application/excel", "application/vnd.ms-excel", "application/x-excel", "application/x-msexcel")) {
                cls = "fa-bar-chart-o";
            } else if (contentType.In("audio/mpeg3", "audio/x-mpeg-3", "video/mpeg", "video/x-mpeg")) {
                cls = "fa-music";
            } else if (contentType.In("video/mpeg", "video/quicktime", "video/x-sgi-movie", "video/x-mpeg", "video/x-qtc", "video/avi", "video/msvideo", "video/x-msvideo")) {
                cls = "fa-film";
            } else if (contentType.In("image/jpeg", "image/pjpeg", "image/jpeg", "image/pjpeg", "image/png", "image/gif", "image/bmp", "image/x-windows-bmp")) {
                cls = String.Empty;
            } else {
                cls = "fa-file";
            }
            return cls;

        }

        public static TItem Session<TItem>(String key, TItem item = default(TItem)) {

            if (!item.Equals(default(TItem))) {
                HttpContext.Current.Session[key] = item;
            }
            return HttpContext.Current.Session[key].ToType<TItem>();
        }

        private static LoginModel _AuthorizedUser;
        public static LoginModel AuthorizedUser {
            get {
                if (_AuthorizedUser == null) {
                    var lmid = SContextProvider.Session<Int32>("login");
                    _AuthorizedUser = AppContext.AppInstance.LoginModels.SingleOrDefault(m => m.ID == lmid);
                    _AuthorizedUser.AssertNotNull();
                }
                return _AuthorizedUser;
            } 
        }

        public static void LogoutUser() {

            _AuthorizedUser = null;
            HttpContext.Current.Session["login"] = 0;

        }

        public static String Route(String action, String controller, String area) {

            var context = new RequestContext(new HttpContextWrapper(HttpContext.Current), new RouteData());
            var urlHelper = new UrlHelper(context);

            // Set the url
            var url = urlHelper.Action(action, controller,
                new RouteValueDictionary(new { Area = area }));
            return url;

        }

    }
}