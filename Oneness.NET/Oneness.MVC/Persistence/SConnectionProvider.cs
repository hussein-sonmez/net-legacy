﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;
using Machinery.Instruments.Constructs.Credentials;
using Machinery.Instruments.InstanceProvider;
using Machinery.Instruments.Protocols;

namespace Oneness.MVC.Persistence {
    internal class SConnectionProvider {
        internal static DbConnection CurrentConnection {
            get {
                if (_CredentialContainer == null) {
                    _CredentialContainer = new CredentialContainer();

                    _CredentialContainer["local"] = SManifester.ToConnectionCredential(EServiceType.MSSQL);
                    _CredentialContainer["local"]
                        .Assign(cr => cr.Server = ".")
                        .Assign(cr => cr.Database = "ondb")
                        .Assign(cr => cr.UserID = "onadmin")
                        .Assign(cr => cr.Password = "1q2w3e4r5t");

                    //_CredentialContainer["remote"] = SManifester.ToConnectionCredential(EServiceType.MSSQL);
                    //_CredentialContainer["remote"]
                    //    .Assign(cr => cr.Server = "code-db.mssql.somee.com")
                    //    .Assign(cr => cr.Database = "code-db")
                    //    .Assign(cr => cr.UserID = "lampiclobe_SQLLogin_1")
                    //    .Assign(cr => cr.Password = "2bwe1v99iv");
                }
                return _CredentialContainer["local"].ProvideConnection<SqlConnection>();
            }
        }

        private static CredentialContainer _CredentialContainer;

    }
}
