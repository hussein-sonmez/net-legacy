/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  LG.ID AS 'LoginID', LG.Email AS 'LoginEmail', *
  FROM [ondb].[dbo].[ProjectItems] PJ
  LEFT OUTER JOIN [ondb].[dbo].[ProfileItems] PR ON PJ.ProfileID = PR.ID
  LEFT OUTER JOIN [ondb].[dbo].[LoginModels] LG ON LG.AssociatedProfileID = PR.ID
  LEFT OUTER JOIN [ondb].[dbo].[ActivityContainers] AC ON PJ.ID = AC.ProjectItemID
  WHERE LG.ID IS NOT NULL